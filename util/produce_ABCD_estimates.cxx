#include "ABCD_Tools/ABCD_Tools.h"


#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <map>
#include <iomanip>

#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TDirectory.h"
#include "TKey.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TMath.h"
#include "TROOT.h"
#include "Math/Random.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"
#include "TStopwatch.h"

#include "docopt.h"

using namespace std;

bool filterBranches(const TString& chain_name) {
  if( 
      chain_name.BeginsWith("PDF4LHC15") ||
      chain_name.BeginsWith("CT14") ||
      chain_name.BeginsWith("MMHT2014") ||
      chain_name.BeginsWith("NNPDF_") ||
      chain_name.BeginsWith("FSR_") ||
      chain_name.BeginsWith("ISR_") ||
      chain_name.BeginsWith("Var3c") ||
      chain_name.BeginsWith("reweighted") ||
      chain_name=="ME" ||
      chain_name=="PS"
    ) return false;
  
  return true;
}

int produceABCDEstimates(const TString& mainDir, TString& outputDir, const TString& configFilename, const TString& lumiFilename, const TString& configBinning, const TString& filelistName, bool doTests, bool doSys, bool doRebin, int debugLevel){

  TH1::AddDirectory(false);

  TEnv* config_binning= new TEnv(configBinning); // This is obsolete! Rebinning is not working now!
  vector<double> binning;
  //SetSeed((unsigned)time(NULL));
  //TEnv* config= new TEnv(arg[3]);
  outputDir=mainDir+ "/" + outputDir;
  ABCD_Tools* abcd = new ABCD_Tools(mainDir,outputDir,configFilename,lumiFilename,filelistName);
  abcd->setDebugLevel(debugLevel); // TODO: Need to implement debug messages!!!
  
  const bool isData = filelistName.Contains("data1") || filelistName.Contains("AllYears");
  vector<TString> histnames = abcd->GetTH1DNames();
  vector<TString> histnames_unf = abcd->GetTH1DNamesUnfolding();
  //double m_lumi = config->GetValue("lumi",1.);
  
  
  vector<TString> histos_for_testing;
  
  histos_for_testing.push_back("fatJet1_mass");
  histos_for_testing.push_back("fatJet1_pt");
  histos_for_testing.push_back("fatJet1_mass_fine");
  histos_for_testing.push_back("fatJet1_y");
  histos_for_testing.push_back("fatJet1_energy");
  histos_for_testing.push_back("fatJet1_eta");
  histos_for_testing.push_back("fatJet1_phi");
  histos_for_testing.push_back("fatJet2_pt");
  histos_for_testing.push_back("fatJet2_mass");
  histos_for_testing.push_back("fatJet2_mass_fine");
  histos_for_testing.push_back("fatJet2_y");
  histos_for_testing.push_back("ttbar_mass");
  histos_for_testing.push_back("ttbar_pt");
  histos_for_testing.push_back("ttbar_y");
  //histos_for_testing.push_back("H_tt_pt_RecoLevel");
  //histos_for_testing.push_back("ttbar_mass_RecoLevel");
  histos_for_testing.push_back("ljets_Ht");
  
  
  TString hist_for_test_of_independence="fatJet1_mass";
  //TString hist_for_test_of_independence="fatJet2_pt";
  //TString hist_for_test_of_independence="nBJets1";
  
  //histnames.clear();
  //histnames=histos_for_testing;
  //histnames_unf.clear();
  TString textfilefolder=outputDir + "/ABCD_tests/Effects_of_Correlations_sys/";
  if (debugLevel>0) cout << textfilefolder << endl;
  gSystem->mkdir(textfilefolder);
  ofstream textfile((textfilefolder + "table.txt").Data());
  textfile << setprecision(4);
  textfile << "Sys name & EC/AF & BI/AH & BC/AD & EI/AG & CI/AO & BE/AJ" << endl; 
  
  TString bTaggingEfficiencyPlotsFolder=outputDir + "/ABCD_tests/BTaggingEfficiencyPlots";
  TString topTaggingEfficiencyPlotsFolder=outputDir + "/ABCD_tests/TopTaggingEfficiencyPlots";
  gSystem->mkdir(bTaggingEfficiencyPlotsFolder);
  gSystem->mkdir(topTaggingEfficiencyPlotsFolder);
  TFile* fBTaggingEfficiency = new TFile(bTaggingEfficiencyPlotsFolder+"/histos.root","recreate");
  TFile* fTopTaggingEfficiency = new TFile(topTaggingEfficiencyPlotsFolder+"/histos.root","recreate");
  
  
  TString level = "RecoLevel";
  vector<TFile*> tfiles = abcd->GetTFiles();
  tfiles[min(1,(int)tfiles.size()-1)]->cd();
  
  vector<TString> chain_names = GetSubdirectories(0);
  // Moving nominal tree to the beginning
  auto it = std::find(chain_names.begin(),chain_names.end(),"nominal");
  if(it!=chain_names.end()){
    chain_names.erase(it);
    chain_names.insert(chain_names.begin(),"nominal");
  }
  
  
  TStopwatch stopwatch;
  double start_time(0),stop_time;
  stopwatch.Start();
  
  if(!doSys){
    chain_names.clear();
    chain_names.push_back("nominal");
  }
  
  //cout << chain_names.size() << endl;
  const unsigned int nchains=chain_names.size();
  
  if(debugLevel>0) {
    cout << "Printing systematic trees:" << endl;
    for(unsigned int ichain=0;ichain<nchains;ichain++) cout << chain_names[ichain] << endl;
  }
  
  bool filesLoaded=true;

  //for(unsigned int ichain=0;ichain<4;ichain++){
  for(unsigned int ichain=0;ichain<nchains;ichain++){
    start_time=stopwatch.RealTime();
    stopwatch.Start();
    TString chain_name=chain_names[ichain];
    
    if(!filterBranches(chain_name)) continue;
    
    // Reloading files if not loaded
    if(!filesLoaded){
      abcd->ReloadTFiles(chain_name);
      filesLoaded=true;
    }
    bool isNominal=chain_name == "nominal";
    
    if (debugLevel>-1) cout << "Proccessing chain: " << ichain+1 << "/" << nchains << " chain name: " << chain_name << endl;     
       
    level = "RecoLevel";
    abcd->CreateFoldersInTFiles(chain_name,level);
    unsigned int size= histnames.size();
    for(unsigned int i=0;i<size;i++) {
	    
	    
      if(isNominal){
	if (debugLevel>-1) cout << i+1 << "/" << size << " " << "Histogram name: " << histnames[i] << endl;
      }
      else if( histnames[i] != "fatJet1_mass" && (std::find( histos_for_testing.begin(),histos_for_testing.end() ,histnames[i])== histos_for_testing.end()) ) continue;
      try {abcd->LoadInputHistograms(chain_name,level,histnames[i]);} catch(const Exception_loading_histograms_from_tfiles& l){l.printMessage();break;}
      
      if(histnames[i] == "J1sj1_mass" || histnames[i] == "J2sj1_mass") {
	std::vector<double> binning {0,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,92,96,100,104,108,112,116,120,124,128,132,136,140,148,164,180,200};
	abcd->RebinHistos(binning);
      }
      
      
      
      abcd->Make_ABCD_Estimates(chain_name,level,histnames[i]);
      
      if(doSys && histnames[i] == "fatJet1_mass")abcd->print_correlation_effects(chain_name,textfile);
      
      
      if(doTests && isNominal){
	vector<TString>::iterator it = find(histos_for_testing.begin(),histos_for_testing.end(),histnames[i]) ;
      
	if(it !=histos_for_testing.end()){
	  abcd->Make_Alternative_ABCD_Estimates(histnames[i]);
	  abcd->test_independence_ABCD9_binbybin(histnames[i]);
	  abcd->test_independence_of_shapes_ABCD9(histnames[i]);
	  if(isData)abcd->test_sensitivity_of_normalization_ABCD9(chain_name,level,histnames[i]);
	  abcd->test_independence_ABCD16_binbybin(histnames[i]);				
	  //abcd->test_independence_of_shapes_ABCD16(histnames[i]);
	  if(isData)abcd->test_sensitivity_of_normalization_ABCD16(chain_name,level,histnames[i]);
	  abcd->GetEstimatedNumberOfEvents(chain_name,level,histnames[i]);
	  abcd->DrawCorrelation1dHistos9regions(histnames[i]);
	  abcd->DrawCorrelation1dHistos16regions(histnames[i]);
	  abcd->MakeBTaggingEfficiencyPlots(fBTaggingEfficiency,histnames[i]);
	  abcd->MakeTopTaggingEfficiencyPlots(fTopTaggingEfficiency,histnames[i]);
	}
	if(histnames[i]==hist_for_test_of_independence){
	  abcd->test_independence_ABCD9(histnames[i]);
	  abcd->DrawCorrelation2dHistos9regions();
	  abcd->DrawCorrelation2dHistos16regions();
	  //abcd->CreateAdditionalHistos(chain_name,level,histnames[i]);
	  //abcd->DrawSignalBackgroundDiscriminationPlots9regions();
	  abcd->test_independence_ABCD16(histnames[i]);
	}
	if(histnames[i].Contains("fatJet1_tau32") || histnames[i].Contains("fatJet1_mass") || histnames[i]=="fatJet1_pt" || histnames[i]=="ttbar_mass"|| histnames[i]=="ttbar_pt"|| histnames[i]=="H_tt_pt_RecoLevel"|| histnames[i]=="ttbar_mass_RecoLevel"){
	  abcd->DrawLeadingFatJetDistribution(histnames[i]);
	}
	if(histnames[i].Contains("fatJet2_tau32") || histnames[i].Contains("fatJet2_mass") || histnames[i]=="fatJet2_pt" || histnames[i]=="ttbar_mass"|| histnames[i]=="ttbar_pt"|| histnames[i]=="H_tt_pt_RecoLevel"|| histnames[i]=="ttbar_mass_RecoLevel"){
	  abcd->DrawRecoilFatJetDistribution(histnames[i]);
	}
	if(histnames[i].Contains("ttbar_mass") || histnames[i].Contains("fatJet1_mass") || histnames[i]=="fatJet1_pt" || histnames[i].Contains("fatJet2_mass") || histnames[i]=="fatJet2_pt"|| histnames[i]=="fatJet1_phi" || histnames[i]=="fatJet2_phi"){
	  abcd->DrawValidationPlots_9regions(histnames[i]);
	  abcd->DrawValidationPlots_16regions(histnames[i]);
	  abcd->TestEffectOfCorrelations(histnames[i]);
	}
      }
      //Deleting histograms and other objects
      abcd->DeleteObjects();
    }
    level="unfolding";	
    abcd->CreateFoldersInTFiles(chain_name,level);
    size=histnames_unf.size();
    for(unsigned int i=0;i<size;i++) {
      if(isNominal){
	if (debugLevel>-1) cout << i+1 << "/" << size << endl;
	if (debugLevel>0) cout << histnames_unf[i] << endl;
      }
      try {abcd->LoadInputHistograms(chain_name,level,histnames_unf[i]);}catch(const Exception_loading_histograms_from_tfiles& l){l.printMessage();break;}
      if(doRebin){
	
	TString binning_name = histnames_unf[i];
	binning_name.ReplaceAll("_RecoLevel","_Particle_reco");
	
	
	
	binning = MakeVectorDouble(config_binning->GetValue(binning_name,"0"));
	
	if (debugLevel>0) if(chain_name=="nominal") {
	  cout << "Rebinning histogram" << endl;
	  cout << binning_name << endl;
	  cout << "Loaded binning: " << config_binning->GetValue(binning_name,"0") << endl;
	}
	
	if(binning.size()>1){
	  abcd->RebinHistos(binning);
	}
	
      }
      abcd->Make_ABCD_Estimates(chain_name,level,histnames_unf[i]);
      //Deleting histograms and other objects
      abcd->DeleteObjects();
    }
    // Closing and deleting tfiles to reduce memory leaks in ROOT
    abcd->DeleteTFiles(chain_name);
    filesLoaded=false;
    
    stop_time=stopwatch.RealTime();
    stopwatch.Start();
    if (debugLevel>0) cout << "Time spent in the chain " << chain_name << ": " << stop_time - start_time << endl;
  }
  if(doSys){
    abcd->print_correlation_effects_max_min(textfile);
    abcd->print_correlation_effects_sys_error(textfile);
  }
  if (debugLevel>0) cout << "Deleting object ABCD_tools" << endl;
  
  start_time=stopwatch.RealTime();
  stopwatch.Start();
  delete abcd;
  
  
  fBTaggingEfficiency->Close();
  fTopTaggingEfficiency->Close();
  delete fBTaggingEfficiency;
  delete fTopTaggingEfficiency;
  
  stop_time=stopwatch.RealTime();
  cout  << stop_time - start_time << endl;
  //abcd->DeleteObjects();
  return 0;
  
  
}

// This is not just a print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to make ABCD estimates with tests of ABCD method.  

Usage:  
  produce_ABCD_estimates [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for ttbar analysis output. [default: $TtbarDiffCrossSection_output_path]
  --outputDir <string>                    Relative path to output directory from mainDir. [default: ABCD.All]
  --config <configfile>               	  Path to ABCD Tools config file. [default: ABCD_Tools/data/Config.env]
  --configLumi <configLumi>               Path to config with lumi info. [default: TtbarDiffCrossSection/data/config.env]
  --configBinning <configBinning>         Path to config with binning for histos rebinning. [default: TtbarDiffCrossSection/data/unfolding_histos_optimizedBinning.env]
  --doRebin <doRebin>			  Do rebinning. [default: 0]
  --fileList <textfile>                   Text file with file names. [default: ABCD_Tools/data/filelist.AllYears.All.txt]
  --doTests <doTests>                     Activates tests of ABCD method. [default: 0]
  --doSys <doSys>                         Activates loops over systematic branches. [default: 1]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {
  
  TString mainDir = "";
  TString outputDir = "";
  TString configFilename = "";
  TString lumiFilename = "";
  TString configBinning = "";
  TString filelistName = "";
  
  bool doTests = 0;
  bool doSys = 0;
  bool doRebin = 0;
  int debugLevel=-1;
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
  try{

    try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
    
    try{ outputDir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputDir  option is expected to be a string. Check input parameters."); }
    
    try{ configFilename=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
    
    try{ lumiFilename=args["--configLumi"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--configLumi option is expected to be a string. Check input parameters."); }
    
    try{ configBinning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
    
    try{ filelistName=args["--fileList"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--fileList option is expected to be a string. Check input parameters."); } 
    
    try{ doTests=args["--doTests"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doTests option is expected to be an integer. Check input parameters."); }
    
    try{ doSys=args["--doSys"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doSys option is expected to be an integer. Check input parameters."); }
    
    try{ doRebin=args["--doRebin"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doRebin option is expected to be an integer. Check input parameters."); }
    
    try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
    
  } catch(const std::invalid_argument& e) {
    cout << e.what() << endl << endl;
    cout << USAGE << endl;
    return -1;
  }
  cout << endl <<"Executing produceABCDEstimates with parameters:" << endl;
  for(const auto& arg : args) {
    std::cout << arg.first << " " << arg.second << std::endl; 
  }
  cout << "----------------------------------------------------" << endl;
  
  if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
  
  return produceABCDEstimates(mainDir, outputDir, configFilename, lumiFilename, configBinning, filelistName, doTests, doSys, doRebin, debugLevel);
}



