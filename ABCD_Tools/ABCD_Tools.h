#ifndef ABCD_TOOLS_h
#define ABCD_TOOLS_h

using namespace std;
#include "functions.h"

#include <vector>
#include <utility>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <map>
#include <iomanip>
#include <numeric>
#include <exception>

#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TDirectory.h"
#include "TKey.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TMath.h"
#include "TROOT.h"
#include "Math/Random.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"
#include "TH1.h"
#include "TH1D.h"


class ABCD_Tools{
public:
  
  // Constructors and assignment operators
  ABCD_Tools (const ABCD_Tools&) = default;
  ABCD_Tools& operator= (const ABCD_Tools&) = default;
  ABCD_Tools (ABCD_Tools&&) = default;
  ABCD_Tools& operator= (ABCD_Tools&&) = default;
  ABCD_Tools(const TString& inputDir,const TString& outputDir,const TString& configname,const TString& configlumi,const TString& filelist);
  virtual ~ABCD_Tools();
  
  void Set_ABCD9_folder_names();
  void Set_ABCD16_folder_names();
  void Set_ABCD9_indexes();
  void Set_ABCD16_indexes();
  void LoadFiles(const TString&);
  void LoadAdditionalFiles();
  void CreateListOfTH1DNames(vector<TString>&,const TString&,const TString&);
  void CreateFoldersInTFiles(const TString& chain_name,const TString& level);
  void CreateAdditionalHistos(const TString& chain_name,const TString& level,const TString& histname);
  
  bool CreateHistVector9(vector<TH1D*>& h_data,TFile* fdata,const TString& chain_name,const TString& level, const TString& histname);
  bool CreateHistVector16(vector<TH1D*>& h_data,TFile* fdata,const TString& chain_name,const TString& level, const TString& histname);
  void AddHistVector9(vector<TH1D*>& h_data,TFile* fnonallhad,const TString& chain_name,const TString& level,const TString& histname,const double& c);
  void AddHistVector16(vector<TH1D*>& h_data,TFile* fnonallhad,const TString& chain_name,const TString& level,const TString& histname,const double& c);
  void RebinHistos(vector<double>& bins);
  
  bool LoadInputHistograms(const TString& chain_name,const TString& level,const TString& histname);
  void CreateVectors();
  void CreateVectors16_data_MC();
  void CreateVector9(const vector<TH1D*>& h_data,vector<double>& totalnumbers,vector<vector<double> >& bincontents, vector<vector<double> >& binerrors);
  void CreateVector16(const vector<TH1D*>& h_data,vector<double>& totalnumbers,vector<vector<double> >& bincontents, vector<vector<double> >& binerrors);
  
  
  void Make_ABCD_Estimates(const TString& chain_name,const TString& level,const TString& histname);
  void Make_Alternative_ABCD_Estimates(const TString& histname);
  
  //void MakeAllABCD9Estimates(vector<vector<double> >& estimates, vector<vector<double> >& bincontents, vector<double>& totalnumbers);
  void MakeAllABCD9Estimates(vector<vector<double> >& estimates, const vector<vector<double> >& bincontents9, const vector<double>& totalnumbers9, const vector<vector<double> >& bincontents16,const vector<double>& totalnumbers16);
  void MakeAllABCD16Estimates(vector<vector<double> >& estimates, const vector<vector<double> >& bincontents, const vector<double>& totalnumbers);
  //void MakeAllABCDEstimates(vector<vector<double> >& estimates9,vector<vector<double> >& estimates16, vector<vector<double> >& bincontents9, vector<double>& totalnumbers9, vector<vector<double> >& bincontents16, vector<double>& totalnumbers16);
  void TestEffectOfCorrelations(const TString& histname);
  void TestEffectOfCorrelations(const TString& histname,vector<vector<double> >& bincontents16_data, vector<vector<double> >& bincontents16_MC,vector<vector<double> >& binerrors16_MC, const unsigned int& niter);
  void MakeTestingEstimates(vector<vector<double> >& estimates, vector<double>& global_estimates ,const vector<vector<double> >& bincontents, const vector<double>& totalnumbers);
  void MakeAlternativeEstimates(vector<vector<double> >& estimates, const vector<vector<double> >& bincontents16_data, const vector<vector<double> >& bincontents16_MC);
  double MakeOneAlternativeEstimate(const vector<double>& data,const vector<double>& MC);
  
  
  void ConvertVector16to9(const vector<vector<double> >& bincontents16,vector<vector<double> >& bincontents9);
  void ConvertVector16to9(const vector<double>& bincontents16,vector<double>& bincontents9);
  
  
  //void Calculate_Errors9(vector<vector<double> >& bincontents, vector<vector<double> >& binerrors, unsigned int niter);
  void Calculate_Errors(const vector<vector<double> >& bincontents16, const vector<vector<double> >& binerrors16, const unsigned int& niter);
  void Calculate_Errors(const vector<vector<double> >& bincontents16_data, const vector<vector<double> >& bincontents16_MC,const vector<vector<double> >& binerrors16_MC, const unsigned int& niter);
  //void Calculate_Errors16(vector<vector<double> >& bincontents, vector<vector<double> >& binerrors, unsigned int niter);
  
  
  void Write_Current_Histos(const TString& chain_name,const TString& level,const TString& histname);
  void DeleteObjects();
  void DeleteTFiles(const TString& chain_name);
  void ReloadTFiles(const TString& chain_name);
  
  vector<TString> GetTH1DNames();
  vector<TString> GetTH1DNamesUnfolding();
  
  void GetEstimatedNumberOfEvents_ABCD9(vector<vector<double> >& bincontents, vector<vector<double> >& binerrors, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means, vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter);
  void GetEstimatedNumberOfEvents_ABCD16(vector<vector<double> >& bincontents, vector<vector<double> >& binerrors, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means,vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter);	
  
  void GetEstimatedNumberOfEvents_ABCD9(vector<vector<double> >& bincontents_data,vector<vector<double> >& bincontents_MC, vector<vector<double> >& binerrors_MC, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means, vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter);
  void GetEstimatedNumberOfEvents_ABCD16(vector<vector<double> >& bincontents_data,vector<vector<double> >& bincontents_MC, vector<vector<double> >& binerrors_MC, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means,vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter);	
  
  
  void DrawCorrelation2dHistos9regions();
  void DrawCorrelation1dHistos9regions(const TString& histname);
  void DrawSignalBackgroundDiscriminationPlots9regions();
  void DrawCorrelation2dHistos16regions();
  void DrawCorrelation1dHistos16regions(const TString& histname);
  void DrawSignalBackgroundDiscriminationPlots16regions();
  void DrawLeadingFatJetDistribution(const TString& histname);
  void DrawRecoilFatJetDistribution(const TString& histname);
  void DrawValidationPlots_9regions(const TString& histname);
  void DrawValidationPlots_16regions(const TString& histname);
  
  void test_independence_ABCD9(vector<TH1D*>&,const TString&);
  void test_independence_ABCD9_binbybin(vector<TH1D*>& h_data,const TString&);
  void test_independence_of_shapes_ABCD9(vector<TH1D*>& h_data,const TString&);
  void create_ABCD9_table(vector<vector<double> >& table,vector<vector<TH1D*> > &table_hist,vector<vector<char> >& label,vector<TH1D*>& h_data);
  
  
  void GetEstimatedNumberOfEvents(const TString& chain_name,const TString& level,const TString& histname);
  void test_independence_ABCD9(const TString& histname);
  void test_independence_ABCD9_binbybin(const TString& histname);
  void test_independence_of_shapes_ABCD9(const TString& histname);
  
  void test_independence_ABCD16(vector<TH1D*>&,const TString&);
  void test_independence_ABCD16_binbybin(vector<TH1D*>&,const TString&);
  void test_independence_of_shapes_ABCD16(vector<TH1D*>& h_data,const TString&);
  void create_ABCD16_table(vector<vector<double> >& table,vector<vector<TH1D*> > &table_hist,vector<vector<char> >& label,vector<TH1D*>& h_data);
  
  void test_independence_ABCD16(const TString& histname);
  void test_independence_ABCD16_binbybin(const TString& histname);
  void test_independence_of_shapes_ABCD16(const TString& histname);
  
  void test_sensitivity_of_normalization_ABCD9(const TString& chain_name,const TString& level,const TString& histname);
  void test_sensitivity_of_normalization_ABCD16(const TString& chain_name,const TString& level,const TString& histname);
  void Set_methods_ABCD9(vector<TString>& method_names,vector<double>& y2min, vector<double>& y2max);
  void Set_methods_ABCD16(vector<TString>& method_names,vector<double>& y2min, vector<double>& y2max);
  
  void print_ABCD9_numbers(vector<double>& total_estimates, vector<double>& total_errors, double factor,ofstream&);
  void print_ABCD16_numbers(vector<double>& total_estimates, vector<double>& total_errors, double factor,ofstream&);
  
  void print_bins_info(vector<vector<double> >& bincontents, vector<vector<double> >& binerrors,const TString& histname, ofstream&);
  void print_validation_tables( ofstream& textfile);
  double print_correlation_effects_one_effect(int k,int l,int m,int n,ofstream& textfile);
  void print_correlation_effects(const TString& sys_name,ofstream& textfile);
  void print_correlation_effects_max_min(ofstream& textfile);
  void print_correlation_effects_sys_error(ofstream& textfile);
  
  
  void calculate_purities9(const TString& chain_name,const TString& level,const TString& histname,ofstream& textfile);
  void calculate_purities16(const TString& chain_name,const TString& level,const TString& histname,ofstream& textfile);
  void SetABCD9names(vector<TString>& names);
  void SetABCD16names(vector<TString>& names);
  
  void MakeTopTaggingEfficiencyPlots(TFile*f,const TString& histname);
  void MakeTopTaggingEfficienncyPlot(TFile*f,const vector<TH1D*>& histos,const TString& label,const TString& histname);
  void MakeTopTaggingBackgroundRejectionPlot(TFile*f,const TString& label,const TString& histname);
  
  void MakeBTaggingEfficiencyPlots(TFile*f,const TString& histname);
  void MakeBTaggingEfficienncyPlot(TFile*f,const vector<TH1D*>& histos,const TString& label,const TString& histname);
  void MakeBTaggingBackgroundRejectionPlot(TFile*f,const TString& label,const TString& histname);
  
  TH1D* Create_top_tagging_histogram(const vector<TH1D*>& histos,const double& SF=1.);
  TH1D* Create_b_matching_histogram(const vector<TH1D*>& histos,const double& SF=1.);
  inline vector<TFile*> GetTFiles(){return m_tfiles;}
  
  inline void setDebugLevel(int debugLevel){m_debug=debugLevel;}
  
protected:
  
  TEnv* m_config;
  
  int m_debug;
  
  TString m_input_mainDir;
  TString m_output_mainDir;
  
  int m_number_of_ABCD9_methods;
  int m_number_of_ABCD16_methods;
  
  TString m_filelist_name;
  
  vector<TString> m_ABCD9_folder_names,m_ABCD16_folder_names;
  vector<TString> m_tfiles_names;
  vector<TFile*> m_tfiles;
  map<TString,TFile*> m_additional_tfiles;
  vector<TString> m_TH1Dnames,m_TH1Dnames_unf;
  vector<TH1D*> m_histvector9,m_histvector16;
  vector<vector<TH1D*> > m_histvectors9,m_histvectors16;
  map<TString,vector<TH1D*> > m_additional_histvectors9;
  
  vector<vector<double> > m_table9;
  vector<vector<TH1D*> > m_table9_hist;
  vector<vector<char> > m_label9;
  
  vector<vector<double> > m_table16;
  vector<vector<TH1D*> > m_table16_hist;
  vector<vector<char> > m_label16;
  
  vector<vector<double> > m_bincontents9,m_binerrors9,m_bincontents16,m_binerrors16;
  vector<vector<double> > m_bincontents16_data,m_bincontents16_MC,m_binerrors16_MC;
  vector<double> m_totalnumbers9,m_totalnumbers16;
  vector<double> m_binlowedges,m_binupedges;
  vector<vector<double> > m_estimates9,m_estimates16;
  vector<vector<double> > m_testing_estimates;
  
  vector<vector<double> > m_alternative_estimates;
  
  vector<pair<TString,double> > m_max_cor_effects_from_sys, m_min_cor_effects_from_sys;
  vector<TString> m_sys_names;
  vector<vector<double> > m_cor_effects_sys;
  
  vector<TFile*> m_tfiles_ABCD9,m_tfiles_ABCD16;
  vector<TString> m_path_to_output_ABCD9,m_path_to_output_ABCD16;
  vector<pair<TString,TString> > m_outputnames_ABCD9,m_outputnames_ABCD16;
  vector<TString> m_write_status_ABCD9,m_write_status_ABCD16;
  vector<TString> m_ABCD9_config_suffixes,m_ABCD16_config_suffixes;
  
  vector<TString> m_methodNames9, m_methodNames16;
  TString m_foldername_tests;
  TFile* m_tfile_sensitivity_of_normalization_ABCD9;
  TFile* m_tfile_sensitivity_of_normalization_ABCD16;
  
  //TDirectory* m_dir_reco, m_dir_unf;
  
  TH1D* m_h_template;
  vector<TH1D*> m_ABCD9_Results;
  vector<TH1D*> m_ABCD16_Results;
  
  
  ofstream m_textfile_test_independence_ABCD9;
  ofstream m_textfile_test_independence_ABCD9_BBB;
  ofstream m_textfile_test_of_shapes_ABCD9;
  ofstream m_textfile_sensitivity_of_normalization_ABCD9;
  
  ofstream m_textfile_test_independence_ABCD16;
  ofstream m_textfile_test_independence_ABCD16_BBB;
  ofstream m_textfile_test_of_shapes_ABCD16;
  ofstream m_textfile_sensitivity_of_normalization_ABCD16;
  
  ofstream m_textfile_hist_info;
  
  unsigned long m_niter_chi2test;
  unsigned int m_niter_uncertainties;
  
  bool m_use_lumi;
  double m_lumi;
  TString m_lumi_string;
  double m_ttbar_SF;
  TString m_nominal_chain_name;
  bool m_IsNominal;
  
  int m_i0t0b;
  int m_i0t1b;
  int m_i0t2b;
  int m_i1t0b;
  int m_i1t1b;
  int m_i1t2b;
  int m_i2t0b;
  int m_i2t1b;
  int m_i2t2b;
  
  int m_iRegionA;
  int m_iRegionB;
  int m_iRegionC;
  int m_iRegionD;
  int m_iRegionE;
  int m_iRegionF;
  int m_iRegionG;
  int m_iRegionH;
  int m_iRegionI;
  int m_iRegionJ;
  int m_iRegionK;
  int m_iRegionL;
  int m_iRegionM;
  int m_iRegionN;
  int m_iRegionO;
  int m_iRegionP; 

	
  ClassDef(ABCD_Tools,1)
};

class Exception_loading_histograms_from_tfiles: public exception{
public:
  Exception_loading_histograms_from_tfiles(TString s):message(s){}
  ~Exception_loading_histograms_from_tfiles() throw() {}
  void printMessage() const {cout << message << endl;}
private:
  TString message;
  ClassDef(Exception_loading_histograms_from_tfiles,1)
};

#endif
