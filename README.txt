# Package is stored in the git repository

https://gitlab.cern.ch/pjacka/ABCD_Tools

# Installation instructions:

#1. Setup your kerberos
  a) Add this lines to you .bash_profile
  KRB5CCNAME_CERN=$HOME/.krb5_cache_cern_${HOSTNAME}
  export KRB5CCNAME=$KRB5CCNAME_CERN
  #Renew your kerberos certificate
  kinit -R -c /home/$USER/.krb5_cache_cern_ui8.farm.particle.cz
  #initialize kerberos for 7 days
  alias kinit7d="kinit -r7d <cern-username>"
  b) create(or open) file
	
		$HOME/.ssh/config
	
	copy inside following lines and replace 'pjacka' by your cern user name:

########################################3 
  
Host *                                  
    Protocol 2                  
    ForwardX11 yes      
    HostbasedAuthentication yes                            
    EnableSSHKeysign yes                        
    GSSAPIAuthentication yes
    GSSAPIDelegateCredentials yes

Host lxplus*.cern.ch lxplus
  User pjacka
  Protocol 2
  PubkeyAuthentication no
  PasswordAuthentication yes
  GSSAPIAuthentication yes
  GSSAPIDelegateCredentials yes
  GSSAPITrustDns yes 
  ForwardX11 yes
#  ForwardX11Trusted yes

Host svn.cern.ch svn
  User pjacka
  GSSAPIAuthentication yes
  GSSAPIDelegateCredentials yes
  GSSAPITrustDns yes 
  Protocol 2
  ForwardX11 no


################################
  c)Open new sesion and try to use command to initialize your kerberos
  
  kinit7d



#2. Download the package using the following command
git clone https://:@gitlab.cern.ch:8443/pjacka/ABCD_Tools.git

#3. Setup RootCore
#Assuming that you are using TtbarDiffCrossSection package
source TtbarDiffCrossSection/scripts/setup.sh

#4. find packages
rc find_packages

#5. compile packages
rc compile


# Instructions for running the code 

# The input for the code are histograms prepared by TtbarDiffCrossSection or other package

#1. Open and modify ABCD_Tools/scripts/ABCD_Tools.sh  and setup input variables:
	config_file_name=<path to ABCD_Tools config file>
	lumi_file_name=<path to config file containing luminosity>
	doTest=0 or 1 switch off/on testing of assumptions of ABCD method
	doSys=0 or 1  switch off/on running over systematic branches
	filelist_name=<path to file containing list of rootfiles>
	outputDir=<path to directory where output files will be stored>
	
#2.	Prepare input histograms (e.g. using TtbarDiffCrossSection package)

#3. Run the code using command
./ABCD_Tools/scripts/ABCD_Tools.sh
