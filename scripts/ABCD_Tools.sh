cd ${TTBAR_PATH}

#defaults

config_file_name=ABCD_Tools/data/Config.env
lumi_file_name=TtbarDiffCrossSection/data/config.env
doTests=1
doSys=1

debugLevel=10

samplesProd=( "All")
#samplesProd=( "MC16a" "MC16d" "MC16e "All")



for MC in ${samplesProd[@]}
do
  if [ $MC == MC16a ] ; then
    data=data1516
  elif [ $MC == MC16d ] ; then
    data=data17
  elif [ $MC == MC16e ] ; then
    data=data18
  else
    data=AllYears
  fi
  
  #filelist_name=ABCD_Tools/data/filelist.${data}.${MC}.txt
  filelist_name=ABCD_Tools/data/filelist.${data}.Herwig713.${MC}.txt
  #filelist_name=ABCD_Tools/data/filelist.Signal.${MC}.txt
  
  #outputDir=ABCD.${MC}
  outputDir=ABCD.Herwig713.${MC}
  #outputDir=ABCD.Signal.${MC}
  echo "Your output directory is: "${TtbarDiffCrossSection_output_path}/${outputDir}
  
  produce_ABCD_estimates --mainDir ${TtbarDiffCrossSection_output_path} --outputDir ${outputDir} --config ${config_file_name} --configLumi ${lumi_file_name} --fileList ${filelist_name} --doTests ${doTests} --doSys ${doSys} --debugLevel ${debugLevel}
done
   
