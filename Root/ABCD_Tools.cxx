#include "ABCD_Tools/ABCD_Tools.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

ClassImp(ABCD_Tools)
ClassImp(Exception_loading_histograms_from_tfiles)

ABCD_Tools::ABCD_Tools(const TString& inputDir,const TString& outputDir,const TString& configname,const TString& configlumi,const TString& filelist){
  m_config= new TEnv(configname);
  m_input_mainDir=inputDir;
  m_output_mainDir=outputDir;
  
  m_debug=0;
  
  m_ABCD9_config_suffixes.push_back("ABCD9_S1_modified");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_S1_modified");
  m_ABCD9_config_suffixes.push_back("ABCD9_S2_modified");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_S2_modified");
  m_ABCD9_config_suffixes.push_back("ABCD9_Averaged_modified");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_Averaged_modified");
  m_ABCD9_config_suffixes.push_back("ABCD9_S1");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_S1");
  m_ABCD9_config_suffixes.push_back("ABCD9_S2");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_S2");
  m_ABCD9_config_suffixes.push_back("ABCD9_Averaged");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_Averaged");
  m_ABCD9_config_suffixes.push_back("ABCD9_bjets");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_bjets");
  m_ABCD9_config_suffixes.push_back("ABCD9_Combined");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_Combined");
  m_ABCD9_config_suffixes.push_back("ABCD4_BBB_Averaged");
  m_ABCD9_config_suffixes.push_back("ABCD9_BBB_cross_terms");
  m_ABCD9_config_suffixes.push_back("ABCD4_BBB_b0");
  m_ABCD9_config_suffixes.push_back("ABCD4_BBB_b1");
  
  m_ABCD16_config_suffixes.push_back("ABCD16");
  m_ABCD16_config_suffixes.push_back("ABCD16_S1");
  m_ABCD16_config_suffixes.push_back("ABCD16_S2");
  m_ABCD16_config_suffixes.push_back("ABCD16_S3");
  m_ABCD16_config_suffixes.push_back("ABCD16_S4");
  m_ABCD16_config_suffixes.push_back("ABCD16_S5");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S1");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S2");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S3");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S4");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S5");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S6");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S7");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S8");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S9");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_S0");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_bjets");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_bjet1");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_ljet1_tau32");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_ljet2_tau32");
  
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionK");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionL");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionM");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionN");
  
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionKS");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionLS");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionMS");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionNS");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionLS_ljet1_substructure");
  m_ABCD16_config_suffixes.push_back("ABCD16_BBB_regionNS_ljet2_substructure");
  
  m_ABCD16_config_suffixes.push_back("leadingJetTopTaggingEfficiency");
  m_ABCD16_config_suffixes.push_back("leadingJetBTaggingEfficiency");
  m_ABCD16_config_suffixes.push_back("subleadingJetTopTaggingEfficiency");
  m_ABCD16_config_suffixes.push_back("subleadingJetBTaggingEfficiency");
  
  m_ABCD16_config_suffixes.push_back("signalSF_regionS");
  m_ABCD16_config_suffixes.push_back("signalSF_regionK");
  m_ABCD16_config_suffixes.push_back("signalSF_regionL");
  m_ABCD16_config_suffixes.push_back("signalSF_regionM");
  m_ABCD16_config_suffixes.push_back("signalSF_regionN");
  
  m_ABCD16_config_suffixes.push_back("signalSF_Ratio_KtoS");
  m_ABCD16_config_suffixes.push_back("signalSF_Ratio_LtoS");
  m_ABCD16_config_suffixes.push_back("signalSF_Ratio_MtoS");
  m_ABCD16_config_suffixes.push_back("signalSF_Ratio_NtoS");
  
  
  m_number_of_ABCD9_methods=m_ABCD9_config_suffixes.size();
  m_number_of_ABCD16_methods=m_ABCD16_config_suffixes.size();
  m_outputnames_ABCD9.resize(m_number_of_ABCD9_methods);
  m_outputnames_ABCD16.resize(m_number_of_ABCD16_methods);
  m_path_to_output_ABCD9.resize(m_number_of_ABCD9_methods);
  m_path_to_output_ABCD16.resize(m_number_of_ABCD16_methods);
  m_ABCD9_Results.resize(m_number_of_ABCD9_methods);
  m_ABCD16_Results.resize(m_number_of_ABCD16_methods);
  m_tfiles_ABCD9.resize(m_number_of_ABCD9_methods);
  m_tfiles_ABCD16.resize(m_number_of_ABCD16_methods);
  m_write_status_ABCD9.resize(m_number_of_ABCD9_methods);
  m_write_status_ABCD16.resize(m_number_of_ABCD16_methods);
  SetABCD9names(m_methodNames9);
  SetABCD16names(m_methodNames16);
  
  for(int i=0;i<m_number_of_ABCD9_methods;i++){
    m_outputnames_ABCD9[i]=make_pair(m_config->GetValue("foldername_output_" + m_ABCD9_config_suffixes[i],""),m_config->GetValue("filename_output_" + m_ABCD9_config_suffixes[i],""));
    gSystem->mkdir(m_output_mainDir + "/" + m_outputnames_ABCD9[i].first,true);
    m_path_to_output_ABCD9[i] = m_output_mainDir + "/" + m_outputnames_ABCD9[i].first + "/" + m_outputnames_ABCD9[i].second;
    m_write_status_ABCD9[i] = m_config->GetValue("write_status_" + m_ABCD9_config_suffixes[i],"none");
    if(m_write_status_ABCD9[i]!="none")m_tfiles_ABCD9[i] = new TFile(m_path_to_output_ABCD9[i],"recreate");
    
  }
  for(int i=0;i<m_number_of_ABCD16_methods;i++){
    m_outputnames_ABCD16[i]=make_pair(m_config->GetValue("foldername_output_" + m_ABCD16_config_suffixes[i],""),m_config->GetValue("filename_output_" + m_ABCD16_config_suffixes[i],""));
    gSystem->mkdir(m_output_mainDir + "/" + m_outputnames_ABCD16[i].first,true);
    m_path_to_output_ABCD16[i] = m_output_mainDir + "/" + m_outputnames_ABCD16[i].first + "/" + m_outputnames_ABCD16[i].second;
    m_write_status_ABCD16[i] = m_config->GetValue("write_status_" + m_ABCD16_config_suffixes[i],"none");
    if(m_write_status_ABCD16[i]!="none")m_tfiles_ABCD16[i] = new TFile(m_path_to_output_ABCD16[i],"recreate");
  }
  
  m_foldername_tests= m_output_mainDir + "/" + m_config->GetValue("foldername_tests","");
  gSystem->mkdir(m_foldername_tests,true);
  cout << "Textfilename: " << m_foldername_tests << endl;
  
  
  m_textfile_test_independence_ABCD9.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_test_independence_ABCD9","")).Data());
  m_textfile_test_of_shapes_ABCD9.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_test_of_shapes_ABCD9","")).Data());
  m_textfile_sensitivity_of_normalization_ABCD9.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_sensitivity_of_normalization_ABCD9","")).Data());
  m_textfile_test_independence_ABCD9_BBB.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_test_independence_ABCD9_BBB","")).Data());
  
  m_textfile_test_independence_ABCD16.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_test_independence_ABCD16","")).Data());
  m_textfile_test_of_shapes_ABCD16.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_test_of_shapes_ABCD16","")).Data());
  m_textfile_sensitivity_of_normalization_ABCD16.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_sensitivity_of_normalization_ABCD16","")).Data());
  m_textfile_test_independence_ABCD16_BBB.open((m_foldername_tests + "/" + m_config->GetValue("textfilename_test_independence_ABCD16_BBB","")).Data());
  
  TString textfilename=m_output_mainDir + "/" + m_config->GetValue("textfile_hist_info_foldername","");
  gSystem->mkdir(textfilename,true);
  m_textfile_hist_info.open((textfilename + "/" + m_config->GetValue("textfile_hist_info_name","")).Data());
  
  m_textfile_test_independence_ABCD9 << setprecision(4);
  m_textfile_test_independence_ABCD9_BBB << setprecision(4);
  m_textfile_test_of_shapes_ABCD9 << setprecision(4);
  m_textfile_sensitivity_of_normalization_ABCD9<< setprecision(4);
  
  m_textfile_test_independence_ABCD16 << setprecision(4);
  m_textfile_test_independence_ABCD16_BBB << setprecision(4);
  m_textfile_test_of_shapes_ABCD16 << setprecision(4);
  m_textfile_sensitivity_of_normalization_ABCD16<< setprecision(4);
  
  
  m_filelist_name=filelist;
  TEnv* config_lumi = new TEnv(configlumi);
  
  
  TString mc_samples_production=config_lumi->GetValue("mc_samples_production","");
  m_lumi = getLumi(config_lumi,mc_samples_production);
  if(m_lumi<0.){
    cout << "Error: Unknown mc_samples_production!\n Exiting now!" << endl;
    exit(-1);
  }
  //if(m_filelist_name.Contains("MC16a"))m_lumi=config_lumi->GetValue("lumi_data1516",1.);
  //else if(m_filelist_name.Contains("MC16c") || m_filelist_name.Contains("MC16d") )  m_lumi=config_lumi->GetValue("lumi_data17",1.);
  //else if(m_filelist_name.Contains("All")) m_lumi=config_lumi->GetValue("lumi_dataAllYears",1.);
  //else 
  
  m_lumi_string="";
  if(mc_samples_production=="MC16a") m_lumi_string=config_lumi->GetValue("lumi_data156_string","");
  if(mc_samples_production=="MC16d") m_lumi_string=config_lumi->GetValue("lumi_data17_string","");
  if(mc_samples_production=="MC16e") m_lumi_string=config_lumi->GetValue("lumi_data18_string","");
  if(mc_samples_production=="All") m_lumi_string=config_lumi->GetValue("lumi_dataAllYears_string","");
  
  
  m_use_lumi=false;
  if(!m_use_lumi) m_lumi=1.;
  
  
  m_nominal_chain_name="nominal";
  
  m_ttbar_SF=config_lumi->GetValue("ttbar_SF",1.);
  m_niter_chi2test=m_config->GetValue("niter_chi2test",1);
  m_niter_uncertainties=m_config->GetValue("niter_uncertainties",1);
  
  cout << m_lumi << endl;
  cout << m_input_mainDir << endl;

  LoadFiles(m_filelist_name);
  //LoadAdditionalFiles();
  Set_ABCD9_indexes();
  Set_ABCD16_indexes();
  Set_ABCD9_folder_names();
  Set_ABCD16_folder_names();
  CreateListOfTH1DNames(m_TH1Dnames,"nominal","RecoLevel");
  CreateListOfTH1DNames(m_TH1Dnames_unf,"nominal","unfolding"); 
  
  m_bincontents9.resize(9);
  m_binerrors9.resize(9);
  m_bincontents16.resize(16);
  m_binerrors16.resize(16);
  m_bincontents16_data.resize(16);
  m_bincontents16_MC.resize(16);
  m_binerrors16_MC.resize(16);
  
  
  m_totalnumbers9.resize(9);
  m_totalnumbers16.resize(16);
  const int tfiles_size=m_tfiles.size();
  m_histvectors9.resize(tfiles_size);
  m_histvectors16.resize(tfiles_size);
  m_histvector9.resize(9);
  m_histvector16.resize(16);
  for(int i=0;i<tfiles_size;i++){
    m_histvectors9[i].resize(9);
    m_histvectors16[i].resize(16);
  }
  m_estimates9.resize(m_number_of_ABCD9_methods);
  m_estimates16.resize(m_number_of_ABCD16_methods);
  
  
  m_max_cor_effects_from_sys.resize(6,make_pair("",FLT_MIN)); 
  m_min_cor_effects_from_sys.resize(6,make_pair("",FLT_MAX));
	
}
bool ABCD_Tools::LoadInputHistograms(const TString& chain_name,const TString& level,const TString& histname){
  const int tfiles_size=m_tfiles.size();
  int i=0,j=0;
  static vector<int> nbins(tfiles_size);
  if(!CreateHistVector9(m_histvectors9[0],m_tfiles[0],m_nominal_chain_name,level,histname)) return false;
  if(!CreateHistVector9(m_histvectors9[0],m_tfiles[0],m_nominal_chain_name,level,histname)) {throw Exception_loading_histograms_from_tfiles((TString)"Error in loading histogram " + histname + " from " + m_tfiles[i]->GetName()); return false;}
  if(!CreateHistVector16(m_histvectors16[0],m_tfiles[0],m_nominal_chain_name,level,histname)) {throw Exception_loading_histograms_from_tfiles((TString)"Error in loading histogram " + histname + " from " + m_tfiles[i]->GetName()); return false;}
  nbins[0] = m_histvectors16[0][0]->GetNbinsX();
  for(i=1;i<tfiles_size;i++){
    if(!CreateHistVector9(m_histvectors9[i],m_tfiles[i],chain_name,level,histname)) {throw Exception_loading_histograms_from_tfiles((TString)"Error in loading histogram " + histname + " from " + m_tfiles[i]->GetName()); return false;}
    if(!CreateHistVector16(m_histvectors16[i],m_tfiles[i],chain_name,level,histname)) {throw Exception_loading_histograms_from_tfiles((TString)"Error in loading histogram " + histname + " from " + m_tfiles[i]->GetName()); return false;}
    if(nbins[0] != m_histvectors16[i][0]->GetNbinsX()){throw Exception_loading_histograms_from_tfiles((TString)"Error: Different number of bins in the histogram: " + histname + "\n in file \n" + m_tfiles[i]->GetName()); return false;}
  }
  if(tfiles_size==1){
    for(j=0;j<9;j++)m_histvectors9[0][j]->Scale(m_lumi);
    for(j=0;j<16;j++)m_histvectors16[0][j]->Scale(m_lumi);
  }
  for(j=0;j<9;j++)m_histvector9[j] = (TH1D*)m_histvectors9[0][j]->Clone();
  for(j=0;j<16;j++)m_histvector16[j] = (TH1D*)m_histvectors16[0][j]->Clone();
  
  m_h_template=(TH1D*)m_histvector9[0]->Clone();
  m_h_template->Reset();
  
  
  
  for(j=0;j<9;j++){
    if(tfiles_size >2){
      m_histvector9[j]->Add(m_histvectors9[1][j],-m_ttbar_SF*m_lumi);
      m_histvector9[j]->Add(m_histvectors9[2][j],-m_ttbar_SF*m_lumi);
    }
    for(i=3;i<tfiles_size;i++) m_histvector9[j]->Add(m_histvectors9[i][j],-m_lumi);
  }
  for(j=0;j<16;j++){
    if(tfiles_size >2){
      m_histvector16[j]->Add(m_histvectors16[1][j],-m_ttbar_SF*m_lumi);
      m_histvector16[j]->Add(m_histvectors16[2][j],-m_ttbar_SF*m_lumi);
    }
    for(i=3;i<tfiles_size;i++) m_histvector16[j]->Add(m_histvectors16[i][j],-m_lumi);
  }  
  return true;
}

void ABCD_Tools::RebinHistos(vector<double>& bins){
  const int tfiles_size = m_tfiles.size();
  const int bins_size = bins.size();
  int i=0,j=0;
  vector<Double_t> bins_double(bins_size);
  for(i=0;i<bins_size;i++)bins_double[i]=bins[i];
  Double_t* bins_array=&bins_double[0];
  //for(i=0;i<bins_size;i++) cout << bins_array[i] << endl;
  
  for(i=0;i<tfiles_size;i++)for(j=0;j<9;j++){
    TH1D* helphist = (TH1D*)m_histvectors9[i][j]->Rebin(bins_size-1,"helphist",bins_array);
    delete m_histvectors9[i][j];
    m_histvectors9[i][j] = (TH1D*)helphist->Clone();
    delete helphist;		
  }
  for(i=0;i<tfiles_size;i++)for(j=0;j<16;j++){
    TH1D* helphist = (TH1D*)m_histvectors16[i][j]->Rebin(bins_size-1,"helphist",bins_array);
    delete m_histvectors16[i][j];
    m_histvectors16[i][j] = (TH1D*)helphist->Clone();
    delete helphist;		
  }
  
  for(j=0;j<9;j++){
    delete m_histvector9[j];
    m_histvector9[j] = (TH1D*)m_histvectors9[0][j]->Clone();
  }
  delete m_h_template;
  m_h_template=(TH1D*)m_histvector9[0]->Clone();
  m_h_template->Reset();
  for(j=0;j<16;j++){
    delete m_histvector16[j];
    m_histvector16[j] = (TH1D*)m_histvectors16[0][j]->Clone();
  }
  for(j=0;j<9;j++){
    if(tfiles_size >2){
      m_histvector9[j]->Add(m_histvectors9[1][j],-m_ttbar_SF*m_lumi);
      m_histvector9[j]->Add(m_histvectors9[2][j],-m_ttbar_SF*m_lumi);
    }
    for(i=3;i<tfiles_size;i++) m_histvector9[j]->Add(m_histvectors9[i][j],-m_lumi);
  }
  for(j=0;j<16;j++){
    if(tfiles_size >2){
      m_histvector16[j]->Add(m_histvectors16[1][j],-m_ttbar_SF*m_lumi);
      m_histvector16[j]->Add(m_histvectors16[2][j],-m_ttbar_SF*m_lumi);
    }
    for(i=3;i<tfiles_size;i++) m_histvector16[j]->Add(m_histvectors16[i][j],-m_lumi);
  }
}
void ABCD_Tools::CreateVectors16_data_MC(){
  vector<vector<double> > bincontents(16),binerrors(16);
  vector<double> totalnumbers(16);
  const int tfiles_size= m_tfiles.size();
  int i,j;
  const int nbins = m_h_template->GetNbinsX();
  CreateVector16(m_histvectors16[0],totalnumbers,bincontents,binerrors);
  m_bincontents16_data = bincontents;
  for(i=0;i<16;i++){
    m_bincontents16_MC[i].resize(nbins);
    m_binerrors16_MC[i].resize(nbins);
    for(j=0;j<nbins;j++){
      m_bincontents16_MC[i][j]=0.;
      m_binerrors16_MC[i][j]=0.;
    }
  }
  double x,y;
  for(int ifile=1;ifile<tfiles_size;ifile++){
    bool use_SF=ifile<3;
    CreateVector16(m_histvectors16[ifile],totalnumbers,bincontents,binerrors);
    for(i=0;i<16;i++)for(j=0;j<nbins;j++){
      x = use_SF ? bincontents[i][j]*m_lumi*m_ttbar_SF :  bincontents[i][j]*m_lumi;
      y = use_SF ? binerrors[i][j]*m_lumi*m_ttbar_SF :  binerrors[i][j]*m_lumi;
      m_bincontents16_MC[i][j]+=x;
      m_binerrors16_MC[i][j] = sqrt(m_binerrors16_MC[i][j]*m_binerrors16_MC[i][j] + y*y);
    }
  }
	
}
void ABCD_Tools::CreateVectors(){
  CreateVectors16_data_MC();
  CreateVector9(m_histvector9,m_totalnumbers9,m_bincontents9,m_binerrors9);
  CreateVector16(m_histvector16,m_totalnumbers16,m_bincontents16,m_binerrors16);
  
  
  
  int i=0;
  const int nbins = m_h_template->GetNbinsX();
  m_binlowedges.resize(nbins);
  m_binupedges.resize(nbins);
  for(i=0;i<nbins;i++){
    m_binlowedges[i]=m_h_template->GetXaxis()->GetBinLowEdge(i+1);
    m_binupedges[i]=m_h_template->GetXaxis()->GetBinUpEdge(i+1);
  }
  for(i=0;i<m_number_of_ABCD9_methods;i++) m_estimates9[i].resize(nbins);
  for(i=0;i<m_number_of_ABCD16_methods;i++) m_estimates16[i].resize(nbins);
  create_ABCD9_table(m_table9,m_table9_hist,m_label9,m_histvector9);
  create_ABCD16_table(m_table16,m_table16_hist,m_label16,m_histvector16);
}


void ABCD_Tools::Make_ABCD_Estimates(const TString& chain_name,const TString& level,const TString& histname){
  m_IsNominal=chain_name == m_nominal_chain_name;
  
  CreateVectors();
  MakeAllABCD9Estimates(m_estimates9,m_bincontents9,m_totalnumbers9,m_bincontents16,m_totalnumbers16);
  MakeAllABCD16Estimates(m_estimates16,m_bincontents16,m_totalnumbers16);
  int i=0;
  for(i=0;i<m_number_of_ABCD9_methods;i++) m_ABCD9_Results[i] =  HistFromVector(m_estimates9[i], m_h_template);
  for(i=0;i<m_number_of_ABCD16_methods;i++) m_ABCD16_Results[i] =  HistFromVector(m_estimates16[i], m_h_template);
  
  const int nbins = m_h_template->GetNbinsX();
  int niter = nbins > 20 ? min(100,(int)m_niter_uncertainties) : m_niter_uncertainties;
  
  //if(m_IsNominal)Calculate_Errors(m_bincontents16,m_binerrors16,niter);
  if(m_IsNominal)Calculate_Errors(m_bincontents16_data,m_bincontents16_MC,m_binerrors16_MC,niter);
  Write_Current_Histos(chain_name, level, histname);
	
}	
void ABCD_Tools::Write_Current_Histos(const TString& chain_name,const TString& level,const TString& histname){
  TString name9=(chain_name + "/" + m_ABCD9_folder_names[8] + "/" + level).Data();
  TString name16=(chain_name + "/" + m_ABCD16_folder_names[15] + "/" + level).Data();
  
  const bool isNominal=chain_name=="nominal";
  
  const int size9=m_tfiles_ABCD9.size();
  const int size16=m_tfiles_ABCD16.size();
  //cout << size9 << endl;
  for(int i=0;i<size9;i++){
    if(m_write_status_ABCD9[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD9[i]!="all")) continue;
    if(!m_tfiles_ABCD9[i]->cd(name9.Data())){
  
      m_tfiles_ABCD9[i]->mkdir(name9.Data());
      m_tfiles_ABCD9[i]->cd(name9.Data());
    }
    m_ABCD9_Results[i]->Write(histname);
  }
  for(int i=0;i<size16;i++){
    if(m_write_status_ABCD16[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD16[i]!="all")) continue;
    TString name = name16;
    if(m_path_to_output_ABCD16[i].Contains("_regionK") || m_path_to_output_ABCD16[i].Contains("_KtoS")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionK] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionL") || m_path_to_output_ABCD16[i].Contains("_LtoS")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionL] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionM") || m_path_to_output_ABCD16[i].Contains("_MtoS")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionM] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionN") || m_path_to_output_ABCD16[i].Contains("_NtoS")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionN] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionKS")) name=chain_name + "/regionKS/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionLS")) name=chain_name + "/regionLS/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionMS")) name=chain_name + "/regionMS/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionNS")) name=chain_name + "/regionNS/" + level;
    if(!m_tfiles_ABCD16[i]->cd(name.Data())){
      m_tfiles_ABCD16[i]->mkdir(name.Data());
      m_tfiles_ABCD16[i]->cd(name.Data());
    }
    //cout << m_ABCD9_Results[i]->Integral() << endl; 
    m_ABCD16_Results[i]->Write(histname); 
  }

}
void ABCD_Tools::DeleteObjects(){
  const int size9=m_tfiles_ABCD9.size();
  const int size16=m_tfiles_ABCD16.size();
  const int tfiles_size= m_tfiles.size();
  int i=0;
  for(i=0;i<size9;i++){
    delete m_ABCD9_Results[i];
  }
  for(i=0;i<size16;i++){
    delete m_ABCD16_Results[i];
  }
  delete m_h_template;
  for(i=0;i<9;i++) delete m_histvector9[i];
  for(i=0;i<16;i++) delete m_histvector16[i];
  for(i=0;i<tfiles_size;i++){
    m_tfiles[i]->Close();
    delete m_tfiles[i];
    m_tfiles[i] = new TFile(m_tfiles_names[i]);
  }
		
}
void ABCD_Tools::DeleteTFiles(const TString& chain_name){
  const int size9=m_tfiles_ABCD9.size();
  const int size16=m_tfiles_ABCD16.size();
  //	const int tfiles_size= m_tfiles.size();
  const bool isNominal=chain_name=="nominal";
  int i=0;
  for(i=0;i<size9;i++){
    if(m_write_status_ABCD9[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD9[i]!="all")) continue;
    m_tfiles_ABCD9[i]->Close();
    delete m_tfiles_ABCD9[i];
    
  }
  for(i=0;i<size16;i++){
    if(m_write_status_ABCD16[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD16[i]!="all")) continue;
    m_tfiles_ABCD16[i]->Close();
    delete m_tfiles_ABCD16[i];
    
  }
  
  
}
void ABCD_Tools::ReloadTFiles(const TString& chain_name){
  const int size9=m_tfiles_ABCD9.size();
  const int size16=m_tfiles_ABCD16.size();
  //	const int tfiles_size= m_tfiles.size();
  const bool isNominal=chain_name=="nominal";
  int i=0;
  for(i=0;i<size9;i++){
    if(m_write_status_ABCD9[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD9[i]!="all")) continue;
    m_tfiles_ABCD9[i]=new TFile(m_path_to_output_ABCD9[i],"update");
  }
  for(i=0;i<size16;i++){
    if(m_write_status_ABCD16[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD16[i]!="all")) continue;
    m_tfiles_ABCD16[i]=new TFile(m_path_to_output_ABCD16[i],"update");
  }
  

}
void ABCD_Tools::LoadAdditionalFiles(){
  m_additional_tfiles["Pythia8_dijet"] = new TFile(m_input_mainDir + "/mc_QCD_LO_merged/allhad.boosted.output.root","read");	
}
void ABCD_Tools::CreateAdditionalHistos(const TString& chain_name,const TString& level,const TString& histname){
  CreateHistVector9(m_additional_histvectors9["Pythia8_dijet"],m_additional_tfiles["Pythia8_dijet"],chain_name,level,histname);
  //cout << "Additional histos Integral: " << m_additional_histvectors9["Pythia8_dijet"][0]->Integral() << endl;
}

ABCD_Tools::~ABCD_Tools(){
  //const int size9=m_tfiles_ABCD9.size();
  //const int size16=m_tfiles_ABCD16.size();
  const int tfiles_size= m_tfiles.size();
  int i=0;
  /*for(i=0;i<size9;i++){
	  m_tfiles_ABCD9[i]->Close();
	  delete m_tfiles_ABCD9[i];
  }
  for(i=0;i<size16;i++){
	  m_tfiles_ABCD16[i]->Close();
	  delete m_tfiles_ABCD16[i];
  }*/
  for(i=0;i<tfiles_size;i++){
	  m_tfiles[i]->Close();
  }
}
void ABCD_Tools::CreateFoldersInTFiles(const TString& chain_name,const TString& level){
  TString name9=(chain_name + "/" + m_ABCD9_folder_names[8] + "/" + level).Data();
  TString name16=(chain_name + "/" + m_ABCD16_folder_names[15] + "/" + level).Data();
  const bool isNominal=chain_name=="nominal";
  const int size9=m_tfiles_ABCD9.size();
  const int size16=m_tfiles_ABCD16.size();
  int i=0;
  for(i=0;i<size9;i++){
    if(m_write_status_ABCD9[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD9[i]!="all")) continue;
    m_tfiles_ABCD9[i]->mkdir(name9.Data());
  }
  for(i=0;i<size16;i++){
    if(m_write_status_ABCD16[i]=="none")continue;
    if((!isNominal) && (m_write_status_ABCD16[i]!="all")) continue;
    TString name = name16;
    if(m_path_to_output_ABCD16[i].Contains("_regionK")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionK] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionL")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionL] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionM")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionM] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionN")) name=chain_name + "/" + m_ABCD16_folder_names[m_iRegionN] + "/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionKS")) name=chain_name + "/regionKS/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionLS")) name=chain_name + "/regionLS/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionMS")) name=chain_name + "/regionMS/" + level;
    if(m_path_to_output_ABCD16[i].Contains("_regionNS")) name=chain_name + "/regionNS/" + level;
    m_tfiles_ABCD16[i]->mkdir(name.Data());
  }
}

void ABCD_Tools::ConvertVector16to9(const vector<vector<double> >& m_bincontents16,vector<vector<double> >& m_bincontents9){
  int nbins=m_bincontents16[0].size();
  int i=0;
  for(i=0;i<9;i++)m_bincontents9[i].resize(nbins);
  for(i=0;i<nbins;i++){

    m_bincontents9[m_i0t0b][i]=m_bincontents16[m_iRegionA][i];
    m_bincontents9[m_i0t1b][i]=m_bincontents16[m_iRegionI][i] + m_bincontents16[m_iRegionB][i];
    m_bincontents9[m_i0t2b][i]=m_bincontents16[m_iRegionH][i];
    m_bincontents9[m_i1t0b][i]=m_bincontents16[m_iRegionC][i] + m_bincontents16[m_iRegionE][i];
    m_bincontents9[m_i1t1b][i]=m_bincontents16[m_iRegionJ][i] + m_bincontents16[m_iRegionD][i] + m_bincontents16[m_iRegionG][i] + m_bincontents16[m_iRegionO][i];
    m_bincontents9[m_i1t2b][i]=m_bincontents16[m_iRegionL][i] + m_bincontents16[m_iRegionN][i];
    m_bincontents9[m_i2t0b][i]=m_bincontents16[m_iRegionF][i];
    m_bincontents9[m_i2t1b][i]=m_bincontents16[m_iRegionK][i] + m_bincontents16[m_iRegionM][i];
    m_bincontents9[m_i2t2b][i]=m_bincontents16[m_iRegionP][i];	
  }
}
void ABCD_Tools::ConvertVector16to9(const vector<double>& m_bincontents16,vector<double>& m_bincontents9){
  m_bincontents9[m_i0t0b]=m_bincontents16[m_iRegionA];
  m_bincontents9[m_i0t1b]=m_bincontents16[m_iRegionI] + m_bincontents16[m_iRegionB];
  m_bincontents9[m_i0t2b]=m_bincontents16[m_iRegionH];
  m_bincontents9[m_i1t0b]=m_bincontents16[m_iRegionC] + m_bincontents16[m_iRegionE];
  m_bincontents9[m_i1t1b]=m_bincontents16[m_iRegionJ] + m_bincontents16[m_iRegionD] + m_bincontents16[m_iRegionG] + m_bincontents16[m_iRegionO];
  m_bincontents9[m_i1t2b]=m_bincontents16[m_iRegionL] + m_bincontents16[m_iRegionN];
  m_bincontents9[m_i2t0b]=m_bincontents16[m_iRegionF];
  m_bincontents9[m_i2t1b]=m_bincontents16[m_iRegionK] + m_bincontents16[m_iRegionM];
  m_bincontents9[m_i2t2b]=m_bincontents16[m_iRegionP];
}
/*void ABCD_Tools::MakeAllABCDEstimates(vector<vector<double> >& estimates9,vector<vector<double> >& estimates16, vector<vector<double> >& m_bincontents9, vector<double>& m_totalnumbers9, vector<vector<double> >& m_bincontents16, vector<double>& m_totalnumbers16){
	//int number_of_ABCD9_methods=m_outputnames_ABCD9.size();

	//ConvertVector16to9(m_bincontents16,m_bincontents9);
	//ConvertVector16to9(m_totalnumbers16,m_totalnumbers9);
	MakeAllABCD9Estimates(estimates9,m_bincontents9,m_totalnumbers9,m_bincontents16,m_totalnumbers16);
	MakeAllABCD16Estimates(estimates16,m_bincontents16,m_totalnumbers16);
}*/

void ABCD_Tools::MakeAllABCD9Estimates(vector<vector<double> >& estimates, const vector<vector<double> >& m_bincontents9, const vector<double>& m_totalnumbers9, const vector<vector<double> >& m_bincontents16, const vector<double>& m_totalnumbers16){
  const int nbins=m_bincontents9[0].size();
  double Nf(0.),Nf_BBB(0.);
  double x1,x2,x_Avr,x_bjets;
  double y,y_BBB;
  double x1_BBB,x2_BBB,x_BBB_Avr,x_BBB_bjets;
  double x_ABCD4_BBB_Avr,x3_BBB,x4_BBB;
  double x_cross1_BBB,x_cross2_BBB,x_cross_BBB_Avr;
  double c,c_BBB;
  double helpterm;
  
  if(m_totalnumbers9[m_i0t0b]>0.) y=m_totalnumbers9[m_i0t1b]>0. ?  (m_totalnumbers9[m_i1t0b]/m_totalnumbers9[m_i0t0b] + m_totalnumbers9[m_i1t1b]/m_totalnumbers9[m_i0t1b])/2 : m_totalnumbers9[m_i1t0b]/m_totalnumbers9[m_i0t0b] ;
  else 					  y=m_totalnumbers9[m_i0t1b]>0. ?  m_totalnumbers9[m_i1t1b]/m_totalnumbers9[m_i0t1b] : 0.;
  
  c= m_totalnumbers16[m_iRegionA] > 0. ? m_totalnumbers16[m_iRegionB]*m_totalnumbers16[m_iRegionI]/m_totalnumbers16[m_iRegionA] : 0.;
    
  for(int i=0;i<nbins;i++){
    
    x1 = m_totalnumbers9[m_i1t0b]>0 ? m_bincontents9[m_i2t0b][i]/m_totalnumbers9[m_i1t0b] : 0. ;
    x2 = m_totalnumbers9[m_i1t1b]>0 ? m_bincontents9[m_i2t1b][i]/m_totalnumbers9[m_i1t1b] : 0. ;
    if(x1>0.) x_Avr= x2 > 0. ? (x1+x2)/2 : x1 ;
    else x_Avr= x2 > 0. ? x2 : 0.;
    x1_BBB =m_bincontents9[m_i1t0b][i]>0 ? m_bincontents9[m_i2t0b][i]/m_bincontents9[m_i1t0b][i] : 0. ;
    x2_BBB =m_bincontents9[m_i1t1b][i]>0 ? m_bincontents9[m_i2t1b][i]/m_bincontents9[m_i1t1b][i] : 0. ;
    x3_BBB =m_bincontents9[m_i0t0b][i]>0 ? m_bincontents9[m_i2t0b][i]/m_bincontents9[m_i0t0b][i] : 0. ;
    x4_BBB =m_bincontents9[m_i0t1b][i]>0 ? m_bincontents9[m_i2t1b][i]/m_bincontents9[m_i0t1b][i] : 0. ;
    helpterm=m_bincontents9[m_i0t1b][i]*m_bincontents9[m_i1t0b][i];
    x_cross1_BBB = helpterm > 0 ? m_bincontents9[m_i2t0b][i]*m_bincontents9[m_i1t1b][i]/helpterm : 0.;
    helpterm=m_bincontents9[m_i0t0b][i]*m_bincontents9[m_i1t1b][i];
    x_cross2_BBB = helpterm > 0 ? m_bincontents9[m_i2t1b][i]*m_bincontents9[m_i1t0b][i]/helpterm : 0.;
    
    
    if(x1_BBB>0.) x_BBB_Avr= x2_BBB > 0. ? (x1_BBB+x2_BBB)/2 : x1_BBB ;
    else x_BBB_Avr= x2_BBB > 0. ? x2_BBB : 0.;
    if(x3_BBB>0.) x_ABCD4_BBB_Avr= x4_BBB > 0. ? (x3_BBB+x4_BBB)/2 : x3_BBB ;
    else x_ABCD4_BBB_Avr= x4_BBB > 0. ? x4_BBB : 0.;
    if(x_cross1_BBB>0.) x_cross_BBB_Avr= x_cross2_BBB > 0. ? (x_cross1_BBB+x_cross2_BBB)/2 : x_cross1_BBB ;
    else x_cross_BBB_Avr= x_cross2_BBB > 0. ? x_cross2_BBB : 0.;
    
    if(m_bincontents9[m_i0t0b][i]>0.) y_BBB=m_bincontents9[m_i0t1b][i]>0. ?  (m_bincontents9[m_i1t0b][i]/m_bincontents9[m_i0t0b][i] + m_bincontents9[m_i1t1b][i]/m_bincontents9[m_i0t1b][i])/2 : m_bincontents9[m_i1t0b][i]/m_bincontents9[m_i0t0b][i];
    else 					  y_BBB=m_bincontents9[m_i0t1b][i]>0. ?  m_bincontents9[m_i1t1b][i]/m_bincontents9[m_i0t1b][i] : 0.;
  
    x_bjets= m_totalnumbers9[m_i0t1b] > 0. ? m_bincontents9[m_i0t2b][i]*(m_totalnumbers9[m_i2t1b]/m_totalnumbers9[m_i0t1b]) : 0.;
    x_BBB_bjets = m_bincontents9[m_i0t1b][i] > 0. ? m_bincontents9[m_i0t2b][i]*(m_bincontents9[m_i2t1b][i]/m_bincontents9[m_i0t1b][i]) : 0.;
  
    c_BBB= m_bincontents16[m_iRegionA][i] > 0. ? m_bincontents16[m_iRegionB][i]*m_bincontents16[m_iRegionI][i]/m_bincontents16[m_iRegionA][i] : 0.;
    
  
    Nf=y*m_totalnumbers9[m_i0t2b];
    Nf_BBB=y_BBB*m_bincontents9[m_i0t2b][i];
    
    estimates[0][i] = x1*m_totalnumbers9[m_i1t2b];
    estimates[1][i] = x2*m_totalnumbers9[m_i1t2b];
    estimates[2][i] = x_Avr*m_totalnumbers9[m_i1t2b]; 
    
    estimates[3][i] = x1_BBB*m_bincontents9[m_i1t2b][i];
    estimates[4][i] = x2_BBB*m_bincontents9[m_i1t2b][i];
    estimates[5][i] = x_BBB_Avr*m_bincontents9[m_i1t2b][i];
    
    estimates[6][i] = x1*Nf;
    estimates[7][i] = x2*Nf;
    estimates[8][i] = x_Avr*Nf;
    
    estimates[9][i] = x1_BBB*Nf_BBB;
    estimates[10][i] = x2_BBB*Nf_BBB;
    estimates[11][i] = x_BBB_Avr*Nf_BBB;
    
    estimates[12][i] = x_bjets;
    estimates[13][i] = x_BBB_bjets;
    
    estimates[14][i] = y*c*x1;
    estimates[15][i] = y_BBB*c_BBB*x1_BBB;
    
    estimates[16][i] = x_ABCD4_BBB_Avr*m_bincontents9[m_i0t2b][i];
    estimates[17][i] = x_cross_BBB_Avr*m_bincontents9[m_i0t2b][i];
    estimates[18][i] = x3_BBB*m_bincontents9[m_i0t2b][i];
    estimates[19][i] = x4_BBB*m_bincontents9[m_i0t2b][i];
    //cout << "Cross terms: " << i << " " << x_cross1_BBB << " " << x_cross2_BBB << " " << x_cross_BBB_Avr << endl;
    //cout << "ABCD4 terms: " << i << " " << x3_BBB << " " << x4_BBB << " " << x_ABCD4_BBB_Avr << endl;
    
  }
	
	
}

void ABCD_Tools::MakeAllABCD16Estimates(vector<vector<double> >& estimates, const vector<vector<double> >& bincontents, const vector<double>& totalnumbers){
  int nbins = bincontents[0].size();
  
  double Np1,Np2,Np5;
  double Np3,Np4;
  double H;
  
  //H=totalnumbers[m_iRegionB]*totalnumbers[m_iRegionI]/totalnumbers[m_iRegionA];
  H=totalnumbers[m_iRegionH];
  Np1 = (totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionO])/(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionC]);
  Np2 = (totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionO]*H)/(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionD]*totalnumbers[m_iRegionI]);
  Np3 = (totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionO]*H)/(totalnumbers[m_iRegionB]*totalnumbers[m_iRegionC]*totalnumbers[m_iRegionG]);
  Np4 = (totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionO]*totalnumbers[m_iRegionI])/(totalnumbers[m_iRegionA]*totalnumbers[m_iRegionC]*totalnumbers[m_iRegionG]);
  Np5 = (totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionO]*totalnumbers[m_iRegionB])/(totalnumbers[m_iRegionA]*totalnumbers[m_iRegionE]*totalnumbers[m_iRegionD]);
  //double normalizationFactor = (Np1 + Np2 + Np3 + Np4 + Np5)/5.;
  double normalizationFactor;  
  if(Np2 > 0) normalizationFactor = Np3 > 0 ? (Np2 + Np3)/2. : Np2 ;
  else normalizationFactor = Np3;
  double bincontent;
  double Np1_BBB,Np2_BBB,Np5_BBB;
  double Np3_BBB,Np4_BBB;
  double Np6_BBB,Np7_BBB,Np8_BBB;
  double Np9_BBB, Np0_BBB;
  double Np_BBB_bjets;
  double Np_BBB_bjet1;
  double Np_BBB_ljet1_tau32;
  double Np_BBB_ljet2_tau32;
  double H_BBB;
  double Np_BBB_regionK,Np_BBB_regionL,Np_BBB_regionM,Np_BBB_regionN;
  double Np_BBB_regionKS,Np_BBB_regionLS,Np_BBB_regionMS,Np_BBB_regionNS;
  double Np_BBB_regionLS_ljet1_substructure;
  double Np_BBB_regionNS_ljet2_substructure;
  
  const double correction_bjets=totalnumbers[m_iRegionF]*totalnumbers[m_iRegionA]/(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionC]);
  const double correction_bjet1=totalnumbers[m_iRegionD]*totalnumbers[m_iRegionG]*totalnumbers[m_iRegionF]*pow(totalnumbers[m_iRegionA],3)/(totalnumbers[m_iRegionB]*totalnumbers[m_iRegionI]*pow(totalnumbers[m_iRegionC],2)*pow(totalnumbers[m_iRegionE],2));
  const double correction_ljet1_tau32=totalnumbers[m_iRegionG]*totalnumbers[m_iRegionH]*pow(totalnumbers[m_iRegionA],2)/(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionB]*pow(totalnumbers[m_iRegionI],2));
  const double correction_ljet2_tau32=totalnumbers[m_iRegionD]*totalnumbers[m_iRegionH]*pow(totalnumbers[m_iRegionA],2)/(totalnumbers[m_iRegionC]*totalnumbers[m_iRegionI]*pow(totalnumbers[m_iRegionB],2));
  

  for(int i=0;i<nbins;i++)  {
    estimates[0][i] = normalizationFactor==normalizationFactor ? normalizationFactor*bincontents[m_iRegionF][i] : 0.;
    estimates[1][i] = Np1==Np1 ? Np1*bincontents[m_iRegionF][i] : 0.;
    estimates[2][i] = Np2==Np2 ? Np2*bincontents[m_iRegionF][i] : 0.;
    estimates[3][i] = Np3==Np3 ? Np3*bincontents[m_iRegionF][i] : 0.;
    estimates[4][i] = Np4==Np4 ? Np4*bincontents[m_iRegionF][i] : 0.;
    estimates[5][i] = Np5==Np5 ? Np5*bincontents[m_iRegionF][i] : 0.;
  
    //H_BBB=bincontents[m_iRegionB][i]*bincontents[m_iRegionI][i]/bincontents[m_iRegionA][i];
    H_BBB=bincontents[m_iRegionH][i];
    Np1_BBB = (bincontents[m_iRegionK][i]*bincontents[m_iRegionL][i]*bincontents[m_iRegionM][i]*bincontents[m_iRegionN][i]*
	      bincontents[m_iRegionB][i]*bincontents[m_iRegionE][i]*bincontents[m_iRegionC][i]*bincontents[m_iRegionI][i])/
	      (bincontents[m_iRegionJ][i]*bincontents[m_iRegionD][i]*bincontents[m_iRegionG][i]*bincontents[m_iRegionO][i]*
	      bincontents[m_iRegionF][i]*bincontents[m_iRegionH][i]*bincontents[m_iRegionA][i]);
    Np2_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionF][i]*H_BBB)/(bincontents[m_iRegionE][i]*bincontents[m_iRegionD][i]*bincontents[m_iRegionI][i]);
    Np3_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionF][i]*H_BBB)/(bincontents[m_iRegionB][i]*bincontents[m_iRegionC][i]*bincontents[m_iRegionG][i]);
    Np4_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionF][i]*bincontents[m_iRegionI][i])/(bincontents[m_iRegionA][i]*bincontents[m_iRegionC][i]*bincontents[m_iRegionG][i]);
    Np5_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionF][i]*bincontents[m_iRegionB][i])/(bincontents[m_iRegionA][i]*bincontents[m_iRegionE][i]*bincontents[m_iRegionD][i]);
    Np6_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*H_BBB)/(bincontents[m_iRegionI][i]*bincontents[m_iRegionB][i]);
    Np7_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionC][i]*H_BBB)/(bincontents[m_iRegionD][i]*bincontents[m_iRegionI][i]*bincontents[m_iRegionA][i]);
    Np8_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionE][i]*H_BBB)/(bincontents[m_iRegionB][i]*bincontents[m_iRegionA][i]*bincontents[m_iRegionG][i]);
    //Np9_BBB = bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]/bincontents[m_iRegionA][i];
    Np9_BBB = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionH][i]*bincontents[m_iRegionF][i]*bincontents[m_iRegionD][i]*bincontents[m_iRegionG][i]*pow(bincontents[m_iRegionA][i],3))/pow(bincontents[m_iRegionB][i]*bincontents[m_iRegionE][i]*bincontents[m_iRegionC][i]*bincontents[m_iRegionI][i],2);
    Np0_BBB = bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]/bincontents[m_iRegionA][i];
    Np_BBB_bjets = bincontents[m_iRegionL][i]*bincontents[m_iRegionN][i]/bincontents[m_iRegionH][i]*correction_bjets;
    Np_BBB_bjet1 = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionH][i])/(bincontents[m_iRegionB][i]*bincontents[m_iRegionI][i])*correction_bjet1;
    Np_BBB_ljet1_tau32 = bincontents[m_iRegionK][i]*bincontents[m_iRegionO][i]/bincontents[m_iRegionC][i]*correction_ljet1_tau32;
    Np_BBB_ljet2_tau32 = bincontents[m_iRegionM][i]*bincontents[m_iRegionJ][i]/bincontents[m_iRegionE][i]*correction_ljet2_tau32;
    
    Np_BBB_regionK = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionD][i]*bincontents[m_iRegionF][i]*bincontents[m_iRegionA][i])/(bincontents[m_iRegionB][i]*bincontents[m_iRegionE][i]*bincontents[m_iRegionC][i]);
    Np_BBB_regionL = (bincontents[m_iRegionJ][i]*bincontents[m_iRegionH][i]*bincontents[m_iRegionG][i]*bincontents[m_iRegionA][i])/(bincontents[m_iRegionB][i]*bincontents[m_iRegionE][i]*bincontents[m_iRegionI][i]);
    Np_BBB_regionM = (bincontents[m_iRegionG][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionF][i]*bincontents[m_iRegionA][i])/(bincontents[m_iRegionI][i]*bincontents[m_iRegionC][i]*bincontents[m_iRegionE][i]);
    Np_BBB_regionN = (bincontents[m_iRegionH][i]*bincontents[m_iRegionO][i]*bincontents[m_iRegionD][i]*bincontents[m_iRegionA][i])/(bincontents[m_iRegionI][i]*bincontents[m_iRegionC][i]*bincontents[m_iRegionB][i]);
    
    Np_BBB_regionKS = ( std::isfinite(Np_BBB_regionK) ? Np_BBB_regionK : 0. ) + ( std::isfinite(Np9_BBB) ? Np9_BBB : 0. );
    Np_BBB_regionLS = ( std::isfinite(Np_BBB_regionL) ? Np_BBB_regionL : 0. ) + ( std::isfinite(Np9_BBB) ? Np9_BBB : 0. );
    Np_BBB_regionMS = ( std::isfinite(Np_BBB_regionM) ? Np_BBB_regionM : 0. ) + ( std::isfinite(Np9_BBB) ? Np9_BBB : 0. );
    Np_BBB_regionNS = ( std::isfinite(Np_BBB_regionN) ? Np_BBB_regionN : 0. ) + ( std::isfinite(Np9_BBB) ? Np9_BBB : 0. );
    
    Np_BBB_regionLS_ljet1_substructure = ( std::isfinite(Np_BBB_regionL) ? Np_BBB_regionL : 0. ) + ( std::isfinite(Np_BBB_ljet1_tau32) ? Np_BBB_ljet1_tau32 : 0. );
    Np_BBB_regionNS_ljet2_substructure = ( std::isfinite(Np_BBB_regionN) ? Np_BBB_regionN : 0. ) + ( std::isfinite(Np_BBB_ljet2_tau32) ? Np_BBB_ljet2_tau32 : 0. );
    
    //bincontent = (Np1_BBB + Np4_BBB + Np5_BBB)/3.;
    if(Np2_BBB > 0) bincontent = Np3_BBB > 0 ? (Np2_BBB + Np3_BBB)/2. : Np2_BBB ;
    else bincontent = Np3_BBB;
    //bincontent = (Np2_BBB + Np3_BBB)/2.;
  
    estimates[6][i] = std::isfinite(bincontent) && (bincontent > 0) ? bincontent : 0.; // std::isfinite = 0 for nan,+-inf, otherwise 1 
    estimates[7][i] = std::isfinite(Np1_BBB) && (Np1_BBB > 0.) ? Np1_BBB : 0.;
    estimates[8][i] = std::isfinite(Np2_BBB) && (Np2_BBB > 0.) ? Np2_BBB : 0.;
    estimates[9][i] = std::isfinite(Np3_BBB) && (Np3_BBB > 0.) ? Np3_BBB : 0.;
    estimates[10][i]= std::isfinite(Np4_BBB) && (Np4_BBB > 0.) ? Np4_BBB : 0.;
    estimates[11][i]= std::isfinite(Np5_BBB) && (Np5_BBB > 0.) ? Np5_BBB : 0.;
    estimates[12][i]= std::isfinite(Np6_BBB) && (Np6_BBB > 0.) ? Np6_BBB : 0.;
    estimates[13][i]= std::isfinite(Np7_BBB) && (Np7_BBB > 0.) ? Np7_BBB : 0.;
    estimates[14][i]= std::isfinite(Np8_BBB) && (Np8_BBB > 0.) ? Np8_BBB : 0.;
    estimates[15][i]= std::isfinite(Np9_BBB) && (Np9_BBB > 0.) ? Np9_BBB : 0.;
    estimates[16][i]= std::isfinite(Np0_BBB) && (Np0_BBB > 0.) ? Np0_BBB : 0.;
    estimates[17][i]= std::isfinite(Np_BBB_bjets) && (Np_BBB_bjets > 0.) ? Np_BBB_bjets : 0.;
    estimates[18][i]= std::isfinite(Np_BBB_bjet1) && (Np_BBB_bjet1 > 0.) ? Np_BBB_bjet1 : 0.;
    estimates[19][i]= std::isfinite(Np_BBB_ljet1_tau32) && (Np_BBB_ljet1_tau32 > 0.)? Np_BBB_ljet1_tau32 : 0.;
    estimates[20][i]= std::isfinite(Np_BBB_ljet2_tau32) && (Np_BBB_ljet2_tau32 > 0.)? Np_BBB_ljet2_tau32 : 0.;
    
    estimates[21][i]= std::isfinite(Np_BBB_regionK) && (Np_BBB_regionK > 0.) ? Np_BBB_regionK : 0.;
    estimates[22][i]= std::isfinite(Np_BBB_regionL) && (Np_BBB_regionL > 0.) ? Np_BBB_regionL : 0.;
    estimates[23][i]= std::isfinite(Np_BBB_regionM) && (Np_BBB_regionM > 0.) ? Np_BBB_regionM : 0.;
    estimates[24][i]= std::isfinite(Np_BBB_regionN) && (Np_BBB_regionN > 0.) ? Np_BBB_regionN : 0.;
    
    estimates[25][i]= std::isfinite(Np_BBB_regionKS) && (Np_BBB_regionKS > 0.) ? Np_BBB_regionKS : 0.;
    estimates[26][i]= std::isfinite(Np_BBB_regionLS) && (Np_BBB_regionLS > 0.) ? Np_BBB_regionLS : 0.;
    estimates[27][i]= std::isfinite(Np_BBB_regionMS) && (Np_BBB_regionMS > 0.) ? Np_BBB_regionMS : 0.;
    estimates[28][i]= std::isfinite(Np_BBB_regionNS) && (Np_BBB_regionNS > 0.) ? Np_BBB_regionNS : 0.;
    estimates[29][i]= std::isfinite(Np_BBB_regionLS_ljet1_substructure) && (Np_BBB_regionLS_ljet1_substructure > 0.) ? Np_BBB_regionLS_ljet1_substructure : 0.;
    estimates[30][i]= std::isfinite(Np_BBB_regionNS_ljet2_substructure) && (Np_BBB_regionNS_ljet2_substructure > 0.) ? Np_BBB_regionNS_ljet2_substructure : 0.;
    
    // Estimates of multijet background mis-tagging efficiencies
    estimates[31][i]= estimates[26][i] > 0. ? estimates[15][i] / estimates[26][i] : 0.;
    estimates[32][i]= estimates[25][i] > 0. ? estimates[15][i] / estimates[25][i] : 0.;
    estimates[33][i]= estimates[28][i] > 0. ? estimates[15][i] / estimates[28][i] : 0.;
    estimates[34][i]= estimates[27][i] > 0. ? estimates[15][i] / estimates[27][i] : 0.;
    
    const double dataS = m_histvectors16[0][m_iRegionP]->GetBinContent(i+1);
    const double dataK = m_histvectors16[0][m_iRegionK]->GetBinContent(i+1);
    const double dataL = m_histvectors16[0][m_iRegionL]->GetBinContent(i+1);
    const double dataM = m_histvectors16[0][m_iRegionM]->GetBinContent(i+1);
    const double dataN = m_histvectors16[0][m_iRegionN]->GetBinContent(i+1);
    
    double mcSignalS = 0.;
    double mcSignalK = 0.;
    double mcSignalL = 0.;
    double mcSignalM = 0.;
    double mcSignalN = 0.;
    
    double mcBackgroundS = 0.;
    double mcBackgroundK = 0.;
    double mcBackgroundL = 0.;
    double mcBackgroundM = 0.;
    double mcBackgroundN = 0.;
    
    if(m_histvectors16.size()>1) {
      mcSignalS = m_histvectors16[1][m_iRegionP]->GetBinContent(i+1);
      mcSignalK = m_histvectors16[1][m_iRegionK]->GetBinContent(i+1);
      mcSignalL = m_histvectors16[1][m_iRegionL]->GetBinContent(i+1);
      mcSignalM = m_histvectors16[1][m_iRegionM]->GetBinContent(i+1);
      mcSignalN = m_histvectors16[1][m_iRegionN]->GetBinContent(i+1);
    }
    for(size_t k=2;k<m_histvectors16.size();k++) {
      mcBackgroundS += m_histvectors16[k][m_iRegionP]->GetBinContent(i+1);
      mcBackgroundK += m_histvectors16[k][m_iRegionK]->GetBinContent(i+1);
      mcBackgroundL += m_histvectors16[k][m_iRegionL]->GetBinContent(i+1);
      mcBackgroundM += m_histvectors16[k][m_iRegionM]->GetBinContent(i+1);
      mcBackgroundN += m_histvectors16[k][m_iRegionN]->GetBinContent(i+1);
    }
    
    estimates[35][i]= mcSignalS ? (dataS - mcBackgroundS - estimates[15][i]) / mcSignalS : 0.;
    estimates[36][i]= mcSignalK ? (dataK - mcBackgroundK - estimates[21][i]) / mcSignalK : 0.;
    estimates[37][i]= mcSignalL ? (dataL - mcBackgroundL - estimates[22][i]) / mcSignalL : 0.;
    estimates[38][i]= mcSignalM ? (dataM - mcBackgroundM - estimates[23][i]) / mcSignalM : 0.;
    estimates[39][i]= mcSignalN ? (dataN - mcBackgroundN - estimates[24][i]) / mcSignalN : 0.;
    
    estimates[40][i]= estimates[35][i] ? estimates[36][i]/estimates[35][i] : 0.;
    estimates[41][i]= estimates[35][i] ? estimates[37][i]/estimates[35][i] : 0.;
    estimates[42][i]= estimates[35][i] ? estimates[38][i]/estimates[35][i] : 0.;
    estimates[43][i]= estimates[35][i] ? estimates[39][i]/estimates[35][i] : 0.;
    
    
  }       
	        
	
	
}
void ABCD_Tools::Calculate_Errors(const vector<vector<double> >& bincontents16_data, const vector<vector<double> >& bincontents16_MC,const vector<vector<double> >& binerrors16_MC, const unsigned int& niter){
  const int size9 = 9;
  const int size16 = bincontents16_data.size();
  const int nbins= bincontents16_data[0].size();
  const int number_of_ABCD9_methods=m_ABCD9_Results.size();
  const int number_of_ABCD16_methods=m_ABCD16_Results.size();
  const int ntfiles=m_tfiles.size();
  int i=0,j=0;
  vector<vector<double> > estimates9(number_of_ABCD9_methods),means9(number_of_ABCD9_methods),errors9(number_of_ABCD9_methods);
  vector<vector<double> > estimates16(number_of_ABCD16_methods),means16(number_of_ABCD16_methods),errors16(number_of_ABCD16_methods);
  for(i=0;i<number_of_ABCD9_methods;i++){
    estimates9[i].resize(nbins);
    means9[i].resize(nbins);
    errors9[i].resize(nbins);
  }
  for(i=0;i<number_of_ABCD16_methods;i++){
    estimates16[i].resize(nbins);
    means16[i].resize(nbins);
    errors16[i].resize(nbins);
  }
 
  vector<vector<double> > bincontents_smeared9(size9),bincontents_smeared16(size16);
  vector<double> totalnumbers_smeared9(size9),totalnumbers_smeared16(size16);
  for(i=0;i<size9;i++) bincontents_smeared9[i].resize(nbins);
  for(i=0;i<size16;i++) bincontents_smeared16[i].resize(nbins);
  for(unsigned int iiter=0;iiter<niter;iiter++){
  //if(iiter%100 ==0) cout << iiter << "/" << niter << endl;
    if(ntfiles>1)smear_bin_contents(bincontents16_data,bincontents16_MC,binerrors16_MC,bincontents_smeared16);
    else smear_bin_contents(m_bincontents16,m_binerrors16,bincontents_smeared16);
    ConvertVector16to9(bincontents_smeared16,bincontents_smeared9);
    
    for(i=0;i<size9;i++) totalnumbers_smeared9[i]= std::accumulate(bincontents_smeared9[i].begin(), bincontents_smeared9[i].end(), 0.0);
    for(i=0;i<size16;i++) totalnumbers_smeared16[i]= std::accumulate(bincontents_smeared16[i].begin(), bincontents_smeared16[i].end(), 0.0);
      MakeAllABCD9Estimates(estimates9,bincontents_smeared9,totalnumbers_smeared9,bincontents_smeared16,totalnumbers_smeared16);
      MakeAllABCD16Estimates(estimates16,bincontents_smeared16,totalnumbers_smeared16);
      for(j=0;j<number_of_ABCD9_methods;j++)for(i=0;i<nbins;i++){
	means9[j][i]+=estimates9[j][i];
	errors9[j][i]+=(estimates9[j][i]*estimates9[j][i]);
      }
      for(j=0;j<number_of_ABCD16_methods;j++)for(i=0;i<nbins;i++){
	means16[j][i]+=estimates16[j][i];
	errors16[j][i]+=(estimates16[j][i]*estimates16[j][i]);
      }
  }
  for(j=0;j<number_of_ABCD9_methods;j++)for(i=0;i<nbins;i++){
    means9[j][i]/=niter;
    errors9[j][i]/=niter;
    errors9[j][i]=sqrt(errors9[j][i] - (means9[j][i]*means9[j][i]));
    m_ABCD9_Results[j]->SetBinError(i+1,errors9[j][i]);
  } 
  for(j=0;j<number_of_ABCD16_methods;j++)for(i=0;i<nbins;i++){
    means16[j][i]/=niter;
    errors16[j][i]/=niter;
    errors16[j][i]=sqrt(errors16[j][i] - (means16[j][i]*means16[j][i]));
    m_ABCD16_Results[j]->SetBinError(i+1,errors16[j][i]);
  } 

	
}
void ABCD_Tools::Make_Alternative_ABCD_Estimates(const TString& histname){
	
  TString dirname = m_foldername_tests + "/MC_scale_factors/";
  gSystem->mkdir(dirname,true);
  ofstream textfile((dirname + histname + ".txt").Data());
  
  m_alternative_estimates.resize(8);
  const int size = m_alternative_estimates.size();
  const int nbins = m_bincontents16_data[0].size();
  vector<double> global_alternative_estimates(size),global_MC_yields(16),global_data_yields(16);
  vector<vector<double> > bincontents16_data=m_bincontents16_data;
  vector<vector<double> > bincontents16_MC=m_bincontents16_MC;
  vector<double> mean(size),error(size);
  
  int indexes[]{m_iRegionK,m_iRegionL,m_iRegionM,m_iRegionN,m_iRegionK,m_iRegionL,m_iRegionM,m_iRegionN};
  int i;
  for(i=0;i<size;i++){
    m_alternative_estimates[i].resize(nbins);
  }
  for(unsigned int iiter=0;iiter<=m_niter_uncertainties;iiter++){
    if(iiter<m_niter_uncertainties){
      smear_bin_contents_data(m_bincontents16_data,bincontents16_data);
      smear_bin_contents(m_bincontents16_MC,m_binerrors16_MC,bincontents16_MC);
    }
    else{
      bincontents16_data=m_bincontents16_data;
      bincontents16_MC=m_bincontents16_MC;
    }
    MakeAlternativeEstimates(m_alternative_estimates,bincontents16_data,bincontents16_MC);
    for(i=0;i<16;i++)global_MC_yields[i]=std::accumulate(bincontents16_MC[i].begin(), bincontents16_MC[i].end(), 0.0);
    for(i=0;i<size;i++){
      
      global_alternative_estimates[i]=std::accumulate(m_alternative_estimates[i].begin(), m_alternative_estimates[i].end(), 0.0);
    //global_data_yields[i]=std::accumulate(bincontents16_data[i].begin(), bincontents16_data[i].end(), 0.0);
      if(iiter<m_niter_uncertainties){
	      mean[i]+=m_ttbar_SF*global_alternative_estimates[i]/global_MC_yields[indexes[i]];
	      error[i]+=pow(m_ttbar_SF*global_alternative_estimates[i]/global_MC_yields[indexes[i]],2);
      }
    }
    
    
  }
  for(i=0;i<size;i++){
    mean[i]/=m_niter_uncertainties;
    error[i]/=m_niter_uncertainties;
    error[i]=sqrt(error[i]-mean[i]*mean[i]);
    textfile << m_ttbar_SF*global_alternative_estimates[i]/global_MC_yields[indexes[i]] << " $\\pm$ " << error[i] << endl;
  }	
	
}	
double ABCD_Tools::MakeOneAlternativeEstimate(const vector<double>& data,const vector<double>& MC){
  const double aD= MC[0]/MC[3];
  const double bD= MC[1]/MC[3];
  const double cD= MC[2]/MC[3];
  const double  a=aD - bD*cD;
  const double b=bD*data[2] + cD*data[1] - aD*data[3] - data[0];
  const double c=data[3]*data[0]-data[1]*data[2];
  const double K=b*b - 4.*a*c;
  //cout << endl << "MakeOneAlternativeEstimate: " << endl;
  //cout << aD << " " << bD << " " << cD << endl;
  //cout << a << " " << b << " " << c << " " << sqrt(K) << endl;
  //cout << (-b + sqrt(K))/(2.*a) << " " << (-b - sqrt(K))/(2.*a)<< endl;
  //return -c/b;
  return K>0 ? (-b - sqrt(K))/(2.*a) : -b/(2.*a);
  /*if (K > 0) {
    //return -b > sqrt(K) ?  (-b - sqrt(K))/(2.*a) : (-b + sqrt(K))/(2.*a);
    double z = (-b - sqrt(K))/(2.*a);
    if(z>0.) return z;
    z = (-b + sqrt(K))/(2.*a);
    if(z>0.) return z;
    return max(0.,-b/(2.*a));
  }
  else return max(0.,-b/(2.*a));*/
}
void ABCD_Tools::MakeAlternativeEstimates(vector<vector<double> >& estimates, const vector<vector<double> >& bincontents16_data, const vector<vector<double> >& bincontents16_MC){
  const int nbins=bincontents16_data[0].size();
  int i;
  vector<double> smallvec_data(4), smallvec_MC(4);
  
  for(i=0;i<nbins;i++){
    smallvec_data[0]=bincontents16_data[m_iRegionE][i];
    smallvec_data[1]=bincontents16_data[m_iRegionF][i];
    smallvec_data[2]=bincontents16_data[m_iRegionJ][i];
    smallvec_data[3]=bincontents16_data[m_iRegionK][i];
    smallvec_MC[0]=bincontents16_MC[m_iRegionE][i];
    smallvec_MC[1]=bincontents16_MC[m_iRegionF][i];
    smallvec_MC[2]=bincontents16_MC[m_iRegionJ][i];
    smallvec_MC[3]=bincontents16_MC[m_iRegionK][i];
    
    estimates[0][i]=MakeOneAlternativeEstimate(smallvec_data,smallvec_MC);
    
    smallvec_data[0]=bincontents16_data[m_iRegionB][i];
    smallvec_data[1]=bincontents16_data[m_iRegionH][i];
    smallvec_data[2]=bincontents16_data[m_iRegionJ][i];
    smallvec_data[3]=bincontents16_data[m_iRegionL][i];
    smallvec_MC[0]=bincontents16_MC[m_iRegionB][i];
    smallvec_MC[1]=bincontents16_MC[m_iRegionH][i];
    smallvec_MC[2]=bincontents16_MC[m_iRegionJ][i];
    smallvec_MC[3]=bincontents16_MC[m_iRegionL][i];
    
    estimates[1][i]=MakeOneAlternativeEstimate(smallvec_data,smallvec_MC);
    
    smallvec_data[0]=bincontents16_data[m_iRegionC][i];
    smallvec_data[1]=bincontents16_data[m_iRegionO][i];
    smallvec_data[2]=bincontents16_data[m_iRegionF][i];
    smallvec_data[3]=bincontents16_data[m_iRegionM][i];
    smallvec_MC[0]=bincontents16_MC[m_iRegionC][i];
    smallvec_MC[1]=bincontents16_MC[m_iRegionO][i];
    smallvec_MC[2]=bincontents16_MC[m_iRegionF][i];
    smallvec_MC[3]=bincontents16_MC[m_iRegionM][i];
    
    estimates[2][i]=MakeOneAlternativeEstimate(smallvec_data,smallvec_MC);
    
    smallvec_data[0]=bincontents16_data[m_iRegionI][i];
    smallvec_data[1]=bincontents16_data[m_iRegionO][i];
    smallvec_data[2]=bincontents16_data[m_iRegionH][i];
    smallvec_data[3]=bincontents16_data[m_iRegionN][i];
    smallvec_MC[0]=bincontents16_MC[m_iRegionI][i];
    smallvec_MC[1]=bincontents16_MC[m_iRegionO][i];
    smallvec_MC[2]=bincontents16_MC[m_iRegionH][i];
    smallvec_MC[3]=bincontents16_MC[m_iRegionN][i];
    
    estimates[3][i]=MakeOneAlternativeEstimate(smallvec_data,smallvec_MC);
    
    estimates[4][i]=bincontents16_data[m_iRegionK][i] - (bincontents16_data[m_iRegionF][i] - bincontents16_MC[m_iRegionF][i])*(bincontents16_data[m_iRegionJ][i] - bincontents16_MC[m_iRegionJ][i])/(bincontents16_data[m_iRegionE][i] - bincontents16_MC[m_iRegionE][i]);
    estimates[5][i]=bincontents16_data[m_iRegionL][i] - (bincontents16_data[m_iRegionH][i] - bincontents16_MC[m_iRegionH][i])*(bincontents16_data[m_iRegionJ][i] - bincontents16_MC[m_iRegionJ][i])/(bincontents16_data[m_iRegionB][i] - bincontents16_MC[m_iRegionB][i]);
    estimates[6][i]=bincontents16_data[m_iRegionM][i] - (bincontents16_data[m_iRegionO][i] - bincontents16_MC[m_iRegionO][i])*(bincontents16_data[m_iRegionF][i] - bincontents16_MC[m_iRegionF][i])/(bincontents16_data[m_iRegionC][i] - bincontents16_MC[m_iRegionC][i]);
    estimates[7][i]=bincontents16_data[m_iRegionN][i] - (bincontents16_data[m_iRegionO][i] - bincontents16_MC[m_iRegionO][i])*(bincontents16_data[m_iRegionH][i] - bincontents16_MC[m_iRegionH][i])/(bincontents16_data[m_iRegionI][i] - bincontents16_MC[m_iRegionI][i]);
    
  }
	
}

void ABCD_Tools::TestEffectOfCorrelations(const TString& histname){
  TestEffectOfCorrelations(histname,m_bincontents16_data,m_bincontents16_MC,m_binerrors16_MC,m_niter_uncertainties);
	
}
void ABCD_Tools::TestEffectOfCorrelations(const TString& histname,vector<vector<double> >& bincontents16_data, vector<vector<double> >& bincontents16_MC,vector<vector<double> >& binerrors16_MC, const unsigned int& niter){
  //const int size9 = 9;
  const int size16 = bincontents16_data.size();
  const int nbins= bincontents16_data[0].size();
  const int ntesting_estimates=22;
  int i=0,j=0;
  m_testing_estimates.resize(ntesting_estimates);
  vector<double> global_estimates(ntesting_estimates),global_means(ntesting_estimates),global_errors(ntesting_estimates);
  vector<vector<double> > bincontents16(size16),bincontents_smeared16(size16),means(ntesting_estimates),errors(ntesting_estimates);
  vector<double> totalnumbers16(size16),totalnumbers_smeared16(size16);
  vector<TString> names(ntesting_estimates);
  names[0] = "DB/AE";
  names[1] = "GB/AH";
  names[2] = "GB/AH v2";
  names[3] = "GB/AH v3";
  names[4] = "DC/AF";
  names[5] = "DC/AF v2";
  names[6] = "DC/AF v3";
  names[7] = "GC/AS";
  names[8] = "GC/AS_v2";
  names[9] = "BC/DA";
  names[10] = "EI/GA";
  names[11] = "EC/FA";
  names[12] = "BI/HA";
  names[13] = "DI/HC";
  names[14] = "BG/HE";
  names[15] = "CG/FI";
  names[16] = "ED/FB";
  names[17] = "CI/OA";
  names[18] = "BE/JA";
  names[19] = "ABCD16 S1";
  names[20] = "ABCD16 S2";
  names[21] = "ABCD16";
  for(i=0;i<ntesting_estimates;i++){
    m_testing_estimates[i].resize(nbins);
    means[i].resize(nbins);
    errors[i].resize(nbins);
  }
  for(i=0;i<size16;i++){
    bincontents16[i].resize(nbins);
    bincontents_smeared16[i].resize(nbins);
    //cout << nbins << " " << bincontents16_data[i].size() << " " << bincontents16_MC[i].size() << endl;
    for(j=0;j<nbins;j++){
	    bincontents16[i][j]=bincontents16_data[i][j]-bincontents16_MC[i][j];
    }
    totalnumbers16[i]=std::accumulate(bincontents16[i].begin(), bincontents16[i].end(), 0.0);	
  }
  
  
  for(unsigned int iiter=0;iiter<niter;iiter++){
    if(m_tfiles.size()>1)smear_bin_contents(bincontents16_data,bincontents16_MC,binerrors16_MC,bincontents_smeared16);
    else smear_bin_contents(bincontents16,m_binerrors16,bincontents_smeared16);
    for(i=0;i<size16;i++) totalnumbers_smeared16[i]= std::accumulate(bincontents_smeared16[i].begin(), bincontents_smeared16[i].end(), 0.0);
    MakeTestingEstimates(m_testing_estimates, global_estimates,bincontents_smeared16,totalnumbers_smeared16);
    for(j=0;j<ntesting_estimates;j++){
      for(i=0;i<nbins;i++){
	means[j][i]+=m_testing_estimates[j][i];
	errors[j][i]+=(m_testing_estimates[j][i]*m_testing_estimates[j][i]);
      }
      global_means[j]+=global_estimates[j];
      global_errors[j]+=global_estimates[j]*global_estimates[j];
    }
  }
  for(j=0;j<ntesting_estimates;j++){
    for(i=0;i<nbins;i++){
      means[j][i]/=niter;
      errors[j][i]/=niter;
      errors[j][i]=sqrt(errors[j][i] - (means[j][i]*means[j][i]));
    }
    global_means[j]/=niter;
    global_errors[j]/=niter;
    global_errors[j]=sqrt(global_errors[j] - (global_means[j]*global_means[j]));
  }
  
  MakeTestingEstimates(m_testing_estimates,global_estimates,bincontents16,totalnumbers16);
  vector<TH1D*> histos(ntesting_estimates);
  TLegend* leg = new TLegend(0.615,0.7,0.92,0.92);
  for(j=0;j<ntesting_estimates;j++){
    //for(i=0;i<nbins;i++) cout << j << " " << m_testing_estimates[j][i] << " +- " << errors[j][i] << endl;
    histos[j] = HistFromVector(m_testing_estimates[j],errors[j],m_h_template);
    SetXaxisTitle(histos[j],histname);
    histos[j]->GetYaxis()->SetTitle("Predicted/Expected");
    //cout << histname << " " << histos[j]->GetXaxis()->GetTitle() << endl;
    histos[j]->SetLineColor(j+1);
    histos[j]->SetMarkerColor(j+1);
  }
  leg->AddEntry(histos[0],names[0]);
  leg->AddEntry(histos[1],names[1]);
  leg->AddEntry(histos[2],names[2]);
  leg->AddEntry(histos[3],names[3]);
  
  TString dirname = m_foldername_tests + "/Test_of_effects_of_correlations/";
  gSystem->mkdir(dirname,true);
  TString y2name="";
  double ymin=0.6,ymax=1.3;
  vector<TH1D*> histos1(histos.begin(),histos.begin()+4);
  vector<TH1D*> histos2(histos.begin()+4,histos.begin()+7);
  plotHistograms(histos1,leg,dirname,histname,ymin, ymax, m_lumi_string);
  TLegend* leg2 = new TLegend(0.615,0.7,0.92,0.92);
  leg2->AddEntry(histos[4],"GB/AH");
  leg2->AddEntry(histos[5],"DC/AF");
  leg2->AddEntry(histos[6],"GC/AS");
  plotHistograms(histos2,leg2,dirname,histname + "_v2",0.2, 1.6, m_lumi_string);
  ofstream textfile((dirname + histname + ".txt").Data());
  textfile << setprecision(4);
  //double stats[4];
  for(i=0;i<ntesting_estimates;i++){
    //histos[i]->GetStats(stats);
    //textfile << stats[0]/nbins << " $\\pm$ " << sqrt(stats[1]/(nbins*(nbins-1))) << endl;
    textfile << names[i] << " " << global_estimates[i] << " $\\pm$ " << global_errors[i] << endl;
  }
textfile.close();
	
}
void ABCD_Tools::MakeTestingEstimates(vector<vector<double> >& estimates, vector<double>& global_estimates, const vector<vector<double> >& bincontents, const vector<double>& totalnumbers){
  //const int size16 = bincontents.size();
  const int nbins= bincontents[0].size();
  int i=0;
  double x,y,z,w;
  for(i=0;i<nbins;i++){
    estimates[0][i] = (bincontents[m_iRegionB][i]*bincontents[m_iRegionE][i] + bincontents[m_iRegionC][i]*bincontents[m_iRegionI][i] + bincontents[m_iRegionB][i]*bincontents[m_iRegionC][i] + bincontents[m_iRegionE][i]*bincontents[m_iRegionI][i])/
				    (bincontents[m_iRegionA][i]*(bincontents[m_iRegionO][i]+bincontents[m_iRegionJ][i]+bincontents[m_iRegionD][i]+bincontents[m_iRegionG][i]));
    x = bincontents[m_iRegionF][i]*(bincontents[m_iRegionB][i] + bincontents[m_iRegionI][i])/bincontents[m_iRegionA][i];
    y= (bincontents[m_iRegionD][i]*bincontents[m_iRegionA][i])/(bincontents[m_iRegionB][i]*bincontents[m_iRegionC][i]);
    z= (bincontents[m_iRegionA][i]*bincontents[m_iRegionG][i])/(bincontents[m_iRegionE][i]*bincontents[m_iRegionI][i]);
    w = bincontents[m_iRegionH][i]*(bincontents[m_iRegionE][i] + bincontents[m_iRegionC][i])/bincontents[m_iRegionA][i];
    estimates[4][i] = x/(bincontents[m_iRegionK][i] + bincontents[m_iRegionM][i]);
    estimates[1][i] = x/(y*(bincontents[m_iRegionJ][i]*bincontents[m_iRegionF][i])/bincontents[m_iRegionE][i] + z*(bincontents[m_iRegionO][i]*bincontents[m_iRegionF][i])/bincontents[m_iRegionC][i]);
    estimates[2][i] = w/(z*(bincontents[m_iRegionJ][i]*bincontents[m_iRegionH][i])/bincontents[m_iRegionB][i] + y*(bincontents[m_iRegionH][i]*bincontents[m_iRegionO][i])/bincontents[m_iRegionI][i]);
    estimates[3][i] = (bincontents[m_iRegionC][i]*bincontents[m_iRegionI][i])/(bincontents[m_iRegionA][i]*bincontents[m_iRegionO][i])*
				    (bincontents[m_iRegionB][i]*bincontents[m_iRegionE][i])/(bincontents[m_iRegionJ][i]*bincontents[m_iRegionA][i])*
				    (bincontents[m_iRegionB][i]*bincontents[m_iRegionC][i])/(bincontents[m_iRegionD][i]*bincontents[m_iRegionA][i])*
				    (bincontents[m_iRegionE][i]*bincontents[m_iRegionI][i])/(bincontents[m_iRegionA][i]*bincontents[m_iRegionG][i]);
    estimates[5][i] = w/(bincontents[m_iRegionL][i] + bincontents[m_iRegionN][i]);
    estimates[6][i] = (bincontents[m_iRegionF][i]*bincontents[m_iRegionH][i])/(bincontents[m_iRegionA][i]*bincontents[m_iRegionP][i]);
  }
  
  const double k_t1b1= (totalnumbers[m_iRegionC]*totalnumbers[m_iRegionI])/(totalnumbers[m_iRegionA]*totalnumbers[m_iRegionO]);
  const double k_t2b2=(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionB])/(totalnumbers[m_iRegionA]*totalnumbers[m_iRegionJ]);
  const double k_t1b2=(totalnumbers[m_iRegionB]*totalnumbers[m_iRegionC])/(totalnumbers[m_iRegionD]*totalnumbers[m_iRegionA]);
  const double k_t2b1=(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionI])/(totalnumbers[m_iRegionG]*totalnumbers[m_iRegionA]);
  const double k_t1t2=(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionC])/(totalnumbers[m_iRegionF]*totalnumbers[m_iRegionA]);
  const double k_b1b2=(totalnumbers[m_iRegionB]*totalnumbers[m_iRegionI])/(totalnumbers[m_iRegionH]*totalnumbers[m_iRegionA]);
  global_estimates[0] = (totalnumbers[m_iRegionB]*totalnumbers[m_iRegionE] + totalnumbers[m_iRegionC]*totalnumbers[m_iRegionI] + totalnumbers[m_iRegionB]*totalnumbers[m_iRegionC] + totalnumbers[m_iRegionE]*totalnumbers[m_iRegionI])/
				    (totalnumbers[m_iRegionA]*(totalnumbers[m_iRegionO]+totalnumbers[m_iRegionJ]+totalnumbers[m_iRegionD]+totalnumbers[m_iRegionG]));
  x = totalnumbers[m_iRegionF]*(totalnumbers[m_iRegionB] + totalnumbers[m_iRegionI])/totalnumbers[m_iRegionA];
  y= (totalnumbers[m_iRegionD]*totalnumbers[m_iRegionA])/(totalnumbers[m_iRegionB]*totalnumbers[m_iRegionC]);
  z= (totalnumbers[m_iRegionA]*totalnumbers[m_iRegionG])/(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionI]);
  w = totalnumbers[m_iRegionH]*(totalnumbers[m_iRegionE] + totalnumbers[m_iRegionC])/totalnumbers[m_iRegionA];
  
  global_estimates[1] = x/(totalnumbers[m_iRegionK] + totalnumbers[m_iRegionM]);
  global_estimates[2] = x/(y*(totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionF])/totalnumbers[m_iRegionE] + z*(totalnumbers[m_iRegionO]*totalnumbers[m_iRegionF])/totalnumbers[m_iRegionC]);
  global_estimates[3] = ( k_t2b2*k_t1b2*totalnumbers[m_iRegionK] + k_t1b1*k_t2b1*totalnumbers[m_iRegionM])/(totalnumbers[m_iRegionK] + totalnumbers[m_iRegionM]);
  
  global_estimates[4] = w/(totalnumbers[m_iRegionL] + totalnumbers[m_iRegionN]);
  global_estimates[5] = w/(z*(totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionH])/totalnumbers[m_iRegionB] + y*(totalnumbers[m_iRegionH]*totalnumbers[m_iRegionO])/totalnumbers[m_iRegionI]);
  global_estimates[6] = ( k_t2b2*k_t2b1*totalnumbers[m_iRegionL] + k_t1b1*k_t1b2*totalnumbers[m_iRegionN])/(totalnumbers[m_iRegionL] + totalnumbers[m_iRegionN]);
  
  global_estimates[7] = (totalnumbers[m_iRegionF]*totalnumbers[m_iRegionH])/(totalnumbers[m_iRegionA]*totalnumbers[m_iRegionP]);
  global_estimates[8] = (totalnumbers[m_iRegionC]*totalnumbers[m_iRegionI])/(totalnumbers[m_iRegionA]*totalnumbers[m_iRegionO])*
			    (totalnumbers[m_iRegionB]*totalnumbers[m_iRegionE])/(totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionA])*
			    (totalnumbers[m_iRegionB]*totalnumbers[m_iRegionC])/(totalnumbers[m_iRegionD]*totalnumbers[m_iRegionA])*
			    (totalnumbers[m_iRegionE]*totalnumbers[m_iRegionI])/(totalnumbers[m_iRegionA]*totalnumbers[m_iRegionG]);
  
  global_estimates[9] = k_t1b2;
  global_estimates[10] = k_t2b1;
  global_estimates[11] = k_t1t2;
  global_estimates[12] = k_b1b2;
  global_estimates[13] = (totalnumbers[m_iRegionD]*totalnumbers[m_iRegionI])/(totalnumbers[m_iRegionH]*totalnumbers[m_iRegionC]);
  global_estimates[14] = (totalnumbers[m_iRegionB]*totalnumbers[m_iRegionG])/(totalnumbers[m_iRegionH]*totalnumbers[m_iRegionE]);
  global_estimates[15] = (totalnumbers[m_iRegionC]*totalnumbers[m_iRegionG])/(totalnumbers[m_iRegionF]*totalnumbers[m_iRegionI]);
  global_estimates[16] = (totalnumbers[m_iRegionE]*totalnumbers[m_iRegionD])/(totalnumbers[m_iRegionF]*totalnumbers[m_iRegionB]);
  global_estimates[17] = k_t1b1;
  global_estimates[18] = k_t2b2;
  global_estimates[19] = (totalnumbers[m_iRegionF]*totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionO]*totalnumbers[m_iRegionH])/(totalnumbers[m_iRegionE]*totalnumbers[m_iRegionI]*totalnumbers[m_iRegionD]);
  global_estimates[20] =(totalnumbers[m_iRegionF]*totalnumbers[m_iRegionJ]*totalnumbers[m_iRegionO]*totalnumbers[m_iRegionH])/(totalnumbers[m_iRegionB]*totalnumbers[m_iRegionC]*totalnumbers[m_iRegionG]);
  global_estimates[21] =(global_estimates[19]+global_estimates[20])/2;
}


void ABCD_Tools::Calculate_Errors(const vector<vector<double> >& bincontents16, const vector<vector<double> >& binerrors16, const unsigned int& niter){

  //int size9 = bincontents9.size();
  const int size9 = 9;
  const int size16 = bincontents16.size();
  const int nbins= bincontents16[0].size();
  const int number_of_ABCD9_methods=m_ABCD9_Results.size();
  const int number_of_ABCD16_methods=m_ABCD16_Results.size();
  int i=0,j=0;
  vector<vector<double> > estimates9(number_of_ABCD9_methods),means9(number_of_ABCD9_methods),errors9(number_of_ABCD9_methods);
  vector<vector<double> > estimates16(number_of_ABCD16_methods),means16(number_of_ABCD16_methods),errors16(number_of_ABCD16_methods);
  for(i=0;i<number_of_ABCD9_methods;i++){
	  estimates9[i].resize(nbins);
	  means9[i].resize(nbins);
	  errors9[i].resize(nbins);
  }
  for(i=0;i<number_of_ABCD16_methods;i++){
	  estimates16[i].resize(nbins);
	  means16[i].resize(nbins);
	  errors16[i].resize(nbins);
  }
 
  vector<vector<double> > bincontents_smeared9(size9),bincontents_smeared16(size16);
  vector<double> totalnumbers_smeared9(size9),totalnumbers_smeared16(size16);
  for(i=0;i<size9;i++) bincontents_smeared9[i].resize(nbins);
  for(i=0;i<size16;i++) bincontents_smeared16[i].resize(nbins);
  for(unsigned int iiter=0;iiter<niter;iiter++){
      //if(iiter%100 ==0) cout << iiter << "/" << niter << endl;
	smear_bin_contents(bincontents16,binerrors16,bincontents_smeared16);
	ConvertVector16to9(bincontents_smeared16,bincontents_smeared9);
	
	for(i=0;i<size9;i++) totalnumbers_smeared9[i]= std::accumulate(bincontents_smeared9[i].begin(), bincontents_smeared9[i].end(), 0.0);
	for(i=0;i<size16;i++) totalnumbers_smeared16[i]= std::accumulate(bincontents_smeared16[i].begin(), bincontents_smeared16[i].end(), 0.0);
	  MakeAllABCD9Estimates(estimates9,bincontents_smeared9,totalnumbers_smeared9,bincontents_smeared16,totalnumbers_smeared16);
	  MakeAllABCD16Estimates(estimates16,bincontents_smeared16,totalnumbers_smeared16);
	  for(j=0;j<number_of_ABCD9_methods;j++)for(i=0;i<nbins;i++){
		  means9[j][i]+=estimates9[j][i];
		  errors9[j][i]+=(estimates9[j][i]*estimates9[j][i]);
	  }
	  for(j=0;j<number_of_ABCD16_methods;j++)for(i=0;i<nbins;i++){
		  means16[j][i]+=estimates16[j][i];
		  errors16[j][i]+=(estimates16[j][i]*estimates16[j][i]);
	  }
  }
  for(j=0;j<number_of_ABCD9_methods;j++)for(i=0;i<nbins;i++){
	  means9[j][i]/=niter;
	  errors9[j][i]/=niter;
	  errors9[j][i]=sqrt(errors9[j][i] - (means9[j][i]*means9[j][i]));
	  m_ABCD9_Results[j]->SetBinError(i+1,errors9[j][i]);
  } 
  for(j=0;j<number_of_ABCD16_methods;j++)for(i=0;i<nbins;i++){
	  means16[j][i]/=niter;
	  errors16[j][i]/=niter;
	  errors16[j][i]=sqrt(errors16[j][i] - (means16[j][i]*means16[j][i]));
	  m_ABCD16_Results[j]->SetBinError(i+1,errors16[j][i]);
  } 
}



void ABCD_Tools::GetEstimatedNumberOfEvents_ABCD9(vector<vector<double> >& bincontents16_data,vector<vector<double> >& bincontents16_MC, vector<vector<double> >& binerrors16_MC, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means, vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter){
	const int size9=9;
	const int size16 = bincontents16_data.size();	
  const int nbins= bincontents16_data[0].size();
  const int ntfiles=m_tfiles.size();
 int i=0,j=0;
 
  const int number_of_ABCD9_methods=m_ABCD9_Results.size();
  vector<vector<double> > estimates(number_of_ABCD9_methods),means(number_of_ABCD9_methods),errors(number_of_ABCD9_methods);
  vector<double> total(number_of_ABCD9_methods),mean(number_of_ABCD9_methods),error(number_of_ABCD9_methods);
  for(i=0;i<number_of_ABCD9_methods;i++){
	  estimates[i].resize(nbins);
	  means[i].resize(nbins);
	  errors[i].resize(nbins);
  }
 
  vector<vector<double> > bincontents_smeared16(size16),bincontents_smeared9(size9);
  vector<double> totalnumbers_smeared16(size16),totalnumbers_smeared9(size9);
  
  for(i=0;i<size16;i++)bincontents_smeared16[i].resize(nbins);
  for(i=0;i<size9;i++)bincontents_smeared9[i].resize(nbins);
  for(unsigned int iiter=0;iiter<=niter;iiter++){
      //if(iiter%100 ==0) cout << iiter << "/" << niter << endl;
	if(iiter<niter)if(ntfiles>1)smear_bin_contents(bincontents16_data,bincontents16_MC,binerrors16_MC,bincontents_smeared16);
					else smear_bin_contents(m_bincontents16,m_binerrors16,bincontents_smeared16);
	else for(i=0;i<size16;i++)for(j=0;j<nbins;j++)bincontents_smeared16[i][j] = bincontents16_data[i][j] - bincontents16_MC[i][j];
	ConvertVector16to9(bincontents_smeared16,bincontents_smeared9);
	
	for(i=0;i<size16;i++) totalnumbers_smeared16[i]= std::accumulate(bincontents_smeared16[i].begin(), bincontents_smeared16[i].end(), 0.0);
	for(i=0;i<size9;i++) totalnumbers_smeared9[i]= std::accumulate(bincontents_smeared9[i].begin(), bincontents_smeared9[i].end(), 0.0);
	  
	  MakeAllABCD9Estimates(estimates,bincontents_smeared9,totalnumbers_smeared9,bincontents_smeared16,totalnumbers_smeared16);
	  
	  for(j=0;j<number_of_ABCD9_methods;j++){
		  total[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	  }
	 
	 if(iiter<niter) for(j=0;j<number_of_ABCD9_methods;j++){
		 mean[j]+=total[j];
		 error[j]+=(total[j]*total[j]);
		 for(i=0;i<nbins;i++){
			means[j][i]+=estimates[j][i];
			errors[j][i]+=(estimates[j][i]*estimates[j][i]);
		 }
	 }
		
  }
  for(j=0;j<number_of_ABCD9_methods;j++){
	mean[j]/=niter;
	error[j]/=niter;
	total_errors[j]=sqrt(error[j]-(mean[j]*mean[j]));
	total_estimates[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	for(i=0;i<nbins;i++){
		means[j][i]/=niter;
		errors[j][i]/=niter;
		local_errors[j][i]=sqrt(errors[j][i] - (means[j][i]*means[j][i]));
		local_estimates[j][i]=estimates[j][i];
		
		
	}
  }
  total_means=mean;
  local_means=means;
	
}


void ABCD_Tools::GetEstimatedNumberOfEvents_ABCD9(vector<vector<double> >& bincontents16, vector<vector<double> >& binerrors16, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means, vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter){
	
	const int size9=9;
	const int size16 = bincontents16.size();	
  int nbins= bincontents16[0].size();
 int i=0,j=0;
 
  const int number_of_ABCD9_methods=m_ABCD9_Results.size();
  vector<vector<double> > estimates(number_of_ABCD9_methods),means(number_of_ABCD9_methods),errors(number_of_ABCD9_methods);
  vector<double> total(number_of_ABCD9_methods),mean(number_of_ABCD9_methods),error(number_of_ABCD9_methods);
  for(i=0;i<number_of_ABCD9_methods;i++){
	  estimates[i].resize(nbins);
	  means[i].resize(nbins);
	  errors[i].resize(nbins);
  }
 
  vector<vector<double> > bincontents_smeared16(size16),bincontents_smeared9(size9);
  vector<double> totalnumbers_smeared16(size16),totalnumbers_smeared9(size9);
  
  for(i=0;i<size16;i++)bincontents_smeared16[i].resize(nbins);
  for(i=0;i<size9;i++)bincontents_smeared9[i].resize(nbins);
  for(unsigned int iiter=0;iiter<=niter;iiter++){
      //if(iiter%100 ==0) cout << iiter << "/" << niter << endl;
	if(iiter<niter)smear_bin_contents(bincontents16,binerrors16,bincontents_smeared16);
	else bincontents_smeared16 = bincontents16;
	ConvertVector16to9(bincontents_smeared16,bincontents_smeared9);
	
	for(i=0;i<size16;i++) totalnumbers_smeared16[i]= std::accumulate(bincontents_smeared16[i].begin(), bincontents_smeared16[i].end(), 0.0);
	for(i=0;i<size9;i++) totalnumbers_smeared9[i]= std::accumulate(bincontents_smeared9[i].begin(), bincontents_smeared9[i].end(), 0.0);
	  
	  MakeAllABCD9Estimates(estimates,bincontents_smeared9,totalnumbers_smeared9,bincontents_smeared16,totalnumbers_smeared16);
	  
	  for(j=0;j<number_of_ABCD9_methods;j++){
		  total[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	  }
	 
	 if(iiter<niter) for(j=0;j<number_of_ABCD9_methods;j++){
		 mean[j]+=total[j];
		 error[j]+=(total[j]*total[j]);
		 for(i=0;i<nbins;i++){
			means[j][i]+=estimates[j][i];
			errors[j][i]+=(estimates[j][i]*estimates[j][i]);
		 }
	 }
		
  }
  for(j=0;j<number_of_ABCD9_methods;j++){
	mean[j]/=niter;
	error[j]/=niter;
	total_errors[j]=sqrt(error[j]-(mean[j]*mean[j]));
	total_estimates[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	for(i=0;i<nbins;i++){
		means[j][i]/=niter;
		errors[j][i]/=niter;
		local_errors[j][i]=sqrt(errors[j][i] - (means[j][i]*means[j][i]));
		local_estimates[j][i]=estimates[j][i];
		
		
	}
  }
  total_means=mean;
  local_means=means;
}

void ABCD_Tools::GetEstimatedNumberOfEvents_ABCD16(vector<vector<double> >& bincontents_data,vector<vector<double> >& bincontents_MC, vector<vector<double> >& binerrors_MC, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means,vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter){
	const int size = bincontents_data.size();
  const int nbins= bincontents_data[0].size();
  const int ntfiles = m_tfiles.size();
 int i,j;
 
  int number_of_ABCD16_methods=m_ABCD16_Results.size();
  vector<vector<double> > estimates(number_of_ABCD16_methods),means(number_of_ABCD16_methods),errors(number_of_ABCD16_methods);
  vector<double> total(number_of_ABCD16_methods),mean(number_of_ABCD16_methods),error(number_of_ABCD16_methods);
  for(int i=0;i<number_of_ABCD16_methods;i++){
	  estimates[i].resize(nbins);
	  means[i].resize(nbins);
	  errors[i].resize(nbins);
  }
 
  vector<vector<double> > bincontents_smeared(size);
  vector<double> totalnumbers_smeared(size);
  
  for(int i=0;i<size;i++){
         bincontents_smeared[i].resize(nbins);
  }
  for(unsigned int iiter=0;iiter<=niter;iiter++){
      //if(iiter%100 ==0) cout << iiter << "/" << niter << endl;
	if(iiter<niter)if(ntfiles>1)smear_bin_contents(bincontents_data,bincontents_MC,binerrors_MC,bincontents_smeared);
					else smear_bin_contents(m_bincontents16,m_binerrors16,bincontents_smeared);
	else for(i=0;i<size;i++)for(j=0;j<nbins;j++)bincontents_smeared[i][j] = bincontents_data[i][j] - bincontents_MC[i][j];
	for(i=0;i<size;i++){
		totalnumbers_smeared[i]= std::accumulate(bincontents_smeared[i].begin(), bincontents_smeared[i].end(), 0.0);
	}
	  MakeAllABCD16Estimates(estimates,bincontents_smeared,totalnumbers_smeared);
	  for(int j=0;j<number_of_ABCD16_methods;j++){
		  total[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	  }
	 
	 if(iiter<niter) for(j=0;j<number_of_ABCD16_methods;j++){
		 mean[j]+=total[j];
		 error[j]+=(total[j]*total[j]);
		 for(i=0;i<nbins;i++){
			means[j][i]+=estimates[j][i];
			errors[j][i]+=(estimates[j][i]*estimates[j][i]);
			
		 }
	 }
		
  }
  for(j=0;j<number_of_ABCD16_methods;j++){
	mean[j]/=niter;
	error[j]/=niter;
	total_errors[j]=sqrt(error[j]-(mean[j]*mean[j]));
	total_estimates[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	for(i=0;i<nbins;i++){
		means[j][i]/=niter;
		errors[j][i]/=niter;
		local_errors[j][i]=sqrt(errors[j][i] - (means[j][i]*means[j][i]));
		local_estimates[j][i]=estimates[j][i];
	}
  }
  total_means=mean;
  local_means=means;
}


void ABCD_Tools::GetEstimatedNumberOfEvents_ABCD16(vector<vector<double> >& bincontents, vector<vector<double> >& binerrors, vector<double>& total_estimates, vector<double>& total_errors, vector<double>& total_means, vector<vector<double> >& local_estimates, vector<vector<double> >& local_errors, vector<vector<double> >& local_means, unsigned int niter){
	
	const int size = bincontents.size();
  const int nbins= bincontents[0].size();
 
  int number_of_ABCD16_methods=m_ABCD16_Results.size();
  vector<vector<double> > estimates(number_of_ABCD16_methods),means(number_of_ABCD16_methods),errors(number_of_ABCD16_methods);
  vector<double> total(number_of_ABCD16_methods),mean(number_of_ABCD16_methods),error(number_of_ABCD16_methods);
  for(int i=0;i<number_of_ABCD16_methods;i++){
	  estimates[i].resize(nbins);
	  means[i].resize(nbins);
	  errors[i].resize(nbins);
  }
 
  vector<vector<double> > bincontents_smeared(size);
  vector<double> totalnumbers_smeared(size);
  
  for(int i=0;i<size;i++){
         bincontents_smeared[i].resize(nbins);
  }
  for(unsigned int iiter=0;iiter<=niter;iiter++){
      //if(iiter%100 ==0) cout << iiter << "/" << niter << endl;
	if(iiter<niter)smear_bin_contents(bincontents,binerrors,bincontents_smeared);
	else bincontents_smeared = bincontents;
	for(int i=0;i<size;i++){
		totalnumbers_smeared[i]= std::accumulate(bincontents_smeared[i].begin(), bincontents_smeared[i].end(), 0.0);
	}
	  MakeAllABCD16Estimates(estimates,bincontents_smeared,totalnumbers_smeared);
	  for(int j=0;j<number_of_ABCD16_methods;j++){
		  total[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	  }
	 
	 if(iiter<niter) for(int j=0;j<number_of_ABCD16_methods;j++){
		 mean[j]+=total[j];
		 error[j]+=(total[j]*total[j]);
		 for(int i=0;i<nbins;i++){
			means[j][i]+=estimates[j][i];
			errors[j][i]+=(estimates[j][i]*estimates[j][i]);
			
		 }
	 }
		
  }
  for(int j=0;j<number_of_ABCD16_methods;j++){
	mean[j]/=niter;
	error[j]/=niter;
	total_errors[j]=sqrt(error[j]-(mean[j]*mean[j]));
	total_estimates[j]=accumulate(estimates[j].begin(),estimates[j].end(),0.0);
	for(int i=0;i<nbins;i++){
		means[j][i]/=niter;
		errors[j][i]/=niter;
		local_errors[j][i]=sqrt(errors[j][i] - (means[j][i]*means[j][i]));
		local_estimates[j][i]=estimates[j][i];
	}
  }
  total_means=mean;
  local_means=means;
}




bool ABCD_Tools::CreateHistVector9(vector<TH1D*>& h_data,TFile* fdata,const TString& chain_name,const TString& level, const TString& histname){  
  vector<TH1D*> helpvec(16);
  if(!CreateHistVector16(helpvec,fdata,chain_name,level,histname))return false;
  
  
  h_data.resize(9);
  h_data[m_i0t0b]=(TH1D*)helpvec[m_iRegionA]->Clone();
  h_data[m_i0t0b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i0t0b] + "/" + histname +"_data");
  h_data[m_i0t1b]=(TH1D*)helpvec[m_iRegionI]->Clone();h_data[m_i0t1b]->Add(helpvec[m_iRegionB]);
  h_data[m_i0t1b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i0t1b] + "/" + histname +"_data");
  h_data[m_i0t2b]=(TH1D*)helpvec[m_iRegionH]->Clone();
  h_data[m_i0t2b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i0t2b] + "/" + histname +"_data");
  h_data[m_i1t0b]=(TH1D*)helpvec[m_iRegionC]->Clone();h_data[m_i0t1b]->Add(helpvec[m_iRegionE]);
  h_data[m_i1t0b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i1t0b] + "/" + histname +"_data");
  h_data[m_i1t1b]=(TH1D*)helpvec[m_iRegionJ]->Clone();h_data[m_i0t1b]->Add(helpvec[m_iRegionD]);h_data[m_i0t1b]->Add(helpvec[m_iRegionG]);h_data[m_i0t1b]->Add(helpvec[m_iRegionO]);
  h_data[m_i1t1b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i1t1b] + "/" + histname +"_data");
  h_data[m_i1t2b]=(TH1D*)helpvec[m_iRegionL]->Clone();h_data[m_i0t1b]->Add(helpvec[m_iRegionN]);
  h_data[m_i1t2b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i1t2b] + "/" + histname +"_data");
  h_data[m_i2t0b]=(TH1D*)helpvec[m_iRegionF]->Clone();
  h_data[m_i2t0b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i2t0b] + "/" + histname +"_data");
  h_data[m_i2t1b]=(TH1D*)helpvec[m_iRegionK]->Clone();h_data[m_i0t1b]->Add(helpvec[m_iRegionM]);
  h_data[m_i2t1b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i2t1b] + "/" + histname +"_data");
  h_data[m_i2t2b]=(TH1D*)helpvec[m_iRegionP]->Clone();
  h_data[m_i2t2b]->SetName(chain_name + "/" + m_ABCD9_folder_names[m_i2t2b] + "/" + histname +"_data");
  
  for(int i=0;i<15;i++)delete helpvec[i];
  helpvec.clear();
    //TString name;
    //TString path;
    //TString filename;
    //for(int i=0;i<9;i++){
		//path = chain_name + "/" + m_ABCD9_folder_names[i] + "/" + level;
		//name = path + "/" +histname;
		//filename=fdata->GetName();
		//gDirectory->cd(filename + ":" + path);
		//if(gDirectory->GetListOfKeys()->Contains(histname)){
			//h_data[i] = (TH1D*)fdata->Get(name);
			//h_data[i]->SetName(name + "data");
		//}
		//else {
			//cout << endl << "Error: Failed to load histogram " << endl << name << endl << "from file " << endl << fdata->GetName() << endl << endl;
			//return false;
		//}
	//}
  return true;
	
}
void ABCD_Tools::AddHistVector9(vector<TH1D*>& h_data,TFile* fnonallhad,const TString& chain_name,const TString& level, const TString& histname,const double& c){
    h_data[m_i0t0b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i0t0b] + "/" + level + "/" +histname),c);
    h_data[m_i0t1b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i0t1b] + "/" + level + "/" +histname),c);
    h_data[m_i0t2b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i0t2b] + "/" + level + "/" +histname),c);
    h_data[m_i1t0b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i1t0b] + "/" + level + "/" +histname),c);
    h_data[m_i1t1b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i1t1b] + "/" + level + "/" +histname),c);
    h_data[m_i1t2b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i1t2b] + "/" + level + "/" +histname),c);
    h_data[m_i2t0b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i2t0b] + "/" + level + "/" +histname),c);
    h_data[m_i2t1b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i2t1b] + "/" + level + "/" +histname),c);
    h_data[m_i2t2b]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD9_folder_names[m_i2t2b] + "/" + level + "/" +histname),c);
}
bool ABCD_Tools::CreateHistVector16(vector<TH1D*>& h_data,TFile* fdata,const TString& chain_name,const TString& level, const TString& histname){
  h_data.resize(16); 

  TString name;
  TString path;
  for(int i=0;i<16;i++){
    
    path = chain_name + "/" + m_ABCD16_folder_names[i] + "/" + level;
    name = path + "/" +histname;
    fdata->cd(path);
    if(gDirectory->GetListOfKeys()->Contains(histname)){
      h_data[i] = (TH1D*)fdata->Get(name);
      h_data[i]->SetName(name + "data");
    }
    else {
      cout << endl << "Error: Failed to load histogram " << endl << name << endl << "from file " << endl << fdata->GetName() << endl << endl;
      return false;
    }
  }
  return true;
}

void ABCD_Tools::AddHistVector16(vector<TH1D*>& h_data,TFile* fnonallhad,const TString& chain_name,const TString& level,const TString& histname,const double& c){
	h_data[m_iRegionA]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionA] + "/" + level + "/" +histname),c);
	h_data[m_iRegionB]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionB] + "/" + level + "/" +histname),c);
	h_data[m_iRegionC]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionC] + "/" + level + "/" +histname),c);
	h_data[m_iRegionD]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionD] + "/" + level + "/" +histname),c);
	h_data[m_iRegionE]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionE] + "/" + level + "/" +histname),c);
	h_data[m_iRegionF]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionF] + "/" + level + "/" +histname),c);
	h_data[m_iRegionG]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionG] + "/" + level + "/" +histname),c);
	h_data[m_iRegionH]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionH] + "/" + level + "/" +histname),c);
	h_data[m_iRegionI]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionI] + "/" + level + "/" +histname),c);
	h_data[m_iRegionJ]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionJ] + "/" + level + "/" +histname),c);
	h_data[m_iRegionK]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionK] + "/" + level + "/" +histname),c);
	h_data[m_iRegionL]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionL] + "/" + level + "/" +histname),c);
	h_data[m_iRegionM]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionM] + "/" + level + "/" +histname),c);
	h_data[m_iRegionN]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionN] + "/" + level + "/" +histname),c);
	h_data[m_iRegionO]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionO] + "/" + level + "/" +histname),c);
	h_data[m_iRegionP]->Add((TH1D*)fnonallhad->Get(chain_name + "/" + m_ABCD16_folder_names[m_iRegionP] + "/" + level + "/" +histname),c); 	
}

void ABCD_Tools::CreateVector9(const vector<TH1D*>& h_data,vector<double>& totalnumbers,vector<vector<double> >& bincontents, vector<vector<double> >& binerrors){
	int i=0;
	int iregion=0;
	const int nbins=h_data[0]->GetXaxis()->GetNbins();
	double stats[4];
	for(iregion=0;iregion<9;iregion++){
		h_data[iregion]->GetStats(stats);
		totalnumbers[iregion]=stats[0];
		bincontents[iregion].resize(nbins);
		binerrors[iregion].resize(nbins);
		for(i=0;i<nbins;i++){
			bincontents[iregion][i] = h_data[iregion]->GetBinContent(i+1);
			binerrors[iregion][i] = h_data[iregion]->GetBinError(i+1);
		}
	}
}



void ABCD_Tools::CreateVector16(const vector<TH1D*>& h_data,vector<double>& totalnumbers,vector<vector<double> >& bincontents, vector<vector<double> >& binerrors){
	int i=0;
	int iregion=0;
	const int nbins=h_data[0]->GetXaxis()->GetNbins();
	double stats[4];
	for(iregion=0;iregion<16;iregion++){
		h_data[iregion]->GetStats(stats);
		totalnumbers[iregion]=stats[0];
		bincontents[iregion].resize(nbins);
		binerrors[iregion].resize(nbins);
		for(i=0;i<nbins;i++){
			bincontents[iregion][i] = h_data[iregion]->GetBinContent(i+1);
			binerrors[iregion][i] = h_data[iregion]->GetBinError(i+1);
		}
	}	
}


void ABCD_Tools::LoadFiles(const TString& filelistname){
	ifstream file(filelistname.Data());
	m_tfiles.clear();
	m_tfiles_names.clear();
	TString name;
	
	cout << "ABCD_Tools::LoadFiles: Loading files from filelist: " << filelistname << endl;
	while(file >> name){
		cout << "Loading input file: " << m_input_mainDir + name << endl;
		m_tfiles_names.push_back(m_input_mainDir + name);
		m_tfiles.push_back(new TFile(m_input_mainDir + name));
		if(m_tfiles.back()->IsZombie()) {
		  cout << "Error: Failed to load input file: " << m_input_mainDir + name << endl;
		  exit(-1);
		}
	}
	file.close();
}

void ABCD_Tools::CreateListOfTH1DNames(vector<TString>& names,const TString& chain_name, const TString& level){
	TDirectory* current_dir = gDirectory;
	TString filename= m_tfiles[0]->GetName();
	gDirectory->cd( filename + ":" + chain_name + "/" + m_ABCD16_folder_names[0]+"/" + level +"/");
	GetListOfTH1DNames(names);
	gDirectory=current_dir;
}

vector<TString> ABCD_Tools::GetTH1DNames(){
	return m_TH1Dnames;	
}	
vector<TString> ABCD_Tools::GetTH1DNamesUnfolding(){
	return m_TH1Dnames_unf;
}	


void ABCD_Tools::Set_ABCD9_folder_names(){
	m_ABCD9_folder_names.resize(9);
	m_ABCD9_folder_names[m_i0t0b]=m_config->GetValue("folder_name_9_regions_0t0b","");
	m_ABCD9_folder_names[m_i0t1b]=m_config->GetValue("folder_name_9_regions_0t1b","");
	m_ABCD9_folder_names[m_i0t2b]=m_config->GetValue("folder_name_9_regions_0t2b","");
	m_ABCD9_folder_names[m_i1t0b]=m_config->GetValue("folder_name_9_regions_1t0b","");
	m_ABCD9_folder_names[m_i1t1b]=m_config->GetValue("folder_name_9_regions_1t1b","");
	m_ABCD9_folder_names[m_i1t2b]=m_config->GetValue("folder_name_9_regions_1t2b","");
	m_ABCD9_folder_names[m_i2t0b]=m_config->GetValue("folder_name_9_regions_2t0b","");
	m_ABCD9_folder_names[m_i2t1b]=m_config->GetValue("folder_name_9_regions_2t1b","");
	m_ABCD9_folder_names[m_i2t2b]=m_config->GetValue("folder_name_9_regions_2t2b","");
}
	


void ABCD_Tools::Set_ABCD16_folder_names(){
	m_ABCD16_folder_names.resize(16);
	m_ABCD16_folder_names[m_iRegionA]=m_config->GetValue("folder_name_16_regions_A","");
	m_ABCD16_folder_names[m_iRegionB]=m_config->GetValue("folder_name_16_regions_B","");
	m_ABCD16_folder_names[m_iRegionC]=m_config->GetValue("folder_name_16_regions_C","");
	m_ABCD16_folder_names[m_iRegionD]=m_config->GetValue("folder_name_16_regions_D","");
	m_ABCD16_folder_names[m_iRegionE]=m_config->GetValue("folder_name_16_regions_E","");
	m_ABCD16_folder_names[m_iRegionF]=m_config->GetValue("folder_name_16_regions_F","");
	m_ABCD16_folder_names[m_iRegionG]=m_config->GetValue("folder_name_16_regions_G","");
	m_ABCD16_folder_names[m_iRegionH]=m_config->GetValue("folder_name_16_regions_H","");
	m_ABCD16_folder_names[m_iRegionI]=m_config->GetValue("folder_name_16_regions_I","");
	m_ABCD16_folder_names[m_iRegionJ]=m_config->GetValue("folder_name_16_regions_J","");
	m_ABCD16_folder_names[m_iRegionK]=m_config->GetValue("folder_name_16_regions_K","");
	m_ABCD16_folder_names[m_iRegionL]=m_config->GetValue("folder_name_16_regions_L","");
	m_ABCD16_folder_names[m_iRegionM]=m_config->GetValue("folder_name_16_regions_M","");
	m_ABCD16_folder_names[m_iRegionN]=m_config->GetValue("folder_name_16_regions_N","");
	m_ABCD16_folder_names[m_iRegionO]=m_config->GetValue("folder_name_16_regions_O","");
	m_ABCD16_folder_names[m_iRegionP]=m_config->GetValue("folder_name_16_regions_P","");
	
	
}

void ABCD_Tools::Set_ABCD9_indexes(){
	m_i0t0b=m_config->GetValue("index9_0t0b",0);
	m_i0t1b=m_config->GetValue("index9_0t1b",1);
	m_i0t2b=m_config->GetValue("index9_0t2b",2);
	m_i1t0b=m_config->GetValue("index9_1t0b",3);
	m_i1t1b=m_config->GetValue("index9_1t1b",4);
	m_i1t2b=m_config->GetValue("index9_1t2b",5);
	m_i2t0b=m_config->GetValue("index9_2t0b",6);
	m_i2t1b=m_config->GetValue("index9_2t1b",7);
	m_i2t2b=m_config->GetValue("index9_2t2b",8);	
}


void ABCD_Tools::Set_ABCD16_indexes(){
	m_iRegionA=m_config->GetValue("index16_A",0);
	m_iRegionB=m_config->GetValue("index16_B",1);
	m_iRegionC=m_config->GetValue("index16_C",2);
	m_iRegionD=m_config->GetValue("index16_D",3);
	m_iRegionE=m_config->GetValue("index16_E",4);
	m_iRegionF=m_config->GetValue("index16_F",5);
	m_iRegionG=m_config->GetValue("index16_G",6);
	m_iRegionH=m_config->GetValue("index16_H",7);
	m_iRegionI=m_config->GetValue("index16_I",8);
	m_iRegionJ=m_config->GetValue("index16_J",9);
	m_iRegionK=m_config->GetValue("index16_K",10);
	m_iRegionL=m_config->GetValue("index16_L",11);
	m_iRegionM=m_config->GetValue("index16_M",12);
	m_iRegionN=m_config->GetValue("index16_N",13);
	m_iRegionO=m_config->GetValue("index16_O",14);
	m_iRegionP=m_config->GetValue("index16_P",15); 	
}
void ABCD_Tools::DrawValidationPlots_9regions(const TString& histname){
	vector<TH1D*> histos(2);
	int i=0,j=0,k=0,m=0,n=0;
	TString y2name = "(B*D) / (A*E)";
	double y2min=0.61;
	double y2max=1.39;
	TString dirname = m_foldername_tests + "/Validation_plots_9regions/";
	gSystem->mkdir(dirname,true);
	stringstream ss;
	i=0;j=1;m=0;n=1;
	for(i=0;i<2;i++)for(j=i+1;j<3;j++)for(m=0;m<2;m++)for(n=m+1;n<3;n++){
		histos[0] = (TH1D*)m_table9_hist[j][n]->Clone();
		histos[1] = (TH1D*)m_table9_hist[i][n]->Clone();
		histos[1]->Divide(m_table9_hist[i][m]);
		histos[1]->Multiply(m_table9_hist[j][m]);
		TLegend* leg = new TLegend(0.615,0.5,0.92,0.92);
		ss.str("");
		ss << m_label9[j][n];
		leg->AddEntry(histos[0],ss.str().c_str());
		ss.str("");
		ss << m_label9[i][n] << '*' << m_label9[j][m] << '/' << m_label9[i][m];
		leg->AddEntry(histos[1],ss.str().c_str());
		for(k=0;k<2;k++){
			DivideByBinWidth(histos[k]);
			SetXaxisTitle(histos[k],histname);
			histos[k]->GetYaxis()->SetTitle("N_{events} [1/GeV]");
			histos[k]->SetLineColor(k+1);
			histos[k]->SetMarkerColor(k+1);
			histos[k]->SetStats(kFALSE);
		}
		ss.str("");ss << "("<< m_label9[i][n] << "*" << m_label9[j][m] << ") / (" << m_label9[i][m] << "*" << m_label9[j][n] << ")";
		y2name=ss.str().c_str();
		ss.str("");ss << "_" << m_label9[i][m] << m_label9[i][n] << m_label9[j][m] << m_label9[j][n];
		plotHistograms(histos,leg,dirname,histname + ss.str().c_str(),histos[0], y2name, y2min, y2max, m_lumi_string);
	}
	
	
}
void ABCD_Tools::DrawValidationPlots_16regions(const TString& histname){
	vector<TH1D*> histos(2);
	int i=0,j=0,k=0,m=0,n=0;
	TString y2name = "(B*D) / (A*E)";
	double y2min=0.71;
	double y2max=1.49;
	TString dirname = m_foldername_tests + "/Validation_plots_16regions/";
	gSystem->mkdir(dirname,true);
	stringstream ss;
	i=0;j=1;m=0;n=1;
	for(i=0;i<3;i++)for(j=i+1;j<4;j++)for(m=0;m<3;m++)for(n=m+1;n<4;n++){
		histos[1] = (TH1D*)m_table16_hist[j][n]->Clone();
		histos[0] = (TH1D*)m_table16_hist[i][n]->Clone();
		histos[0]->Divide(m_table16_hist[i][m]);
		histos[0]->Multiply(m_table16_hist[j][m]);
		TLegend* leg = new TLegend(0.615,0.48,0.92,0.8);
		ss.str("");
		ss << m_label16[j][n];
		leg->AddEntry(histos[1],ss.str().c_str());
		ss.str("");
		ss << m_label16[i][n] << '*' << m_label16[j][m] << '/' << m_label16[i][m];
		leg->AddEntry(histos[0],ss.str().c_str());
		for(k=0;k<2;k++){
			DivideByBinWidth(histos[k]);
			SetXaxisTitle(histos[k],histname);
			histos[k]->GetYaxis()->SetTitle("N_{events} [1/GeV]");
			histos[k]->SetLineColor(k+1);
			histos[k]->SetMarkerColor(k+1);
			histos[k]->SetStats(kFALSE);
		}
		ss.str("");ss << "(" << m_label16[i][m] << "*" << m_label16[j][n] << ") / (" << m_label16[i][n] << "*" << m_label16[j][m] << ")";
		y2name=ss.str().c_str();
		ss.str("");ss << "_" << m_label16[i][m] << m_label16[i][n] << m_label16[j][m] << m_label16[j][n];
		plotHistograms(histos,leg,dirname,histname + ss.str().c_str(),histos[0], y2name, y2min, y2max, m_lumi_string);
	}
	TLegend* leg;
	for(i=0;i<4;i++){
		histos[1] = (TH1D*)m_table16_hist[i][3]->Clone();
		histos[0] = (TH1D*)m_table16_hist[i][2]->Clone();
		histos[0]->Divide(m_table16_hist[i][0]);
		histos[0]->Multiply(m_table16_hist[i][1]);
		leg = new TLegend(0.615,0.5,0.92,0.8);
		ss.str("");
		ss << m_label16[i][3];
		leg->AddEntry(histos[1],ss.str().c_str());
		ss.str("");
		ss << m_label16[i][2] << '*' << m_label16[i][1] << '/' << m_label16[i][0];
		leg->AddEntry(histos[0],ss.str().c_str());
		for(k=0;k<2;k++){
			DivideByBinWidth(histos[k]);
			SetXaxisTitle(histos[k],histname);
			histos[k]->GetYaxis()->SetTitle("N_{events} [1/GeV]");
			histos[k]->SetLineColor(k+1);
			histos[k]->SetMarkerColor(k+1);
			histos[k]->SetStats(kFALSE);
		}
		ss.str("");ss << "(" << m_label16[i][0] << "*" << m_label16[i][3] << ") / (" << m_label16[i][2] << "*" << m_label16[i][1] << ")";
		y2name=ss.str().c_str();
		ss.str("");ss << "_" << m_label16[i][0] << m_label16[i][1] << m_label16[i][2] << m_label16[i][3];
		plotHistograms(histos,leg,dirname,histname + ss.str().c_str(),histos[0], y2name, y2min, y2max, m_lumi_string);
		delete leg;
		
		histos[1] = (TH1D*)m_table16_hist[3][i]->Clone();
		histos[0] = (TH1D*)m_table16_hist[2][i]->Clone();
		histos[0]->Divide(m_table16_hist[0][i]);
		histos[0]->Multiply(m_table16_hist[1][i]);
		leg = new TLegend(0.615,0.5,0.92,0.8);
		ss.str("");
		ss << m_label16[3][i];
		leg->AddEntry(histos[1],ss.str().c_str());
		ss.str("");
		ss << m_label16[2][i] << '*' << m_label16[1][i] << '/' << m_label16[0][i];
		leg->AddEntry(histos[0],ss.str().c_str());
		for(k=0;k<2;k++){
			DivideByBinWidth(histos[k]);
			SetXaxisTitle(histos[k],histname);
			histos[k]->GetYaxis()->SetTitle("N_{events} [1/GeV]");
			histos[k]->SetLineColor(k+1);
			histos[k]->SetMarkerColor(k+1);
			histos[k]->SetStats(kFALSE);
		}
		ss.str("");ss << "(" << m_label16[0][i] << "*" << m_label16[3][i] << ") / (" << m_label16[2][i] << "*" << m_label16[1][i] << ")";
		y2name=ss.str().c_str();
		ss.str("");ss << "_" << m_label16[0][i] << m_label16[1][i] << m_label16[2][i] << m_label16[3][i];
		plotHistograms(histos,leg,dirname,histname + ss.str().c_str(),histos[0], y2name, y2min, y2max, m_lumi_string);
		delete leg;	
	}
}
void ABCD_Tools::DrawLeadingFatJetDistribution(const TString& histname){
	vector<TH1D*> histos(2);
	histos[0] = (TH1D*)m_h_template->Clone();
	histos[1] = (TH1D*)m_h_template->Clone();
	int i=0,j=0;
	for(i=0;i<2;i++)for(j=0;j<4;j++){
		histos[0]->Add(m_table16_hist[j][i]);
		histos[1]->Add(m_table16_hist[j][i+2]);
		//cout << m_label16[j][i] << " " << m_label16[j][i+2] << endl;
	}
	TString y2name = "1b / 0b";
	double y2min=0.61;
	double y2max=1.39;
	TString dirname = m_foldername_tests + "/Comparison_of_tagging_variables/";
	gSystem->mkdir(dirname,true);
	TLegend* leg = new TLegend(0.615,0.5,0.92,0.92);
	leg->AddEntry(histos[0],"0b");
	leg->AddEntry(histos[1],"1b");
	for(i=0;i<2;i++){
		DivideByBinWidth(histos[i]);
		histos[i]->Scale(1./histos[i]->Integral());
		SetXaxisTitle(histos[i],histname);
		histos[i]->GetYaxis()->SetTitle("Normalized distributions");
		histos[i]->SetLineColor(i+1);
		histos[i]->SetMarkerColor(i+1);
		histos[i]->SetStats(kFALSE);
	}
	plotHistograms(histos,leg,dirname,histname,histos[0], y2name, y2min, y2max, m_lumi_string);
}
void ABCD_Tools::DrawRecoilFatJetDistribution(const TString& histname){
	vector<TH1D*> histos(2);
	histos[0] = (TH1D*)m_h_template->Clone();
	histos[1] = (TH1D*)m_h_template->Clone();
	int i=0,j=0;
	for(i=0;i<2;i++)for(j=0;j<4;j++){
		histos[0]->Add(m_table16_hist[i][j]);
		histos[1]->Add(m_table16_hist[i+2][j]);
		//cout << m_label16[i][j] << " " << m_label16[i+2][j] << endl;
	}
	TString y2name = "1b / 0b";
	double y2min=0.61;
	double y2max=1.39;
	TString dirname = m_foldername_tests + "/Comparison_of_tagging_variables/";
	gSystem->mkdir(dirname,true);
	TLegend* leg = new TLegend(0.615,0.5,0.92,0.92);
	leg->AddEntry(histos[0],"0b");
	leg->AddEntry(histos[1],"1b");
	for(i=0;i<2;i++){
		DivideByBinWidth(histos[i]);
		histos[i]->Scale(1./histos[i]->Integral());
		SetXaxisTitle(histos[i],histname);
		histos[i]->GetYaxis()->SetTitle("Normalized distributions");
		histos[i]->SetLineColor(i+1);
		histos[i]->SetMarkerColor(i+1);
		histos[i]->SetStats(kFALSE);
	}
	plotHistograms(histos,leg,dirname,histname,histos[0], y2name, y2min, y2max, m_lumi_string);
}
void ABCD_Tools::DrawCorrelation1dHistos9regions(const TString& histname){
	vector<double> covs,cors,Cbs;
	TString dirname = m_foldername_tests + "/Correlation_in_bins_9regions";
	gSystem->mkdir(dirname,true);
	get_Correlation_of_Table_binbybin(m_table9_hist,cors,covs,Cbs);
	TH1D* hist_correl_factor = HistFromVector(cors,m_h_template);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	hist_correl_factor->SetStats(false);
	plot_histogram(hist_correl_factor,dirname,histname,m_lumi_string,"");
	delete hist_correl_factor;
}
void ABCD_Tools::DrawCorrelation2dHistos9regions(){
	const int size = m_table9.size();
	TString dirname;
	TH2D* h2d_toptag_vs_btag = new TH2D("","",3,0.,3.,3,0.,3.);
	//TH2D* h2d_toptag_vs_btag_errors = new TH2D("","",3,0.,3.,3,0.,3.);
	h2d_toptag_vs_btag->SetStats(kFALSE);
	h2d_toptag_vs_btag->GetXaxis()->SetNdivisions(3);
	h2d_toptag_vs_btag->GetXaxis()->SetTitle("Number of top-tags");
	h2d_toptag_vs_btag->GetYaxis()->SetNdivisions(3);
	h2d_toptag_vs_btag->GetYaxis()->SetTitle("Number of b-matches");
	h2d_toptag_vs_btag->GetXaxis()->SetBinLabel(1,"0t");
	h2d_toptag_vs_btag->GetXaxis()->SetBinLabel(2,"1t");
	h2d_toptag_vs_btag->GetXaxis()->SetBinLabel(3,"2t");
	h2d_toptag_vs_btag->GetYaxis()->SetBinLabel(1,"0b");
	h2d_toptag_vs_btag->GetYaxis()->SetBinLabel(2,"1b");
	h2d_toptag_vs_btag->GetYaxis()->SetBinLabel(3,"2b");
	h2d_toptag_vs_btag->GetZaxis()->SetTitle("Event yields");
	TH2D* h2d_toptag_vs_btag_errors=(TH2D*)h2d_toptag_vs_btag->Clone();
	//h2d_toptag_vs_btag_errors->SetStats(kFALSE);
	//h2d_toptag_vs_btag_errors->GetXaxis()->SetNdivisions(3);
	//h2d_toptag_vs_btag_errors->GetXaxis()->SetTitle("number of top-tags");
	//h2d_toptag_vs_btag_errors->GetYaxis()->SetNdivisions(3);
	//h2d_toptag_vs_btag_errors->GetYaxis()->SetTitle("number of b-matches");
	
	double stats[4];
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			m_table9_hist[i][j]->GetStats(stats);
			h2d_toptag_vs_btag->SetBinContent(i+1,j+1,stats[0]);
			h2d_toptag_vs_btag_errors->SetBinContent(i+1,j+1,sqrt(stats[1])/stats[0]);
		}
	}
	m_textfile_test_independence_ABCD9 << "Correlation factor from TH2::GetCorrelationFactor(): " << h2d_toptag_vs_btag->GetCorrelationFactor() << endl;
	
	dirname = m_foldername_tests + "/Correlation_2D_plots_ntoptags_vs_nbmatches";
	gSystem->mkdir(dirname,true);
	plotCorrelationHistograms(h2d_toptag_vs_btag,dirname,"toptag_vs_btag", m_lumi_string,"colztext");
	plotCorrelationHistograms(h2d_toptag_vs_btag_errors,dirname,"toptag_vs_btag_errors", m_lumi_string,"colztext");
	delete h2d_toptag_vs_btag;
	delete h2d_toptag_vs_btag_errors;
}
void ABCD_Tools::DrawSignalBackgroundDiscriminationPlots9regions(){
	const int size = m_tfiles.size() + 1;
	vector<TH1D*> h_toptags(size),h_bmatch(size);
	int i=0;
	double SF=1.;
	
	h_toptags[0] = Create_top_tagging_histogram(m_histvectors9[0],SF);
	h_bmatch[0] = Create_b_matching_histogram(m_histvectors9[0],SF);
	for(i=1;i<size-1;i++){
		SF = (i==1 || i==2) ? m_ttbar_SF*m_lumi : m_lumi;
		h_toptags[i] = Create_top_tagging_histogram(m_histvectors9[i],SF);
		h_bmatch[i] = Create_b_matching_histogram(m_histvectors9[i],SF);
	}
	h_toptags[4] = Create_top_tagging_histogram(m_additional_histvectors9["Pythia8_dijet"],m_lumi);
	h_bmatch[4] = Create_b_matching_histogram(m_additional_histvectors9["Pythia8_dijet"],m_lumi);
	
	for(i=0;i<size;i++){
		h_toptags[i]->SetLineColor(i+2);
		h_toptags[i]->SetMarkerColor(i+2);
		h_toptags[i]->GetXaxis()->SetTitle("Number of top tags");
		h_toptags[i]->GetYaxis()->SetTitle("N_{events}");
		h_toptags[i]->Scale(1./h_toptags[i]->Integral());
		
		h_bmatch[i]->SetLineColor(i+2);
		h_bmatch[i]->SetMarkerColor(i+2);
		h_bmatch[i]->GetXaxis()->SetTitle("Number of B matches");
		h_bmatch[i]->GetYaxis()->SetTitle("N_{events}");
		h_bmatch[i]->Scale(1./h_bmatch[i]->Integral());
		
		for(int j=1;j<h_toptags[i]->GetNbinsX()+1;++j){
			h_toptags[i]->SetBinError(j,0.);
			h_bmatch[i]->SetBinError(j,0.);
		}
	}	
	TLegend *leg_toptags = new TLegend(0.615,0.08,0.92,0.3);
	leg_toptags->AddEntry(h_toptags[0],"Data");
	leg_toptags->AddEntry(h_toptags[1],"Signal prediction");
	leg_toptags->AddEntry(h_toptags[2],"ttbar nonallhad");
	leg_toptags->AddEntry(h_toptags[3],"Wt single-top");
	//leg_toptags->AddEntry(h_toptags[4],"Multijet background");
	leg_toptags->AddEntry(h_toptags[4],"Pythia8 dijet");
	plotHistograms(h_toptags,leg_toptags,m_foldername_tests + "/","top_tagging_discrimination_power",h_toptags[0],"OTHERS / DATA",0.01,111.099,m_lumi_string,true);

	TLegend *leg_bmatch = new TLegend(0.615,0.08,0.92,0.3);
	leg_bmatch->AddEntry(h_bmatch[0],"Data");
	leg_bmatch->AddEntry(h_bmatch[1],"Signal prediction");
	leg_bmatch->AddEntry(h_bmatch[2],"ttbar nonallhad");
	leg_bmatch->AddEntry(h_bmatch[3],"Wt single-top");
	//leg_bmatch->AddEntry(h_bmatch[4],"Multijet background");
	leg_bmatch->AddEntry(h_bmatch[4],"Pythia8 dijet");
	plotHistograms(h_bmatch,leg_bmatch,m_foldername_tests + "/","B_matching_discrimination_power",h_bmatch[0],"OTHERS / DATA",0.01,111.099,m_lumi_string,true);
	for(i=0;i<size;i++){
		delete h_toptags[i];
		delete h_bmatch[i];
	}
	
}
void ABCD_Tools::DrawCorrelation1dHistos16regions(const TString& histname){
	vector<double> covs,cors,Cbs;
	TString dirname = m_foldername_tests + "/Correlation_in_bins_16regions";
	TString dirname_output;
	TString dirname_all=dirname + "/leading_vs_recoil";
	gSystem->mkdir(dirname_all,true);
	get_Correlation_of_Table_binbybin(m_table16_hist,cors,covs,Cbs);
	TH1D* hist_correl_factor = HistFromVector(cors,m_h_template);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	hist_correl_factor->SetStats(false);
	plot_histogram(hist_correl_factor,dirname_all,histname,m_lumi_string,"");
	delete hist_correl_factor;
	
	vector<vector<TH1D*> > table_hist(2);
	table_hist[0].resize(2);table_hist[1].resize(2);
	// Leading ljet top-tagging vs. leading ljet b-matching
	table_hist[0][0]=m_table16_hist[0][0];
	table_hist[0][1]=m_table16_hist[0][1];
	table_hist[1][0]=m_table16_hist[0][2];
	table_hist[1][1]=m_table16_hist[0][3];
	get_Correlation_of_Table_binbybin(table_hist,cors,covs,Cbs);
	hist_correl_factor = HistFromVector(cors,m_h_template);
	hist_correl_factor->SetStats(false);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	dirname_output=dirname + "/leading_toptagging_vs_leading_bmatching";
	gSystem->mkdir(dirname_output,true);
	plot_histogram(hist_correl_factor,dirname_output,histname,m_lumi_string,"");
	delete hist_correl_factor;
	
	// Recoil ljet top-tagging vs. recoil ljet b-matching
	table_hist[0][0]=m_table16_hist[0][0];
	table_hist[0][1]=m_table16_hist[1][0];
	table_hist[1][0]=m_table16_hist[2][0];
	table_hist[1][1]=m_table16_hist[3][0];
	get_Correlation_of_Table_binbybin(table_hist,cors,covs,Cbs);
	hist_correl_factor = HistFromVector(cors,m_h_template);
	hist_correl_factor->SetStats(false);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	dirname_output=dirname + "/recoil_toptagging_vs_recoil_bmatching";
	gSystem->mkdir(dirname_output,true);
	plot_histogram(hist_correl_factor,dirname_output,histname,m_lumi_string,"");
	delete hist_correl_factor;
	
	// Leading ljet top-tagging vs. recoil ljet top-tagging
	table_hist[0][0]=m_table16_hist[0][0];
	table_hist[0][1]=m_table16_hist[0][1];
	table_hist[1][0]=m_table16_hist[1][0];
	table_hist[1][1]=m_table16_hist[1][1];
	get_Correlation_of_Table_binbybin(table_hist,cors,covs,Cbs);
	hist_correl_factor = HistFromVector(cors,m_h_template);
	hist_correl_factor->SetStats(false);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	dirname_output=dirname + "/leading_toptagging_vs_recoil_toptagging";
	gSystem->mkdir(dirname_output,true);
	plot_histogram(hist_correl_factor,dirname_output,histname,m_lumi_string,"");
	delete hist_correl_factor;
	
	// Leading ljet b-matching vs. recoil ljet b-matching
	table_hist[0][0]=m_table16_hist[0][0];
	table_hist[0][1]=m_table16_hist[0][2];
	table_hist[1][0]=m_table16_hist[2][0];
	table_hist[1][1]=m_table16_hist[2][2];
	get_Correlation_of_Table_binbybin(table_hist,cors,covs,Cbs);
	hist_correl_factor = HistFromVector(cors,m_h_template);
	hist_correl_factor->SetStats(false);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	dirname_output=dirname + "/leading_bmatching_vs_recoil_bmatching";
	gSystem->mkdir(dirname_output,true);
	plot_histogram(hist_correl_factor,dirname_output,histname,m_lumi_string,"");
	delete hist_correl_factor;
	
	// Leading ljet top-tagging vs. recoil ljet b-matching
	table_hist[0][0]=m_table16_hist[0][0];
	table_hist[0][1]=m_table16_hist[0][1];
	table_hist[1][0]=m_table16_hist[2][0];
	table_hist[1][1]=m_table16_hist[2][1];
	get_Correlation_of_Table_binbybin(table_hist,cors,covs,Cbs);
	hist_correl_factor = HistFromVector(cors,m_h_template);
	hist_correl_factor->SetStats(false);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	dirname_output=dirname + "/leading_toptagging_vs_recoil_bmatching";
	gSystem->mkdir(dirname_output,true);
	plot_histogram(hist_correl_factor,dirname_output,histname,m_lumi_string,"");
	delete hist_correl_factor;
	
	// Leading ljet b-matching vs. recoil ljet top-tagging
	table_hist[0][0]=m_table16_hist[0][0];
	table_hist[0][1]=m_table16_hist[0][2];
	table_hist[1][0]=m_table16_hist[1][0];
	table_hist[1][1]=m_table16_hist[1][2];
	get_Correlation_of_Table_binbybin(table_hist,cors,covs,Cbs);
	hist_correl_factor = HistFromVector(cors,m_h_template);
	hist_correl_factor->SetStats(false);
	SetXaxisTitle(hist_correl_factor,histname);
	hist_correl_factor->GetYaxis()->SetTitle("Correlation factor");
	dirname_output=dirname + "/leading_bmatching_vs_recoil_toptagging";
	gSystem->mkdir(dirname_output,true);
	plot_histogram(hist_correl_factor,dirname_output,histname,m_lumi_string,"");
	delete hist_correl_factor;
	
}
void ABCD_Tools::DrawCorrelation2dHistos16regions(){
	const int size = m_table16.size();
	TH2D* h2d_leading_vs_recoil = new TH2D("","",4,0.,4.,4,0.,4.);
	
	TString dirname;
	dirname = m_foldername_tests + "/Correlation_2D_plots_16regions";
	gSystem->mkdir(dirname,true);
	double stats[4];
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			m_table16_hist[i][j]->GetStats(stats);
			//h2d_leading_vs_recoil->SetBinContent(j+1,i+1,m_table16[i][j]);
			h2d_leading_vs_recoil->SetBinContent(j+1,i+1,stats[0]);
		}
	}
	m_textfile_test_independence_ABCD16 << "Correlation factor from TH2::GetCorrelationFactor(): " << h2d_leading_vs_recoil->GetCorrelationFactor() << endl;
	h2d_leading_vs_recoil->SetStats(kFALSE);
	h2d_leading_vs_recoil->GetXaxis()->SetNdivisions(4);
	h2d_leading_vs_recoil->GetXaxis()->SetTitle("Leading large jet");
	h2d_leading_vs_recoil->GetYaxis()->SetNdivisions(4);
	h2d_leading_vs_recoil->GetYaxis()->SetTitle("Recoil large jet");
	h2d_leading_vs_recoil->GetXaxis()->SetBinLabel(1,"0t0b");
	h2d_leading_vs_recoil->GetXaxis()->SetBinLabel(2,"1t0b");
	h2d_leading_vs_recoil->GetXaxis()->SetBinLabel(3,"0t1b");
	h2d_leading_vs_recoil->GetXaxis()->SetBinLabel(4,"1t1b");
	h2d_leading_vs_recoil->GetYaxis()->SetBinLabel(1,"0t0b");
	h2d_leading_vs_recoil->GetYaxis()->SetBinLabel(2,"1t0b");
	h2d_leading_vs_recoil->GetYaxis()->SetBinLabel(3,"0t1b");
	h2d_leading_vs_recoil->GetYaxis()->SetBinLabel(4,"1t1b");
	plotCorrelationHistograms(h2d_leading_vs_recoil,dirname,"leading_vs_recoil", m_lumi_string,"colztext");
	TH2D* h2d_leading_vs_recoil_errors= (TH2D*)h2d_leading_vs_recoil->Clone();
	
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			m_table16_hist[i][j]->GetStats(stats);
			h2d_leading_vs_recoil_errors->SetBinContent(i+1,j+1,sqrt(stats[1])/stats[0]);
		}
	}
	plotCorrelationHistograms(h2d_leading_vs_recoil_errors,dirname,"leading_vs_recoil_errors", m_lumi_string,"colztext");
	delete h2d_leading_vs_recoil;
	delete h2d_leading_vs_recoil_errors;
	//cout << m_label16[0][1] << endl;
	
	TH2D* h2d_leading_jet_toptagging_vs_bmatching = new TH2D("","",2,0.,2.,2,0.,2.);
	h2d_leading_jet_toptagging_vs_bmatching->SetBinContent(1,1,m_table16[0][0]);
	h2d_leading_jet_toptagging_vs_bmatching->SetBinContent(1,2,m_table16[0][1]);
	h2d_leading_jet_toptagging_vs_bmatching->SetBinContent(2,1,m_table16[0][2]);
	h2d_leading_jet_toptagging_vs_bmatching->SetBinContent(2,2,m_table16[0][3]);

	h2d_leading_jet_toptagging_vs_bmatching->SetStats(kFALSE);
	h2d_leading_jet_toptagging_vs_bmatching->GetXaxis()->SetNdivisions(2);
	h2d_leading_jet_toptagging_vs_bmatching->GetXaxis()->SetTitle("Leading large jet b-matching");
	h2d_leading_jet_toptagging_vs_bmatching->GetYaxis()->SetNdivisions(2);
	h2d_leading_jet_toptagging_vs_bmatching->GetYaxis()->SetTitle("Leading large jet top-tagging");
	plotCorrelationHistograms(h2d_leading_jet_toptagging_vs_bmatching,dirname,"leading_jet_toptagging_vs_bmatching", m_lumi_string,"colztext");
	delete h2d_leading_jet_toptagging_vs_bmatching;
	
	//cout << m_label16[1][0] << endl;
	TH2D* h2d_recoil_jet_toptagging_vs_bmatching = new TH2D("","",2,0.,2.,2,0.,2.);
	h2d_recoil_jet_toptagging_vs_bmatching->SetBinContent(1,1,m_table16[0][0]);
	h2d_recoil_jet_toptagging_vs_bmatching->SetBinContent(1,2,m_table16[1][0]);
	h2d_recoil_jet_toptagging_vs_bmatching->SetBinContent(2,1,m_table16[2][0]);
	h2d_recoil_jet_toptagging_vs_bmatching->SetBinContent(2,2,m_table16[3][0]);

	h2d_recoil_jet_toptagging_vs_bmatching->SetStats(kFALSE);
	h2d_recoil_jet_toptagging_vs_bmatching->GetXaxis()->SetNdivisions(2);
	h2d_recoil_jet_toptagging_vs_bmatching->GetXaxis()->SetTitle("Recoil large jet b-matching");
	h2d_recoil_jet_toptagging_vs_bmatching->GetYaxis()->SetNdivisions(2);
	h2d_recoil_jet_toptagging_vs_bmatching->GetYaxis()->SetTitle("Recoil large jet top-tagging");
	plotCorrelationHistograms(h2d_recoil_jet_toptagging_vs_bmatching,dirname,"recoil_jet_toptagging_vs_bmatching", m_lumi_string,"colztext");
	delete h2d_recoil_jet_toptagging_vs_bmatching;
	
	TH2D* h2d_leading_jet_toptagging_vs_recoil_jet_toptagging = new TH2D("","",2,0.,2.,2,0.,2.);
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->SetBinContent(1,1,m_table16[0][0]);
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->SetBinContent(1,2,m_table16[1][0]);
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->SetBinContent(2,1,m_table16[0][1]);
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->SetBinContent(2,2,m_table16[1][1]);

	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->SetStats(kFALSE);
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->GetXaxis()->SetNdivisions(2);
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->GetXaxis()->SetTitle("Leading large jet top-tagging");
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->GetYaxis()->SetNdivisions(2);
	h2d_leading_jet_toptagging_vs_recoil_jet_toptagging->GetYaxis()->SetTitle("Recoil large jet top-tagging");
	plotCorrelationHistograms(h2d_leading_jet_toptagging_vs_recoil_jet_toptagging,dirname,"leading_jet_toptagging_vs_recoil_jet_toptagging", m_lumi_string,"colztext");
	delete h2d_leading_jet_toptagging_vs_recoil_jet_toptagging;
	
	TH2D* h2d_leading_jet_bmatching_vs_recoil_jet_bmatching = new TH2D("","",2,0.,2.,2,0.,2.);
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->SetBinContent(1,1,m_table16[0][0]);
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->SetBinContent(1,2,m_table16[2][0]);
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->SetBinContent(2,1,m_table16[0][2]);
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->SetBinContent(2,2,m_table16[2][2]);

	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->SetStats(kFALSE);
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->GetXaxis()->SetNdivisions(2);
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->GetXaxis()->SetTitle("Leading large jet b-matching");
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->GetYaxis()->SetNdivisions(2);
	h2d_leading_jet_bmatching_vs_recoil_jet_bmatching->GetYaxis()->SetTitle("Recoil large jet b-matching");
	plotCorrelationHistograms(h2d_leading_jet_bmatching_vs_recoil_jet_bmatching,dirname,"leading_jet_bmatching_vs_recoil_jet_bmatching", m_lumi_string,"colztext");
	delete h2d_leading_jet_bmatching_vs_recoil_jet_bmatching;
	
	TH2D* h2d_leading_jet_toptagging_vs_recoil_jet_bmatching = new TH2D("","",2,0.,2.,2,0.,2.);
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->SetBinContent(1,1,m_table16[0][0]);
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->SetBinContent(1,2,m_table16[2][0]);
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->SetBinContent(2,1,m_table16[0][1]);
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->SetBinContent(2,2,m_table16[2][1]);

	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->SetStats(kFALSE);
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->GetXaxis()->SetNdivisions(2);
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->GetXaxis()->SetTitle("Leading large jet top-tagging");
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->GetYaxis()->SetNdivisions(2);
	h2d_leading_jet_toptagging_vs_recoil_jet_bmatching->GetYaxis()->SetTitle("Recoil large jet b-matching");
	plotCorrelationHistograms(h2d_leading_jet_toptagging_vs_recoil_jet_bmatching,dirname,"leading_jet_toptagging_vs_recoil_jet_bmatching", m_lumi_string,"colztext");
	delete h2d_leading_jet_toptagging_vs_recoil_jet_bmatching;
	
	TH2D* h2d_leading_jet_bmatching_vs_recoil_jet_toptagging = new TH2D("","",2,0.,2.,2,0.,2.);
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->SetBinContent(1,1,m_table16[0][0]);
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->SetBinContent(1,2,m_table16[1][0]);
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->SetBinContent(2,1,m_table16[0][2]);
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->SetBinContent(2,2,m_table16[1][2]);

	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->SetStats(kFALSE);
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->GetXaxis()->SetNdivisions(2);
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->GetXaxis()->SetTitle("Leading large jet b-matching");
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->GetYaxis()->SetNdivisions(2);
	h2d_leading_jet_bmatching_vs_recoil_jet_toptagging->GetYaxis()->SetTitle("Recoil large jet top-tagging");
	plotCorrelationHistograms(h2d_leading_jet_bmatching_vs_recoil_jet_toptagging,dirname,"leading_jet_bmatching_vs_recoil_jet_toptagging", m_lumi_string,"colztext");
	delete h2d_leading_jet_bmatching_vs_recoil_jet_toptagging;
	
	
	
	
}


void ABCD_Tools::MakeBTaggingEfficiencyPlots(TFile*f,const TString& histname){
  const int ntfiles = m_tfiles.size();
  if(ntfiles < 2) return;
  
  MakeBTaggingEfficienncyPlot(f,m_histvectors16[0],"Data",histname);
  MakeBTaggingEfficienncyPlot(f,m_histvectors16[1],"Signal",histname);
  MakeBTaggingBackgroundRejectionPlot(f,"Background",histname);
}
void ABCD_Tools::MakeBTaggingEfficienncyPlot(TFile*f,const vector<TH1D*>& histos,const TString& label,const TString& histname){
  
  const int nbins=histos[m_iRegionA]->GetNbinsX();
  
  TH1D* hLeading = (TH1D*)histos[m_iRegionI]->Clone();
  TH1D* hSubleading =  (TH1D*)histos[m_iRegionB]->Clone();
  
  double denumenator(0.);
  double leading_num(0.);
  double subleading_num(0.);
  
  vector<double> mean_leading(nbins),mean_subleading(nbins);
  vector<double> mean2_leading(nbins),mean2_subleading(nbins);
  
  vector<TH1D*> histos_smeared(16);
  for(int iRegion=0;iRegion<16;iRegion++)histos_smeared[iRegion]=(TH1D*) histos[iRegion]->Clone();
  
  const int nToys=1000; 
  
  for(int iToy=0;iToy<nToys;iToy++){
  
    for(int iRegion=0;iRegion<16;iRegion++) smear_histogram(histos[iRegion],histos_smeared[iRegion]);
    
    for(int ibin=1;ibin<=nbins;ibin++){
      int i=ibin-1;
      denumenator=0.;
      for(int iRegion=0;iRegion<16;iRegion++) denumenator+=histos_smeared[iRegion]->GetBinContent(ibin);
      
      leading_num = histos_smeared[m_iRegionB]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionG]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionH]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionL]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionO]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionM]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionN]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionP]->GetBinContent(ibin);
		    
      subleading_num = histos_smeared[m_iRegionB]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionD]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionH]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionN]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionJ]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionK]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionL]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionP]->GetBinContent(ibin);
      
      double x = denumenator > 0. ? leading_num / denumenator : 0.;
      mean_leading[i] += x;
      mean2_leading[i] += x*x;
      
      x  = denumenator > 0. ? subleading_num / denumenator : 0.;
      
      mean_subleading[i] += x;
      mean2_subleading[i] += x*x;
      
    }
    
  }
  
  for(int ibin=1;ibin<=nbins;ibin++){
    int i=ibin-1;
    mean_leading[i]/=nToys;
    mean2_leading[i]/=nToys;
    mean_subleading[i]/=nToys;
    mean2_subleading[i]/=nToys;
    
    hLeading->SetBinContent(ibin,mean_leading[i]);
    hLeading->SetBinError( ibin , sqrt( mean2_leading[i] - mean_leading[i]*mean_leading[i] ) );
  
    hSubleading->SetBinContent(ibin,mean_subleading[i]);
    hSubleading->SetBinError( ibin , sqrt( mean2_subleading[i] - mean_subleading[i]*mean_subleading[i] ) );
  
  }
  
  TString dirname = m_foldername_tests + "/BTaggingEfficiencyPlots/";
  gSystem->mkdir(dirname,true);
  
  TCanvas *c = new TCanvas("c","",1);
  c->cd();
  
  hLeading->Draw();
  
  c->SaveAs(dirname+label+"_Leading_"+histname+".png");
  
  hSubleading->Draw();
  
  c->SaveAs(dirname+label+"_Subleading_"+histname+".png");
  
  f->cd();
  hLeading->Write(label+"_Leading_"+histname);
  hSubleading->Write(label+"_Subleading_"+histname);
  
  delete c;
  delete hLeading;
  delete hSubleading;
  for(int iRegion=0;iRegion<16;iRegion++) delete histos_smeared[iRegion];
  
}
void ABCD_Tools::MakeBTaggingBackgroundRejectionPlot(TFile*f,const TString& label,const TString& histname){
  
  TH1D* h1 = (TH1D*)m_histvectors16[0][0]->Clone();
  TH1D* h2 = (TH1D*)m_histvectors16[0][0]->Clone();
  h1->Reset();
  h2->Reset();
  const int nbins = h1->GetNbinsX();
  for(int i=0;i<nbins;i++){
    int ibin=i+1;
    h1->SetBinContent(ibin,m_estimates16[32][i]);
    h1->SetBinError(ibin,m_ABCD16_Results[32]->GetBinError(ibin) );
    h2->SetBinContent(ibin,m_estimates16[34][i]);
    h2->SetBinError(ibin,m_ABCD16_Results[34]->GetBinError(ibin) );
  }
  
  TString dirname = m_foldername_tests + "/BTaggingEfficiencyPlots/";
  gSystem->mkdir(dirname,true);
  
  TCanvas *c = new TCanvas("c","",1);
  c->cd();
  
  h1->Draw();
  c->SaveAs(dirname+label+"_Leading_"+histname+".png");
  h2->Draw();
  c->SaveAs(dirname+label+"_Subleading_"+histname+".png");
  
  f->cd();
  h1->Write(label+"_Leading_"+histname);
  h2->Write(label+"_Subleading_"+histname);
  
  
  delete c;
  delete h1;
  delete h2;
  
  
  
}



void ABCD_Tools::MakeTopTaggingEfficiencyPlots(TFile* f,const TString& histname){
  
  const int ntfiles = m_tfiles.size();
  if(ntfiles < 2) return;
  
  MakeTopTaggingEfficienncyPlot(f,m_histvectors16[0],"Data",histname);
  MakeTopTaggingEfficienncyPlot(f,m_histvectors16[1],"Signal",histname);
  MakeTopTaggingBackgroundRejectionPlot(f,"Background",histname);
  
  
  
}

void ABCD_Tools::MakeTopTaggingBackgroundRejectionPlot(TFile*f,const TString& label,const TString& histname){
  TH1D* h1 = (TH1D*)m_histvectors16[0][0]->Clone();
  TH1D* h2 = (TH1D*)m_histvectors16[0][0]->Clone();
  h1->Reset();
  h2->Reset();
  const int nbins = h1->GetNbinsX();
  for(int i=0;i<nbins;i++){
    int ibin=i+1;
    h1->SetBinContent(ibin,m_estimates16[31][i]);
    h1->SetBinError(ibin,m_ABCD16_Results[31]->GetBinError(ibin) );
    h2->SetBinContent(ibin,m_estimates16[33][i]);
    h2->SetBinError(ibin,m_ABCD16_Results[33]->GetBinError(ibin) );
  }
  
  TString dirname = m_foldername_tests + "/TopTaggingEfficiencyPlots/";
  gSystem->mkdir(dirname,true);
  
  TCanvas *c = new TCanvas("c","",1);
  c->cd();
  
  h1->Draw();
  c->SaveAs(dirname+label+"_Leading_"+histname+".png");
  h2->Draw();
  c->SaveAs(dirname+label+"_Subleading_"+histname+".png");
  
  
  f->cd();
  h1->Write(label+"_Leading_"+histname);
  h2->Write(label+"_Subleading_"+histname);
  
  delete c;
  delete h1;
  delete h2;
}

void ABCD_Tools::MakeTopTaggingEfficienncyPlot(TFile*f,const vector<TH1D*>& histos,const TString& label,const TString& histname){

  TH1D* hLeading = (TH1D*)histos[m_iRegionC]->Clone();
  TH1D* hSubleading =  (TH1D*)histos[m_iRegionE]->Clone();
  
  const int nbins=histos[m_iRegionA]->GetNbinsX();
  
  double denumenator(0.);
  double leading_num(0.);
  double subleading_num(0.);
  
  vector<double> mean_leading(nbins),mean_subleading(nbins);
  vector<double> mean2_leading(nbins),mean2_subleading(nbins);
  
  vector<TH1D*> histos_smeared(16);
  for(int iRegion=0;iRegion<16;iRegion++)histos_smeared[iRegion]=(TH1D*) histos[iRegion]->Clone();
  
  const int nToys=1000; 
  
  for(int iToy=0;iToy<nToys;iToy++){
  
    for(int iRegion=0;iRegion<16;iRegion++) smear_histogram(histos[iRegion],histos_smeared[iRegion]);
    
    for(int ibin=1;ibin<=nbins;ibin++){
      int i=ibin-1;
      denumenator=0.;
      for(int iRegion=0;iRegion<16;iRegion++) denumenator+=histos_smeared[iRegion]->GetBinContent(ibin);
      
      leading_num = histos_smeared[m_iRegionC]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionD]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionF]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionK]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionO]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionM]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionN]->GetBinContent(ibin) +
		    histos_smeared[m_iRegionP]->GetBinContent(ibin);
		    
      subleading_num = histos_smeared[m_iRegionE]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionF]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionG]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionM]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionJ]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionK]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionL]->GetBinContent(ibin) +
		       histos_smeared[m_iRegionP]->GetBinContent(ibin);
      
      double x = denumenator > 0. ? leading_num / denumenator : 0.;
      mean_leading[i] += x;
      mean2_leading[i] += x*x;
      
      x  = denumenator > 0. ? subleading_num / denumenator : 0.;
      
      mean_subleading[i] += x;
      mean2_subleading[i] += x*x;
      
    }
    
  }
  
  for(int ibin=1;ibin<=nbins;ibin++){
    int i=ibin-1;
    mean_leading[i]/=nToys;
    mean2_leading[i]/=nToys;
    mean_subleading[i]/=nToys;
    mean2_subleading[i]/=nToys;
    
    hLeading->SetBinContent(ibin,mean_leading[i]);
    hLeading->SetBinError( ibin , sqrt( mean2_leading[i] - mean_leading[i]*mean_leading[i] ) );
  
    hSubleading->SetBinContent(ibin,mean_subleading[i]);
    hSubleading->SetBinError( ibin , sqrt( mean2_subleading[i] - mean_subleading[i]*mean_subleading[i] ) );
  
  }
  
  TString dirname = m_foldername_tests + "/TopTaggingEfficiencyPlots/";
  gSystem->mkdir(dirname,true);
  
  TCanvas *c = new TCanvas("c","",1);
  c->cd();
  
  hLeading->Draw();
  
  c->SaveAs(dirname+label+"_Leading_"+histname+".png");
  
  hSubleading->Draw();
  
  c->SaveAs(dirname+label+"_Subleading_"+histname+".png");
  
  f->cd();
  hLeading->Write(label+"_Leading_"+histname);
  hSubleading->Write(label+"_Subleading_"+histname);
  
  delete c;
  //delete h1;
  delete hLeading;
  delete hSubleading;
  for(int iRegion=0;iRegion<16;iRegion++) delete histos_smeared[iRegion];
}



void ABCD_Tools::DrawSignalBackgroundDiscriminationPlots16regions(){
	
	
}


TH1D* ABCD_Tools::Create_top_tagging_histogram(const vector<TH1D*>& histos,const double& SF){
	TH1D* h_toptag = new TH1D("","",3,0,3);
	h_toptag->SetBinContent(1,(histos[m_i0t0b]->Integral() + histos[m_i0t1b]->Integral() + histos[m_i0t2b]->Integral())*SF);
	h_toptag->SetBinContent(2,(histos[m_i1t0b]->Integral() + histos[m_i1t1b]->Integral() + histos[m_i1t2b]->Integral())*SF);
	h_toptag->SetBinContent(3,(histos[m_i2t0b]->Integral() + histos[m_i2t1b]->Integral() + histos[m_i2t2b]->Integral())*SF);
	return h_toptag;
}
TH1D* ABCD_Tools::Create_b_matching_histogram(const vector<TH1D*>& histos,const double& SF){
	TH1D* h_bmatch = new TH1D("","",3,0,3);
	h_bmatch->SetBinContent(1,(histos[m_i0t0b]->Integral() + histos[m_i1t0b]->Integral() + histos[m_i2t0b]->Integral())*SF);
	h_bmatch->SetBinContent(2,(histos[m_i0t1b]->Integral() + histos[m_i1t1b]->Integral() + histos[m_i2t1b]->Integral())*SF);
	h_bmatch->SetBinContent(3,(histos[m_i0t2b]->Integral() + histos[m_i1t2b]->Integral() + histos[m_i2t2b]->Integral())*SF);
	return h_bmatch;
}

void ABCD_Tools::test_independence_ABCD9(vector<TH1D*>& h_data, const TString& histname){
   vector<double> allregions(h_data.size());
   for(unsigned int i=0;i<h_data.size();i++) {
      if(histname=="nBJets"){
        // h_data[i]->SetBinContent(1,0);
        // h_data[i]->SetBinContent(2,0);
        // h_data[i]->SetBinContent(3,0);
      }
      allregions[i]=h_data[i]->Integral();
   }
   const int size = m_table9.size();
   m_textfile_test_independence_ABCD9 << endl << "Name of histogram for testing: " << histname << endl << endl;
   m_textfile_test_independence_ABCD9 << endl << endl << "Total number of events in 9 regions: " << endl;
   
   
   for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			m_textfile_test_independence_ABCD9  << m_label9[j][i] << ": " << m_table9[j][i]<< " " ;
		}
      m_textfile_test_independence_ABCD9 << endl;
	}
	m_textfile_test_independence_ABCD9 << endl;
	
	m_textfile_test_independence_ABCD9 << "\\hline " << endl;
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			m_textfile_test_independence_ABCD9 << m_table9[j][i];
			if(j!=size-1) m_textfile_test_independence_ABCD9 << " & ";
		}
      m_textfile_test_independence_ABCD9 << "\\\\ \\hline" << endl;
	}
	
	double ABCD4_b0 = m_table9[0][2]*(m_table9[2][0]/m_table9[0][0]);
	double ABCD4_b1 = m_table9[0][2]*(m_table9[2][1]/m_table9[0][1]);
	double xterm1 = m_table9[0][2]*(m_table9[2][0]*m_table9[1][1])/(m_table9[0][1]*m_table9[1][0]);
	double xterm2 = m_table9[0][2]*(m_table9[2][1]*m_table9[1][0])/(m_table9[1][1]*m_table9[0][0]);
	double ABCD4 = (ABCD4_b0 + ABCD4_b1)/2;
	double xterms = (xterm1 + xterm2)/2;
	double ABCD9 = (ABCD4 + xterms)/2;
	double numerator =m_table9[0][2]*(m_table9[2][0]*m_table9[1][1] - m_table9[2][1]*m_table9[1][0])*(m_table9[0][1]*m_table9[1][0] - m_table9[1][1]*m_table9[0][0]);
	double denumerator = 4.*m_table9[1][1]*m_table9[0][0]*m_table9[0][1]*m_table9[1][0];
	
	m_textfile_test_independence_ABCD9 << endl << "Difference between ABCD4 and ABCD9' method" << endl;
	m_textfile_test_independence_ABCD9 << "ABCD4: " << ABCD4 << endl;
	m_textfile_test_independence_ABCD9 << "ABCD9: " << ABCD9 << endl;
	m_textfile_test_independence_ABCD9 << "ABCD4 - ABCD9: " << ABCD4 - ABCD9<< endl;
	m_textfile_test_independence_ABCD9 << "ABCD4_b0: " << ABCD4_b0 << endl;
	m_textfile_test_independence_ABCD9 << "ABCD4_b1: " << ABCD4_b1 << endl;
	m_textfile_test_independence_ABCD9 << "cross term1: " << xterm1 << endl;
	m_textfile_test_independence_ABCD9 << "cross term2: " << xterm2 << endl;
	m_textfile_test_independence_ABCD9 << "GE - HD: " << m_table9[2][0]*m_table9[1][1] - m_table9[2][1]*m_table9[1][0] << endl;
	m_textfile_test_independence_ABCD9 << "EA - BD: " << m_table9[1][1]*m_table9[0][0] - m_table9[0][1]*m_table9[1][0] << endl;
	m_textfile_test_independence_ABCD9 << "C*(GE - HD)(BD - EA): " << numerator << endl;
	m_textfile_test_independence_ABCD9 << "4ABDE: " << denumerator << endl;
	m_textfile_test_independence_ABCD9 << "C*(GE - HD)(BD - EA)/4ABDE: " << numerator/denumerator << endl;
	
	
	
	
	 m_textfile_test_independence_ABCD9 << endl;
	 
  
  m_textfile_test_independence_ABCD9 << endl << endl;
  m_textfile_test_independence_ABCD9 << "\\hline" << endl;
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD9 << " & " << m_label9[j][i];
	m_textfile_test_independence_ABCD9 << " \\\\ \\hline" << endl;
	m_textfile_test_independence_ABCD9 << "$N_{\\text{events}}$ ";
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD9 << " & " << m_table9[j][i];
	m_textfile_test_independence_ABCD9 << " \\\\ \\hline" << endl;
 
   vector<double> effective_numbers; 

   vector<vector<double> > table(2);
   vector<vector<TH1D*> > hist_table(2);
  double chi2(0.),cov(0.),cor(0.),Cb(0);
   double Nb1(0.),Nb2(0.),N(0.),Nt1(0.),Nt2(0.);
   Nb1 = m_table9[2][1] + m_table9[1][1] +m_table9[0][1];
   Nb2 = m_table9[2][2] + m_table9[1][2] +m_table9[0][2];
   Nt1 = m_table9[1][2] + m_table9[1][1] +m_table9[1][0];
   Nt2 = m_table9[1][2] + m_table9[1][2] +m_table9[1][0];
   N = Nb1 + Nb2 + m_table9[2][0] + m_table9[1][0] +m_table9[0][0];
  
  m_textfile_test_independence_ABCD9 << endl << "Cb coefficient for nbjets:" << endl;
  m_textfile_test_independence_ABCD9 << "Cb = " << 4.*N*Nb2/pow(Nb1 + 2.*Nb2,2);
  m_textfile_test_independence_ABCD9 << endl << "Cb coefficient for ntop-tags:" << endl;
  m_textfile_test_independence_ABCD9 << "Cb = " << 4.*N*Nt2/pow(Nt1 + 2.*Nt2,2) << endl;

   m_textfile_test_independence_ABCD9 << endl << "Testing of independence ntop-tags vs nb-matches in all 9 regions: " << endl;
   get_Correlation_of_Table(m_table9_hist,cor,cov,Cb);
   m_textfile_test_independence_ABCD9 << "p-value: " << chi2_ABCD_weighted(m_table9_hist, chi2,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD9 << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD9 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD9 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD9 << endl;  
   m_textfile_test_independence_ABCD9 << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD9 << "Correlation: " << cor << endl; 
   m_textfile_test_independence_ABCD9 << endl;



  vector<vector<TH1D*> > tableABCDEF(2),tableABDEGH(3);

   tableABCDEF[0].resize(3);tableABCDEF[0][0]=m_table9_hist[0][0];tableABCDEF[0][1]=m_table9_hist[0][1];tableABCDEF[0][2]=m_table9_hist[0][2];
   tableABCDEF[1].resize(3);tableABCDEF[1][0]=m_table9_hist[1][0];tableABCDEF[1][1]=m_table9_hist[1][1];tableABCDEF[1][2]=m_table9_hist[1][2];
  
   m_textfile_test_independence_ABCD9 << endl << "Testing of independence ntop-tags vs nb-matches in regions " << m_label9[0][0]  << m_label9[0][1]  << m_label9[0][2]  << m_label9[1][0]  << m_label9[1][1]  << m_label9[1][2]  << ": " << endl;
   get_Correlation_of_Table(tableABCDEF,cor,cov,Cb);
   m_textfile_test_independence_ABCD9 << "p-value: " << chi2_ABCD_weighted(tableABCDEF, chi2,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD9 << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD9 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD9 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD9 << endl;  
   m_textfile_test_independence_ABCD9 << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD9 << "Correlation: " << cor << endl; 
   m_textfile_test_independence_ABCD9 << endl;

   tableABDEGH[0].resize(2);tableABDEGH[0][0]=m_table9_hist[0][0];tableABDEGH[0][1]=m_table9_hist[0][1];
   tableABDEGH[1].resize(2);tableABDEGH[1][0]=m_table9_hist[1][0];tableABDEGH[1][1]=m_table9_hist[1][1];
   tableABDEGH[2].resize(2);tableABDEGH[2][0]=m_table9_hist[2][0];tableABDEGH[2][1]=m_table9_hist[2][1];
   m_textfile_test_independence_ABCD9 << endl << "Testing of independence ntop-tags vs nb-matches in regions " << m_label9[0][0]  << m_label9[0][1]  << m_label9[1][0]  << m_label9[1][1]  << m_label9[2][0]  << m_label9[2][1]  << ": " << endl;
   get_Correlation_of_Table(tableABDEGH,cor,cov,Cb);
   m_textfile_test_independence_ABCD9 << "p-value: " << chi2_ABCD_weighted(tableABDEGH, chi2,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD9 << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD9 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD9 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD9 << endl;  
   m_textfile_test_independence_ABCD9 << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD9 << "Correlation: " << cor << endl; 
   m_textfile_test_independence_ABCD9 << endl;




  

   for(int i=0;i<size-1;i++)for(int j=i+1;j<size;j++)for(int k=0;k<size-1;k++)for(int l=k+1;l<size;l++){
      table[0].resize(2);table[0][0]=m_table9[i][k];table[0][1]=m_table9[i][l];
      table[1].resize(2);table[1][0]=m_table9[j][k];table[1][1]=m_table9[j][l];
      m_textfile_test_independence_ABCD9 << endl << "Testing of independence ntop-tags vs nb-matches in regions " << m_label9[i][k]  << m_label9[i][l]  << m_label9[j][k]  << m_label9[j][l]  << ": " << endl;
  //m_textfile_test_independence_ABCD9 << "p-value: " << chi2_ABCD(table, chi2) << endl;
  //m_textfile_test_independence_ABCD9 << "chi square: " << chi2 << endl;

    

      hist_table[0].resize(2);hist_table[0][0]=m_table9_hist[i][k];hist_table[0][1]=m_table9_hist[i][l];
      hist_table[1].resize(2);hist_table[1][0]=m_table9_hist[j][k];hist_table[1][1]=m_table9_hist[j][l];
    get_Correlation_of_Table(hist_table,cor,cov,Cb);
    m_textfile_test_independence_ABCD9 << "p-value: " << chi2_ABCD_weighted(hist_table, chi2,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD9 << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD9 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD9 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD9 << endl;
    m_textfile_test_independence_ABCD9 << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD9 << "Correlation: " << cor << endl; 
    m_textfile_test_independence_ABCD9 << "Cb: " << Cb << endl;        

   
   }





  m_textfile_test_independence_ABCD9 << endl;
}
void ABCD_Tools::test_independence_ABCD9_binbybin(vector<TH1D*>& /*h_data*/,const TString& histname){
   const int size = m_table9.size();
   vector<vector<double> > effective_numbers;
   
   m_textfile_test_independence_ABCD9_BBB << endl << "Name of histogram for testing: " << histname << endl << endl;
   m_textfile_test_independence_ABCD9_BBB << endl << endl << "Total number of events in 9 regions: " << endl;
  for(int i=0;i<size;i++){
      for(int j=0;j<size;j++){
      m_textfile_test_independence_ABCD9_BBB  << m_label9[j][i] << ": " << m_table9[j][i]<< " " ;
     // m_textfile_test_independence_ABCD9_BBB << m_table9_unweighted[j][i] << " " ;
      }
      m_textfile_test_independence_ABCD9_BBB << endl;
  }
  m_textfile_test_independence_ABCD9_BBB << endl << endl;
  m_textfile_test_independence_ABCD9_BBB << "\\hline" << endl;
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD9_BBB << " & " << m_label9[j][i];
	m_textfile_test_independence_ABCD9_BBB << " \\\\ \\hline" << endl;
	m_textfile_test_independence_ABCD9_BBB << "$N_{\\text{events}}$ ";
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD9_BBB << " & " << m_table9[j][i];
	m_textfile_test_independence_ABCD9_BBB << " \\\\ \\hline" << endl;

  vector<double> chisquares,covs,cors,Cbs;

   vector<vector<double> > table(2);
   vector<vector<TH1D*> > hist_table(2);
  double chi2(0.),cov(0.),cor(0.),Cb(0);
   double Nb1(0.),Nb2(0.),N(0.),Nt1(0.),Nt2(0.);
   Nb1 = m_table9[2][1] + m_table9[1][1] +m_table9[0][1];
   Nb2 = m_table9[2][2] + m_table9[1][2] +m_table9[0][2];
   Nt1 = m_table9[1][2] + m_table9[1][1] +m_table9[1][0];
   Nt2 = m_table9[1][2] + m_table9[1][2] +m_table9[1][0];
   N = Nb1 + Nb2 + m_table9[2][0] + m_table9[1][0] +m_table9[0][0];
  
  m_textfile_test_independence_ABCD9_BBB << endl << "Cb coefficient for nbjets:" << endl;
  m_textfile_test_independence_ABCD9_BBB << "Cb = " << 4.*N*Nb2/pow(Nb1 + 2.*Nb2,2);
  m_textfile_test_independence_ABCD9_BBB << endl << "Cb coefficient for ntop-tags:" << endl;
  m_textfile_test_independence_ABCD9_BBB << "Cb = " << 4.*N*Nt2/pow(Nt1 + 2.*Nt2,2) << endl;

   m_textfile_test_independence_ABCD9_BBB << endl << "Testing of independence ntop-tags vs nb-matches in all 9 regions: " << endl;
   get_Correlation_of_Table(m_table9_hist,cor,cov,Cb);
   get_Correlation_of_Table_binbybin(m_table9_hist,cors,covs,Cbs);
   m_textfile_test_independence_ABCD9_BBB << "p-value: " << chi2_ABCD_weighted_binbybin(m_table9_hist, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD9_BBB << "chi square: " << chi2 << endl;
   m_textfile_test_independence_ABCD9_BBB << "chisquares: ";  
   for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD9_BBB << chisquares[ibin] << " ";
   m_textfile_test_independence_ABCD9_BBB << endl;
   m_textfile_test_independence_ABCD9_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD9_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD9_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD9_BBB << endl;
    }  
   m_textfile_test_independence_ABCD9_BBB << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD9_BBB << "Correlation: " << cor << endl;
   m_textfile_test_independence_ABCD9_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD9_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl;
        m_textfile_test_independence_ABCD9_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD9_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl; 
   m_textfile_test_independence_ABCD9_BBB << endl;



  vector<vector<TH1D*> > tableABCDEF(2),tableABDEGH(3);

   tableABCDEF[0].resize(3);tableABCDEF[0][0]=m_table9_hist[0][0];tableABCDEF[0][1]=m_table9_hist[0][1];tableABCDEF[0][2]=m_table9_hist[0][2];
   tableABCDEF[1].resize(3);tableABCDEF[1][0]=m_table9_hist[1][0];tableABCDEF[1][1]=m_table9_hist[1][1];tableABCDEF[1][2]=m_table9_hist[1][2];
   m_textfile_test_independence_ABCD9_BBB << endl << "Testing of independence ntop-tags vs nb-matches in regions " << m_label9[0][0]  << m_label9[0][1]  << m_label9[0][2]  << m_label9[1][0]  << m_label9[1][1]  << m_label9[1][2]  << ": " << endl;
   get_Correlation_of_Table(tableABCDEF,cor,cov,Cb);
   get_Correlation_of_Table_binbybin(tableABCDEF,cors,covs,Cbs);
   m_textfile_test_independence_ABCD9_BBB << "p-value: " << chi2_ABCD_weighted_binbybin(tableABCDEF, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD9_BBB << "chi square: " << chi2 << endl;
   m_textfile_test_independence_ABCD9_BBB << "chisquares: ";  
    for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD9_BBB << chisquares[ibin] << " ";
    m_textfile_test_independence_ABCD9_BBB << endl;
   m_textfile_test_independence_ABCD9_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD9_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD9_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD9_BBB << endl;
    }  
   m_textfile_test_independence_ABCD9_BBB << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD9_BBB << "Correlation: " << cor << endl;
   m_textfile_test_independence_ABCD9_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD9_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl;
        m_textfile_test_independence_ABCD9_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD9_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl; 
   m_textfile_test_independence_ABCD9_BBB << endl;

   tableABDEGH[0].resize(2);tableABDEGH[0][0]=m_table9_hist[0][0];tableABDEGH[0][1]=m_table9_hist[0][1];
   tableABDEGH[1].resize(2);tableABDEGH[1][0]=m_table9_hist[1][0];tableABDEGH[1][1]=m_table9_hist[1][1];
   tableABDEGH[2].resize(2);tableABDEGH[2][0]=m_table9_hist[2][0];tableABDEGH[2][1]=m_table9_hist[2][1];
   m_textfile_test_independence_ABCD9_BBB << endl << "Testing of independence ntop-tags vs nb-matches in regions " << m_label9[0][0]  << m_label9[0][1]  << m_label9[1][0]  << m_label9[1][1]  << m_label9[2][0]  << m_label9[2][1]  << ": " << endl;
   get_Correlation_of_Table(tableABDEGH,cor,cov,Cb);
   get_Correlation_of_Table_binbybin(tableABDEGH,cors,covs,Cbs);
   m_textfile_test_independence_ABCD9_BBB << "p-value: " << chi2_ABCD_weighted_binbybin(tableABDEGH, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD9_BBB << "chi square: " << chi2 << endl;
   m_textfile_test_independence_ABCD9_BBB << "chisquares: ";  
    for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD9_BBB << chisquares[ibin] << " ";
    m_textfile_test_independence_ABCD9_BBB << endl;  
   m_textfile_test_independence_ABCD9_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD9_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD9_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD9_BBB << endl;
    }
   m_textfile_test_independence_ABCD9_BBB << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD9_BBB << "Correlation: " << cor << endl; 
   m_textfile_test_independence_ABCD9_BBB << endl;
   m_textfile_test_independence_ABCD9_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD9_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl;
        m_textfile_test_independence_ABCD9_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD9_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl;




  

   for(int i=0;i<size-1;i++)for(int j=i+1;j<size;j++)for(int k=0;k<size-1;k++)for(int l=k+1;l<size;l++){
      table[0].resize(2);table[0][0]=m_table9[i][k];table[0][1]=m_table9[i][l];
      table[1].resize(2);table[1][0]=m_table9[j][k];table[1][1]=m_table9[j][l];
      m_textfile_test_independence_ABCD9_BBB << endl << "Testing of independence ntop-tags vs nb-matches in regions " << m_label9[i][k]  << m_label9[i][l]  << m_label9[j][k]  << m_label9[j][l]  << ": " << endl;
  //m_textfile_test_independence_ABCD9_BBB << "p-value: " << chi2_ABCD(table, chi2) << endl;
  //m_textfile_test_independence_ABCD9_BBB << "chi square: " << chi2 << endl;

    

      hist_table[0].resize(2);hist_table[0][0]=m_table9_hist[i][k];hist_table[0][1]=m_table9_hist[i][l];
      hist_table[1].resize(2);hist_table[1][0]=m_table9_hist[j][k];hist_table[1][1]=m_table9_hist[j][l];
    get_Correlation_of_Table(hist_table,cor,cov,Cb);
    get_Correlation_of_Table_binbybin(hist_table,cors,covs,Cbs);
    m_textfile_test_independence_ABCD9_BBB << "p-value: " << chi2_ABCD_weighted_binbybin(hist_table, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD9_BBB << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD9_BBB << "chisquares: ";  
    for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD9_BBB << chisquares[ibin] << " ";
    m_textfile_test_independence_ABCD9_BBB << endl;
    m_textfile_test_independence_ABCD9_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD9_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD9_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD9_BBB << endl;
    }
    m_textfile_test_independence_ABCD9_BBB << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD9_BBB << "Correlation: " << cor << endl; 
    m_textfile_test_independence_ABCD9_BBB << "Cb: " << Cb << endl;        
	m_textfile_test_independence_ABCD9_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD9_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl;
        m_textfile_test_independence_ABCD9_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD9_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl;
        m_textfile_test_independence_ABCD9_BBB << "Cbs: ";  
       for(unsigned int ibin=0;ibin<Cbs.size();ibin++) m_textfile_test_independence_ABCD9_BBB << Cbs[ibin] << " ";
        m_textfile_test_independence_ABCD9_BBB << endl;
   
   }





  m_textfile_test_independence_ABCD9_BBB << endl;

}
void ABCD_Tools::test_independence_of_shapes_ABCD9(vector<TH1D*>& h_data,const TString& histname){
	const int size=h_data.size();
    vector<TH1D*> histos(size);
    for(int i=0;i<size;i++) { 
        histos[i] = (TH1D*)h_data[i]->Clone((TString)h_data[i]->GetName() + "_copy");
     //   m_textfile_test_of_shapes_ABCD9 << histos[i]->GetName() << endl;
     //   histos[i]->Scale(1./histos[i]->Integral());
    }
    
	Double_t chi2(0.);
	Int_t ndf(0),igood(0);
	double chi2_1(0.), chi2_2(0.), chi2_3(0.);
	int ndf_1(0), ndf_2(0), ndf_3(0);
	
	m_textfile_test_of_shapes_ABCD9 << endl << endl << "Histogram name: " << histname << endl;
	 //m_textfile_test_of_shapes_ABCD9 << "Chi squared test (h1->Chi2TestX(h2,\"chi2\",\"ndf\",\"igood\",\"WW\")): " << endl << endl;
	m_textfile_test_of_shapes_ABCD9 << endl << "Testing independence of shape of distribution on the b-matching: " << endl;
    histos[m_i0t0b]->Chi2TestX(histos[m_i0t1b],chi2_1,ndf_1,igood,"WW");
	histos[m_i1t0b]->Chi2TestX(histos[m_i1t1b],chi2_2,ndf_2,igood,"WW");
	histos[m_i2t0b]->Chi2TestX(histos[m_i2t1b],chi2_3,ndf_3,igood,"WW");
    chi2= chi2_1 + chi2_2 + chi2_3;
    ndf= ndf_1 + ndf_2 + ndf_3;

    m_textfile_test_of_shapes_ABCD9 << "p-value: " << TMath::Prob(chi2,ndf) << endl;
    m_textfile_test_of_shapes_ABCD9 << "chi2: " << chi2 << endl;
    m_textfile_test_of_shapes_ABCD9 << "ndf: " << ndf << endl;
    m_textfile_test_of_shapes_ABCD9 << "chi-squares: " << chi2_1 << " " << chi2_2 << " " << chi2_3 << endl;
    m_textfile_test_of_shapes_ABCD9 << "p-values: " << TMath::Prob(chi2_1,ndf_1) << " " << TMath::Prob(chi2_2,ndf_2) << " " << TMath::Prob(chi2_3,ndf_3) << endl;
    
    
    
    m_textfile_test_of_shapes_ABCD9 << endl << "Testing independence of shape of distribution on the top-tagging: " << endl;
    histos[m_i0t0b]->Chi2TestX(histos[m_i1t0b],chi2_1,ndf_1,igood,"WW");
	histos[m_i0t1b]->Chi2TestX(histos[m_i1t1b],chi2_2,ndf_2,igood,"WW");
	histos[m_i0t2b]->Chi2TestX(histos[m_i1t2b],chi2_3,ndf_3,igood,"WW");
    chi2= chi2_1 + chi2_2 + chi2_3;
    ndf= ndf_1 + ndf_2 + ndf_3;

    m_textfile_test_of_shapes_ABCD9 << "p-value: " << TMath::Prob(chi2,ndf) << endl;
    m_textfile_test_of_shapes_ABCD9 << "chi2: " << chi2 << endl;
    m_textfile_test_of_shapes_ABCD9 << "ndf: " << ndf << endl;
    m_textfile_test_of_shapes_ABCD9 << "chi-squares: " << chi2_1 << " " << chi2_2 << " " << chi2_3 << endl;
    m_textfile_test_of_shapes_ABCD9 << "p-values: " << TMath::Prob(chi2_1,ndf_1) << " " << TMath::Prob(chi2_2,ndf_2) << " " << TMath::Prob(chi2_3,ndf_3) << endl;


   
   /*m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions A-B: p-value: " << histos[m_i0t0b]->Chm_i2TestX(histos[m_i0t1b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 << " chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions A-C: p-value: " <<  histos[m_i0t0b]->Chm_i2TestX(histos[m_i0t2b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 <<" chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood <<  endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions B-C: p-value: " << histos[m_i0t1b]->Chm_i2TestX(histos[m_i0t2b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 << " chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions D-E: p-value: " <<  histos[m_i1t0b]->Chm_i2TestX(histos[m_i1t1b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 <<" chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood <<  endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions D-F: p-value: " <<  histos[m_i1t0b]->Chm_i2TestX(histos[m_i1t2b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 << " chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions E-F: p-value: " <<  histos[m_i1t1b]->Chm_i2TestX(histos[m_i1t2b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 << " chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions G-H: p-value: " << histos[m_i2t0b]->Chm_i2TestX(histos[m_i2t1b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 << " chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions G-S: p-value: " << histos[m_i2t0b]->Chm_i2TestX(histos[m_i2t2b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 <<" chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood <<  endl;
   m_textfile_test_of_shapes_ABCD9 << "Chm_i2TestX regions H-S: p-value: " << histos[m_i2t1b]->Chm_i2TestX(histos[m_i2t2b],chm_i2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD9 << " chm_i2: " << chm_i2 << " ndf: " << ndf << " igood: " << igood << endl;
   */
   /* m_textfile_test_of_shapes_ABCD9 << endl << "AndersonDarlingTest (h1->AndersonDarlingTest(h2)): " << endl << endl;

   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions A-B: p-value: " << histos[m_i0t0b]->AndersonDarlingTest(histos[m_i0t1b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions A-C: p-value: " <<  histos[m_i0t0b]->AndersonDarlingTest(histos[m_i0t2b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions B-C: p-value: " << histos[m_i0t1b]->AndersonDarlingTest(histos[m_i0t2b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions D-E: p-value: " <<  histos[m_i1t0b]->AndersonDarlingTest(histos[m_i1t1b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions D-F: p-value: " <<  histos[m_i1t0b]->AndersonDarlingTest(histos[m_i1t2b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions E-F: p-value: " <<  histos[m_i1t1b]->AndersonDarlingTest(histos[m_i1t2b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions G-H: p-value: " << histos[m_i2t0b]->AndersonDarlingTest(histos[m_i2t1b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions G-S: p-value: " << histos[m_i2t0b]->AndersonDarlingTest(histos[m_i2t2b]) << endl;
   m_textfile_test_of_shapes_ABCD9 << "AndersonDarlingTest regions H-S: p-value: " << histos[m_i2t1b]->AndersonDarlingTest(histos[m_i2t2b]) << endl;*/

}








void ABCD_Tools::test_independence_ABCD16(vector<TH1D*>& h_data,const TString& histname){
	
   vector<double> allregions(h_data.size());
      for(unsigned int i=0;i<h_data.size();i++) {
      if(histname=="nBJets"){
        // h_data[i]->SetBinContent(1,0);
        // h_data[i]->SetBinContent(2,0);
        // h_data[i]->SetBinContent(3,0);
      }
      allregions[i]=h_data[i]->Integral();
   }
   const int size = m_table16.size();
   int precision = m_textfile_test_independence_ABCD16.precision();
   m_textfile_test_independence_ABCD16 << setprecision(6);
   m_textfile_test_independence_ABCD16 << endl << endl << "Total number of events in regions: " << endl;
   for(int i=size-1;i>=0;i--){
      for(int j=0;j<size;j++){
      m_textfile_test_independence_ABCD16  << m_label16[i][j] << ": " << m_table16[i][j]<< " " ;
      //m_textfile_test_independence_ABCD16 << m_table16_unweighted[i][j] << " " ;
      }
      m_textfile_test_independence_ABCD16 << endl;
  }
  m_textfile_test_independence_ABCD16 << endl;
	
	m_textfile_test_independence_ABCD16 << "\\hline " << endl;
	for(int i=size-1;i>=0;i--){
		for(int j=0;j<size;j++){
			m_textfile_test_independence_ABCD16 << m_table16[j][i];
			if(j!=size-1) m_textfile_test_independence_ABCD16 << " & ";
		}
      m_textfile_test_independence_ABCD16 << "\\\\ \\hline" << endl;
	}
  m_textfile_test_independence_ABCD16 << setprecision(precision);
  
  
  m_textfile_test_independence_ABCD16 << endl;
  m_textfile_test_independence_ABCD16 << "\\hline" << endl;
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD16 << " & " << m_label16[j][i];
	m_textfile_test_independence_ABCD16 << " \\\\ \\hline" << endl;
	m_textfile_test_independence_ABCD16 << "$N_{\\text{events}}$ ";
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD16 << " & " << m_table16[j][i];
	m_textfile_test_independence_ABCD16 << " \\\\ \\hline" << endl;
     vector<double> effective_numbers;
   vector<vector<double> > table(2);
   vector<vector<TH1D*> > hist_table(2);
   vector<vector<TH1D*> > hist_table_3x3(3),hist_table_4x3(4),hist_table_3x4(3);
   double chi2(0.),cov(0.),cor(0.),Cb(0);

 m_textfile_test_independence_ABCD16 << endl << "Testing of independence leading tagging vs. recoil tagging in all regions: " << endl;
    get_Correlation_of_Table(m_table16_hist,cor,cov,Cb);
    m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD_weighted(m_table16_hist, chi2,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD16 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD16 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD16 << endl;   
    m_textfile_test_independence_ABCD16 << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD16 << "Correlation: " << cor << endl; 

    m_textfile_test_independence_ABCD16 << endl << "Testing of independence leading tagging vs. recoil tagging in green regions ABCDEFGHI: " << endl;
   hist_table_3x3[0].assign(m_table16_hist[0].begin(),m_table16_hist[0].end()-1);
   hist_table_3x3[1].assign(m_table16_hist[1].begin(),m_table16_hist[1].end()-1);
   hist_table_3x3[2].assign(m_table16_hist[2].begin(),m_table16_hist[2].end()-1);
   get_Correlation_of_Table(hist_table_3x3,cor,cov,Cb);
    m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD_weighted(hist_table_3x3, chi2,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD16 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD16 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD16 << endl;   
    m_textfile_test_independence_ABCD16 << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD16 << "Correlation: " << cor << endl; 

 m_textfile_test_independence_ABCD16 << endl << "Testing of independence leading tagging vs. recoil tagging in green regions ABCDEFGHI + blue regions JKL: " << endl;

   hist_table_4x3[0].assign(m_table16_hist[0].begin(),m_table16_hist[0].end()-1);
   hist_table_4x3[1].assign(m_table16_hist[1].begin(),m_table16_hist[1].end()-1);
   hist_table_4x3[2].assign(m_table16_hist[2].begin(),m_table16_hist[2].end()-1);
   hist_table_4x3[3].assign(m_table16_hist[3].begin(),m_table16_hist[3].end()-1);
   get_Correlation_of_Table(hist_table_4x3,cor,cov,Cb);
   m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD_weighted(hist_table_4x3, chi2,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl; 
   m_textfile_test_independence_ABCD16 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD16 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD16 << endl;  
   m_textfile_test_independence_ABCD16 << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD16 << "Correlation: " << cor << endl; 

m_textfile_test_independence_ABCD16 << endl << "Testing of independence leading tagging vs. recoil tagging in green regions ABCDEFGHI + blue regions MNO: " << endl;

   hist_table_3x4[0].assign(m_table16_hist[0].begin(),m_table16_hist[0].end());
   hist_table_3x4[1].assign(m_table16_hist[1].begin(),m_table16_hist[1].end());
   hist_table_3x4[2].assign(m_table16_hist[2].begin(),m_table16_hist[2].end());
   get_Correlation_of_Table(hist_table_3x4,cor,cov,Cb);
   m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD_weighted(hist_table_3x4, chi2,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl;  
   m_textfile_test_independence_ABCD16 << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD16 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD16 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD16 << endl; 
   m_textfile_test_independence_ABCD16 << "Correlation: " << cor << endl; 

  
   for(int i=0;i<size-1;i++)for(int j=i+1;j<size;j++)for(int k=0;k<size-1;k++)for(int l=k+1;l<size;l++){
      table[0].resize(2);table[0][0]=m_table16[i][k];table[0][1]=m_table16[i][l];
      table[1].resize(2);table[1][0]=m_table16[j][k];table[1][1]=m_table16[j][l];
      m_textfile_test_independence_ABCD16 << endl << "Testing of independence in regions " << m_label16[i][k]  << m_label16[i][l]  << m_label16[j][k]  << m_label16[j][l]  << ": " << endl;
  //m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD(table, chi2) << endl;
  //m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl;

    

      hist_table[0].resize(2);hist_table[0][0]=m_table16_hist[i][k];hist_table[0][1]=m_table16_hist[i][l];
      hist_table[1].resize(2);hist_table[1][0]=m_table16_hist[j][k];hist_table[1][1]=m_table16_hist[j][l];
    get_Correlation_of_Table(hist_table,cor,cov,Cb);
    m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD_weighted(hist_table, chi2,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD16 << "Effective numbers of events: "; 
    for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD16 << effective_numbers[l] << " ";
    m_textfile_test_independence_ABCD16 << endl;   
    m_textfile_test_independence_ABCD16 << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD16 << "Correlation: " << cor << endl; 
    m_textfile_test_independence_ABCD16 << "Cb: " << Cb << endl;        

   
   }
   for(int i=0;i<size;i++){
       m_textfile_test_independence_ABCD16 << endl << "Leading fat jet top-tagging vs leading b-matching in regions " << m_label16[i][0]  << m_label16[i][1]  << m_label16[i][2]  << m_label16[i][3]  << ": " << endl;
       hist_table[0].resize(2);hist_table[0][0]=m_table16_hist[i][0];hist_table[0][1]=m_table16_hist[i][1];
       hist_table[1].resize(2);hist_table[1][0]=m_table16_hist[i][2];hist_table[1][1]=m_table16_hist[i][3];
       get_Correlation_of_Table(hist_table,cor,cov,Cb);
       m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD_weighted(hist_table, chi2,effective_numbers, m_niter_chi2test) << endl;
       m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl;  
       m_textfile_test_independence_ABCD16 << "Effective numbers of events: "; 
       for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD16 << effective_numbers[l] << " ";
       m_textfile_test_independence_ABCD16 << endl; 
       m_textfile_test_independence_ABCD16 << "Covariance: " << cov << endl;
       m_textfile_test_independence_ABCD16 << "Correlation: " << cor << endl; 
       m_textfile_test_independence_ABCD16 << "Cb: " << Cb << endl; 

   }
   for(int i=0;i<size;i++){
       m_textfile_test_independence_ABCD16 << endl << "Recoil fat jet top-tagging vs recoil b-matching in regions " << m_label16[0][i]  << m_label16[1][i]  << m_label16[2][i]  << m_label16[3][i]  << ": " << endl;
       hist_table[0].resize(2);hist_table[0][0]=m_table16_hist[0][i];hist_table[0][1]=m_table16_hist[1][i];
       hist_table[1].resize(2);hist_table[1][0]=m_table16_hist[2][i];hist_table[1][1]=m_table16_hist[3][i];
       get_Correlation_of_Table(hist_table,cor,cov,Cb);
       m_textfile_test_independence_ABCD16 << "p-value: " << chi2_ABCD_weighted(hist_table, chi2,effective_numbers, m_niter_chi2test) << endl;
       m_textfile_test_independence_ABCD16 << "chi square: " << chi2 << endl;  
       m_textfile_test_independence_ABCD16 << "Effective numbers of events: "; 
       for(unsigned int l=0;l<effective_numbers.size();l++) m_textfile_test_independence_ABCD16 << effective_numbers[l] << " ";
       m_textfile_test_independence_ABCD16 << endl; 
       m_textfile_test_independence_ABCD16 << "Covariance: " << cov << endl;
       m_textfile_test_independence_ABCD16 << "Correlation: " << cor << endl; 
       m_textfile_test_independence_ABCD16 << "Cb: " << Cb << endl; 

   }
 
}
void ABCD_Tools::test_independence_ABCD16_binbybin(vector<TH1D*>& h_data,const TString& histname){

   vector<double> allregions(h_data.size());
      for(unsigned int i=0;i<h_data.size();i++) {
      if(histname=="nBJets"){
        // h_data[i]->SetBinContent(1,0);
        // h_data[i]->SetBinContent(2,0);
        // h_data[i]->SetBinContent(3,0);
      }
      allregions[i]=h_data[i]->Integral();
   }
   const int size = m_table16.size();

   m_textfile_test_independence_ABCD16_BBB << endl << "Name of histogram for testing: " << histname << endl << endl;
   m_textfile_test_independence_ABCD16_BBB << endl << endl << "Total number of events in regions: " << endl;
   for(int i=size-1;i>=0;i--){
      for(int j=0;j<size;j++){
      m_textfile_test_independence_ABCD16_BBB  << m_label16[i][j] << ": " << m_table16[i][j]<< " " ;
      //m_textfile_test_independence_ABCD16_BBB << m_table16_unweighted[i][j] << " " ;
      }
      m_textfile_test_independence_ABCD16_BBB << endl;
  }
  m_textfile_test_independence_ABCD16_BBB << endl;
  m_textfile_test_independence_ABCD16_BBB << "\\hline" << endl;
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD16_BBB << " & " << m_label16[j][i];
	m_textfile_test_independence_ABCD16_BBB << " \\\\ \\hline" << endl;
	m_textfile_test_independence_ABCD16_BBB << "$N_{\\text{events}}$ ";
	for(int j=0;j<size;j++) for(int i=0;i<size;i++) m_textfile_test_independence_ABCD16_BBB << " & " << m_table16[j][i];
	m_textfile_test_independence_ABCD16_BBB << " \\\\ \\hline" << endl;
     vector<vector<double> > effective_numbers;
   vector<double> chisquares,cors,covs,Cbs;
   vector<vector<double> > table(2);
   vector<vector<TH1D*> > hist_table(2);
   vector<vector<TH1D*> > hist_table_3x3(3),hist_table_4x3(4),hist_table_3x4(3);
   double chi2(0.),cov(0.),cor(0.),Cb(0);

 m_textfile_test_independence_ABCD16_BBB << endl << "Testing of independence leading tagging vs. recoil tagging in all regions: " << endl;
    get_Correlation_of_Table(m_table16_hist,cor,cov,Cb);
    get_Correlation_of_Table_binbybin(m_table16_hist,cors,covs,Cbs);
    m_textfile_test_independence_ABCD16_BBB << "p-value: " << chi2_ABCD_weighted_binbybin(m_table16_hist, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD16_BBB << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD16_BBB << "chisquares: ";  
   for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD16_BBB << chisquares[ibin] << " ";
   m_textfile_test_independence_ABCD16_BBB << endl;
   m_textfile_test_independence_ABCD16_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD16_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD16_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD16_BBB << endl;
    }    
    m_textfile_test_independence_ABCD16_BBB << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD16_BBB << "Correlation: " << cor << endl;
    m_textfile_test_independence_ABCD16_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD16_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl; 

    m_textfile_test_independence_ABCD16_BBB  << endl << "Testing of independence leading tagging vs. recoil tagging in green regions ABCDEFGHI: " << endl;
   hist_table_3x3[0].assign(m_table16_hist[0].begin(),m_table16_hist[0].end()-1);
   hist_table_3x3[1].assign(m_table16_hist[1].begin(),m_table16_hist[1].end()-1);
   hist_table_3x3[2].assign(m_table16_hist[2].begin(),m_table16_hist[2].end()-1);
   get_Correlation_of_Table(hist_table_3x3,cor,cov,Cb);
   get_Correlation_of_Table_binbybin(hist_table_3x3,cors,covs,Cbs);
    m_textfile_test_independence_ABCD16_BBB  << "p-value: " << chi2_ABCD_weighted_binbybin(hist_table_3x3, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD16_BBB  << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD16_BBB << "chisquares: ";  
   for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD16_BBB << chisquares[ibin] << " ";
   m_textfile_test_independence_ABCD16_BBB << endl;
   m_textfile_test_independence_ABCD16_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD16_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD16_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD16_BBB << endl;
    }    
    m_textfile_test_independence_ABCD16_BBB  << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD16_BBB  << "Correlation: " << cor << endl;
    m_textfile_test_independence_ABCD16_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD16_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
         

 m_textfile_test_independence_ABCD16_BBB  << endl << "Testing of independence leading tagging vs. recoil tagging in green regions ABCDEFGHI + blue regions JKL: " << endl;

   hist_table_4x3[0].assign(m_table16_hist[0].begin(),m_table16_hist[0].end()-1);
   hist_table_4x3[1].assign(m_table16_hist[1].begin(),m_table16_hist[1].end()-1);
   hist_table_4x3[2].assign(m_table16_hist[2].begin(),m_table16_hist[2].end()-1);
   hist_table_4x3[3].assign(m_table16_hist[3].begin(),m_table16_hist[3].end()-1);
   get_Correlation_of_Table(hist_table_4x3,cor,cov,Cb);
   get_Correlation_of_Table_binbybin(hist_table_4x3,cors,covs,Cbs);
   m_textfile_test_independence_ABCD16_BBB  << "p-value: " << chi2_ABCD_weighted_binbybin(hist_table_4x3, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD16_BBB  << "chi square: " << chi2 << endl; 
   m_textfile_test_independence_ABCD16_BBB << "chisquares: ";  
   for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD16_BBB << chisquares[ibin] << " ";
   m_textfile_test_independence_ABCD16_BBB << endl;
   m_textfile_test_independence_ABCD16_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD16_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD16_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD16_BBB << endl;
    }   
   m_textfile_test_independence_ABCD16_BBB  << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD16_BBB  << "Correlation: " << cor << endl; 
   m_textfile_test_independence_ABCD16_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD16_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;

m_textfile_test_independence_ABCD16_BBB  << endl << "Testing of independence leading tagging vs. recoil tagging in green regions ABCDEFGHI + blue regions MNO: " << endl;

   hist_table_3x4[0].assign(m_table16_hist[0].begin(),m_table16_hist[0].end());
   hist_table_3x4[1].assign(m_table16_hist[1].begin(),m_table16_hist[1].end());
   hist_table_3x4[2].assign(m_table16_hist[2].begin(),m_table16_hist[2].end());
   get_Correlation_of_Table(hist_table_3x4,cor,cov,Cb);
   get_Correlation_of_Table_binbybin(hist_table_3x4,cors,covs,Cbs);
   m_textfile_test_independence_ABCD16_BBB  << "p-value: " << chi2_ABCD_weighted_binbybin(hist_table_3x4, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
   m_textfile_test_independence_ABCD16_BBB  << "chi square: " << chi2 << endl;  
   m_textfile_test_independence_ABCD16_BBB  << "Covariance: " << cov << endl;
   m_textfile_test_independence_ABCD16_BBB << "chisquares: ";  
   for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD16_BBB << chisquares[ibin] << " ";
   m_textfile_test_independence_ABCD16_BBB << endl;
   m_textfile_test_independence_ABCD16_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD16_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD16_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD16_BBB << endl;
    }  
   m_textfile_test_independence_ABCD16_BBB  << "Correlation: " << cor << endl;
   m_textfile_test_independence_ABCD16_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD16_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl; 

  
   for(int i=0;i<size-1;i++)for(int j=i+1;j<size;j++)for(int k=0;k<size-1;k++)for(int l=k+1;l<size;l++){
      table[0].resize(2);table[0][0]=m_table16[i][k];table[0][1]=m_table16[i][l];
      table[1].resize(2);table[1][0]=m_table16[j][k];table[1][1]=m_table16[j][l];
      m_textfile_test_independence_ABCD16_BBB  << endl << "Testing of independence in regions " << m_label16[i][k]  << m_label16[i][l]  << m_label16[j][k]  << m_label16[j][l]  << ": " << endl;
  //m_textfile_test_independence_ABCD16_BBB  << "p-value: " << chi2_ABCD(table, chi2) << endl;
  //m_textfile_test_independence_ABCD16_BBB  << "chi square: " << chi2 << endl;

    

      hist_table[0].resize(2);hist_table[0][0]=m_table16_hist[i][k];hist_table[0][1]=m_table16_hist[i][l];
      hist_table[1].resize(2);hist_table[1][0]=m_table16_hist[j][k];hist_table[1][1]=m_table16_hist[j][l];
    get_Correlation_of_Table(hist_table,cor,cov,Cb);
    get_Correlation_of_Table_binbybin(hist_table,cors,covs,Cbs);
    m_textfile_test_independence_ABCD16_BBB  << "p-value: " << chi2_ABCD_weighted_binbybin(hist_table, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
    m_textfile_test_independence_ABCD16_BBB  << "chi square: " << chi2 << endl;
    m_textfile_test_independence_ABCD16_BBB << "chisquares: ";  
   for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD16_BBB << chisquares[ibin] << " ";
   m_textfile_test_independence_ABCD16_BBB << endl;
   m_textfile_test_independence_ABCD16_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD16_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD16_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD16_BBB << endl;
    }    
    m_textfile_test_independence_ABCD16_BBB  << "Covariance: " << cov << endl;
    m_textfile_test_independence_ABCD16_BBB  << "Correlation: " << cor << endl; 
    m_textfile_test_independence_ABCD16_BBB  << "Cb: " << Cb << endl;        
	m_textfile_test_independence_ABCD16_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD16_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Cbs: ";  
       for(unsigned int ibin=0;ibin<Cbs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << Cbs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
   
   }
   for(int i=0;i<size;i++){
       m_textfile_test_independence_ABCD16_BBB  << endl << "Leading fat jet top-tagging vs leading b-matching in regions " << m_label16[i][0]  << m_label16[i][1]  << m_label16[i][2]  << m_label16[i][3]  << ": " << endl;
       hist_table[0].resize(2);hist_table[0][0]=m_table16_hist[i][0];hist_table[0][1]=m_table16_hist[i][1];
       hist_table[1].resize(2);hist_table[1][0]=m_table16_hist[i][2];hist_table[1][1]=m_table16_hist[i][3];
       get_Correlation_of_Table(hist_table,cor,cov,Cb);
       get_Correlation_of_Table_binbybin(hist_table,cors,covs,Cbs);
       m_textfile_test_independence_ABCD16_BBB  << "p-value: " << chi2_ABCD_weighted_binbybin(hist_table, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
       m_textfile_test_independence_ABCD16_BBB  << "chi square: " << chi2 << endl;  
    m_textfile_test_independence_ABCD16_BBB << "chisquares: ";  
   for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD16_BBB << chisquares[ibin] << " ";
   m_textfile_test_independence_ABCD16_BBB << endl;
   m_textfile_test_independence_ABCD16_BBB << "effective numbers: " << endl;  
    for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
       m_textfile_test_independence_ABCD16_BBB << "bin " << ibin +1 << ": ";
       for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD16_BBB << effective_numbers[ibin][l] << " ";
       m_textfile_test_independence_ABCD16_BBB << endl;
    }     
       m_textfile_test_independence_ABCD16_BBB  << "Covariance: " << cov << endl;
       m_textfile_test_independence_ABCD16_BBB  << "Correlation: " << cor << endl; 
       m_textfile_test_independence_ABCD16_BBB  << "Cb: " << Cb << endl;
       m_textfile_test_independence_ABCD16_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD16_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Cbs: ";  
       for(unsigned int ibin=0;ibin<Cbs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << Cbs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl; 

   }
   for(int i=0;i<size;i++){
       m_textfile_test_independence_ABCD16_BBB  << endl << "Recoil fat jet top-tagging vs recoil b-matching in regions " << m_label16[0][i]  << m_label16[1][i]  << m_label16[2][i]  << m_label16[3][i]  << ": " << endl;
       hist_table[0].resize(2);hist_table[0][0]=m_table16_hist[0][i];hist_table[0][1]=m_table16_hist[1][i];
       hist_table[1].resize(2);hist_table[1][0]=m_table16_hist[2][i];hist_table[1][1]=m_table16_hist[3][i];
       get_Correlation_of_Table(hist_table,cor,cov,Cb);
       get_Correlation_of_Table_binbybin(hist_table,cors,covs,Cbs);
       m_textfile_test_independence_ABCD16_BBB  << "p-value: " << chi2_ABCD_weighted_binbybin(hist_table, chi2,chisquares,effective_numbers, m_niter_chi2test) << endl;
       m_textfile_test_independence_ABCD16_BBB  << "chi square: " << chi2 << endl;  
       m_textfile_test_independence_ABCD16_BBB << "chisquares: ";  
       for(unsigned int ibin=0;ibin<chisquares.size();ibin++) m_textfile_test_independence_ABCD16_BBB << chisquares[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
       m_textfile_test_independence_ABCD16_BBB << "effective numbers: " << endl;  
       for(unsigned int ibin=0;ibin<effective_numbers.size();ibin++) { 
            m_textfile_test_independence_ABCD16_BBB << "bin " << ibin +1 << ": ";
            for(unsigned int l=0;l<effective_numbers[ibin].size();l++) m_textfile_test_independence_ABCD16_BBB << effective_numbers[ibin][l] << " ";
			m_textfile_test_independence_ABCD16_BBB << endl;
		}     
       m_textfile_test_independence_ABCD16_BBB  << "Covariance: " << cov << endl;
       m_textfile_test_independence_ABCD16_BBB  << "Correlation: " << cor << endl; 
       m_textfile_test_independence_ABCD16_BBB  << "Cb: " << Cb << endl;
       m_textfile_test_independence_ABCD16_BBB << "Covariances: ";  
       for(unsigned int ibin=0;ibin<covs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << covs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Correlations: ";  
       for(unsigned int ibin=0;ibin<cors.size();ibin++) m_textfile_test_independence_ABCD16_BBB << cors[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;
        m_textfile_test_independence_ABCD16_BBB << "Cbs: ";  
       for(unsigned int ibin=0;ibin<Cbs.size();ibin++) m_textfile_test_independence_ABCD16_BBB << Cbs[ibin] << " ";
        m_textfile_test_independence_ABCD16_BBB << endl;  

   }
   

}
void ABCD_Tools::test_independence_of_shapes_ABCD16(vector<TH1D*>& h_data,const TString& histname){
	const int size=h_data.size();
    vector<TH1D*> histos(size);
    for(int i=0;i<size;i++) { 
        histos[i] = (TH1D*)h_data[i]->Clone((TString)h_data[i]->GetName() + "_copy");
     //   textfile << histos[i]->GetName() << endl;
     //   histos[i]->Scale(1./histos[i]->Integral());
    }
    m_textfile_test_of_shapes_ABCD16 << endl << "Testing independence of shape of distribution on the b-matching: " << endl;
    m_textfile_test_of_shapes_ABCD16 << "Histogram name: " << histname << endl << endl;
    
    m_textfile_test_of_shapes_ABCD16 << "Chi squared test (h1->Chi2TestX(h2,\"WW\")): " << endl << endl;

    m_textfile_test_of_shapes_ABCD16 << "Testing effect of leading fat jet b-matching: " << endl << endl;    

  Double_t chi2(0.);
   Int_t ndf(0),igood(0);
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions A-B: p-value: " << histos[m_iRegionA]->Chi2TestX(histos[m_iRegionB],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions C-D: p-value: " << histos[m_iRegionC]->Chi2TestX(histos[m_iRegionD],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions I-H: p-value: " << histos[m_iRegionI]->Chi2TestX(histos[m_iRegionH],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions O-N: p-value: " << histos[m_iRegionO]->Chi2TestX(histos[m_iRegionN],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions E-J: p-value: " << histos[m_iRegionE]->Chi2TestX(histos[m_iRegionJ],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions F-K: p-value: " << histos[m_iRegionF]->Chi2TestX(histos[m_iRegionK],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions G-L: p-value: " << histos[m_iRegionG]->Chi2TestX(histos[m_iRegionL],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions M-S: p-value: " << histos[m_iRegionM]->Chi2TestX(histos[m_iRegionP],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;


    m_textfile_test_of_shapes_ABCD16 << endl << "Testing effect of sub-leading fat jet b-matching: " << endl << endl;

   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions A-I: p-value: " << histos[m_iRegionA]->Chi2TestX(histos[m_iRegionI],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions E-G: p-value: " << histos[m_iRegionE]->Chi2TestX(histos[m_iRegionG],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions B-H: p-value: " << histos[m_iRegionB]->Chi2TestX(histos[m_iRegionH],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions J-L: p-value: " << histos[m_iRegionJ]->Chi2TestX(histos[m_iRegionL],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions C-O: p-value: " << histos[m_iRegionC]->Chi2TestX(histos[m_iRegionO],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions F-M: p-value: " << histos[m_iRegionF]->Chi2TestX(histos[m_iRegionM],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions D-N: p-value: " << histos[m_iRegionD]->Chi2TestX(histos[m_iRegionN],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions K-S: p-value: " << histos[m_iRegionK]->Chi2TestX(histos[m_iRegionP],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;


   m_textfile_test_of_shapes_ABCD16 << endl << "Testing effect of 0 b-matches --> 2 b-matches: " << endl << endl;

   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions A-H: p-value: " << histos[m_iRegionA]->Chi2TestX(histos[m_iRegionH],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions E-L: p-value: " << histos[m_iRegionE]->Chi2TestX(histos[m_iRegionL],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;
   m_textfile_test_of_shapes_ABCD16 << "Chi2TestX regions C-N: p-value: " << histos[m_iRegionC]->Chi2TestX(histos[m_iRegionN],chi2,ndf,igood,"WW");m_textfile_test_of_shapes_ABCD16 << " chi2: " << chi2 << " ndf: " << ndf << " igood: " << igood << endl;



    m_textfile_test_of_shapes_ABCD16 << endl << "AndersonDarlingTest (h1->AndersonDarlingTest(h2)): " << endl << endl;

   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions A-B: p-value: " << histos[m_iRegionA]->AndersonDarlingTest(histos[m_iRegionB]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions C-D: p-value: " << histos[m_iRegionC]->AndersonDarlingTest(histos[m_iRegionD]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions I-H: p-value: " << histos[m_iRegionI]->AndersonDarlingTest(histos[m_iRegionH]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions O-N: p-value: " << histos[m_iRegionO]->AndersonDarlingTest(histos[m_iRegionN]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions E-J: p-value: " << histos[m_iRegionE]->AndersonDarlingTest(histos[m_iRegionJ]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions F-K: p-value: " << histos[m_iRegionF]->AndersonDarlingTest(histos[m_iRegionK]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions G-L: p-value: " << histos[m_iRegionG]->AndersonDarlingTest(histos[m_iRegionL]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions M-S: p-value: " << histos[m_iRegionM]->AndersonDarlingTest(histos[m_iRegionP]) << endl;


    m_textfile_test_of_shapes_ABCD16 << endl << "Testing effect of sub-leading fat jet b-matching: " << endl << endl;

   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions A-I: p-value: " << histos[m_iRegionA]->AndersonDarlingTest(histos[m_iRegionI]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions E-G: p-value: " << histos[m_iRegionE]->AndersonDarlingTest(histos[m_iRegionG]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions B-H: p-value: " << histos[m_iRegionB]->AndersonDarlingTest(histos[m_iRegionH]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions J-L: p-value: " << histos[m_iRegionJ]->AndersonDarlingTest(histos[m_iRegionL]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions C-O: p-value: " << histos[m_iRegionC]->AndersonDarlingTest(histos[m_iRegionO]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions F-M: p-value: " << histos[m_iRegionF]->AndersonDarlingTest(histos[m_iRegionM]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions D-N: p-value: " << histos[m_iRegionD]->AndersonDarlingTest(histos[m_iRegionN]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions K-S: p-value: " << histos[m_iRegionK]->AndersonDarlingTest(histos[m_iRegionP]) << endl;


   m_textfile_test_of_shapes_ABCD16 << endl << "Testing effect of 0 b-matches --> 2 b-matches: " << endl << endl;

   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions A-H: p-value: " << histos[m_iRegionA]->AndersonDarlingTest(histos[m_iRegionH]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions E-L: p-value: " << histos[m_iRegionE]->AndersonDarlingTest(histos[m_iRegionL]) << endl;
   m_textfile_test_of_shapes_ABCD16 << "AndersonDarlingTest regions C-N: p-value: " << histos[m_iRegionC]->AndersonDarlingTest(histos[m_iRegionN]) << endl;

}

void ABCD_Tools::create_ABCD9_table(vector<vector<double> >& table,vector<vector<TH1D*> > &table_hist,vector<vector<char> >& label,vector<TH1D*>& h_data){
	  int size=sqrt(h_data.size());
  if((int)h_data.size()  !=size*size) {
     cout << "Error in create_ABCD9_table(): Wrong number of regions" << endl;
     return;
  } 
   table.resize(size);
   label.resize(size);
   table_hist.resize(size);
   for(int i=0;i<size;i++){
       table[i].resize(size);
       label[i].resize(size);   
       table_hist[i].resize(size);
   }
   table[0][0]= h_data[m_i0t0b]->Integral();label[0][0]= 'A';table_hist[0][0]=(TH1D*)h_data[m_i0t0b]->Clone();;
   table[0][1]= h_data[m_i0t1b]->Integral();label[0][1]= 'B';table_hist[0][1]=(TH1D*)h_data[m_i0t1b]->Clone();;
   table[0][2]= h_data[m_i0t2b]->Integral();label[0][2]= 'C';table_hist[0][2]=(TH1D*)h_data[m_i0t2b]->Clone();;
 

   table[1][0]= h_data[m_i1t0b]->Integral();label[1][0]= 'D';table_hist[1][0]=(TH1D*)h_data[m_i1t0b]->Clone();;
   table[1][1]= h_data[m_i1t1b]->Integral();label[1][1]= 'E';table_hist[1][1]=(TH1D*)h_data[m_i1t1b]->Clone();;
   table[1][2]= h_data[m_i1t2b]->Integral();label[1][2]= 'F';table_hist[1][2]=(TH1D*)h_data[m_i1t2b]->Clone();;
 

   table[2][0]= h_data[m_i2t0b]->Integral();label[2][0]= 'G';table_hist[2][0]=(TH1D*)h_data[m_i2t0b]->Clone();;
   table[2][1]= h_data[m_i2t1b]->Integral();label[2][1]= 'H';table_hist[2][1]=(TH1D*)h_data[m_i2t1b]->Clone();;
   table[2][2]= h_data[m_i2t2b]->Integral();label[2][2]= 'S';table_hist[2][2]=(TH1D*)h_data[m_i2t2b]->Clone();;
 
}

void ABCD_Tools::create_ABCD16_table(vector<vector<double> >& table,vector<vector<TH1D*> > &table_hist,vector<vector<char> >& label,vector<TH1D*>& h_data){
	 int size=sqrt(h_data.size());
  if((int)h_data.size()  !=size*size) {
     cout << "Error in create_ABCD16_table(): Wrong number of regions" << endl;
     return;
  } 
   table.resize(size);
   label.resize(size);

   table_hist.resize(size);
   for(int i=0;i<size;i++){
       table[i].resize(size);
       label[i].resize(size);
       table_hist[i].resize(size);
   }
   table[0][0]= h_data[m_iRegionA]->Integral();label[0][0]= 'A';table_hist[0][0]=(TH1D*)h_data[m_iRegionA]->Clone();
   table[0][1]= h_data[m_iRegionC]->Integral();label[0][1]= 'C';table_hist[0][1]=(TH1D*)h_data[m_iRegionC]->Clone();
   table[0][2]= h_data[m_iRegionI]->Integral();label[0][2]= 'I';table_hist[0][2]=(TH1D*)h_data[m_iRegionI]->Clone();
   table[0][3]= h_data[m_iRegionO]->Integral();label[0][3]= 'O';table_hist[0][3]=(TH1D*)h_data[m_iRegionO]->Clone();

   table[1][0]= h_data[m_iRegionE]->Integral();label[1][0]= 'E';table_hist[1][0]=(TH1D*)h_data[m_iRegionE]->Clone();
   table[1][1]= h_data[m_iRegionF]->Integral();label[1][1]= 'F';table_hist[1][1]=(TH1D*)h_data[m_iRegionF]->Clone();
   table[1][2]= h_data[m_iRegionG]->Integral();label[1][2]= 'G';table_hist[1][2]=(TH1D*)h_data[m_iRegionG]->Clone();
   table[1][3]= h_data[m_iRegionM]->Integral();label[1][3]= 'M';table_hist[1][3]=(TH1D*)h_data[m_iRegionM]->Clone();

   table[2][0]= h_data[m_iRegionB]->Integral();label[2][0]= 'B';table_hist[2][0]=(TH1D*)h_data[m_iRegionB]->Clone();
   table[2][1]= h_data[m_iRegionD]->Integral();label[2][1]= 'D';table_hist[2][1]=(TH1D*)h_data[m_iRegionD]->Clone();
   table[2][2]= h_data[m_iRegionH]->Integral();label[2][2]= 'H';table_hist[2][2]=(TH1D*)h_data[m_iRegionH]->Clone();
   table[2][3]= h_data[m_iRegionN]->Integral();label[2][3]= 'N';table_hist[2][3]=(TH1D*)h_data[m_iRegionN]->Clone();

   table[3][0]= h_data[m_iRegionJ]->Integral();label[3][0]= 'J';table_hist[3][0]=(TH1D*)h_data[m_iRegionJ]->Clone();
   table[3][1]= h_data[m_iRegionK]->Integral();label[3][1]= 'K';table_hist[3][1]=(TH1D*)h_data[m_iRegionK]->Clone();
   table[3][2]= h_data[m_iRegionL]->Integral();label[3][2]= 'L';table_hist[3][2]=(TH1D*)h_data[m_iRegionL]->Clone();
   table[3][3]= h_data[m_iRegionP]->Integral();label[3][3]= 'S';table_hist[3][3]=(TH1D*)h_data[m_iRegionP]->Clone();

}


void ABCD_Tools::GetEstimatedNumberOfEvents(const TString& chain_name,const TString& level,const TString& histname){
	const int nbins = m_h_template->GetNbinsX();
    //vector<double> estimate(nbins);
	const int number_of_ABCD9_methods=m_ABCD9_Results.size();
	const int number_of_ABCD16_methods=m_ABCD16_Results.size();
	vector<double> total_estimates_ABCD9(number_of_ABCD9_methods),total_errors_ABCD9(number_of_ABCD9_methods),total_means_ABCD9(number_of_ABCD9_methods);
	vector<vector<double> > local_estimates_ABCD9(number_of_ABCD9_methods),local_errors_ABCD9(number_of_ABCD9_methods),local_means_ABCD9(number_of_ABCD9_methods);
	vector<double> total_estimates_ABCD16(number_of_ABCD16_methods),total_errors_ABCD16(number_of_ABCD16_methods),total_means_ABCD16(number_of_ABCD16_methods);
	vector<vector<double> > local_estimates_ABCD16(number_of_ABCD16_methods),local_errors_ABCD16(number_of_ABCD16_methods),local_means_ABCD16(number_of_ABCD16_methods);
	for(int i=0;i<number_of_ABCD9_methods;i++){
		local_estimates_ABCD9[i].resize(nbins);
		local_errors_ABCD9[i].resize(nbins);
		local_means_ABCD9[i].resize(nbins);
	}
	for(int i=0;i<number_of_ABCD16_methods;i++){
		local_estimates_ABCD16[i].resize(nbins);
		local_errors_ABCD16[i].resize(nbins);
		local_means_ABCD16[i].resize(nbins);
	}



	//GetEstimatedNumberOfEvents_ABCD9(m_histvector9, hist);
	GetEstimatedNumberOfEvents_ABCD9(m_bincontents16_data,m_bincontents16_MC,m_binerrors16_MC,total_estimates_ABCD9,total_errors_ABCD9,total_means_ABCD9,local_estimates_ABCD9,local_errors_ABCD9,local_means_ABCD9,m_niter_uncertainties);
	//GetEstimatedNumberOfEvents_ABCD9(m_bincontents16,m_binerrors16,total_estimates_ABCD9,total_errors_ABCD9,total_means_ABCD9,local_estimates_ABCD9,local_errors_ABCD9,local_means_ABCD9,m_niter_uncertainties);
	//print_ABCD9_numbers(hist,m_ttbar_SF,m_textfile_test_independence_ABCD9);

	//GetEstimatedNumberOfEvents_ABCD16(m_histvector16, hist2);
	GetEstimatedNumberOfEvents_ABCD16(m_bincontents16_data,m_bincontents16_MC,m_binerrors16_MC,total_estimates_ABCD16,total_errors_ABCD16,total_means_ABCD16,local_estimates_ABCD16,local_errors_ABCD16,local_means_ABCD16,m_niter_uncertainties);
	//GetEstimatedNumberOfEvents_ABCD16(m_bincontents16,m_binerrors16,total_estimates_ABCD16,total_errors_ABCD16,total_means_ABCD16,local_estimates_ABCD16,local_errors_ABCD16,local_means_ABCD16,m_niter_uncertainties);
	//print_ABCD16_numbers(hist2,m_ttbar_SF,m_textfile_test_independence_ABCD16);


	TString textfilename = m_output_mainDir + "/" + m_config->GetValue("textfile_hist_info_foldername","");
	ofstream textfile((textfilename + "/" + histname + ".txt").Data());
	textfile << setprecision(4);
	double stats[4];
	textfile << "Event yields in 9 regions: " << endl;
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			textfile << m_label9[j][i] << " " << m_table9[j][i] << " ";
		}
		textfile << endl;
	}
	textfile << "Errors in 9 regions: " << endl;
	
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			m_table9_hist[j][i]->GetStats(stats);
			textfile << m_label9[j][i] << " " << sqrt(stats[1]) << " ";
		}
		textfile << endl;
	}
	textfile << "Relative errors in 9 regions: " << endl;
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			m_table9_hist[j][i]->GetStats(stats);
			textfile << m_label9[j][i] << " " << sqrt(stats[1])/stats[0] << " ";
		}
		textfile << endl;
	}
	textfile << endl;
	
	textfile << "Event yields in 16 regions: " << endl;
	for(int i=3;i>-1;i--){
		for(int j=0;j<4;j++){
			textfile << m_label16[i][j] << " " << m_table16[i][j] << " ";
		}
		textfile << endl;
	}
	textfile << "Errors in 16 regions: " << endl;
	
	for(int i=3;i>-1;i--){
		for(int j=0;j<4;j++){
			m_table16_hist[i][j]->GetStats(stats);
			textfile << m_label16[i][j] << " " << sqrt(stats[1]) << " ";
		}
		textfile << endl;
	}
	textfile << "Relative errors in 16 regions: " << endl;
	for(int i=3;i>-1;i--){
		for(int j=0;j<4;j++){
			m_table16_hist[i][j]->GetStats(stats);
			textfile << m_label16[i][j] << " " << sqrt(stats[1])/stats[0] << " ";
		}
		textfile << endl;
	}
	textfile << endl;
	
	print_bins_info(m_bincontents9,m_binerrors9,histname,textfile);
	//calculate_purities9(chain_name,level,histname,textfile);
	calculate_purities16(chain_name,level,histname,textfile);
	
	textfile << endl << endl <<"Total ABCD9 estimates" << endl;
	textfile << " \\hline" << endl;
	for(int i=0;i<number_of_ABCD9_methods;i++) textfile << " & " << m_methodNames9[i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Estimates";
	for(int i=0;i<number_of_ABCD9_methods;i++) textfile << " & " << total_estimates_ABCD9[i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Uncertainties";
	for(int i=0;i<number_of_ABCD9_methods;i++) textfile << " & " << total_errors_ABCD9[i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Relative uncertainties";
	for(int i=0;i<number_of_ABCD9_methods;i++)if(total_estimates_ABCD9[i] > 0) textfile << " & " << total_errors_ABCD9[i]/total_estimates_ABCD9[i];
											else textfile << " & " << 0;
	textfile << " \\\\ \\hline" << endl;
	textfile << "Means";
	for(int i=0;i<number_of_ABCD9_methods;i++) textfile << " & " << total_means_ABCD9[i];
	textfile << " \\\\ \\hline" << endl; 
	
	int ichosen=16;
	textfile << endl << endl <<"Local ABCD9 estimates from the " << m_methodNames9[ichosen] << " method" << endl;
	textfile << " \\hline" << endl << "Bin number";
	for(int i=0;i<nbins;i++) textfile << " & " << i+1;
	textfile << " \\\\ \\hline" << endl << "Bin range [GeV]";
	for(int i=0;i<nbins;i++) textfile << " & " << m_binlowedges[i] << "-" << m_binupedges[i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Estimates";
	for(int i=0;i<nbins;i++) textfile << " & " << local_estimates_ABCD9[ichosen][i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Uncertainties";
	for(int i=0;i<nbins;i++) textfile << " & " << local_errors_ABCD9[ichosen][i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Relative uncertainties";
	for(int i=0;i<nbins;i++) if(local_estimates_ABCD9[ichosen][i] > 0) textfile << " & " << local_errors_ABCD9[ichosen][i]/local_estimates_ABCD9[ichosen][i];
							else  textfile << " & " << 0;
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Means";
	for(int i=0;i<nbins;i++) textfile << " & " << local_means_ABCD9[ichosen][i];
	textfile << " \\\\ \\hline" << endl;
	
	
	textfile << endl << endl <<"Total ABCD16 estimates" << endl;
	textfile << " \\hline" << endl;
	for(int i=0;i<number_of_ABCD16_methods;i++) textfile << " & " << m_methodNames16[i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Estimates";
	for(int i=0;i<number_of_ABCD16_methods;i++) textfile << " & " << total_estimates_ABCD16[i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Uncertainties";
	for(int i=0;i<number_of_ABCD16_methods;i++) textfile << " & " << total_errors_ABCD16[i];
	textfile << " \\\\ \\hline" << endl;
	textfile << "Relative uncertainties";
	for(int i=0;i<number_of_ABCD16_methods;i++)if(total_estimates_ABCD16[i] > 0) textfile << " & " << total_errors_ABCD16[i]/total_estimates_ABCD16[i];
											else textfile << " & " << 0;
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Means";
	for(int i=0;i<number_of_ABCD16_methods;i++) textfile << " & " << total_means_ABCD16[i];
	textfile << " \\\\ \\hline" << endl; 
	
	ichosen=6;
	textfile << endl << endl <<"Local ABCD16 estimates from the " << m_methodNames16[ichosen] << " method" << endl;
	textfile << " \\hline" << endl << "Bin number";
	for(int i=0;i<nbins;i++) textfile << " & " << i+1;
	textfile << " \\\\ \\hline" << endl << "Bin range [GeV]";
	for(int i=0;i<nbins;i++) textfile << " & " << m_binlowedges[i] << "-" << m_binupedges[i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Estimates";
	for(int i=0;i<nbins;i++) textfile << " & " << local_estimates_ABCD16[ichosen][i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Uncertainties";
	for(int i=0;i<nbins;i++) textfile << " & " << local_errors_ABCD16[ichosen][i];
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Relative uncertainties";
	for(int i=0;i<nbins;i++) if(local_estimates_ABCD16[ichosen][i] > 0) textfile << " & " << local_errors_ABCD16[ichosen][i]/local_estimates_ABCD16[ichosen][i];
							else  textfile << " & " << 0;
	textfile << " \\\\ \\hline" << endl; 
	textfile << "Means";
	for(int i=0;i<nbins;i++) textfile << " & " << local_means_ABCD16[ichosen][i];
	textfile << " \\\\ \\hline" << endl;
	
	
	print_validation_tables( textfile);
	
	textfile.close();
	
}	
void ABCD_Tools::print_validation_tables( ofstream& textfile){
	double stats1[4];
	double stats2[4];
	TH1D* h1;
	TH1D* h2;
	int i,j,m,n;
	textfile << setprecision(5);
	textfile << endl << "Comparison of total ABCD9 estimates and total event yields" << endl;
	textfile << "Region & Predicted events & Observed events & Predicted / Observed" << " \\\\ \\hline"<< endl;
	for(i=0;i<2;i++)for(j=i+1;j<3;j++)for(m=0;m<2;m++)for(n=m+1;n<3;n++){
		h1 = (TH1D*)m_table9_hist[j][n]->Clone();
		h2 = (TH1D*)m_table9_hist[i][n]->Clone();
		h2->Divide(m_table9_hist[i][m]);
		h2->Multiply(m_table9_hist[j][m]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label9[j][n] << " = " << m_label9[i][n] << "*" << m_label9[j][m] << "/" << m_label9[i][m] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
			<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;
	}	
	textfile << endl << "Testing t_1 vs b_1 correlation effect" << endl;
	textfile << "Region & Predicted events & Observed events & Predicted / Observed" << " \\\\ \\hline"<< endl;
	for(i=0;i<4;i++){
		h1 = (TH1D*)m_table16_hist[i][3]->Clone();
		h2 = (TH1D*)m_table16_hist[i][2]->Clone();
		h2->Divide(m_table16_hist[i][0]);
		h2->Multiply(m_table16_hist[i][1]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label16[i][3] << " = " << m_label16[i][2] << "*" << m_label16[i][1] << "/" << m_label16[i][0] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
				<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;
	}
	textfile << endl << "Testing t_2 vs b_2 correlation effect" << endl;
	textfile << "Region & Predicted events & Observed events & Predicted / Observed" << " \\\\ \\hline"<< endl;
	for(i=0;i<4;i++){
		h1 = (TH1D*)m_table16_hist[3][i]->Clone();
		h2 = (TH1D*)m_table16_hist[2][i]->Clone();
		h2->Divide(m_table16_hist[0][i]);
		h2->Multiply(m_table16_hist[1][i]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label16[3][i] << " = " << m_label16[2][i] << "*" << m_label16[1][i] << "/" << m_label16[0][i] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
				<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];		
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;	
	}
	//textfile << endl << "Comparison of total ABCD16 estimates and total event yields" << endl;
	//textfile << "Region & Predicted events & Observed events & Predicted / Observed " << " \\\\ \\hline"<< endl;
	
	
	textfile << endl << "Testing t_1 vs b_2 correlation effect" << endl;
	textfile << "Region & Predicted events & Observed events & Predicted / Observed" << " \\\\ \\hline"<< endl;
	for(i=0;i<2;i++)for(m=0;m<3;m=m+2){
		j=i+2;
		n=m+1;
		h1 = (TH1D*)m_table16_hist[j][n]->Clone();
		h2 = (TH1D*)m_table16_hist[i][n]->Clone();
		h2->Divide(m_table16_hist[i][m]);
		h2->Multiply(m_table16_hist[j][m]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label16[j][n] << " = " << m_label16[i][n] << "*" << m_label16[j][m] << "/" << m_label16[i][m] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
			<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;
		
	}
	textfile << endl << "Testing b_1 vs t_2 correlation effect" << endl;
	textfile << "Region & Predicted events & Observed events & Predicted / Observed" << " \\\\ \\hline"<< endl;
	for(i=0;i<3;i=i+2)for(m=0;m<2;m++){
		j=i+1;
		n=m+2;
		h1 = (TH1D*)m_table16_hist[j][n]->Clone();
		h2 = (TH1D*)m_table16_hist[i][n]->Clone();
		h2->Divide(m_table16_hist[i][m]);
		h2->Multiply(m_table16_hist[j][m]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label16[j][n] << " = " << m_label16[i][n] << "*" << m_label16[j][m] << "/" << m_label16[i][m] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
			<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;
		
	}
	
	
	
	
	
	textfile << endl << "Testing t_1 vs t_2 correlation effect" << endl;
	textfile << "Region & Predicted events & Observed events & Predicted / Observed" << " \\\\ \\hline"<< endl;
	for(i=0;i<3;i=i+2)for(m=0;m<3;m=m+2){
		j=i+1;
		n=m+1;
		h1 = (TH1D*)m_table16_hist[j][n]->Clone();
		h2 = (TH1D*)m_table16_hist[i][n]->Clone();
		h2->Divide(m_table16_hist[i][m]);
		h2->Multiply(m_table16_hist[j][m]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label16[j][n] << " = " << m_label16[i][n] << "*" << m_label16[j][m] << "/" << m_label16[i][m] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
			<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;
		
	}
	textfile << endl << "Testing b_1 vs b_2 correlation effect" << endl;
	textfile << "Region & Predicted events & Observed events & Predicted / Observed" << " \\\\ \\hline"<< endl;
	for(i=0;i<2;i++)for(m=0;m<2;m++){
		j=i+2;
		n=m+2;
		h1 = (TH1D*)m_table16_hist[j][n]->Clone();
		h2 = (TH1D*)m_table16_hist[i][n]->Clone();
		h2->Divide(m_table16_hist[i][m]);
		h2->Multiply(m_table16_hist[j][m]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label16[j][n] << " = " << m_label16[i][n] << "*" << m_label16[j][m] << "/" << m_label16[i][m] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
			<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;
		
	}	
	
	/*for(i=0;i<3;i++)for(j=i+1;j<4;j++)for(m=0;m<3;m++)for(n=m+1;n<4;n++){
		h1 = (TH1D*)m_table16_hist[j][n]->Clone();
		h2 = (TH1D*)m_table16_hist[i][n]->Clone();
		h2->Divide(m_table16_hist[i][m]);
		h2->Multiply(m_table16_hist[j][m]);
		h1->GetStats(stats1);
		h2->GetStats(stats2);
		textfile << m_label16[j][n] << " = " << m_label16[i][n] << "*" << m_label16[j][m] << "/" << m_label16[i][m] << " & " << stats2[0] << " $\\pm$ " << sqrt(stats2[1]) 
			<< " & " << stats1[0] << " $\\pm$ " << sqrt(stats1[1]) << " & " << stats2[0]/stats1[0];
		textfile << " $\\pm$ " << 1./(stats1[0]*stats1[0])*sqrt(stats1[0]*stats1[0]*stats2[1] + stats2[0]*stats2[0]*stats1[1]) << " \\\\ \\hline"<< endl;
	}*/	
}
void ABCD_Tools::print_correlation_effects(const TString& sys_name,ofstream& textfile){
	textfile << sys_name;
	m_sys_names.push_back(sys_name);
	
	const int size16 = m_bincontents16_data.size();
	const int nbins= m_bincontents16_data[0].size();
	const int ntesting_estimates=22;
	vector<double> global_estimates(ntesting_estimates);
	vector<double> helpvec(m_max_cor_effects_from_sys.size());
	vector<double> totalnumbers16(size16);
	vector<vector<double> > bincontents16(size16);
	int i=0,j=0;
	
	m_testing_estimates.resize(ntesting_estimates);
	for(i=0;i<ntesting_estimates;i++){
		m_testing_estimates[i].resize(nbins);
	}
	for(i=0;i<size16;i++){
		bincontents16[i].resize(nbins);
		for(j=0;j<nbins;j++){
			bincontents16[i][j]=m_bincontents16_data[i][j]-m_bincontents16_MC[i][j];
		}
		totalnumbers16[i]=std::accumulate(bincontents16[i].begin(), bincontents16[i].end(), 0.0);	
	}
	
	//cout << m_testing_estimates.size() << " " << m_testing_estimates[0].size() << endl;
	MakeTestingEstimates(m_testing_estimates,global_estimates,bincontents16,totalnumbers16);
	
	double x;
	x = print_correlation_effects_one_effect(0,1,0,1,textfile);
	if(x>m_max_cor_effects_from_sys[0].second)m_max_cor_effects_from_sys[0]=make_pair(sys_name,x);
	if(x<m_min_cor_effects_from_sys[0].second)m_min_cor_effects_from_sys[0]=make_pair(sys_name,x);
	helpvec[0]=x;
	x = print_correlation_effects_one_effect(0,2,0,2,textfile);
	if(x>m_max_cor_effects_from_sys[1].second)m_max_cor_effects_from_sys[1]=make_pair(sys_name,x);
	if(x<m_min_cor_effects_from_sys[1].second)m_min_cor_effects_from_sys[1]=make_pair(sys_name,x);
	helpvec[1]=x;
	x = print_correlation_effects_one_effect(0,2,0,1,textfile);
	if(x>m_max_cor_effects_from_sys[2].second)m_max_cor_effects_from_sys[2]=make_pair(sys_name,x);
	if(x<m_min_cor_effects_from_sys[2].second)m_min_cor_effects_from_sys[2]=make_pair(sys_name,x);
	helpvec[2]=x;
	x = print_correlation_effects_one_effect(0,1,0,2,textfile);
	if(x>m_max_cor_effects_from_sys[3].second)m_max_cor_effects_from_sys[3]=make_pair(sys_name,x);
	if(x<m_min_cor_effects_from_sys[3].second)m_min_cor_effects_from_sys[3]=make_pair(sys_name,x);
	helpvec[3]=x;
	x = (m_table16[0][1]*m_table16[0][2])/(m_table16[0][0]*m_table16[0][3]) ;
	if(x>m_max_cor_effects_from_sys[4].second)m_max_cor_effects_from_sys[4]=make_pair(sys_name,x);
	if(x<m_min_cor_effects_from_sys[4].second)m_min_cor_effects_from_sys[4]=make_pair(sys_name,x);
	helpvec[4]=x;
	textfile << " & " << x ;
	x = (m_table16[1][0]*m_table16[2][0])/(m_table16[0][0]*m_table16[3][0]) ;
	if(x>m_max_cor_effects_from_sys[5].second)m_max_cor_effects_from_sys[5]=make_pair(sys_name,x);
	if(x<m_min_cor_effects_from_sys[5].second)m_min_cor_effects_from_sys[5]=make_pair(sys_name,x);
	helpvec[5]=x;
	textfile << " & " << x ;
	textfile << " \\\\ \\hline" << endl;
	//m_cor_effects_sys.push_back(helpvec);
	m_cor_effects_sys.push_back(global_estimates);
}

double ABCD_Tools::print_correlation_effects_one_effect(int k,int l,int m,int n,ofstream& textfile){
	double x = (m_table16[k][n]*m_table16[l][m])/(m_table16[k][m]*m_table16[l][n]);
	textfile << " & " << x ;
	return x;	
}
void ABCD_Tools::print_correlation_effects_max_min(ofstream& textfile){
	const int size = m_max_cor_effects_from_sys.size();
	int i=0;
	textfile << "max effects";
	for( i=0;i<size;i++) textfile << " & " << m_max_cor_effects_from_sys[i].first << " " << m_max_cor_effects_from_sys[i].second;
	textfile << " \\\\ \\hline" << endl;
	textfile << "min effects";
	for( i=0;i<size;i++) textfile << " & " << m_min_cor_effects_from_sys[i].first << " " << m_min_cor_effects_from_sys[i].second;
	textfile << " \\\\ \\hline" << endl;
}
void ABCD_Tools::print_correlation_effects_sys_error(ofstream& textfile){
	const int size=m_cor_effects_sys.size();
	const int nsys=(size-1)/2;
	const int nvalues=m_cor_effects_sys[0].size();
	cout << size << " " << nsys << endl;
	
	vector<double> nominal_values=m_cor_effects_sys[0];
	vector<double> helpvec(nvalues);
	vector<vector<double> > errors_up,errors_down;
	vector<double> total_errors_up(nvalues),total_errors_down(nvalues);
	vector<vector<pair<double,double> > > sys_values_pairs;
	vector<TString> sys_names;
	TString opposite_sys_name;
	
	int i=0,j,k;
	for(i=1;i<size;i++){
		//cout << i << endl;
		opposite_sys_name=m_sys_names[i];
		if(m_sys_names[i].EndsWith("up")){
			opposite_sys_name.Replace(opposite_sys_name.Length() -2,2,"down");
		}
		else if(m_sys_names[i].EndsWith("down")){
			opposite_sys_name.Replace(opposite_sys_name.Length() -4,4,"up");
		}
		else cout << "Error: Systematic does not end with up or down" << endl; 
		
		vector<TString>::iterator it = find(m_sys_names.begin(),m_sys_names.end(),opposite_sys_name);
		if(it==m_sys_names.end()){
			cout << "Found systematic without partner" << endl;
			cout << m_sys_names[i] << endl;
			for(k=0;k<nvalues;k++) helpvec[k]=max(0.,m_cor_effects_sys[i][k] - nominal_values[k]);
			errors_up.push_back(helpvec);
			for(k=0;k<nvalues;k++) helpvec[k]=max(0.,-m_cor_effects_sys[i][k] + nominal_values[k]);
			errors_down.push_back(helpvec);
		}
		else {
			j=distance(m_sys_names.begin(),it);
			if(j<i) continue; 
			sys_names.push_back(m_sys_names[i]);
		 
			for(k=0;k<nvalues;k++) helpvec[k]=max(max(0.,m_cor_effects_sys[i][k] - nominal_values[k]),m_cor_effects_sys[j][k] - nominal_values[k]);
			errors_up.push_back(helpvec);
			for(k=0;k<nvalues;k++) helpvec[k]=max(max(0.,-m_cor_effects_sys[i][k] + nominal_values[k]),-m_cor_effects_sys[j][k] + nominal_values[k]);
			errors_down.push_back(helpvec);
		 
		 
			//cout << i << " " << j << endl;
			//cout << m_sys_names[i] << " " << m_sys_names[j] << endl;
			//cout << m_cor_effects_sys[i][0] - nominal_values[0] << " " << m_cor_effects_sys[j][0] - nominal_values[0] << endl;
		}
			
	}
	const int size2 = errors_up.size();
	for(i=0;i<size2;i++){
		for(k=0;k<nvalues;k++){
			total_errors_up[k]+=pow(errors_up[i][k],2);
			total_errors_down[k]+=pow(errors_down[i][k],2);
		}
		
	}
	vector<TString> names(nvalues);
	names[0] = "DB/AE";
	names[1] = "GB/AH";
	names[2] = "GB/AH v2";
	names[3] = "GB/AH v3";
	names[4] = "DC/AF";
	names[5] = "DC/AF v2";
	names[6] = "DC/AF v3";
	names[7] = "GC/AS";
	names[8] = "GC/AS_v2";
	names[9] = "BC/DA";
	names[10] = "EI/GA";
	names[11] = "EC/FA";
	names[12] = "BI/HA";
	names[13] = "DI/HC";
	names[14] = "BG/HE";
	names[15] = "CG/FI";
	names[16] = "ED/FB";
	names[17] = "CI/OA";
	names[18] = "BE/JA";
	names[19] = "ABCD16 S2";
	names[20] = "ABCD16 S3";
	names[21] = "ABCD16";
	
	for(k=0;k<nvalues;k++){
			total_errors_up[k]=sqrt(total_errors_up[k]);
			total_errors_down[k]=sqrt(total_errors_down[k]);
			textfile << names[k] << " " << nominal_values[k] << " + " << total_errors_up[k] << " - " << total_errors_down[k] << endl;
	}
	
}

void ABCD_Tools::test_independence_ABCD9(const TString& histname){
	test_independence_ABCD9(m_histvector9,histname);
}
void ABCD_Tools::test_independence_ABCD9_binbybin(const TString& histname){
	test_independence_ABCD9_binbybin(m_histvector9,histname);
}
void ABCD_Tools::test_independence_of_shapes_ABCD9(const TString& histname){
	test_independence_of_shapes_ABCD9(m_histvector9,histname);;
}

void ABCD_Tools::test_independence_ABCD16(const TString& histname){
	test_independence_ABCD16(m_histvector16,histname);
}
void ABCD_Tools::test_independence_ABCD16_binbybin(const TString& histname){
	test_independence_ABCD16_binbybin(m_histvector16,histname);
}
void ABCD_Tools::test_independence_of_shapes_ABCD16(const TString& histname){
	test_independence_of_shapes_ABCD16(m_histvector16,histname);
}

void ABCD_Tools::Set_methods_ABCD9(vector<TString>& method_names,vector<double>& y2min, vector<double>& y2max){
	
	const int number_of_ABCD9_methods=m_outputnames_ABCD9.size();
	method_names.resize(number_of_ABCD9_methods);
	y2min.resize(number_of_ABCD9_methods);
	y2max.resize(number_of_ABCD9_methods);
	TString name;
	int i=0;
	for(i=0;i<number_of_ABCD9_methods;i++){
		name=m_outputnames_ABCD9[i].first;
		name.ReplaceAll("_tight","");
		method_names[i]=(TString)"_"+name;
		if(i<6){
			y2min[i]=0.3;
			y2max[i]=1.99;
		}
		else{
			y2min[i]=0.61;
			y2max[i]=1.39;
		}
		
		//cout << method_names[i] << endl;
	}
	
	
}
void ABCD_Tools::Set_methods_ABCD16(vector<TString>& method_names,vector<double>& y2min, vector<double>& y2max){
	
	const int number_of_ABCD16_methods=m_outputnames_ABCD16.size();
	method_names.resize(number_of_ABCD16_methods);
	y2min.resize(number_of_ABCD16_methods);
	y2max.resize(number_of_ABCD16_methods);
	TString name;
	int i=0;
	for(i=0;i<number_of_ABCD16_methods;i++){
		name=m_outputnames_ABCD16[i].first;
		name.ReplaceAll("_tight","");
		method_names[i]=(TString)"_"+name;
		y2min[i]=0.61;
		y2max[i]=1.39;
		
		
		//cout << method_names[i] << endl;
	}
	
	
}


void ABCD_Tools::test_sensitivity_of_normalization_ABCD9(const TString& chain_name,const TString& level,const TString& histname){
	TString outputdir= m_foldername_tests + "/histos_normalization_ABCD9";
	unsigned int tfilessize=m_tfiles.size();
	int number_of_ABCD9_methods=m_ABCD9_Results.size();
	vector<TH1D*> histos(16);
	CreateHistVector16(histos,m_tfiles[0],m_nominal_chain_name,level,histname);
	TH1D* h_template=(TH1D*)histos[0]->Clone();
	h_template->Reset();
	int nbins = histos[0]->GetNbinsX();
	int i=0,j=0;
	vector<vector<double> > bincontents16(16),binerrors16(16), local_estimates(number_of_ABCD9_methods), local_errors(number_of_ABCD9_methods), local_means(number_of_ABCD9_methods);
	for(i=0;i<number_of_ABCD9_methods;i++){
		local_estimates[i].resize(nbins);
		local_errors[i].resize(nbins);
		local_means[i].resize(nbins);
		
	}
	for(i=0;i<16;i++){
		bincontents16[i].resize(nbins);
		binerrors16[i].resize(nbins);
		
	}
	vector<double> totalnumbers16(16), totalnumbers9(9), total_estimates(number_of_ABCD9_methods), total_errors(number_of_ABCD9_methods), total_means(number_of_ABCD9_methods);
	 
	// vector<vector<double> > total_estimates_vector, total_errors_vector;
	
	
	
	
	double factor=0.8;
	const double max_factor=1.2; const double step=0.05;

	
	
	
	for(unsigned int i=3;i<tfilessize;i++) AddHistVector16(histos,m_tfiles[i],chain_name,level,histname,-m_lumi);
	if(tfilessize>1)AddHistVector16(histos,m_tfiles[1],chain_name,level,histname,-factor*m_lumi);
	if(tfilessize>2)AddHistVector16(histos,m_tfiles[2],chain_name,level,histname,-factor*m_lumi);
	CreateVector16(histos,totalnumbers16,bincontents16,binerrors16);

	
	
	m_textfile_sensitivity_of_normalization_ABCD9 << endl << endl << histname << endl;
	m_textfile_sensitivity_of_normalization_ABCD9 << "\\hline" << endl << "factor";
	for(i=0;i<number_of_ABCD9_methods; i++) {
		if(i<3 || (i<9 && i>5) || (i<15 && i>11)) continue;
		m_textfile_sensitivity_of_normalization_ABCD9 << " & " << m_methodNames9[i];	
	}
	m_textfile_sensitivity_of_normalization_ABCD9 << " \\\\ \\hline" << endl;
	cout << m_foldername_tests << endl;
	
	gSystem->mkdir(outputdir,true);
	//vector<TH1D*> hist_results((int)(max_factor - factor)/step + 1);
	vector<vector<TH1D*> >hist_results(number_of_ABCD9_methods), hist_results_total_bkg(number_of_ABCD9_methods);
	vector<TGraph*> Event_yeilds_graphs(number_of_ABCD9_methods);
	for(i=0;i<number_of_ABCD9_methods;i++) Event_yeilds_graphs[i] = new TGraph;
	vector<TH1D*> hist_other_bkg(tfilessize-2);
	for(unsigned int i=0;i<tfilessize-2;i++) hist_other_bkg[i] = (TH1D*)m_tfiles[i+2]->Get(chain_name + "/" + m_ABCD9_folder_names[m_i2t2b] + "/" + level + "/" +histname);
	
	vector<TString> legend_names;
	ostringstream strs;
	strs.precision(3);
	
	int ichosen_factor(0);
	TLegend *leg = new TLegend(0.615,0.5,0.92,0.92);
	
	vector<TString> method_names;
	vector<double> y2mins,y2maxs;
	double y2min,y2max;
	Set_methods_ABCD9(method_names,y2mins,y2maxs);
	
	i=0;j=0;
	while(factor <= max_factor + step/10.){
		GetEstimatedNumberOfEvents_ABCD9(bincontents16,binerrors16,total_estimates,total_errors, total_means,local_estimates, local_errors, local_means,m_niter_uncertainties);
		for(j=0;j<number_of_ABCD9_methods; j++){
			hist_results[j].push_back((TH1D*)HistFromVector(local_estimates[j],local_errors[j],h_template));
			
			//hist_results_total_bkg[j].push_back((TH1D*)HistFromVector(local_estimates[j],local_errors[j],m_h_template));
			//hist_results_total_bkg[j][i]->Add(hist_other_bkg[0],m_ttbar_SF*m_lumi);
			//for(unsigned int k=1;k<tfilessize-2;k++)hist_results_total_bkg[j][i]->Add(hist_other_bkg[k],m_lumi);
			
			DivideByBinWidth(hist_results[j][i]);
			//DivideByBinWidth(hist_results_total_bkg[j][i]);
			Event_yeilds_graphs[j]->SetPoint(i,factor,total_estimates[j]);
			Event_yeilds_graphs[j]->SetLineColor(j);
			if(abs(factor-m_ttbar_SF) < step/10.) {
				ichosen_factor=i;
				hist_results[j][i]->SetLineColor(kBlack);
				hist_results[j][i]->SetMarkerColor(kBlack);
			}
			else{
				hist_results[j][i]->SetLineColor(i+2);
				hist_results[j][i]->SetMarkerColor(i+2);
			}
			SetXaxisTitle(hist_results[j][i],histname);
			hist_results[j][i]->GetYaxis()->SetTitle("N_{events} [1/GeV]");
			//SetXaxisTitle(Event_yeilds_graphs[j],histname);
			Event_yeilds_graphs[j]->GetXaxis()->SetTitle("MC scale factor");
			Event_yeilds_graphs[j]->GetYaxis()->SetTitle("N_{events}");
			
			if(j==0){ 
				strs.str("");
				strs << factor;
				legend_names.push_back((TString)"f="+strs.str().c_str());
				//cout << legend_names[i] << endl;
				leg->AddEntry(hist_results[j][i],legend_names[i]);
				//hist_results[j][i]->SetName(legend_names[i]);
			}
		}
			
			
		
		m_textfile_sensitivity_of_normalization_ABCD9 << factor;
	    for(j=0;j<number_of_ABCD9_methods; j++) {
			if(j<3 || (j<9 && j>5) || (j<15 && j>11)) continue;
			m_textfile_sensitivity_of_normalization_ABCD9 << " & " << total_estimates[j] << " $\\pm$ " << total_errors[j];
			
		}
	    m_textfile_sensitivity_of_normalization_ABCD9 << " \\\\ \\hline" << endl;
		if(tfilessize>1)AddHistVector16(histos,m_tfiles[1],chain_name,level,histname,-step*m_lumi);
		if(tfilessize>2)AddHistVector16(histos,m_tfiles[2],chain_name,level,histname,-step*m_lumi);
		CreateVector16(histos,totalnumbers16,bincontents16,binerrors16);

		factor+=step;
		i++;
	}
	vector<TGraph*> graphs;
	TLegend *legGraphs = new TLegend(0.615,0.5,0.92,0.92);

	for(j=0;j<number_of_ABCD9_methods; j++){
		if(j<6 && (histname.Contains("fatJet1_mass") || histname.Contains("fatJet2_mass"))) {
			 y2min=0.001;
			 y2max=4.999;
		 }
		 else{
			 y2min=y2mins[j];
			 y2max=y2maxs[j];
		 }
		plotHistograms(hist_results[j],leg,outputdir + "/",histname + method_names[j],hist_results[j][ichosen_factor],(TString)"OTHERS / " + legend_names[ichosen_factor],y2min,y2max,m_lumi_string);
		//if( j!=5 && j!=11 && j!=15 && j!=16 && j!=17) continue;
		if(  j!=11  && j!=16 && j!=17) continue;
		cout << m_outputnames_ABCD9[j].first << endl;
		Event_yeilds_graphs[j]->SetName(m_outputnames_ABCD9[j].first);
		//legGraphs->AddEntry(Event_yeilds_graphs[j]->GetName(),m_outputnames_ABCD9[j].first,"l");
		graphs.push_back(Event_yeilds_graphs[j]);
	}
	plotGraphs(graphs,legGraphs,outputdir + "/",histname +"_event_yields",m_lumi_string);
	
}

void ABCD_Tools::test_sensitivity_of_normalization_ABCD16(const TString& chain_name,const TString& level,const TString& histname){
	unsigned int tfilessize=m_tfiles.size();
	int number_of_ABCD16_methods=m_ABCD16_Results.size();
	vector<TH1D*> histos(16);
	CreateHistVector16(histos,m_tfiles[0],m_nominal_chain_name,level,histname);
	int nbins = histos[0]->GetNbinsX();
	TH1D* h_template=(TH1D*)histos[0]->Clone();
	h_template->Reset();
	vector<vector<double> > bincontents(16),binerrors(16), local_estimates(number_of_ABCD16_methods), local_errors(number_of_ABCD16_methods), local_means(number_of_ABCD16_methods);
	for(int i=0;i<number_of_ABCD16_methods;i++){
		local_estimates[i].resize(nbins);
		local_errors[i].resize(nbins);
		local_means[i].resize(nbins);
		
	}
	for(int i=0;i<16;i++){
		bincontents[i].resize(nbins);
		binerrors[i].resize(nbins);
		
	}
	vector<double> totalnumbers(16), total_estimates(number_of_ABCD16_methods), total_errors(number_of_ABCD16_methods), total_means(number_of_ABCD16_methods);;
	m_textfile_sensitivity_of_normalization_ABCD16 << endl << endl << histname << endl; 
	m_textfile_sensitivity_of_normalization_ABCD16 << endl << "factor";
	for(int i=0;i<number_of_ABCD16_methods; i++) m_textfile_sensitivity_of_normalization_ABCD16 << " & " << m_methodNames16[i];
	m_textfile_sensitivity_of_normalization_ABCD16 << " \\\\ \\hline" << endl;
	 
	
	double factor=0.8;
	const double max_factor=1.2; const double step=0.1;
	TH1D* hist= new TH1D("","",15,0.5,15.5);
	
	TString outputdir= m_foldername_tests + "/histos_normalization_ABCD16";
	gSystem->mkdir(outputdir,true);
	//vector<TH1D*> hist_results((int)(max_factor - factor)/step + 1);
	vector<TH1D*> hist_results;
	vector<TString> legend_names;
	ostringstream strs;
	strs.precision(3);
	int i=0;
	int ichosen_factor(0);
	TLegend *leg = new TLegend(0.615,0.5,0.92,0.92);
	int imethod=15; // ABCD16 BBB S9
	
	TString method_name;
	double y2min,y2max;
	if(imethod==15){
		method_name="_ABCD16_BBB";
		y2min=0.6;
		y2max=1.4;
	}
	
	
	
	
	for(unsigned int i=3;i<tfilessize;i++) AddHistVector16(histos,m_tfiles[i],chain_name,level,histname,-m_lumi);
	if(tfilessize>1)AddHistVector16(histos,m_tfiles[1],chain_name,level,histname,-factor*m_lumi);
	if(tfilessize>2)AddHistVector16(histos,m_tfiles[2],chain_name,level,histname,-factor*m_lumi);
	CreateVector16(histos,totalnumbers,bincontents,binerrors);
	while(factor <= max_factor + step/10.){
		     GetEstimatedNumberOfEvents_ABCD16(bincontents,binerrors,total_estimates,total_errors, total_means,local_estimates, local_errors, local_means,m_niter_uncertainties);
             hist_results.push_back((TH1D*)HistFromVector(local_estimates[imethod],local_errors[imethod],h_template));
			DivideByBinWidth(hist_results[i]);
			if(abs(factor-m_ttbar_SF) < step/2.) {
				ichosen_factor=i;
				hist_results[i]->SetLineColor(kBlack);
				hist_results[i]->SetMarkerColor(kBlack);
			}
			else{
				hist_results[i]->SetLineColor(i+2);
				hist_results[i]->SetMarkerColor(i+2);
			}
			SetXaxisTitle(hist_results[i],histname);
			hist_results[i]->GetYaxis()->SetTitle("N_{events} [1/GeV]");
			strs.str("");
			strs << factor;
			legend_names.push_back((TString)"f="+strs.str().c_str());
			//cout << legend_names[i] << endl;
			leg->AddEntry(hist_results[i],legend_names[i]);
			hist_results[i]->SetName(legend_names[i]);

			m_textfile_sensitivity_of_normalization_ABCD16 << factor;
		    for(int i=0;i<number_of_ABCD16_methods; i++) m_textfile_sensitivity_of_normalization_ABCD16 << " & " << total_estimates[i] << " $\\pm$ " << total_errors[i];
		    m_textfile_sensitivity_of_normalization_ABCD16 << " \\\\ \\hline" << endl;
			if(tfilessize>1)AddHistVector16(histos,m_tfiles[1],chain_name,level,histname,-step*m_lumi);
			if(tfilessize>2)AddHistVector16(histos,m_tfiles[2],chain_name,level,histname,-step*m_lumi);
             CreateVector16(histos,totalnumbers,bincontents,binerrors);


             factor+=step;
             i++;
    }
	plotHistograms(hist_results,leg,outputdir + "/",histname + method_name,hist_results[ichosen_factor],(TString)"OTHERS / " + legend_names[ichosen_factor],y2min,y2max,m_lumi_string);
	delete hist;
}



void ABCD_Tools::print_ABCD9_numbers(vector<double>& total_estimates, vector<double>& total_errors,double factor,ofstream& textfile){
             textfile << endl << "Data - " << factor << "*(signal + ttbar nonallhad) - Wt: " << endl;
             
             int nestimates= total_estimates.size();             
             textfile << "\\hline " << endl << " & " << "estimates & errors" << " \\\\ \\hline" << endl;
             for(int i=0;i<nestimates;i++) textfile << m_methodNames9[i] << " & " << total_estimates[i] << " & " << total_errors[i] << " \\\\ \\hline" << endl;
	
}
void ABCD_Tools::print_ABCD16_numbers(vector<double>& total_estimates, vector<double>& total_errors, double factor,ofstream& textfile){
             textfile << endl << "Data - " << factor << "*(signal + ttbar nonallhad) - Wt: " << endl;
             
             int nestimates= total_estimates.size();             
             textfile << "\\hline " << endl << " & " << "estimates & errors" << " \\\\ \\hline" << endl;
             for(int i=0;i<nestimates;i++) textfile << m_methodNames16[i] << " & " << total_estimates[i] << " & " << total_errors[i] << " \\\\ \\hline" << endl;
}

void ABCD_Tools::print_bins_info(vector<vector<double> >& bincontents, vector<vector<double> >& binerrors,const TString& histname,ofstream& textfile){
	
	
	int nRegions=bincontents.size();
	int nbins=bincontents[0].size();
	
	
	vector<string> column_names,row_names;
	vector<stringstream> ss(nRegions);
	
	char x='A';
	for(int i=0;i<nRegions-1;i++){
		ss[i] << x;
		row_names.push_back(ss[i].str());
		//cout << row_names[i] << " " << x << " ";
		x++;
	}
	row_names.push_back("S");
	
	for(int j=0;j<nbins;j++){
		ss[0].str("");
//		ss[0] << j + 1;
		ss[0] << m_binlowedges[j] << "-" << m_binupedges[j];
		column_names.push_back(ss[0].str());
		//cout << row_names[j] << " ";
	}
	
	
	textfile << "Printing info about bin contents of the histogram in all regions:" << endl;
	textfile << "Histogram name: " << histname << endl << endl; 
	textfile << "Bincontents:" << endl << endl;
	print_table(bincontents,row_names,column_names,textfile);
	textfile << endl << endl << "Binerrors:" << endl << endl;
	print_table(binerrors,row_names,column_names,textfile);
	
	
	
	vector<vector<double> > eff_numbers(nRegions),rel_uncertainties(nRegions);
	for(int iRegion=0;iRegion<nRegions;iRegion++){
		eff_numbers[iRegion].resize(nbins);
		rel_uncertainties[iRegion].resize(nbins);
		for(int ibin=0;ibin<nbins;ibin++) {
			eff_numbers[iRegion][ibin] = binerrors[iRegion][ibin] > 0 ? bincontents[iRegion][ibin]*bincontents[iRegion][ibin]/pow(binerrors[iRegion][ibin],2) : 0;
			rel_uncertainties[iRegion][ibin] = abs(bincontents[iRegion][ibin]) > 0 ? binerrors[iRegion][ibin]/abs(bincontents[iRegion][ibin]) : 0;
		}
	}
	textfile << endl << endl << "Relative uncertainties:" << endl << endl;
	print_table(rel_uncertainties,row_names,column_names,textfile);
	textfile << endl << endl << "Effective numbers of events:" << endl << endl;
	print_table(eff_numbers,row_names,column_names,textfile);
	
	
}

void ABCD_Tools::calculate_purities9(const TString& chain_name,const TString& level,const TString& histname,ofstream& textfile){
	int tfiles_size = m_tfiles.size();
	vector<vector<vector<double> > > bincontents(tfiles_size),binerrors(tfiles_size);
	vector<vector<vector<double> > > totalnumbers(tfiles_size),table(tfiles_size);
	vector<vector<char> > label;
	vector<vector<vector<TH1D*> > > table_hist(tfiles_size);
	vector<string> column_names,row_names;
	vector<string> one_name(1);
	vector<stringstream> ss(8);
	one_name[0]="Purities";
	char x='A';
	for(int i=0;i<8;i++){
		ss[i] << x;
		row_names.push_back(ss[i].str());
		//cout << row_names[i] << " " << x << " ";
		x++;
	}
	row_names.push_back("S");
	
	vector<vector<TH1D*> > histos(tfiles_size);
	for(int i=0;i<tfiles_size;i++){
		bincontents[i].resize(9);
		binerrors[i].resize(9);
		totalnumbers[i].resize(1);
		totalnumbers[i][0].resize(9);
		histos[i].resize(9);
		CreateHistVector9(histos[i],m_tfiles[i],chain_name,level,histname);
		if(i>0) for(int j=0;j<9;j++){
			histos[i][j]->Scale(m_lumi);
			if(i<3)histos[i][j]->Scale(m_ttbar_SF);
		}
		create_ABCD9_table(table[i],table_hist[i],label,histos[i]);		
		CreateVector9(histos[i],totalnumbers[i][0],bincontents[i],binerrors[i]);
		if(i==0) { //ss.resize(bincontents[0][0].size());
			for(unsigned int j=0;j<bincontents[0][0].size();j++){
				ss[0].str("");
				//ss[0] << j + 1;
				ss[0] << m_binlowedges[j] << "-" << m_binupedges[j];
				column_names.push_back(ss[0].str());
			}
		}
		if(i>0){
			//for(int j=0;j<9;j++)
			textfile << setprecision(3);
			textfile << endl << endl << "Purities in 9 regions from the " << m_tfiles[i]->GetName() << " sample:" << endl << endl;
			print_purities(bincontents[0],bincontents[i],row_names,column_names,histname,textfile);
			textfile << endl;
			textfile << "\\hline" << endl;
			textfile << " & 0t & 1t & 2t  \\\\ \\hline" << endl;
			for(int j=0;j<3;j++){
				if(j==0) textfile << "0b ";
				if(j==1)textfile << "1b ";
				if(j==2)textfile << "2b ";
				for(int k=0;k<3;k++){
					textfile << " & ";
					if(k==2 && j==2 ) textfile << "\\cellcolor{red}";
					else if((k==0|| (k==2 && j!=2))) textfile << "\\cellcolor{yellow}";
					textfile <<  label[k][j] << " (" << table[i][k][j]/table[0][k][j]*100. << "\\%)";
				}
				textfile << " \\\\ \\hline" << endl;
			}
			textfile << endl;
			textfile << endl;
			print_purities(totalnumbers[0],totalnumbers[i],one_name,row_names,histname,textfile);
			textfile << setprecision(4);
		}
	}
	
}
void ABCD_Tools::calculate_purities16(const TString& chain_name,const TString& level,const TString& histname,ofstream& textfile){
	int tfiles_size = m_tfiles.size();
	vector<vector<vector<double> > > bincontents(tfiles_size),binerrors(tfiles_size);
	vector<vector<vector<double> > > totalnumbers(tfiles_size), table(tfiles_size+1);
	vector<vector<char> > label;
	vector<vector<vector<TH1D*> > > table_hist(tfiles_size);
	vector<string> column_names,row_names;
	vector<string> one_name(1);
	vector<stringstream> ss(15);
	one_name[0]="Purities";
	char x='A';
	for(int i=0;i<15;i++){
		ss[i] << x;
		row_names.push_back(ss[i].str());
		//cout << row_names[i] << " " << x << " ";
		x++;
	}
	row_names.push_back("S");
	
	
	textfile << setprecision(3);
	vector<vector<TH1D*> > histos(tfiles_size);
	
	
	
	
	for(int i=0;i<tfiles_size;i++){
		bincontents[i].resize(16);
		binerrors[i].resize(16);
		totalnumbers[i].resize(1);
		totalnumbers[i][0].resize(16);
		histos[i].resize(16);
		CreateHistVector16(histos[i],m_tfiles[i],chain_name,level,histname);
		
		if(i>0) for(int j=0;j<16;j++){
			histos[i][j]->Scale(m_lumi);
			if(i<3)histos[i][j]->Scale(m_ttbar_SF);
		}
		create_ABCD16_table(table[i],table_hist[i],label,histos[i]);	
		CreateVector16(histos[i],totalnumbers[i][0],bincontents[i],binerrors[i]);
	}
	int size=table[0].size();
	int itableall=table.size()-1;
	
	table[itableall].resize(size);
	for(int i=0;i<size;i++)table[itableall][i].resize(size);
	for(int i=1;i<tfiles_size;i++)for(int k=0;k<size;k++)for(int j=0;j<size;j++)table[itableall][j][k]+=table[i][j][k];
	
	
	for(int i=0;i<tfiles_size+1;i++){
		if(i==0) { //ss.resize(bincontents[0][0].size());
			for(unsigned int j=0;j<bincontents[0][0].size();j++){
				ss[0].str("");
				//ss[0] << j + 1;
				ss[0] << m_binlowedges[j] << "-" << m_binupedges[j];
				column_names.push_back(ss[0].str());
				//cout << column_names[j] << " ";
			}
		}
		if(i>0){
			//for(int j=0;j<16;j++)
			if(i<itableall)textfile << endl << endl << "Purities in 16 regions from the " << m_tfiles[i]->GetName() << " sample:" << endl << endl;
			else textfile << endl << endl << "Overal corrections of presence of other proccesses:" << endl << endl;
			if(i<itableall)print_purities(bincontents[0],bincontents[i],row_names,column_names,histname,textfile);
			textfile << endl;
			textfile << "\\cline{2-6}" << endl;
			textfile << "\\multirow{4}{*}{\\rotatebox{90}{2nd large-$R$ jet}}&1t1b";
			for(int k=3;k>=0;k--){
				if(k==2) textfile << "&  0t1b";
				if(k==1)textfile << "&  1t0b";
				if(k==0)textfile << "&  0t0b";
				for(int j=0;j<4;j++){
					textfile << " & ";
					if(k==3 && j==3 ) textfile << "\\cellcolor{red}";
					else if((k==3 && j!=0) || (j==3 && k!=0)) textfile << "\\cellcolor{cyan}";
					else textfile << "\\cellcolor{green}";
					textfile <<  label[k][j] << " (" << table[i][k][j]/table[0][k][j]*100. << "\\%)";
				}
				textfile << " \\\\ \\cline{2-6}" << endl;
			}
			textfile << "& &0t0b&1t0b&0t1b&1t1b \\\\ \\cline{2-6}" << endl;
			textfile << "\\multicolumn{1}{c}{}& \\multicolumn{5}{c}{1st large-$R$ jet} \\\\" << endl;
			textfile << endl;
			if(i<itableall)print_purities(totalnumbers[0],totalnumbers[i],one_name,row_names,histname,textfile);
		}
	}
	textfile << setprecision(4);
}


void ABCD_Tools::SetABCD9names(vector<TString>& names){
	names.resize(m_ABCD9_Results.size());
	names[0]="$\\text{S}^{(1)}$";
	names[1]="$\\text{S}^{(2)}$";
	names[2]="$\\text{S}^{(\\text{Avr})}$";
					 
	names[3]= "$\\text{S}^{(1)}_{\\text{BBB}}$";
	names[4]="$\\text{S}^{(2)}_{\\text{BBB}}$";
	names[5]="$\\text{S}^{(\\text{Avr})}_{\\text{BBB}}$";
					 
	names[6]="$\\text{S}^{(1)}_{\\text{modif}}$";
	names[7]="$\\text{S}^{(2)}_{\\text{modif}}$";
	names[8]="$\\text{S}^{(\\text{Avr})}_{\\text{modif}}$";
					 
	names[9]= "$\\text{S}^{(1)}_{\\text{BBB,modif}}$";
	names[10]="$\\text{S}^{(2)}_{\\text{BBB,modif}}$";
	names[11]="$\\text{S}^{(\\text{Avr})}_{\\text{BBB,modif}}$";
	
	names[12]="$\\text{S}$ b-jets";
	names[13]="$\\text{S}_{\\text{BBB}}$ b-jets";
	
	names[14]="$\\text{S}_{\\text{combined}}$";
	names[15]="$\\text{S}_{\\text{BBB,combined}}$ ";
	
	names[16]="ABCD4 Averaged";
	names[17]="ABCD9 Cross-terms";
	names[18]="ABCD4 b0";
	names[19]="ABCD4 b1";

}
void ABCD_Tools::SetABCD16names(vector<TString>& names){
	names.resize(m_ABCD16_Results.size());
	names[0]="$\\text{S}^{(\\text{Avr})}$";
	names[1]="$\\text{S}^{(1)}$";
	names[2]="$\\text{S}^{(2)}$";
	names[3]="$\\text{S}^{(3)}$";
	names[4]="$\\text{S}^{(4)}$";
	names[5]="$\\text{S}^{(5)}$";
	names[6]="$\\text{S}^{(\\text{Avr})}_{\\text{BBB}}$";
	names[7]="$\\text{S}^{(1)}_{\\text{BBB}}$";
	names[8]="$\\text{S}^{(2)}_{\\text{BBB}}$";
	names[9]="$\\text{S}^{(3)}_{\\text{BBB}}$";
	names[10]="$\\text{S}^{(4)}_{\\text{BBB}}$";
	names[11]="$\\text{S}^{(5)}_{\\text{BBB}}$";
	names[12]="$\\text{S}^{(6)}_{\\text{BBB}}$";
	names[13]="$\\text{S}^{(7)}_{\\text{BBB}}$";
	names[14]="$\\text{S}^{(8)}_{\\text{BBB}}$";
	names[15]="$\\text{S}^{(9)}_{\\text{BBB}}$";
	names[16]="$\\text{S}^{(0)}_{\\text{BBB}}$";
}



