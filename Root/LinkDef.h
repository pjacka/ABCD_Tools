#ifdef __CINT__

#include "ABCD_Tools/ABCD_Tools.h"
#include "ABCD_Tools/functions.h"


#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class ABCD_Tools+;
#pragma link C++ class Exception_loading_histograms_from_tfiles+;

#endif
