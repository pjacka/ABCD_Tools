// Peter Berta, 16.10.2012

#include "ABCD_Tools/functions.h"
#include "TRandom3.h"
//ROOT::Math::Random<ROOT::Math::GSLRngMT>* g_random = new ROOT::Math::Random<ROOT::Math::GSLRngMT>;
TRandom3* g_random = new TRandom3;

void SetSeed(unsigned int seed){
	g_random->SetSeed(seed);
}

double getLumi(TEnv* configLumi,TString mcProduction){
  double lumi(-1000.);
  if(mcProduction=="MC16a")lumi=configLumi->GetValue("lumi_data1516",0.)/1000.;
  else if (mcProduction=="MC16c" || mcProduction=="MC16d") lumi=configLumi->GetValue("lumi_data17",0.)/1000.;
  else if (mcProduction=="MC16e" || mcProduction=="MC16f") lumi=configLumi->GetValue("lumi_data18",0.)/1000.;
  else if (mcProduction=="All") lumi = configLumi->GetValue("lumi_dataAllYears",0.)/1000.;
  else cout << "Error: Unknown MC production" << endl;
  
  return lumi;
}

vector<TString> MakeVector(string text){
  vector<TString> dividedText;
  string::size_type end;
  if (text.size() > 0) {
    do {
      end = text.find(";");
      dividedText.push_back(text.substr(0,end));
      if (end != string::npos) text = text.substr(end+1);
    } while (end != string::npos);
  }
  return dividedText;
}


vector<float> MakeVectorFloat(string text){
  vector<float> dividedText;
  string::size_type end;
  if (text.size() > 0) {
    do {
      end = text.find(";");
      dividedText.push_back(atof(text.substr(0,end).c_str()));
      if (end != string::npos) text = text.substr(end+1);
    } while (end != string::npos);
  }
  return dividedText;
}

vector<double> MakeVectorDouble(string text){
  vector<double> dividedText;
  string::size_type end;
  if (text.size() > 0) {
    do {
      end = text.find(";");
      dividedText.push_back(atof(text.substr(0,end).c_str()));
      if (end != string::npos) text = text.substr(end+1);
    } while (end != string::npos);
  }
  return dividedText;
}


vector<TString> GetSubdirectories(int debug){
  vector<TString> subdirs;
  TDirectory *current_dir = gDirectory;
  TIter nextkey(current_dir->GetListOfKeys());
  TKey *key, *oldkey=0;
  while (( key = (TKey*)nextkey())){
    if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
    TObject *obj=key->ReadObj();
    if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
    if (obj->IsA()->InheritsFrom(TDirectory::Class())){
      if (debug > 1) cout << "it is a subdirectory"  << endl;
      subdirs.push_back(obj->GetName());
    }
    delete obj;
  }
  return(subdirs);
}

vector<TString> GetTTrees(int debug){
  vector<TString> ttrees;
  TDirectory *current_dir = gDirectory;
  TIter nextkey(current_dir->GetListOfKeys());
  TKey *key, *oldkey=0;
  while (( key = (TKey*)nextkey())){
    if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
    TObject *obj=key->ReadObj();
    if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
    if (obj->IsA()->InheritsFrom(TTree::Class())){
      if (debug > 1) cout << "it is a ttree"  << endl;
      ttrees.push_back(obj->GetName());
    }
    delete obj;
  }
  return(ttrees);
}


map<TString, TH1F*> GetTH1Fs(int debug){
  map<TString, TH1F*> objects;
  TDirectory *current_dir = gDirectory;
  TIter nextkey(current_dir->GetListOfKeys());
  TKey *key, *oldkey=0;
  while (( key = (TKey*)nextkey())){
    if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
    TObject *obj=key->ReadObj();
    if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
    if (obj->IsA()->InheritsFrom(TH1F::Class())){
      if (debug > 1) cout << "it is a TH1F object"  << endl;
      objects[obj->GetName()]=dynamic_cast<TH1F*>(current_dir->Get(obj->GetName()));
      objects[obj->GetName()]->SetTitle("");
      //objects[obj->GetName()]->Sumw2();
    }
    delete obj;
  }
  return(objects);
}

void GetListOfTH1DNames(vector<TString>& names){
  TIter nextkey( gDirectory->GetListOfKeys()); 
  TKey* key=0;
  while( (key = (TKey*)nextkey())){
    
    TObject *obj=key->ReadObj();
    if (obj->IsA()->InheritsFrom(TH1D::Class())){
        // cout << key->GetName() << endl;
         names.push_back(key->GetName());
    }
  }

}

void smear_histogram(TH1D* h,TH1D* hsmeared){
   int n=h->GetNbinsX();
   for(int i=1;i<=n;i++){
      hsmeared->SetBinContent(i,g_random->Gaus(h->GetBinContent(i),h->GetBinError(i)));
   }

}
void smear_bin_contents(const vector<vector<double> >& bincontents,const vector<vector<double> >& binerrors,vector<vector<double> >& bincontents_smeared){
	int const size=bincontents.size();
	int const size2=bincontents[0].size();
	for(int i=0;i<size;i++)for(int j=0;j<size2;j++) bincontents_smeared[i][j]=g_random->Gaus(bincontents[i][j],binerrors[i][j]);
}
void smear_bin_contents_data(const vector<vector<double> >& bincontents,vector<vector<double> >& bincontents_smeared){
	int const size=bincontents.size();
	int const size2=bincontents[0].size();
	for(int i=0;i<size;i++)for(int j=0;j<size2;j++)bincontents_smeared[i][j]=g_random->Gaus(bincontents[i][j],sqrt(bincontents[i][j]));
}
void smear_bin_contents(const vector<vector<double> >& bincontents_data,const vector<vector<double> >& bincontents_MC,const vector<vector<double> >& binerrors_MC,vector<vector<double> >& bincontents_smeared){
	int const size=bincontents_data.size();
	int const size2=bincontents_data[0].size();
	double x;
	for(int i=0;i<size;i++)for(int j=0;j<size2;j++) {
		x = bincontents_data[i][j] > 100 ? g_random->Gaus(bincontents_data[i][j],sqrt(bincontents_data[i][j])) : g_random->Poisson(bincontents_data[i][j]) ; 
		bincontents_smeared[i][j]=x - g_random->Gaus(bincontents_MC[i][j],binerrors_MC[i][j]);
	}
}

TH1D* HistFromVector(vector<double>& bincontents, TH1D* hist_template){
	TH1D* h = (TH1D*)hist_template->Clone("");
	int nbins= bincontents.size();
	for(int i=0;i<nbins;i++) h->SetBinContent(i+1,bincontents[i]);
	return h;
}
TH1D* HistFromVector(vector<double>& bincontents, vector<double>& binerrors, TH1D* hist_template){
	TH1D* h = (TH1D*)hist_template->Clone("");
	int nbins= bincontents.size();
	for(int i=0;i<nbins;i++) {
		h->SetBinContent(i+1,bincontents[i]);
		h->SetBinError(i+1,binerrors[i]);
	}
	return h;
}


map<TString, TH1D*> GetTH1Ds(int debug){
  map<TString, TH1D*> objects;
  TDirectory *current_dir = gDirectory;
  TIter nextkey(current_dir->GetListOfKeys());
  TKey *key, *oldkey=0;
  while (( key = (TKey*)nextkey())){
    if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
    TObject *obj=key->ReadObj();
    if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
    if (obj->IsA()->InheritsFrom(TH1D::Class())){
      if (debug > 1) cout << "it is a TH1D object"  << endl;
      objects[obj->GetName()]=dynamic_cast<TH1D*>(current_dir->Get(obj->GetName()));
      objects[obj->GetName()]->SetTitle("");
      //objects[obj->GetName()]->Sumw2();
    }
    delete obj;
  }
  return(objects);
}


map<TString, TH2F*> GetTH2Fs(int debug){
  map<TString, TH2F*> objects;
  TDirectory *current_dir = gDirectory;
  TIter nextkey(current_dir->GetListOfKeys());
  TKey *key, *oldkey=0;
  while (( key = (TKey*)nextkey())){
    if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
    TObject *obj=key->ReadObj();
    if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
    if (obj->IsA()->InheritsFrom(TH2F::Class())){
      if (debug > 1) cout << "it is a TH1F object"  << endl;
      objects[obj->GetName()]=dynamic_cast<TH2F*>(current_dir->Get(obj->GetName()));
      objects[obj->GetName()]->SetTitle("");
      //objects[obj->GetName()]->Sumw2();
    }
    delete obj;
  }
  return(objects);
}

map<TString, TH2D*> GetTH2Ds(int debug){
  map<TString, TH2D*> objects;
  TDirectory *current_dir = gDirectory;
  TIter nextkey(current_dir->GetListOfKeys());
  TKey *key, *oldkey=0;
  while (( key = (TKey*)nextkey())){
    if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
    TObject *obj=key->ReadObj();
    if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
    if (obj->IsA()->InheritsFrom(TH2D::Class())){
      if (debug > 1) cout << "it is a TH1F object"  << endl;
      objects[obj->GetName()]=dynamic_cast<TH2D*>(current_dir->Get(obj->GetName()));
      objects[obj->GetName()]->SetTitle("");
      //objects[obj->GetName()]->Sumw2();
    }
    delete obj;
  }
  return(objects);
}

map<TString, TProfile*> GetTProfiles(int debug){
  map<TString, TProfile*> objects;
  TDirectory *current_dir = gDirectory;
  TIter nextkey(current_dir->GetListOfKeys());
  TKey *key, *oldkey=0;
  while (( key = (TKey*)nextkey())){
    if (oldkey && !strcmp(oldkey->GetName(),key->GetName())) continue;
    TObject *obj=key->ReadObj();
    if (debug > 1) cout <<  "found object: " << obj->GetName() << endl;
    if (obj->IsA()->InheritsFrom(TProfile::Class())){
      if (debug > 1) cout << "it is a TH1F object"  << endl;
      objects[obj->GetName()]=dynamic_cast<TProfile*>(current_dir->Get(obj->GetName()));
      objects[obj->GetName()]->SetTitle("");
      //objects[obj->GetName()]->Sumw2();
    }
    delete obj;
  }
  return(objects);
}

/*
Double_t invert(Double_t *x, Double_t *par){
  if (fabs(x[1])<1e-8) return(1e8);
  else return(1/x[1]);
}
*/


string convertNumberToString(double number, int nDigits){
  stringstream ss;//create a stringstream
  
  if (nDigits>=0){
    if (number < 1e-6) return("0");
    int number_of_digits_before_dot=0;
    if (fabs(number)>=1){
      float result=number;
      while (fabs(result)>=1){
	result=result / 10.;
	number_of_digits_before_dot++;
      }
    }
    else{
      float result=number*10;
      while (fabs(result)<1){
	result=result * 10;
	number_of_digits_before_dot--;
      }
    }
    int rounding=nDigits-number_of_digits_before_dot;
    //cout << "rounding to " << rounding << " digits" << endl;
    number=floor(number*pow(10.,rounding)+0.5)/pow(10.,rounding);
    ss.precision(rounding);
  }
  
  ss << number;//add number to the stream
  return ss.str();//return a string with the contents of the stream
}


void Draw2Histograms(TH1* hist1, TH1* hist2, TString name1, TString name2, Float_t xmin, Float_t ymax, Float_t xmax, Float_t ymin){
  float yaxis_max=-10000;
  float yaxis_min=10000;
  for (int i=1;i<hist1->GetNbinsX()+1;++i){
    float ibincontent=hist1->GetBinContent(i);
    //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
    if (yaxis_max<ibincontent) yaxis_max=ibincontent;
    if (ibincontent>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
  }
  for (int i=1;i<hist2->GetNbinsX()+1;++i){
    float ibincontent=hist2->GetBinContent(i);
    //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
    if (yaxis_max<ibincontent) yaxis_max=ibincontent;
    if (ibincontent>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
  }

  yaxis_min=yaxis_min-(yaxis_max-yaxis_min)/15.;
  yaxis_max=yaxis_max+(yaxis_max-yaxis_min)/4.;
  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max <<  "  yaxis_min_hist2 " << yaxis_min_hist2 <<  "  yaxis_max_hist2 " << yaxis_max_hist2 << endl;
  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max << endl;
  hist1->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  hist1->SetTitle("");
  hist2->SetTitle("");
  hist1->SetMarkerStyle(20);
  hist1->SetMarkerColor(4);
  hist1->SetLineColor(4);
  hist1->SetLineWidth(2);
  hist2->SetMarkerStyle(22);
  hist2->SetMarkerColor(2);
  hist2->SetLineColor(2);
  hist2->SetLineWidth(2);
  TLegend *leg = new TLegend(xmin,ymax,xmax,ymin);
  leg->SetFillStyle(0);
  leg->AddEntry(hist1,name1);
  leg->AddEntry(hist2,name2);
  hist1->Draw("E1");
  hist2->Draw("E1same");
  leg->Draw(); 
}




void Draw3Histograms(TH1* hist1, TH1* hist2, TH1* hist3, TString name1, TString name2, TString name3, float yaxis_min, float yaxis_max, Float_t xmin, Float_t ymax, Float_t xmax, Float_t ymin){
  if (yaxis_min > yaxis_max){
    yaxis_max=-10000;
    yaxis_min=10000;
    for (int i=1;i<hist1->GetNbinsX()+1;++i){
      float ibincontent=hist1->GetBinContent(i);
      //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
      if (yaxis_max<ibincontent) yaxis_max=ibincontent;
      if (ibincontent>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
    }
    for (int i=1;i<hist2->GetNbinsX()+1;++i){
      float ibincontent=hist2->GetBinContent(i);
      //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
      if (yaxis_max<ibincontent) yaxis_max=ibincontent;
      if (ibincontent>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
    }
    for (int i=1;i<hist3->GetNbinsX()+1;++i){
      float ibincontent=hist3->GetBinContent(i);
      //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
      if (yaxis_max<ibincontent) yaxis_max=ibincontent;
      if (ibincontent>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
    }
    
    
    yaxis_min=yaxis_min-(yaxis_max-yaxis_min)/15.;
    yaxis_max=yaxis_max+(yaxis_max-yaxis_min)/3.;
  }

  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max <<  "  yaxis_min_hist2 " << yaxis_min_hist2 <<  "  yaxis_max_hist2 " << yaxis_max_hist2 << endl;
  cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max << endl;
  hist1->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  hist1->SetTitle("");
  hist2->SetTitle("");
  hist3->SetTitle("");
  hist1->SetMarkerStyle(20);
  hist1->SetMarkerColor(4);
  hist1->SetLineColor(4);
  hist1->SetLineWidth(2);
  hist2->SetMarkerStyle(22);
  hist2->SetMarkerColor(2);
  hist2->SetLineColor(2);
  hist2->SetLineWidth(2);
  hist3->SetMarkerStyle(25);
  hist3->SetMarkerColor(419);
  hist3->SetLineColor(419);
  hist3->SetLineWidth(2);
  TLegend *leg = new TLegend(xmin,ymax,xmax,ymin);
  leg->SetFillStyle(0);
  leg->AddEntry(hist1,name1);
  leg->AddEntry(hist2,name2);
  leg->AddEntry(hist3,name3);
  hist1->Draw("E1");
  hist2->Draw("E1same");
  hist3->Draw("E1same");
  leg->Draw(); 
}


void PrintCanvas(TCanvas *can,TString sampleDir, TString filename,Bool_t logy){
//  TVirtualPad *currentPad=gPad;
  if (logy){
    can->SetLogy(); 
    can->SaveAs(sampleDir+"pdf/"+filename+"_log.pdf");
  //  can->SaveAs(sampleDir+"png/"+filename+"_log.png");
  //  can->SaveAs(sampleDir+"C/"+filename+"_log.C");
  //  can->SaveAs(sampleDir+"root/"+filename+"_log.root");
    can->SetLogy(0); 
  }
  else{
    can->SaveAs(sampleDir+"pdf/"+filename+".pdf");
  //  can->SaveAs(sampleDir+"png/"+filename+".png");
    //cout << "png saved" << endl;
  //  can->SaveAs(sampleDir+"C/"+filename+".C");
  //  can->SaveAs(sampleDir+"root/"+filename+".root");
  }

}


TString FindRange(TString range){
  range.Remove(0,range.Index("-")-4);
  if (range.Length()>9) range.Remove(range.Index("-")+5,range.Length());
  for (int istring=range.Length()-1;istring>-1;--istring){
    TString letter=range[istring];
    if (!(letter.IsDigit() || letter=="-")) range.Remove(istring,1);
  }

  range.ReplaceAll("-",",");
  cout << "range: " << range << endl;
  return(range);
}


void ATLASLabel(Double_t x,Double_t y,const char* text, float tsize, Color_t color){
  TLatex l; //l.SetTextAlign(12);
  if (tsize>0) l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  //double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());
  double delx = 0.14;

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    if (tsize>0) p.SetTextSize(tsize); 
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}


void WriteGeneralInfo(TString cut_label, TString lumi, float size, float x, float y){
  TString label="";
  if (lumi=="") label+="  Simulation";
  label+=" Internal";
  //  label+="  Preliminary";
  ATLASLabel(x,y,label.Data(),size*1.15);
  TString ToWrite="";
  TLatex l;
  l.SetNDC();
  l.SetTextFont(42);
  l.SetTextSize(size*0.9); 
  l.SetTextColor(1);
  l.DrawLatex(x-0.005,y-0.07,cut_label.Data());

  double shift=0.2;
  ToWrite="#sqrt{s}=13 TeV";
  if (lumi!=""){
    //ToWrite="L_{int}=";
    ToWrite+=", ";
    ToWrite+=lumi;
    ToWrite+=" fb^{-1}";
    shift=0.43;
  }
  l.DrawLatex(x+shift,y,ToWrite.Data());
}



void WriteInfo(TString info, float size, float x, float y, int color){
  TLatex l;
  l.SetNDC();
  l.SetTextFont(42);
  l.SetTextSize(size); 
  l.SetTextColor(color);
  l.DrawLatex(x,y,info.Data());
}


float round(float d, int n){
  return floor(pow(10,n)*d + 0.5)/(float)pow(10,n);
}


void NormalizeColumns(TH2D *hist, bool use_overlow_underflow){
  int startingBin=0;
  int additionalBins=2;
  if (!use_overlow_underflow){
    startingBin=1;
    additionalBins=1;
  }

  TH2D* total=(TH2D*)hist->Clone();
  total->GetZaxis()->SetTitle("normalized number of entries in columns");
  for (int i=startingBin;i<hist->GetNbinsX()+additionalBins;++i){
    double sum=0;
    double squaredSum=0;
    for (int j=startingBin;j<hist->GetNbinsY()+additionalBins;++j){
      if (hist->GetBinContent(i,j) < 0)  hist->SetBinContent(i,j,0);  // setting to 0 the negative bins!
      sum+=hist->GetBinContent(i,j);
      squaredSum+=pow(hist->GetBinError(i,j),2.);
    }
    //if (sum < 1e-5) continue;
    for (int j=startingBin;j<hist->GetNbinsY()+additionalBins;++j){
      total->SetBinContent(i,j,sum);
      total->SetBinError(i,j,sqrt(squaredSum));
      //hist->SetBinContent(i,j,hist->GetBinContent(i,j)/sum);
      //hist->SetBinError(i,j,hist->GetBinError(i,j)/sum);
    }
  }

  hist->GetZaxis()->SetTitle("normalized number of entries in columns");
  hist->Divide(hist,total,1,1,"b");

}


void NormalizeRows(TH2 *hist){
  for (int irow=0;irow<hist->GetNbinsY()+2;++irow){
    float sum=0;
    for (int icolumn=0;icolumn<hist->GetNbinsX()+2;++icolumn){
      if (hist->GetBinContent(icolumn,irow) < 0)  hist->SetBinContent(icolumn,irow,0);  // setting to 0 the negative bins!
      sum+=hist->GetBinContent(icolumn,irow);
    }
    if (sum < 1e-5) continue;
    for (int icolumn=0;icolumn<hist->GetNbinsX()+2;++icolumn){
      hist->SetBinContent(icolumn,irow,hist->GetBinContent(icolumn,irow)/sum);
      hist->SetBinError(icolumn,irow,hist->GetBinError(icolumn,irow)/sum);
    }
  }
  hist->GetZaxis()->SetTitle("normalized number of entries in rows");
}


void Normalize(TH1 *hist){
  hist->Scale(1./hist->Integral(0,-1));
  hist->GetYaxis()->SetTitle("Arbitrary Normalization");
}




void DrawMCvsData(TH1* mc, TH1* data, Float_t xmin, Float_t ymax, Float_t xmax, Float_t ymin){
  float yaxis_max=-10000;
  float yaxis_min=10000;
  for (int i=1;i<mc->GetNbinsX()+1;++i){
    float ibincontent=mc->GetBinContent(i);
    //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
    if (yaxis_max<ibincontent) yaxis_max=ibincontent;
    if (ibincontent>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
  }
  for (int i=1;i<data->GetNbinsX()+1;++i){
    float ibincontent=data->GetBinContent(i);
    //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
    if (yaxis_max<ibincontent) yaxis_max=ibincontent;
    if (ibincontent>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
  }

  yaxis_min=yaxis_min-(yaxis_max-yaxis_min)/15.;
  yaxis_max=yaxis_max+(yaxis_max-yaxis_min)/20.;
  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max <<  "  yaxis_min_data " << yaxis_min_data <<  "  yaxis_max_data " << yaxis_max_data << endl;
  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max << endl;
  mc->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc->SetTitle("");
  data->SetTitle("");
  mc->SetMarkerStyle(0);
  mc->SetMarkerColor(2);
  mc->SetLineColor(2);
  mc->SetFillColor(2);
  mc->SetLineWidth(1);
  data->SetMarkerStyle(20);
  data->SetMarkerColor(1);
  data->SetLineColor(1);
  data->SetLineWidth(1);
  TLegend *leg = new TLegend(xmin,ymax,xmax,ymin);
  leg->SetFillStyle(0);
  leg->AddEntry(mc,"MC");
  leg->AddEntry(data,"Data");
  mc->Draw("E1h");
  data->Draw("E1same");
  leg->Draw(); 
}



vector<TObject*> DrawMCvsData_correction(TPad *pad1, TPad *pad2, TPad *pad3, TPad *pad4, TH1* mc_reco, TH1* mc_corr, TH1* mc_truth, TH1* data_reco, TH1* data_corr, TString option){
  pad1->cd();
  //cout << "drawing pad1" << endl;

  float yaxis_max=-10000;
  float yaxis_min=10000;
  //cout << "changing y range" << endl;
  ChangeYRange(mc_reco, yaxis_min, yaxis_max);
  ChangeYRange(mc_corr, yaxis_min, yaxis_max);
  ChangeYRange(mc_truth, yaxis_min, yaxis_max);
  ChangeYRange(data_reco, yaxis_min, yaxis_max);
  ChangeYRange(data_corr, yaxis_min, yaxis_max);
  yaxis_min=yaxis_min-(yaxis_max-yaxis_min)/15.;
  yaxis_max=yaxis_max+(yaxis_max-yaxis_min)/3.;
  cout << "y range changed" << endl;

  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max <<  "  yaxis_min_data " << yaxis_min_data <<  "  yaxis_max_data " << yaxis_max_data << endl;
  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max << endl;
  mc_reco->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc_reco->SetTitle("");
  mc_reco->SetMarkerStyle(0);
  mc_reco->SetMarkerColor(4);
  mc_reco->SetLineColor(4);
  //mc_reco->SetFillColor(4);
  mc_reco->SetLineWidth(2);
  mc_corr->SetMarkerStyle(0);
  mc_corr->SetMarkerColor(2);
  mc_corr->SetLineColor(2);
  //mc_corr->SetFillColor(2);
  mc_corr->SetLineWidth(2);
  mc_truth->SetMarkerStyle(0);
  mc_truth->SetMarkerColor(419);
  mc_truth->SetLineColor(419);
  //mc_truth->SetFillColor(419);
  mc_truth->SetLineWidth(2);
  data_reco->SetMarkerStyle(20);
  data_reco->SetMarkerColor(4);
  data_reco->SetLineColor(4);
  data_reco->SetLineWidth(1);
  data_corr->SetMarkerStyle(22);
  data_corr->SetMarkerColor(2);
  data_corr->SetLineColor(2);
  data_corr->SetLineWidth(1);
  TLegend *leg1 = new TLegend(0.78,0.89,0.94,0.55);
  leg1->SetFillStyle(0);
  leg1->AddEntry(mc_reco,"MC reco","le");
  leg1->AddEntry(mc_corr,"MC corr","le");
  leg1->AddEntry(mc_truth,"MC truth","le");
  leg1->AddEntry(data_reco,"Data reco","p");
  leg1->AddEntry(data_corr,"Data corr","p");
  mc_reco->Draw("E1"+option);
  mc_corr->Draw("E1same"+option);
  mc_truth->Draw("E1same"+option);
  data_reco->Draw("E1same");
  data_corr->Draw("E1same");
  leg1->DrawClone();

  pad2->cd();
  //cout << "drawing pad2" << endl;
  TH1* data_over_mc_reco=0;
  TH1* data_over_mc_corr=0;
  if (data_reco->IsA()->InheritsFrom(TProfile::Class())){
    cout << "it is a TProfile" << endl;
    TProfile* data_over_mc_reco_prof=dynamic_cast<TProfile*>(data_reco->Clone("reco"));
    TProfile* data_over_mc_corr_prof=dynamic_cast<TProfile*>(data_corr->Clone("corr"));
    data_over_mc_reco=data_over_mc_reco_prof->ProjectionX();
    data_over_mc_corr=data_over_mc_corr_prof->ProjectionX();
    delete data_over_mc_corr_prof;
    delete data_over_mc_reco_prof;
  }  
  else{
    cout << "it is not a TProfile" << endl;
    data_over_mc_reco=(TH1*)data_reco->Clone("reco");
    data_over_mc_corr=(TH1*)data_corr->Clone("corr");
  }  

  //TH1* data_over_mc_corr=;
  data_over_mc_reco->Divide(mc_reco);
  data_over_mc_corr->Divide(mc_corr);
  //cout << "data_reco bin5 content: " << data_reco->GetBinContent(5) << endl;
  //cout << "data_reco bin5 error: " << data_reco->GetBinError(5) << endl;
  //cout << "mc_reco bin5 content: " << mc_reco->GetBinContent(5) << endl;
  //cout << "mc_reco bin5 error: " << mc_reco->GetBinError(5) << endl;
  //cout << "data_over_mc_reco bin5 content: " << data_over_mc_reco->GetBinContent(5) << endl;
  //cout << "data_over_mc_reco bin5 error: " << data_over_mc_reco->GetBinError(5) << endl;

  yaxis_max=1.4;
  yaxis_min=0.71;
  //yaxis_max=-10000;
  //yaxis_min=10000;
  //ChangeYRange(data_over_mc_reco, yaxis_min, yaxis_max);
  //ChangeYRange(data_over_mc_corr, yaxis_min, yaxis_max);
  data_over_mc_reco->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  data_over_mc_reco->SetYTitle("Data / MC");
  data_over_mc_reco->SetTitle("");
  data_over_mc_reco->SetMarkerStyle(20);
  data_over_mc_reco->SetMarkerColor(4);
  data_over_mc_reco->SetLineColor(4);
  data_over_mc_reco->SetLineWidth(2);
  data_over_mc_reco->GetYaxis()->SetTitleSize(0.12);
  data_over_mc_reco->GetYaxis()->SetLabelSize(0.12);
  data_over_mc_reco->GetYaxis()->SetTitleOffset(0.35);

  data_over_mc_corr->SetMarkerStyle(22);
  data_over_mc_corr->SetMarkerColor(2);
  data_over_mc_corr->SetLineColor(2);
  data_over_mc_corr->SetLineWidth(2);

  TLegend *leg2 = new TLegend(0.78,0.99,1,0.55);
  leg2->SetFillStyle(0);
  leg2->AddEntry(data_over_mc_reco,"reco","pl");
  leg2->AddEntry(data_over_mc_corr,"corr","pl");
  data_over_mc_reco->Draw("E1");
  data_over_mc_corr->Draw("E1same");
  leg2->Draw();

  pad3->cd();
  //cout << "drawing pad3" << endl;
  TH1* mc_corr_over_truth=(TH1*)mc_corr->Clone("mc");
  TH1* data_corr_over_truth=(TH1*)data_corr->Clone("data");
  TH1* truth_rel_error=0;
  if (mc_truth->IsA()->InheritsFrom(TProfile::Class())){
    TProfile* truth_rel_error_prof=dynamic_cast<TProfile*>(mc_truth->Clone("truth_rel_error"));
    truth_rel_error=truth_rel_error_prof->ProjectionX();
    delete truth_rel_error_prof;
  }
  else{
    truth_rel_error=(TH1*)mc_truth->Clone("truth_rel_error");
  }
  mc_corr_over_truth->Divide(mc_truth);
  data_corr_over_truth->Divide(mc_truth);
  for(int ibin=1;ibin<truth_rel_error->GetNbinsX()+1;++ibin){
    truth_rel_error->SetBinContent(ibin,1);
    truth_rel_error->SetBinError(ibin,mc_truth->GetBinError(ibin)/mc_truth->GetBinContent(ibin));
  }
  yaxis_max=2;
  yaxis_min=0.5;
  //ChangeYRange(mc_corr_over_truth, yaxis_min, yaxis_max);
  //ChangeYRange(data_corr_over_truth, yaxis_min, yaxis_max);
  truth_rel_error->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  truth_rel_error->SetYTitle("");
  truth_rel_error->SetTitle("");
  truth_rel_error->SetMarkerStyle(0);
  truth_rel_error->SetMarkerColor(5);
  truth_rel_error->SetLineColor(319);
  truth_rel_error->SetFillColor(5);
  truth_rel_error->SetLineWidth(1);
  truth_rel_error->GetXaxis()->SetTitle(mc_reco->GetXaxis()->GetTitle());
  truth_rel_error->GetXaxis()->SetLabelSize(0.10);
  truth_rel_error->GetXaxis()->SetTitleSize(0.13);
  truth_rel_error->GetYaxis()->SetTitleSize(0.1);
  truth_rel_error->GetYaxis()->SetLabelSize(0.1);
  truth_rel_error->GetYaxis()->SetTitleOffset(0.4);
  mc_corr_over_truth->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc_corr_over_truth->SetTitle("");
  mc_corr_over_truth->SetMarkerStyle(0);
  mc_corr_over_truth->SetMarkerColor(597);
  mc_corr_over_truth->SetLineColor(597);
  mc_corr_over_truth->SetLineWidth(2);
  data_corr_over_truth->SetMarkerStyle(22);
  data_corr_over_truth->SetMarkerColor(629);
  data_corr_over_truth->SetLineColor(629);
  data_corr_over_truth->SetLineWidth(2);
  TLegend *leg3 = new TLegend(0.25,0.99,1,0.5);
  leg3->SetFillStyle(0);
  leg3->AddEntry(mc_corr_over_truth,"MC corr / MC truth","pl");
  leg3->AddEntry(data_corr_over_truth,"Data corr / MC truth","pl");
  leg3->AddEntry(truth_rel_error,"rel unc of truth","fl");
  truth_rel_error->DrawClone("e3");
  truth_rel_error->SetFillColor(0);
  truth_rel_error->DrawClone("HISTsame");
  truth_rel_error->SetFillColor(5);
  mc_corr_over_truth->Draw("histlsame");
  data_corr_over_truth->Draw("histlsame");
  leg3->Draw();
  
  pad4->cd();
  //cout << "drawing pad4" << endl;

  TH1* mc_reco_over_corr=(TH1*)mc_reco->Clone("mc");
  TH1* data_reco_over_corr=(TH1*)data_reco->Clone("data");
  mc_reco_over_corr->Divide(mc_corr);
  data_reco_over_corr->Divide(data_corr);
  yaxis_max=10;
  yaxis_min=0.1;
  //cout << "changing style" << endl;

  mc_reco_over_corr->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc_reco_over_corr->SetYTitle("reco / corr");
  mc_reco_over_corr->SetTitle("");
  mc_reco_over_corr->SetMarkerStyle(0);
  mc_reco_over_corr->SetMarkerColor(435);
  mc_reco_over_corr->SetLineColor(435);
  mc_reco_over_corr->SetLineWidth(2);
  mc_reco_over_corr->GetXaxis()->SetLabelSize(0.10);
  mc_reco_over_corr->GetXaxis()->SetTitleSize(0.13);
  mc_reco_over_corr->GetYaxis()->SetTitleSize(0.1);
  mc_reco_over_corr->GetYaxis()->SetLabelSize(0.1);
  mc_reco_over_corr->GetYaxis()->SetTitleOffset(0.4);
  data_reco_over_corr->SetMarkerStyle(22);
  data_reco_over_corr->SetMarkerColor(612);
  data_reco_over_corr->SetLineColor(612);
  data_reco_over_corr->SetLineWidth(2);
  TLegend *leg4 = new TLegend(0.78,0.99,1,0.6);
  leg4->SetFillStyle(0);
  leg4->AddEntry(mc_reco_over_corr,"MC","l");
  leg4->AddEntry(data_reco_over_corr,"Data","pl");
  mc_reco_over_corr->Draw("histl");
  data_reco_over_corr->Draw("histlsame");
  //cout << " drawing legend" << endl;
  leg4->Draw();
  // cout << "legend drawn" << endl;


  vector<TObject*> vectorOfObjects;
  vectorOfObjects.push_back(leg1);
  vectorOfObjects.push_back(leg2);
  vectorOfObjects.push_back(leg3);
  vectorOfObjects.push_back(leg4);
  vectorOfObjects.push_back(data_over_mc_corr);
  vectorOfObjects.push_back(data_over_mc_reco);
  vectorOfObjects.push_back(mc_corr_over_truth);
  vectorOfObjects.push_back(data_corr_over_truth);
  vectorOfObjects.push_back(truth_rel_error);
  vectorOfObjects.push_back(data_reco_over_corr);
  vectorOfObjects.push_back(mc_reco_over_corr);
  return vectorOfObjects;
}


vector<TObject*> DrawMCvsData_shapeTProfiles(TPad *pad1, TPad *pad2, TPad *pad3, TH1* mc_reco, TH1* mc_corr, TH1* mc_truth, TH1* data_reco, TH1* data_corr, TString option, float xaxis_min, float xaxis_max, float yaxis_min, float yaxis_max){
  pad1->cd();
  //cout << "drawing pad1" << endl;

  if (yaxis_min>yaxis_max){
    ChangeYRange(mc_reco, yaxis_min, yaxis_max);
    ChangeYRange(mc_corr, yaxis_min, yaxis_max);
    ChangeYRange(mc_truth, yaxis_min, yaxis_max);
    ChangeYRange(data_reco, yaxis_min, yaxis_max);
    ChangeYRange(data_corr, yaxis_min, yaxis_max);
    yaxis_min=yaxis_min-(yaxis_max-yaxis_min)/8.;
    yaxis_max=yaxis_max+(yaxis_max-yaxis_min)/0.8;
  }

  if (xaxis_min<xaxis_max){
    mc_reco->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
    mc_corr->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
    mc_truth->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
    data_reco->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
    data_corr->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
  }
  mc_reco->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc_reco->GetYaxis()->SetTitleOffset(1.15);
  mc_reco->GetYaxis()->SetTitleSize(0.07);
  mc_reco->GetYaxis()->SetLabelSize(0.07);
  mc_reco->SetTitle("");
  mc_reco->SetMarkerStyle(24);
  mc_reco->SetMarkerColor(4);
  mc_reco->SetMarkerSize(1.8);
  mc_reco->SetLineColor(4);
  //mc_reco->SetFillColor(4);
  mc_reco->SetLineWidth(2);
  mc_corr->SetMarkerStyle(26);
  mc_corr->SetMarkerSize(1.8);
  mc_corr->SetMarkerColor(2);
  mc_corr->SetLineColor(2);
  //mc_corr->SetFillColor(2);
  mc_corr->SetLineWidth(2);
  mc_truth->SetMarkerStyle(25);
  mc_truth->SetMarkerSize(1.8);
  mc_truth->SetMarkerColor(419);
  mc_truth->SetLineColor(419);
  //mc_truth->SetFillColor(419);
  mc_truth->SetLineWidth(2);
  data_reco->SetMarkerStyle(20);
  data_reco->SetMarkerSize(1.8);
  data_reco->SetMarkerColor(4);
  data_reco->SetLineColor(4);
  data_reco->SetLineWidth(1);
  data_corr->SetMarkerStyle(22);
  data_corr->SetMarkerSize(1.8);
  data_corr->SetMarkerColor(2);
  data_corr->SetLineColor(2);
  data_corr->SetLineWidth(1);
  TLegend *leg1 = new TLegend(0.55,0.81,0.95,0.5);
  leg1->SetFillStyle(0);
  leg1->AddEntry(mc_reco,"MC: Uncorrected","lep");
  leg1->AddEntry(mc_corr,"MC: Corrected","lep");
  leg1->AddEntry(data_reco,"Data: Uncorrected","lep");
  leg1->AddEntry(data_corr,"Data: Corrected","lep");
  leg1->AddEntry(mc_truth,"Truth-particle jets","lep");
  mc_reco->Draw("E1"+option);
  mc_corr->Draw("E1same"+option);
  mc_truth->Draw("E1same"+option);
  data_reco->Draw("E1same");
  data_corr->Draw("E1same");
  leg1->DrawClone();

  pad2->cd();
  //cout << "drawing pad2" << endl;
  TH1* data_over_mc_reco=0;
  TH1* data_over_mc_corr=0;
  if (data_reco->IsA()->InheritsFrom(TProfile::Class())){
    cout << "it is a TProfile" << endl;
    TProfile* data_over_mc_reco_prof=dynamic_cast<TProfile*>(data_reco->Clone("reco"));
    TProfile* data_over_mc_corr_prof=dynamic_cast<TProfile*>(data_corr->Clone("corr"));
    data_over_mc_reco=data_over_mc_reco_prof->ProjectionX();
    data_over_mc_corr=data_over_mc_corr_prof->ProjectionX();
    delete data_over_mc_corr_prof;
    delete data_over_mc_reco_prof;
  }  
  else{
    cout << "it is not a TProfile" << endl;
    data_over_mc_reco=(TH1*)data_reco->Clone("reco");
    data_over_mc_corr=(TH1*)data_corr->Clone("corr");
  }  

  //TH1* data_over_mc_corr=;
  data_over_mc_reco->Divide(mc_reco);
  data_over_mc_corr->Divide(mc_corr);
  if (xaxis_min<xaxis_max){
    data_over_mc_reco->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
    data_over_mc_corr->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
  }

  yaxis_max=1.14;
  yaxis_min=0.86;
  //yaxis_max=-10000;
  //yaxis_min=10000;
  //ChangeYRange(data_over_mc_reco, yaxis_min, yaxis_max);
  //ChangeYRange(data_over_mc_corr, yaxis_min, yaxis_max);
  data_over_mc_reco->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  data_over_mc_reco->SetYTitle("Data / MC");
  data_over_mc_reco->SetTitle("");
  data_over_mc_reco->SetMarkerStyle(20);
  data_over_mc_reco->SetMarkerColor(4);
  data_over_mc_reco->SetLineColor(4);
  data_over_mc_reco->SetLineWidth(2);
  data_over_mc_reco->GetYaxis()->SetTitleSize(0.22);
  data_over_mc_reco->GetYaxis()->SetLabelSize(0.22);
  data_over_mc_reco->GetYaxis()->SetTitleOffset(0.35);

  data_over_mc_corr->SetMarkerStyle(22);
  data_over_mc_corr->SetMarkerColor(2);
  data_over_mc_corr->SetLineColor(2);
  data_over_mc_corr->SetLineWidth(2);

  TLegend *leg2 = new TLegend(0.5,1,1,0.52);
  leg2->SetFillStyle(0);
  leg2->AddEntry(data_over_mc_reco,"Uncorrected","pl");
  leg2->AddEntry(data_over_mc_corr,"Corrected","pl");
  data_over_mc_reco->Draw("E1");
  data_over_mc_corr->Draw("E1same");
  //leg2->Draw();

  pad3->cd();
  //cout << "drawing pad3" << endl;
  TH1* mc_corr_over_truth=(TH1*)mc_corr->Clone("mc");
  TH1* data_corr_over_truth=(TH1*)data_corr->Clone("data");
  TH1* truth_rel_error=0;
  if (mc_truth->IsA()->InheritsFrom(TProfile::Class())){
    TProfile* truth_rel_error_prof=dynamic_cast<TProfile*>(mc_truth->Clone("truth_rel_error"));
    truth_rel_error=truth_rel_error_prof->ProjectionX();
    delete truth_rel_error_prof;
  }
  else{
    truth_rel_error=(TH1*)mc_truth->Clone("truth_rel_error");
  }
  mc_corr_over_truth->Divide(mc_truth);
  data_corr_over_truth->Divide(mc_truth);
  for(int ibin=1;ibin<truth_rel_error->GetNbinsX()+1;++ibin){
    truth_rel_error->SetBinContent(ibin,1);
    truth_rel_error->SetBinError(ibin,mc_truth->GetBinError(ibin)/mc_truth->GetBinContent(ibin));
  }
  if (xaxis_min<xaxis_max){
    truth_rel_error->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
    mc_corr_over_truth->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
    data_corr_over_truth->GetXaxis()->SetRangeUser(xaxis_min,xaxis_max);
  }

  yaxis_max=1.29;
  yaxis_min=0.71;
  //ChangeYRange(mc_corr_over_truth, yaxis_min, yaxis_max);
  //ChangeYRange(data_corr_over_truth, yaxis_min, yaxis_max);
  truth_rel_error->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  truth_rel_error->SetYTitle("");
  truth_rel_error->SetTitle("");
  truth_rel_error->SetMarkerStyle(0);
  truth_rel_error->SetMarkerColor(5);
  truth_rel_error->SetLineColor(319);
  truth_rel_error->SetFillColor(5);
  truth_rel_error->SetLineWidth(1);
  truth_rel_error->GetXaxis()->SetTitle(mc_reco->GetXaxis()->GetTitle());
  truth_rel_error->GetXaxis()->SetLabelSize(0.15);
  truth_rel_error->GetXaxis()->SetTitleSize(0.15);
  truth_rel_error->GetXaxis()->SetTitleOffset(1.1);
  truth_rel_error->GetYaxis()->SetTitleSize(0.15);
  truth_rel_error->GetYaxis()->SetLabelSize(0.15);
  truth_rel_error->GetYaxis()->SetTitleOffset(0.4);

  mc_corr_over_truth->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc_corr_over_truth->SetYTitle("");
  mc_corr_over_truth->SetTitle("");
  mc_corr_over_truth->GetXaxis()->SetTitle(mc_reco->GetXaxis()->GetTitle());
  mc_corr_over_truth->GetXaxis()->SetLabelSize(0.15);
  mc_corr_over_truth->GetXaxis()->SetTitleSize(0.15);
  mc_corr_over_truth->GetXaxis()->SetTitleOffset(1.1);
  mc_corr_over_truth->GetYaxis()->SetTitleSize(0.15);
  mc_corr_over_truth->GetYaxis()->SetLabelSize(0.15);
  mc_corr_over_truth->GetYaxis()->SetTitleOffset(0.4);


  mc_corr_over_truth->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc_corr_over_truth->SetTitle("");
  mc_corr_over_truth->SetMarkerStyle(0);
  mc_corr_over_truth->SetMarkerColor(597);
  mc_corr_over_truth->SetLineColor(597);
  mc_corr_over_truth->SetLineWidth(2);
  data_corr_over_truth->SetMarkerStyle(22);
  data_corr_over_truth->SetMarkerColor(629);
  data_corr_over_truth->SetLineColor(629);
  data_corr_over_truth->SetLineWidth(2);
  TLegend *leg3 = new TLegend(0.3,0.99,1,0.75);
  leg3->SetFillStyle(0);
  leg3->AddEntry(mc_corr_over_truth,"MC Corrected / Truth-particle jets","l");
  leg3->AddEntry(data_corr_over_truth,"Data Corrected / Truth-particle jets","l");
  //leg3->AddEntry(truth_rel_error,"rel unc of truth","fl");
  //truth_rel_error->DrawClone("e3");
  //truth_rel_error->SetFillColor(0);
  //truth_rel_error->SetLineColor(319);
  //truth_rel_error->DrawClone("HISTsame");
  //truth_rel_error->SetFillColor(5);
  mc_corr_over_truth->Draw("histl");
  data_corr_over_truth->Draw("histlsame");
  leg3->Draw();
  
  vector<TObject*> vectorOfObjects;
  vectorOfObjects.push_back(leg1);
  vectorOfObjects.push_back(leg2);
  vectorOfObjects.push_back(leg3);
  vectorOfObjects.push_back(data_over_mc_corr);
  vectorOfObjects.push_back(data_over_mc_reco);
  vectorOfObjects.push_back(mc_corr_over_truth);
  vectorOfObjects.push_back(data_corr_over_truth);
  vectorOfObjects.push_back(truth_rel_error);
  return vectorOfObjects;
}




void DeletePointers(vector<TObject*> vec){
  for (int i=0;i<(int)vec.size();++i){
    delete vec[i];
  }
}

void ChangeYRange(TH1* hist, float &yaxis_min, float &yaxis_max){
  for (int i=1;i<hist->GetNbinsX()+1;++i){
    float ibincontent=hist->GetBinContent(i);
    //cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
    if (yaxis_max<ibincontent) yaxis_max=ibincontent;
    if (fabs(ibincontent)>1e-5 && yaxis_min>ibincontent) yaxis_min=ibincontent;
  }
}



TH1F* GetNPVHistogram(TH2F* hist2D, int iNPV, int cummulativeFactor, TString name){
  name+=hist2D->GetName();
  int nBins=hist2D->GetNbinsY();
  TH1F* hist=new TH1F(name,"",nBins,hist2D->GetYaxis()->GetBinLowEdge(1),hist2D->GetYaxis()->GetBinUpEdge(nBins));
  // cout << "nbins: " << hist2D->GetNbinsY() << endl;
  // cout << "low: " << hist2D->GetYaxis()->GetBinLowEdge(1) << endl;
  //  cout << "up: " << hist2D->GetYaxis()->GetBinLowEdge(-1) << endl;
  for (int i=0;i<hist2D->GetNbinsY()+2;++i){
    float sum=0;
    float sumWeights=0;
    for (int j=iNPV;j<iNPV+cummulativeFactor;++j){
      sum+=hist2D->GetBinContent(j,i);
      sumWeights+=hist2D->GetBinError(j,i)*hist2D->GetBinError(j,i);
    }
    if (sum>0){
      hist->SetBinContent(i,sum);
      hist->SetBinError(i,sqrt(sumWeights));
    }
  }

  hist->SetXTitle(hist2D->GetYaxis()->GetTitle());
  return hist;
}



TH1F* Get1DHistogramFrom2D(TH2F* hist2D, int low_int, int up_int, TString name){
  float low=low_int+0.0001;
  float up=up_int-0.0001;
  name+=hist2D->GetName();
  int nBins=hist2D->GetNbinsY();
  TH1F* hist=new TH1F(name,"",nBins,hist2D->GetYaxis()->GetBinLowEdge(1),hist2D->GetYaxis()->GetBinUpEdge(nBins));
  // cout << "nbins: " << hist2D->GetNbinsY() << endl;
  // cout << "low: " << hist2D->GetYaxis()->GetBinLowEdge(1) << endl;
  //  cout << "up: " << hist2D->GetYaxis()->GetBinLowEdge(-1) << endl;
  for (int i=0;i<hist2D->GetNbinsY()+2;++i){
    float sum=0;
    float sumWeights=0;
    //cout << "i: " << i << endl;
    for (int j=hist2D->GetXaxis()->FindBin(low);j<hist2D->GetXaxis()->FindBin(up)+1;++j){
      sum+=hist2D->GetBinContent(j,i);
      sumWeights+=hist2D->GetBinError(j,i)*hist2D->GetBinError(j,i);
    }
    if (sum>0){
      hist->SetBinContent(i,sum);
      hist->SetBinError(i,sqrt(sumWeights));
    }
  }

  hist->SetXTitle(hist2D->GetYaxis()->GetTitle());
  return hist;
}



void Draw4Histograms(TH1* mc_reco, TH1* mc_corr, TH1* data_reco, TH1* data_corr, TString mc_reco_string, TString mc_corr_string, TString data_reco_string, TString data_corr_string, Float_t xmin, Float_t ymax, Float_t xmax, Float_t ymin){
  float yaxis_max=-10000;
  float yaxis_min=10000;
  ChangeYRange(mc_reco, yaxis_min, yaxis_max);
  ChangeYRange(mc_corr, yaxis_min, yaxis_max);
  ChangeYRange(data_reco, yaxis_min, yaxis_max);
  ChangeYRange(data_corr, yaxis_min, yaxis_max);
  yaxis_min=yaxis_min-(yaxis_max-yaxis_min)/15.;
  yaxis_max=yaxis_max+(yaxis_max-yaxis_min)/4.;
  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max <<  "  yaxis_min_data " << yaxis_min_data <<  "  yaxis_max_data " << yaxis_max_data << endl;
  //cout << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max << endl;
  mc_reco->GetYaxis()->SetRangeUser(yaxis_min,yaxis_max);
  mc_reco->SetTitle("");
  mc_reco->SetMarkerStyle(0);
  mc_reco->SetMarkerColor(4);
  mc_reco->SetLineColor(4);
  mc_reco->SetFillColor(0);
  mc_reco->SetLineWidth(1);
  mc_corr->SetMarkerStyle(0);
  mc_corr->SetMarkerColor(2);
  mc_corr->SetLineColor(2);
  mc_corr->SetFillColor(0);
  mc_corr->SetLineWidth(1);
  data_reco->SetMarkerStyle(20);
  data_reco->SetMarkerColor(4);
  data_reco->SetLineColor(4);
  data_reco->SetLineWidth(1);
  data_corr->SetMarkerStyle(22);
  data_corr->SetMarkerColor(2);
  data_corr->SetLineColor(2);
  data_corr->SetLineWidth(1);
  TLegend *leg = new TLegend(xmin,ymax,xmax,ymin);
  leg->SetFillStyle(0);
  leg->AddEntry(mc_reco,mc_reco_string ,"le");
  leg->AddEntry(mc_corr,mc_corr_string,"le");
  leg->AddEntry(data_reco,data_reco_string,"p");
  leg->AddEntry(data_corr,data_corr_string,"p");
  mc_reco->Draw("E1h");
  mc_corr->Draw("E1hsame");
  data_reco->Draw("E1same");
  data_corr->Draw("E1same");
  leg->Draw(); 
}





void DrawTH1s(map<TString, TH1F*> mymap, Float_t xmin, Float_t xmax, Float_t ymin, Float_t ymax, Float_t xmin_legend, Float_t ymax_legend, Float_t xmax_legend, Float_t ymin_legend){
  TLegend *leg = new TLegend(xmin_legend,ymax_legend,xmax_legend,ymin_legend);
  leg->SetFillStyle(0);
  int i=0;
  TString option="";
  if (ymin >= ymax){
    ymax=-10000;
    ymin=10000;
    for (map<TString, TH1F*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
      ChangeYRange(it->second, ymin, ymax);
    }
    ymin=ymin+(ymax-ymin)/50.; //changed to remove 0
    ymax=ymax+(ymax-ymin)/1.5;
  }

  for (map<TString, TH1F*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
    TH1* hist=it->second;
    if (!hist) continue;
    if (xmin < xmax) hist->GetXaxis()->SetRangeUser(xmin,xmax);
    hist->GetYaxis()->SetRangeUser(ymin,ymax);
    i++;
    hist->SetTitle("");
    hist->SetMarkerStyle(21+i);
    if (i!=5 && i<10){
      hist->SetMarkerColor(i);
      hist->SetLineColor(i);
    }
    else{
      hist->SetMarkerColor(414+i);
      hist->SetLineColor(414+i);
    }
    hist->SetMarkerSize(1.0);
    hist->SetLineWidth(1);
    leg->AddEntry(hist,it->first,"p");
    hist->Draw(option);
    option="same";
  }
  leg->Draw(); 
}




TH1F* GetMedian(TH2* hist){
  int nBinsX=hist->GetNbinsX();
  TH1F* median=new TH1F("median","",nBinsX,hist->GetXaxis()->GetBinLowEdge(1),hist->GetXaxis()->GetBinUpEdge(nBinsX));
  median->Sumw2();
  for (int x=1;x<nBinsX+1;++x){
    //cout << "searching the median for x bin: " << x << endl;
    float total=0;
    for (int y=0;y<hist->GetNbinsY()+2;++y){
      total+=hist->GetBinContent(x,y);
    }
    if (total<1e-5) continue;
    float sum=0;
    for (int y=0;y<hist->GetNbinsY()+2;++y){
      sum+=hist->GetBinContent(x,y);
      if (sum>total/2.){
	median->SetBinContent(x,hist->GetYaxis()->GetBinCenter(y));
	median->SetBinError(x,hist->GetYaxis()->GetBinWidth(y)/2.);  
	//cout << "median found" << endl;
	break;
      }
    }
  }
  median->SetXTitle(hist->GetXaxis()->GetTitle());
  median->SetYTitle("median");
  return median;
}


TH1F* GetEfficiencyVSNPV(TH2* hist, bool larger, float cutValue, float& cutValue_real){
  int nBinsX=hist->GetNbinsX();
  TString name=hist->GetName();
  TH1F* eff=new TH1F(name+"_eff","",nBinsX,hist->GetXaxis()->GetBinLowEdge(1),hist->GetXaxis()->GetBinUpEdge(nBinsX));
  for (int x=1;x<nBinsX+1;++x){
    float total=0;
    for (int y=0;y<hist->GetNbinsY()+2;++y){
      total+=hist->GetBinContent(x,y);
    }
    if (total<1) continue;
    float sum=0;
    for (int y=0;y<hist->GetNbinsY()+2;++y){
      sum+=hist->GetBinContent(x,y);
      if (hist->GetYaxis()->GetBinUpEdge(y)>cutValue){
	cutValue_real=hist->GetYaxis()->GetBinUpEdge(y);
	float eff_value=0;
	float unc=0.;
	if (larger) eff_value=(total-sum)/total;
	else eff_value=sum/total;
	int total_int=(int)total;
	int sum_int=(int)sum;
	if (larger) unc=TEfficiency::Normal(total_int,total_int-sum_int,0.683,true)-eff_value;
	else  unc=TEfficiency::Normal(total_int,sum_int,0.683,true)-eff_value;
	eff->SetBinContent(x,eff_value);
	eff->SetBinError(x,unc);  
	break;
      }
    }
  }
  eff->SetXTitle(hist->GetXaxis()->GetTitle());
  eff->SetYTitle("tagging efficiency");
  return eff;
}




void DrawTwoPadsForHistos(TPad *pad1, TPad* pad2, map<TString, TH1D*> mymap, Bool_t logy, TString yaxisTitle_pad2, Float_t xmin, Float_t xmax, Float_t ymin, Float_t ymax, Float_t xmin_legend, Float_t ymax_legend, Float_t xmax_legend, Float_t ymin_legend){
  //cout << "changing to pad1" << endl;
  pad1->cd();
  if (logy) pad1->SetLogy();
  else pad1->SetLogy(0);
  bool changeXaxis=false;
  bool change_yaxis_title=(yaxisTitle_pad2=="");
  if (xmin < xmax) changeXaxis=true;
  TLegend *leg = new TLegend(xmin_legend,ymax_legend,xmax_legend,ymin_legend);
  int i=0;
  TString option="";
  if (ymin >= ymax){
    //cout << "searching for y range" << endl;
    ymax=-10000;
    ymin=10000;
    for (map<TString, TH1D*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
      TH1D* hist=it->second;
      if (!hist) continue;
      ChangeYRange(hist,ymin,ymax);
    }
  ymin=ymin-(ymax-ymin)/15.;
  ymax=ymax+(ymax-ymin)/1.5;
  }

  TH1D* graphFirst=0;

  for (map<TString, TH1D*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
    //cout << "preparing historam: " << it->first << endl;
    if (it->first == "truth") graphFirst=it->second;
    TH1D* graph=it->second;
    if (!graph) continue;
    if (changeXaxis) graph->GetXaxis()->SetRangeUser(xmin,xmax);
    graph->GetYaxis()->SetRangeUser(ymin,ymax);
    i++;
    graph->SetTitle("");
    graph->SetMarkerStyle(21+i);
    //graph->SetMarkerStyle(24);
    if (i!=5 && i<10){
      graph->SetMarkerColor(i);
      graph->SetLineColor(i);
    }
    else{
      graph->SetMarkerColor(414+i);
      graph->SetLineColor(414+i);
    }
    graph->SetMarkerSize(1.0);
    graph->SetLineWidth(1);
    leg->AddEntry(graph,it->first,"p");
    graph->Draw(option);
    option="same";
    if (change_yaxis_title){
      yaxisTitle_pad2=" / "+it->first;
      if (i==2) yaxisTitle_pad2=it->first+yaxisTitle_pad2;
    }
  }
  leg->SetLineColor(0);
  leg->SetShadowColor(0);
  leg->SetFillStyle(0);
  leg->Draw(); 

  pad2->cd();
  option="";
  for (map<TString, TH1D*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
    if (it->first == "truth" || it->first == "reco" ) continue;
    TH1D* graph=(TH1D*)it->second->Clone();
    if (!graph) continue;
    graph->Divide(graphFirst);    
    graph->GetYaxis()->SetRangeUser(0.8,1.2);
    if (changeXaxis) graph->GetXaxis()->SetRangeUser(xmin,xmax);
    graph->GetXaxis()->SetTitleSize(0.1);
    graph->SetMarkerSize(0.7);
    graph->GetYaxis()->SetTitleSize(0.07);
    graph->GetXaxis()->SetLabelSize(0.09);
    graph->GetYaxis()->SetLabelSize(0.08);
    graph->GetYaxis()->SetTitleOffset(0.5);
    graph->GetXaxis()->SetTitle(it->second->GetXaxis()->GetTitle());
    graph->GetYaxis()->SetTitle(yaxisTitle_pad2);
    graph->Draw(option);
    option="same";
    //delete graph;
  }
}



void DrawTwoPadsForProfiles(TPad *pad1, TPad* pad2, map<TString, TProfile*> mymap, Bool_t logy, TString yaxisTitle_pad2, Float_t xmin, Float_t xmax, Float_t ymin, Float_t ymax, Float_t xmin_legend, Float_t ymax_legend, Float_t xmax_legend, Float_t ymin_legend){
  //cout << "changing to pad1" << endl;
  pad1->cd();
  if (logy) pad1->SetLogy();
  else pad1->SetLogy(0);
  bool changeXaxis=false;
  bool change_yaxis_title=(yaxisTitle_pad2=="");
  if (xmin < xmax) changeXaxis=true;
  TLegend *leg = new TLegend(xmin_legend,ymax_legend,xmax_legend,ymin_legend);
  int i=0;
  TString option="";
  if (ymin >= ymax){
    //cout << "searching for y range" << endl;
    ymax=-10000;
    ymin=10000;
    for (map<TString, TProfile*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
      TProfile* hist=it->second;
      if (!hist) continue;
      ChangeYRange(hist,ymin,ymax);
    }
  ymin=ymin-(ymax-ymin)/5.;
  ymax=ymax+(ymax-ymin)/1.5;
  }

  TProfile* graphFirst=0;

  for (map<TString, TProfile*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
    //cout << "preparing historam: " << it->first << endl;
    if (it->first == "truth") graphFirst=it->second;
    TProfile* graph=it->second;
    if (!graph) continue;
    if (changeXaxis) graph->GetXaxis()->SetRangeUser(xmin,xmax);
    graph->GetYaxis()->SetRangeUser(ymin,ymax);
    i++;
    graph->SetTitle("");
    graph->SetMarkerStyle(21+i);
    //graph->SetMarkerStyle(24);
    if (i!=5 && i<10){
      graph->SetMarkerColor(i);
      graph->SetLineColor(i);
    }
    else{
      graph->SetMarkerColor(414+i);
      graph->SetLineColor(414+i);
    }
    graph->SetMarkerSize(1.0);
    graph->SetLineWidth(1);
    leg->AddEntry(graph,it->first,"p");
    graph->Draw(option);
    option="same";
    if (change_yaxis_title){
      yaxisTitle_pad2=" / "+it->first;
      if (i==2) yaxisTitle_pad2=it->first+yaxisTitle_pad2;
    }
  }
  leg->SetLineColor(0);
  leg->SetShadowColor(0);
  leg->SetFillStyle(0);
  leg->Draw(); 

  pad2->cd();
  option="P";
  for (map<TString, TProfile*>::iterator it=mymap.begin(); it!=mymap.end();++it ){
    if (it->first == "truth" || it->first == "reco" ) continue;
    TH1D* graph=it->second->ProjectionX();
    graph->SetName(it->first);
    if (!graph) continue;
    graph->Divide(graphFirst);    
    graph->GetYaxis()->SetRangeUser(0.81,1.19);
    if (changeXaxis) graph->GetXaxis()->SetRangeUser(xmin,xmax);
    graph->GetXaxis()->SetTitleSize(0.1);
    graph->SetMarkerStyle(it->second->GetMarkerStyle());
    graph->SetMarkerColor(it->second->GetMarkerColor());
    graph->SetMarkerSize(0.7);
    graph->SetLineColor(it->second->GetLineColor());
    graph->SetLineWidth(it->second->GetLineWidth());  
    graph->GetXaxis()->SetTitleSize(0.12);
    graph->GetYaxis()->SetTitleSize(0.15);
    graph->GetXaxis()->SetLabelSize(0.12);
    graph->GetYaxis()->SetLabelSize(0.13);
    graph->GetYaxis()->SetTitleOffset(0.5);
    graph->GetXaxis()->SetTitleOffset(0.9);
    graph->GetXaxis()->SetTitle(it->second->GetXaxis()->GetTitle());
    graph->GetYaxis()->SetTitle(yaxisTitle_pad2);
    graph->Draw(option);
    option+="same";
    //delete graph;
  }
}



void  PrepareHistCONF(TH1* hist, int rebin, int color, int style, float scale, TString units){
  if (rebin>1) hist->Rebin(rebin);
  if (scale>-1.5){
    if (scale>0) hist->Scale(scale);
    else hist->Scale(1./hist->Integral(0,-1));
    float width=hist->GetXaxis()->GetBinWidth(1);
    TString width_string="";
    width_string+=width;
    bool found_decimal=false;
    int nZeros=0;
    int nNines=0;
    for (int istring=0;istring<width_string.Length();++istring){
      TString letter=width_string[istring];
      if (letter==".") found_decimal=true;
      if (letter=="0" && found_decimal) ++nZeros;
      if (letter!="0" && found_decimal) nZeros=0;
      if (letter=="9" && found_decimal) ++nNines;
      if (letter!="9" && found_decimal) nNines=0;
      if (nZeros==4){
	width_string.Remove(istring-3,width_string.Length());
	break;
      }
      if (nNines==4){
	TString lastDigit_string=width_string[istring-4];
	int lastDigit=lastDigit_string.Atoi();
	if (lastDigit!=9){
	  lastDigit++;
	  width_string.Remove(istring-4,width_string.Length());
	  width_string+=lastDigit;
	  break;
	}
      }
    }
    width_string+=units;
    if (scale>0) hist->GetYaxis()->SetTitle("Jets / "+width_string);
    else hist->GetYaxis()->SetTitle("Arbitrary Normalization");
  }

  hist->SetLineColor(color);
  hist->SetMarkerColor(color);
  hist->SetMarkerStyle(style);
}




void WriteGeneralInfoCONF(TString shape, TString name, bool lumi, bool info_for_all_events, TString sample, float size, float x, float y, TString type){
  TLatex l;
  l.SetNDC();
  l.SetTextFont(42);
  l.SetTextSize(size); 
  l.SetTextColor(1);
  TString ToWrite="";
  float offset=0;
  if (!lumi){
    ToWrite+="  Simulation  ";
    offset=0.22;
  }
  ToWrite+=type;
  ATLASLabel(x,y,ToWrite,1.3*size);
  ToWrite="#sqrt{s}=14 TeV";
  if (lumi) ToWrite+=", #int L dt = 20.3 fb^{-1}";
  l.DrawLatex(x+0.35+offset,y,ToWrite);

  float y_lineSize=0.043*size/0.04;
  int multiple=0;

  if (sample!=""){
    multiple++;
    l.DrawLatex(x,y-y_lineSize*multiple,sample.Data());
  }
  if (info_for_all_events) return;
  ToWrite="anti-k_{t} LCW jets with R=1.0";
  multiple++;
  l.DrawLatex(x,y-y_lineSize*multiple,ToWrite.Data());

  TString pt_low=name;
  TString pt_high=name;
  pt_low.Remove(0,pt_low.Index("-")-4);
  pt_low.Remove(pt_low.Index("-"),pt_low.Length());
  for (int istring=pt_low.Length()-1;istring>-1;--istring){
    TString letter=pt_low[istring];
    if (!(letter.IsDigit())) pt_low.Remove(istring,1);
  }
  pt_high.Remove(0,pt_high.Index("-"));
  pt_high.Remove(5,pt_high.Length());
  for (int istring=pt_high.Length()-1;istring>-1;--istring){
    TString letter=pt_high[istring];
    if (!(letter.IsDigit())) pt_high.Remove(istring,1);
  }

  ToWrite=pt_low+" #leq p_{T}^{jet} < "+pt_high+" GeV";
  multiple++;
  l.DrawLatex(x,y-y_lineSize*multiple,ToWrite.Data());

  if (shape.Contains("WithCut")){
    if (pt_low.Atof()>450){
      multiple++;
      WriteInfo("leading jets",size, x, y-y_lineSize*multiple);
    }
    multiple++;
    WriteInfo("corr m^{jet} > 30 GeV",size, x, y-y_lineSize*multiple);
    multiple++;
    WriteInfo("corr #tau_{21} > 0.1",size, x, y-y_lineSize*multiple);
  }
  if (shape.Contains("tau21")){
    multiple++;
    WriteInfo("corr #tau_{1}>0.01",size, x, y-y_lineSize*multiple);
  }
}



TString CreateLegendName(int low, int high, TString variable){
  TString legend_name="";
  legend_name+=low;
  legend_name+=" #leq "+variable+" #leq ";
  legend_name+=high;
  return legend_name;
}





TH1F* GetWidthHistogram(TH2F* hist2D, float frac, TString name){
  name+=hist2D->GetName();
  int nBins=hist2D->GetNbinsX();
  TH1F* hist=new TH1F(name,"",nBins,hist2D->GetXaxis()->GetBinLowEdge(1),hist2D->GetXaxis()->GetBinUpEdge(nBins));
  // cout << "nbins: " << hist2D->GetNbinsY() << endl;
  // cout << "low: " << hist2D->GetYaxis()->GetBinLowEdge(1) << endl;
  //  cout << "up: " << hist2D->GetYaxis()->GetBinLowEdge(-1) << endl;
  for (int x=0;x<hist2D->GetNbinsX()+2;++x){
    if (hist2D->Integral(x,x,0,hist2D->GetNbinsY()+1) < 0.000001) continue;
    if (hist2D->Integral(x,x,1,hist2D->GetNbinsY())/hist2D->Integral(x,x,0,hist2D->GetNbinsY()+1) <= frac) continue;

    float minWidth = 100000;
    for(int y=1;y<hist2D->GetNbinsY()+1;y++){
      float tempFrac=0;
      int y_max=y;
      while(tempFrac<frac && y_max != hist2D->GetNbinsY()+1){
	tempFrac+=hist2D->GetBinContent(x,y_max)/hist2D->Integral(x,x,0,hist2D->GetNbinsY()+1);
	y_max++;
      }
      if (tempFrac<frac){
	break;
      }
      //std::cout << tempFrac << " " << y << " " << y_max << endl;
      float width = hist2D->GetYaxis()->GetBinCenter(y_max)-hist2D->GetYaxis()->GetBinCenter(y);
      if(width<minWidth) minWidth = width;
    }
    //cout << "minWidth: " << minWidth << endl;
    if (minWidth>99999.) continue;
    hist->SetBinContent(x,minWidth);
    hist->SetBinError(x,hist2D->GetYaxis()->GetBinWidth(1));
  }

  hist->SetXTitle(hist2D->GetXaxis()->GetTitle());
  hist->SetYTitle("width");
  return hist;
}

void MigrateUnderOverFlowBins(TH1* hist){

  hist->SetBinContent(1,hist->GetBinContent(0)+hist->GetBinContent(1));
  hist->SetBinError(1,sqrt(hist->GetBinError(0)*hist->GetBinError(0)+hist->GetBinError(1)*hist->GetBinError(1)));
  hist->SetBinContent(0,0);

  int lastBin=hist->GetNbinsX();

  hist->SetBinContent(lastBin,hist->GetBinContent(lastBin)+hist->GetBinContent(lastBin+1));
  hist->SetBinError(lastBin,sqrt(hist->GetBinError(lastBin)*hist->GetBinError(lastBin)+hist->GetBinError(lastBin+1)*hist->GetBinError(lastBin+1)));
  hist->SetBinContent(lastBin+1,0);

}



void MigrateUnderOverFlowBins(TH2* hist){
  int nBins=hist->GetXaxis()->GetNbins();

  for (int ix=0;ix<nBins+2;++ix){
    hist->SetBinContent(ix,1,hist->GetBinContent(ix,0)+hist->GetBinContent(ix,1));
    hist->SetBinContent(ix,0,0);
    hist->SetBinError(ix,1,sqrt(hist->GetBinError(ix,0)*hist->GetBinError(ix,0)+hist->GetBinError(ix,1)*hist->GetBinError(ix,1)));
    hist->SetBinError(ix,0,0);

    hist->SetBinContent(ix,nBins,hist->GetBinContent(ix,nBins)+hist->GetBinContent(ix,nBins+1));
    hist->SetBinContent(ix,nBins+1,0);
    hist->SetBinError(ix,nBins,sqrt(hist->GetBinError(ix,nBins)*hist->GetBinError(ix,nBins)+hist->GetBinError(ix,nBins+1)*hist->GetBinError(ix,nBins+1)));
    hist->SetBinError(ix,nBins+1,0);
  }

  for (int iy=0;iy<nBins+2;++iy){
    hist->SetBinContent(1,iy,hist->GetBinContent(0,iy)+hist->GetBinContent(1,iy));
    hist->SetBinContent(0,iy,0);
    hist->SetBinError(1,iy,sqrt(hist->GetBinError(0,iy)*hist->GetBinError(0,iy)+hist->GetBinError(1,iy)*hist->GetBinError(1,iy)));

    hist->SetBinContent(nBins,iy,hist->GetBinContent(nBins,iy)+hist->GetBinContent(nBins+1,iy));
    hist->SetBinContent(nBins+1,iy,0);
    hist->SetBinError(nBins,iy,sqrt(hist->GetBinError(nBins,iy)*hist->GetBinError(nBins,iy)+hist->GetBinError(nBins+1,iy)*hist->GetBinError(nBins+1,iy)));
  }
}



TH2D* ProduceHistogramWithSwitchedAxis(TH2* hist){
  TString name=hist->GetName();
  TH2D* hist_switchedAxis=new TH2D(name+"_switchedAxis","",hist->GetYaxis()->GetNbins(),hist->GetYaxis()->GetXbins()->GetArray(),hist->GetXaxis()->GetNbins(),hist->GetXaxis()->GetXbins()->GetArray());
  hist_switchedAxis->GetXaxis()->SetTitle(hist->GetYaxis()->GetTitle());
  hist_switchedAxis->GetYaxis()->SetTitle(hist->GetXaxis()->GetTitle());
  for (int ix=0;ix<hist->GetXaxis()->GetNbins()+2;++ix){
    for (int iy=0;iy<hist->GetYaxis()->GetNbins()+2;++iy){
      double content=hist->GetBinContent(ix,iy);
      double error=hist->GetBinError(ix,iy);
      hist_switchedAxis->SetBinContent(iy,ix,content);
      hist_switchedAxis->SetBinError(iy,ix,error);
    }
  }
  return hist_switchedAxis;
}



TH1D* GetEfficiency(TH1* passed, TH1* total){
  TH1D* eff=(TH1D*)passed->Clone();
  eff->Divide(passed,total,1,1,"b");   // the new way
  // for (int ix=0;ix<eff->GetXaxis()->GetNbins()+2;++ix){
  //   float passed_content=passed->GetBinContent(ix);
  //   float total_content=total->GetBinContent(ix);
  //   if (total_content>1e-7){
  //     eff->SetBinContent(ix,passed_content/total_content);
  //     eff->SetBinError(ix,0);
  //   }
  //   else{
  //     eff->SetBinContent(ix,0);
  //     eff->SetBinError(ix,0);
  //   }
  // }
  return eff;
}



void ConvertToCrossSection(TH1* hist,float lumi){
  hist->Scale(1./lumi);
  for (int ix=1;ix<hist->GetXaxis()->GetNbins()+1;++ix){
    hist->SetBinContent(ix, hist->GetBinContent(ix)/hist->GetBinWidth(ix));
    hist->SetBinError(ix, hist->GetBinError(ix)/hist->GetBinWidth(ix));
  }
  hist->SetYTitle("d#sigma/d p_{T}^{t} [fb/GeV]");
  hist->SetXTitle("p_{T}^{t} [GeV]");
}




void DivideByBinWidth(TH1* hist){
  for (int i=1;i<hist->GetNbinsX()+1;++i){
    hist->SetBinContent(i,hist->GetBinContent(i)/hist->GetBinWidth(i));
    hist->SetBinError(i,hist->GetBinError(i)/hist->GetBinWidth(i));
  }
}



void SetOverflowBinsToZero(TH2D* hist){
  for (int i=0;i<hist->GetNbinsX()+2;++i){
    hist->SetBinContent(i,0,0);
    hist->SetBinContent(i,hist->GetNbinsY()+1,0);
  }
  for (int i=0;i<hist->GetNbinsY()+2;++i){
    hist->SetBinContent(0,i,0);
    hist->SetBinContent(hist->GetNbinsX()+1,i,0);
  }
}


void RemoveNegatives(TH1* hist){
  for (int i=1;i<hist->GetNbinsX()+1;++i){
    if (hist->GetBinContent(i)<0) hist->SetBinContent(i,1e-100);
  }
}









  
TH2D* Rebin2D(TH2* hist, vector<float> x, vector<float> y, TString name){
  TString changedName=hist->GetName()+name;
  TH2D* rebined=new TH2D(changedName+"_rebined",";"+(TString)(hist->GetXaxis()->GetTitle())+";"+(TString)(hist->GetYaxis()->GetTitle()),x.size()-1,GetArrayOfBins(x),y.size()-1,GetArrayOfBins(y));
  TH2D* rebined_content=new TH2D(changedName+"_rebined_content","",x.size()-1,GetArrayOfBins(x),y.size()-1,GetArrayOfBins(y));
  TH2D* rebined_squaredError=new TH2D(changedName+"_rebined_squaredError","",x.size()-1,GetArrayOfBins(x),y.size()-1,GetArrayOfBins(y));
  for (int i=1;i<hist->GetNbinsX()+1;++i){
    for (int j=1;j<hist->GetNbinsY()+1;++j){
      rebined_content->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetYaxis()->GetBinCenter(j),hist->GetBinContent(i,j));
      rebined_squaredError->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetYaxis()->GetBinCenter(j),hist->GetBinError(i,j)*hist->GetBinError(i,j));
    }
  }
  for (int i=1;i<rebined->GetNbinsX()+1;++i){
    for (int j=1;j<hist->GetNbinsY()+1;++j){
      rebined->SetBinContent(i,j,rebined_content->GetBinContent(i,j));
      rebined->SetBinError(i,j,sqrt(rebined_squaredError->GetBinContent(i,j)));
    }
  }
  
  /*    for (int i=1;i<hist->GetNbinsX()+1;++i){
	for (int j=1;j<hist->GetNbinsY()+1;++j){
	//cout << "i: " << i << " j: " << j << " bin content: " << hist->GetBinContent(i,j) << endl;
	rebined->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetYaxis()->GetBinCenter(j),hist->GetBinContent(i,j));
	}
	}*/  // the old way
  //rebined->Sumw2();
  delete rebined_content;
  delete rebined_squaredError;
  return rebined;
}



TH1D* Rebin1D(TH1* hist, vector<float> x, TString name){
  TString changedName=hist->GetName()+name;
  TH1D* rebined=new TH1D(changedName+"_rebined",";"+(TString)(hist->GetXaxis()->GetTitle()),x.size()-1,GetArrayOfBins(x));
  TH1D* rebined_content=new TH1D(changedName+"_rebined_content","",x.size()-1,GetArrayOfBins(x));
  TH1D* rebined_squaredError=new TH1D(changedName+"_rebined_squaredError","",x.size()-1,GetArrayOfBins(x));
  for (int i=1;i<hist->GetNbinsX()+1;++i){
    rebined_content->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetBinContent(i));
    rebined_squaredError->Fill(hist->GetXaxis()->GetBinCenter(i),hist->GetBinError(i)*hist->GetBinError(i));
  }
  for (int i=1;i<rebined->GetNbinsX()+1;++i){
    rebined->SetBinContent(i,rebined_content->GetBinContent(i));
    rebined->SetBinError(i,sqrt(rebined_squaredError->GetBinContent(i)));
  }
  
  //rebined->Sumw2();
  delete rebined_content;
  delete rebined_squaredError;
  return rebined;
}

  

Double_t* GetArrayOfBins(vector<Float_t> bins){
  Int_t size=bins.size();
  Double_t *array=new Double_t[size];
  for (int i=0;i<size;++i){
    array[i]=bins[i];
  }
  return(array);
}


void DrawHistos(TPad* pad1, TPad* pad2, vector<TH1D*> histos, vector<TString> names, Float_t xmin, Float_t xmax, Float_t ymin, Float_t ymax, Float_t xmin_legend, Float_t ymax_legend, Float_t xmax_legend, Float_t ymin_legend){
  pad1->cd();
  //pad1->SetLogy();
  bool changeXaxis=false;
  int nSamples=histos.size();

  if (xmin < xmax) changeXaxis=true;
  TLegend *leg = new TLegend(xmin_legend,ymax_legend,xmax_legend,ymin_legend);
  leg->SetFillStyle(0);
  int i=0;
  TString option="";
  if (ymin >= ymax){
    ymax=-10000;
    ymin=10000;
    double x=-1;
    double y=-1;
    for (int it=0;it<nSamples;++it){
      TH1D* hist=histos[it];
      if (!hist) continue;
      for (int ipoint=1;ipoint<hist->GetNbinsX()+1;++ipoint){
	x=hist->GetXaxis()->GetBinCenter(ipoint);
	y=hist->GetBinContent(ipoint);
	//cout << i << " binContent   "  << ibincontent  << "yaxis_min " << yaxis_min <<  "  yaxis_max " << yaxis_max<< endl;
	if (y<1e6 && ymax<y && (!changeXaxis || (x>xmin && x<xmax))) ymax=y;
	if (y>1e-5 && ymin>y && (!changeXaxis || (x>xmin && x<xmax))) ymin=y;
      }      
    }
    ymin=ymin-(ymax-ymin)/15.;
  ymax=ymax+(ymax-ymin)/5.;
  }

  for (int it=0;it<nSamples;++it){
    TH1D* hist=histos[it];
    if (!hist) continue;
    if (changeXaxis) hist->GetXaxis()->SetRangeUser(xmin,xmax);
    hist->GetYaxis()->SetRangeUser(ymin,ymax);
    i++;
    if (i==5) i++;
    hist->SetTitle("");
    hist->SetMarkerStyle(22+i);
    //hist->SetMarkerStyle(24);
    hist->SetMarkerSize(1.0);
    if (i==3){
      hist->SetMarkerColor(419);
      hist->SetLineColor(419);
    }
    else{
      hist->SetMarkerColor(i);
      hist->SetLineColor(i);
    } 
    hist->SetLineWidth(1);
    leg->AddEntry(hist,names[it],"p");
    hist->Draw(option);
    option="same";
  }
  leg->Draw(); 

  pad2->cd();
  option="";
  for (int it=1;it<nSamples;++it){
    TH1D* ratio=(TH1D*)histos[it]->Clone();
    ratio->Divide(histos[0]);
    ratio->GetYaxis()->SetTitle("others / "+names[0]);
    ratio->GetYaxis()->SetRangeUser(0.501,1.49);
    ratio->GetYaxis()->SetTitleSize(0.08);
    ratio->GetYaxis()->SetLabelSize(0.08);
    ratio->GetXaxis()->SetTitleSize(0.08);
    ratio->GetXaxis()->SetLabelSize(0.08);

    ratio->Draw(option);
    option="same";
  }
}

double chi2_ABCD(vector<vector<double> >& table,double& chi2){
     int n=table.size();
     int m=table[0].size();     
     if(n == 0) return -1;  
     vector<double> sumcolumns(table.size()),sumrows(table[0].size());
     double x(0.),ntotal1(0),ntotal2(0);
     for(int i=0; i<n; i++) {
        x=0.;
        for(int j=0; j< m; j++) x+= table[i][j];
        sumcolumns[i] = x;
     }
     for(int j=0; j<m; j++) {
        x=0.;
        for(int i=0; i< n; i++) x+= table[i][j];
        sumrows[j] = x;
     }
     for(int i=0; i<n; i++) ntotal1 += sumcolumns[i];  
     for(int j=0; j<m; j++) ntotal2 += sumrows[j]; 
    // cout << ntotal1 << " " << ntotal2 << endl;

     chi2=0.;

     for(int i=0; i<n; i++)for(int j=0; j<m; j++) chi2+= pow(table[i][j] - (sumcolumns[i]*sumrows[j]/ntotal1),2)/(sumcolumns[i]*sumrows[j]/ntotal1);
  
     return TMath::Prob(chi2,(m-1)*(n-1));
}

double chi2_ABCD_weighted_binbybin(vector<vector<TH1D*> >& hist_table,double& chi2,vector<double>& chisquares,vector<vector<double> >& effective_numbers,unsigned long niter){
    int k=0;
    int n1=hist_table.size();
    if(n1 == 0) return -1;  
    int m=hist_table[0].size();
    if(m==0) return -1;
    int nbins=hist_table[0][0]->GetNbinsX();
    chisquares.clear();
    chisquares.resize(nbins);  
    chi2=0;
    vector<vector<double> > table(n1),table2(n1);
    effective_numbers.clear();
    effective_numbers.resize(nbins);
    for(int ibin=1;ibin<=nbins;ibin++){
        bool ok=1;
        //if(hist_table[0][0]->GetBinContent(ibin) <=0){
        //    continue;
        //}
        //effective_numbers.clear();
        for(int i=0; i<n1; i++) {
           table[i].resize(m);
           table2[i].resize(m);        
           for(int j=0; j< m; j++) {
              table[i][j]=hist_table[i][j]->GetBinContent(ibin);
              table2[i][j]=pow(hist_table[i][j]->GetBinError(ibin),2);
              effective_numbers[ibin-1].push_back(table[i][j]*table[i][j]/table2[i][j]);
              if(table2[i][j]==0 || table2[i][j]!=table2[i][j]) ok=0;
            }
         }
         
         if(ok==0){
            cout << "Warning: At least one histogram has empty bin number " << ibin << endl;
            continue;
         }
         
         //for(unsigned l=0;l<effective_numbers[ibin-1].size();l++) cout << effective_numbers[ibin-1][l] << " ";
         //cout << endl; 
         if(*min_element(effective_numbers[ibin-1].begin(),effective_numbers[ibin-1].end())<10) {
            //cout << "Warning: Found bin with less than 10 effective number of events" << endl;
             if(*min_element(effective_numbers[ibin-1].begin(),effective_numbers[ibin-1].end())<5) continue;
         }
         
     
    
         vector<vector<vector<vector<double> > > > cov4D;
         Estimate_Covariance(table,table2,cov4D,niter);


     
         chisquares[ibin-1]= chi2_ABCD_weighted(table,cov4D,0,0);
         chi2+=chisquares[ibin-1];
         k++;
     }
  
     return TMath::Prob(chi2,(m-1)*(n1-1)*k);




}

double chi2_ABCD_weighted(vector<vector<TH1D*> >& hist_table,double& chi2,vector<double>& effective_numbers, unsigned long niter){
    
    int n1=hist_table.size();
    if(n1 == 0) return -1;  
    int m=hist_table[0].size(); 
    double stats[4];
    effective_numbers.clear();

    vector<vector<double> > table(n1),table2(n1);
    bool ok=1;
    for(int i=0; i<n1; i++) {
        table[i].resize(m);
        table2[i].resize(m);        
        for(int j=0; j< m; j++) {
            table[i][j]=hist_table[i][j]->Integral();
            hist_table[i][j]->GetStats(stats);
            table2[i][j]=stats[1];
            effective_numbers.push_back(table[i][j]*table[i][j]/table2[i][j]);
            if(table2[i][j]==0 || table2[i][j]!=table2[i][j]) ok=0;
        }
    }


         if(ok==0){
            cout << "Warning: At least one region is empty" << endl;
         }
         
         //for(unsigned l=0;l<effective_numbers[ibin-1].size();l++) cout << effective_numbers[ibin-1][l] << " ";
         //cout << endl; 
         if(*min_element(effective_numbers.begin(),effective_numbers.end())<25) {
            cout << "Warning: Found bin with less than 25 effective number of events" << endl;
         }     
     

     vector<vector<vector<vector<double> > > > cov4D;
     Estimate_Covariance(table,table2,cov4D,niter);


     /*vector<double> chi_squares;
     for(int i=0;i<n1;i++) for(int j=0;j<m;j++){
      //  chi_squares.push_back( chi2_ABCD_weighted(table,table2,sumrows,sumcolumns,ntotal1,N,i,j));
      //  cout << endl << i << " " << j <<endl;
      //  cout << chi2_ABCD_weighted(table,sumrows,sumcolumns,ntotal1,cov4D,i,j) << endl;
        //chi_squares.push_back(chi2_ABCD_weighted(table,sumrows,sumcolumns,ntotal1,cov4D,i,j));
     }
     std::sort(chi_squares.begin(), chi_squares.end());
        if(chi_squares.size() % 2 == 0)
                chi2 = (chi_squares[chi_squares.size()/2 - 1] + chi_squares[chi_squares.size()/2]) / 2;
        else
                chi2 = chi_squares[chi_squares.size()/2];
     */ 
   //  chi2 = chi2_ABCD_weighted(table,sumrows,sumcolumns,ntotal1,cov4D,0,0);
     chi2 = chi2_ABCD_weighted(table,cov4D,0,0);
     return TMath::Prob(chi2,(m-1)*(n1-1));




}
double chi2_ABCD_weighted(vector<vector<double> > table,vector<vector<vector<vector<double> > > >& cov4D,int /*i0*/,int /*j0*/){
 //double chi2=0.;

     int r=table.size();
     int s=table[0].size();
    // int ndf=(r-1)*(s-1);
    int ndf=r*s;


     vector<double> sumcolumns(table.size()),sumrows(table[0].size());
     double x(0.),ntotal(0);
     for(int i=0; i<r; i++) {
        x=0.;
        for(int j=0; j< s; j++) x+= table[i][j];
        sumcolumns[i] = x;
     }
     for(int j=0; j<s; j++) {
        x=0.;
        for(int i=0; i< r; i++) x+= table[i][j];
        sumrows[j] = x;
     }
     for(int i=0; i<r; i++) ntotal += sumcolumns[i];  
   //  for(int j=0; j<s; j++) ntotal2 += sumrows[j];


 //    TVectorD res(ndf);     
     TMatrixD row(1,ndf);
     TMatrixD col(ndf,1);


     TMatrixD cov(ndf,ndf);
  // j0=0;
  // i0=0;
  for(int i=0;i<r;i++)for(int j=0;j<s;j++)for(int k=0;k<r;k++)for(int l=0;l<s;l++){      
     // if(i0==i || i0==k || j0==j || j0==l ) continue;       
      int i1=i,j1=j,k1=k,l1=l;
     // if(i>i0) i1--;
     // if(j>j0) j1--;
     // if(k>i0) k1--;
     // if(l>j0) l1--;
      int m= i1*(s-1) + j1;
      int n= k1*(s-1) + l1;
       
      //cout << i-i1 << " " << j-j1 << endl;
      //cout << m << " " << n << endl;
      cov[m][n] = cov4D[i][j][k][l];
//      res[m]=ntotal*table[i][j] - sumcolumns[i]*sumrows[j];
      row[0][m]=ntotal*table[i][j] - sumcolumns[i]*sumrows[j];
      col[m][0] = row[0][m];
  }
   

  // cout << i0 << " " << j0 << endl;
  /*for(int m=0;m<ndf;m++){
     
     for(int n=0;n<ndf;n++){
        cout << cov[m][n]/sqrt(cov[m][m]*cov[n][n]) << " " ;
     }
     cout << endl;
  */
  TVectorD eigen_values;
  TMatrixD eigen_vectors = cov.EigenVectors(eigen_values); 
 // for(int i= 0;i<ndf;i++) cout << i << " " << eigen_values[i] << endl;
 //  cout << endl;
   
  /*for(int m=0;m<ndf;m++){
     
     for(int n=0;n<ndf;n++){
        cout << inverse[m][n] << " " ;
     }
     cout << endl;
  }*/
  
 TMatrixD eigen_inv= eigen_vectors;
 eigen_inv.Invert();
 TMatrixD diag= eigen_inv*cov*eigen_vectors;
  ndf=(r-1)*(s-1);

  TMatrixD small_diag(ndf,ndf);

  for(int m=0;m<ndf;m++){
     
     for(int n=0;n<ndf;n++){
         small_diag[m][n] = diag[m][n];
       // cout << small_diag[m][n] << " " ;
     }
     //cout << endl;
  }

TMatrixD row2 = row*eigen_vectors;
TMatrixD col2 = eigen_inv*col;

TMatrixD small_row(1,ndf);
TMatrixD small_col(ndf,1);

for(int i=0;i<ndf;i++){
  small_row[0][i]=row2[0][i];
  small_col[i][0]=col2[i][0];
}

 TMatrixD inverse = small_diag.Invert();
  return (small_row*inverse*small_col)[0][0];

}

double chi2_ABCD_weighted(vector<vector<double> >& table,vector<vector<double> >& table2,vector<double>& sumrows ,vector<double>& sumcolumns,double ntotal1,int i0,int j0){

     int n=table.size();
     int m= table[0].size();
     double chi2=0.;
     double r;
     
     double sum1(0),sum2(0);
     double Pij;
     for(int i=0; i<n; i++)for(int j=0; j<m; j++) {
          if(i==i0 && j==j0) continue;
          r=table[i][j]/table2[i][j];
          //r=1;
          Pij=sumcolumns[i]*sumrows[j]/ntotal1;
          chi2+= r*pow(table[i][j] - Pij,2)/(Pij);
          sum1+=r*(table[i][j] - Pij);
          sum2+=r*Pij/ntotal1;
         // cout << r << endl;
     }
     //cout << sum1 << " " << sum2 << endl;
     chi2+=sum1*sum1/(ntotal1*(1.-sum2));
     return chi2;

}


double chi2_ABCD_weighted(vector<vector<double> >& table,vector<vector<double> >& table2,vector<double>& sumrows ,vector<double>& sumcolumns,double W,double N,int i0,int j0){
    
     int n=table.size();
     int m= table[0].size();
     double chi2=0.;
     double r;
     
     double sum1(0),sum2(0);
     double Pij;
     for(int i=0; i<n; i++)for(int j=0; j<m; j++) {
          if(i==i0 && j==j0) continue;
          r=table[i][j]/table2[i][j];
          //r=1;
          Pij=sumcolumns[i]*sumrows[j]/(W*W);
          chi2+= r*pow(table[i][j] - W*Pij,2)/(W*Pij);
          sum1+=r*(table[i][j] - W*Pij);
          sum2+=r*W/N*Pij;
         // cout << r << endl;
     }
     //cout << sum1 << " " << sum2 << endl;
     chi2+=sum1*sum1/(N*(1.-sum2));
     return chi2;

}

void Estimate_Covariance(vector<vector<double> >& table,vector<vector<double> >& table2,vector<vector<vector<vector<double> > > >& cov4D,unsigned long niter){

int r=table.size();
int s=table[0].size();
//int size=r*s;
//int ndf=(r-1)*(s-1);





  vector<vector<vector<vector<double> > > > sum2(r);
  vector<vector<double> >sum(r);
  vector<vector<double> > random_table(r);
  for(int i=0;i<r;i++) {
     random_table[i].resize(s);
     sum[i].resize(s);
  }
  for(int i=0;i<r;i++) {
    sum2[i].resize(s);
    for(int j=0;j<s;j++) {
       sum2[i][j].resize(r); 
      for(int k=0;k<r;k++) { 
          sum2[i][j][k].resize(s,0);
          for(int l=0;l<s;l++) {
         }
      }
    }
  }


 // ROOT::Math::Random<ROOT::Math::GSLRngMT>* g_random = new ROOT::Math::Random<ROOT::Math::GSLRngMT>((unsigned)time(NULL));
     vector<double> sumcolumns(table.size()),sumrows(table[0].size());
     double x(0.),ntotal(0);


  for(unsigned long iiter=0;iiter<niter;iiter++){
      for(int m=0;m<r;m++)for(int n=0;n<s;n++) random_table[m][n]=g_random->Gaus(table[m][n],sqrt(table2[m][n]));


     for(int i=0; i<r; i++) {
        x=0.;
        for(int j=0; j<s; j++) x+= random_table[i][j];
        sumcolumns[i] = x;
     }
     for(int j=0; j<s; j++) {
        x=0.;
        for(int i=0; i< r; i++) x+= random_table[i][j];
        sumrows[j] = x;
     }
     ntotal=0;
     for(int i=0; i<r; i++) ntotal += sumcolumns[i];  

     for(int m=0;m<r;m++)for(int n=0;n<s;n++) random_table[m][n]= ntotal*random_table[m][n] - sumcolumns[m]*sumrows[n];
           

      for(int i=0;i<r;i++)for(int j=0;j<s;j++) {
         sum[i][j]+=random_table[i][j];
         for(int k=0;k<r;k++)for(int l=0;l<s;l++) { 
           sum2[i][j][k][l]+=random_table[i][j]*random_table[k][l];
                
         }
      }

  } 
  for(int i=0;i<r;i++)for(int j=0;j<s;j++) sum[i][j]/=niter;
  for(int i=0;i<r;i++)for(int j=0;j<s;j++)for(int k=0;k<r;k++)for(int l=0;l<s;l++) {
      sum2[i][j][k][l]/=niter;
      sum2[i][j][k][l]-=sum[i][j]*sum[k][l];
  }
  cov4D = sum2;

  


}



double get_t_statistics(vector<vector<TH1D*> >& hist_table,double& w){
  double t=0;
  double cor(0),cov(0),Cb(0);
  w=0.;
  int n=hist_table.size(),m=hist_table[0].size();
  get_Correlation_of_Table(hist_table,cor,cov,Cb);
  for(int i=0;i<n;i++)for(int j=0;j<m;j++) w+=hist_table[i][j]->Integral();

  t=(cor*sqrt(w - 2.))/(1-cor*cor);

  return t;
}



void get_Correlation_of_Table(vector<vector<TH1D*> >& hist_table,double& cor,double& cov,double& Cb){
     int n=hist_table.size();
    if(n == 0) return;  
    int m=hist_table[0].size();
    double Kij; 
    double X(0.),X2(0.),Y(0.),Y2(0.),XY(0.);
    vector<vector<double> > table(n);
    double sumw(0.);
    for(int i=0; i<n; i++) {
        table[i].resize(m);  
        for(int j=0; j< m; j++) {
            Kij=hist_table[i][j]->Integral();
            table[i][j]=Kij;
            X+=Kij*i;
           Y+=Kij*j;
           X2+=Kij*i*i;
           Y2+=Kij*j*j;
           XY+=Kij*i*j;
           sumw+=Kij;
        }
    }
    X/=sumw; 
    X2/=sumw;
    Y/=sumw;
    Y2/=sumw;
    XY/=sumw;  

    cov = XY - X*Y;
    cor = cov/sqrt((X2 - X*X)*(Y2-Y*Y));

    if(n==2 && m ==2){
        Cb=table[1][1]*sumw/((table[1][1] + table[1][0])*(table[1][1] + table[0][1]));
        
    }

}

void get_Correlation_of_Table_binbybin(vector<vector<TH1D*> >& hist_table,vector<double>& cor,vector<double>& cov,vector<double>& Cb){
	
	const int n=hist_table.size();
    if(n == 0) return;  
    const int m=hist_table[0].size();
    const int nbins=hist_table[0][0]->GetNbinsX();
    if((int)cor.size()!=nbins){
		cor.resize(nbins);
		cov.resize(nbins);
		Cb.resize(nbins);
	}
    double Kij; 
    int ibin=0;
    vector<double> X(nbins,0.),X2(nbins,0.),Y(nbins,0.),Y2(nbins,0.),XY(nbins,0.);
    vector<vector<vector<double> > > table(n);
    vector<double> sumw(nbins,0.);
    for(int i=0; i<n; i++) {
        table[i].resize(m);  
        for(int j=0; j< m; j++) {
			table[i][j].resize(nbins);
			for(ibin=0;ibin<nbins;ibin++){
				
				Kij=hist_table[i][j]->GetBinContent(ibin+1);
				table[i][j][ibin]=Kij;
				X[ibin]+=Kij*i;
				Y[ibin]+=Kij*j;
				X2[ibin]+=Kij*i*i;
				Y2[ibin]+=Kij*j*j;
				XY[ibin]+=Kij*i*j;
				sumw[ibin]+=Kij;
			}
        }
    }
    for(ibin=0;ibin<nbins;ibin++){
		X[ibin]/=sumw[ibin]; 
		X2[ibin]/=sumw[ibin];
		Y[ibin]/=sumw[ibin];
		Y2[ibin]/=sumw[ibin];
		XY[ibin]/=sumw[ibin];  
	

		cov[ibin] = XY[ibin] - X[ibin]*Y[ibin];
		cor[ibin] = cov[ibin]/sqrt((X2[ibin] - X[ibin]*X[ibin])*(Y2[ibin]-Y[ibin]*Y[ibin]));
	
		if(n==2 && m ==2){
			Cb[ibin]=table[1][1][ibin]*sumw[ibin]/((table[1][1][ibin] + table[1][0][ibin])*(table[1][1][ibin] + table[0][1][ibin]));
        
		}
	}
	
	
	
}

void print_table(vector<vector<double> > & tablecontent,vector<string>& row_names,vector<string>& column_names,ofstream& textfile){
	
	char regionName='A';
	int nRegions=tablecontent.size();
	int nbins=tablecontent[0].size();
	textfile << "\\hline" << endl;
	for(int iRegion=0;iRegion<nRegions;iRegion++){
		if(iRegion==0) {
			for(int ibin=0;ibin<nbins;ibin++){
				//if(ibin==0) textfile << " & ";
				textfile << " & " << column_names[ibin];
			}
			textfile << " \\\\ \\hline" << endl;
		}
		textfile << row_names[iRegion];
		for(int ibin=0;ibin<nbins;ibin++)textfile << " & " << tablecontent[iRegion][ibin];
		//if(iRegion < nRegions -1) textfile << " \\\\ \\hline" << endl;
		//else textfile << " \\hline" << endl;
		textfile << " \\\\ \\hline" << endl;
		regionName++;
	}
	
}
void print_purities(vector<vector<double> > & data,vector<vector<double> > & sample,vector<string>& row_names,vector<string>& column_names,TString /*histname*/,ofstream& textfile){
	vector<vector<double> > efficiencies=sample;
	int nRegions=data.size();
	int nbins=data[0].size();
	for(int iRegion=0;iRegion<nRegions;iRegion++){
		for(int ibin=0;ibin<nbins;ibin++) efficiencies[iRegion][ibin] = (data[iRegion][ibin] > 0 ? efficiencies[iRegion][ibin]/data[iRegion][ibin] : 0.)*100.;
		
	}
	print_table(efficiencies,row_names,column_names,textfile);
}
template<typename T> 
void SetXaxisTitle(T* hist,TString xname){
	if (xname.Contains("fatJet1_pt")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate p_{T} [GeV]");
	else if (xname.Contains("fatJet1_mass")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate mass [GeV]");
	else if (xname.Contains("fatJet1_eta")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate |#eta|");
	else if (xname.Contains("fatJet1_energy")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate energy");
	else if (xname.Contains("fatJet1_phi")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate #phi");
	else if (xname.Contains("fatJet1_y")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate |y|");
	else if (xname.Contains("fatJet1_split12")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate #sqrt{d_{12}} [GeV]");
	else if (xname.Contains("fatJet1_tau32")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate #tau_{32}");
	else if (xname.Contains("fatJet1_tau21")) hist->GetXaxis()->SetTitle("Leading Top-jet candidate #tau_{21}");
	else if (xname.Contains("fatJet2_pt")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate p_{T} [GeV]");
	else if (xname.Contains("fatJet2_mass")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate mass [GeV]");
	else if (xname.Contains("fatJet2_eta")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate |#eta|");
	else if (xname.Contains("fatJet2_energy")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate energy");
	else if (xname.Contains("fatJet2_phi")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate #phi");
	else if (xname.Contains("fatJet2_y")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate |y|");
	else if (xname.Contains("fatJet2_split12")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate #sqrt{d_{12}} [GeV]");
	else if (xname.Contains("fatJet2_tau32")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate #tau_{32}");
	else if (xname.Contains("fatJet2_tau21")) hist->GetXaxis()->SetTitle("2nd leading Top-jet candidate #tau_{21}");

	else if (xname.Contains("ttbar_pt")) hist->GetXaxis()->SetTitle("t#bar{t} p_{T} [GeV]");
	else if (xname.Contains("ttbar_mass")) hist->GetXaxis()->SetTitle("t#bar{t} invariant mass [GeV]");
	else if (xname.Contains("ttbar_eta")) hist->GetXaxis()->SetTitle("t#bar{t} |#eta|");
	else if (xname.Contains("ttbar_energy")) hist->GetXaxis()->SetTitle("t#bar{t} energy");
	else if (xname.Contains("ttbar_phi")) hist->GetXaxis()->SetTitle("t#bar{t} #phi");
	else if (xname.Contains("ttbar_y")) hist->GetXaxis()->SetTitle("t#bar{t} |y|");
	else if (xname.Contains("ttbar_deltaphi")) hist->GetXaxis()->SetTitle("t#bar{t} |#Delta#phi|");

	else if (xname.Contains("jets_phi")) hist->GetXaxis()->SetTitle("Small-R jets #phi");
	else if (xname.Contains("jets_eta")) hist->GetXaxis()->SetTitle("Small-R jets #eta");
	else if (xname.Contains("jets_pt")) hist->GetXaxis()->SetTitle("Small-R jets p_{T} [GeV]");
	else if (xname.Contains("bjets_phi")) hist->GetXaxis()->SetTitle("Small-R b-jets #phi");
	else if (xname.Contains("bjets_eta")) hist->GetXaxis()->SetTitle("Small-R b-jets #eta");
	else if (xname.Contains("bjets_pt")) hist->GetXaxis()->SetTitle("Small-R b-jets p_{T} [GeV]");
}
template void SetXaxisTitle<TH1D>(TH1D*,TString);
template void SetXaxisTitle<TGraph>(TGraph*,TString);


void plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name, double ymin, double ymax, TString lumi,bool /*use_logscale*/){
	
	gStyle->SetEndErrorSize(5);
	int n=hist.size();
	
	for(int i=0;i<n;i++) hist[i]->SetMarkerSize(0.7);
	
	
	TH2D* h = new TH2D("","",hist[0]->GetXaxis()->GetNbins(),hist[0]->GetXaxis()->GetXmin(),hist[0]->GetXaxis()->GetXmax(),2.,ymin,ymax);
	h->SetStats(kFALSE);
	h->SetXTitle(hist[0]->GetXaxis()->GetTitle());
	//h->SetXTitle(xname);
	h->SetYTitle(hist[0]->GetYaxis()->GetTitle());
    //h->SetYTitle("N_{events} [1/GeV]");
    //h->SetYTitle(yname);
    h->GetYaxis()->SetTitleSize(0.055);
    h->GetYaxis()->SetTitleOffset(0.7);
    h->GetYaxis()->SetLabelSize(0.055);
	h->GetXaxis()->SetTitleSize(0.055);
    h->GetXaxis()->SetTitleOffset(0.85);
    h->GetXaxis()->SetLabelSize(0.055);


	TCanvas* c = new TCanvas("c","",1);

    /*c->SetBottomMargin(0.);
    c->SetTopMargin(0.05);
    c->SetRightMargin(0.05);
    c->SetTicks();
    if(use_logscale){
		c->SetLogy();
	}*/
    h->Draw();
	WriteGeneralInfo("",lumi,0.06,0.2,0.87);
	for(int i=0;i<n;i++) hist[i]->DrawClone("e1same");
	leg->Draw();
	cout << dirname + name + ".pdf" << endl;
	c->SaveAs(dirname + name + ".pdf");
	delete c;
	delete h;
}

void plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TH1D* hd, TString y2name, double y2min, double y2max, TString lumi,bool use_logscale){
	
	TH1D* hdenumerator = (TH1D*)hd->Clone("hdenumerator");
	vector<TH1D*> hist_ratio;

	gStyle->SetEndErrorSize(5);
	int n=hist.size();
	//double legX1 = leg->GetX1();
	//cout << legX1 << endl;
	int imiddlebin=1;
	const int nbins=hist[0]->GetNbinsX();
	const double upper_edge = hist[0]->GetBinLowEdge(nbins-1) + hist[0]->GetBinWidth(nbins-1);
	const double low_edge = hist[0]->GetBinLowEdge(1);
	const double middle =(upper_edge + low_edge)*0.55;
	for(int i=1;i<nbins;i++){
		if(hist[0]->GetBinLowEdge(i+1) > middle){
			imiddlebin=i;
			break;
		}
		imiddlebin=nbins;
	}
	//cout << imiddlebin << endl;

	for(int i=0;i<n;i++) hist_ratio.push_back((TH1D*)hist[i]->Clone());


	for(int i=0;i<n;i++) hist[i]->SetMarkerSize(0.7);
	double max1(0.),max2(0.);
	double min1(FLT_MAX);
	double y,y2,z;
	for(int i=0;i<n;i++){
		y=hist[i]->GetMaximum();
		if(max1 < y) max1 = y;
		for(int j=imiddlebin;j<=nbins;j++){
			y2=hist[i]->GetBinContent(j);
			if(max2<y2)max2=y2;
			
		}
		z=hist[i]->GetMinimum(0.);
		if(min1 > z) min1=z;
	}
	
	//cout << name << " " << max1 << " " << max2 << endl;
	//cout << imiddlebin << endl;
	//cout << low_edge << " " << upper_edge << " " << middle << endl;
	
	TH2D* h = new TH2D("","",hist[0]->GetXaxis()->GetNbins(),hist[0]->GetXaxis()->GetXmin(),hist[0]->GetXaxis()->GetXmax(),2.,min1/100,max(1.5*max1,2.5*max2));
	h->SetStats(kFALSE);
	h->SetXTitle(hist[0]->GetXaxis()->GetTitle());
	//h->SetXTitle(xname);
	h->SetYTitle(hist[0]->GetYaxis()->GetTitle());
    //h->SetYTitle("N_{events} [1/GeV]");
    //h->SetYTitle(yname);
    h->GetYaxis()->SetTitleSize(0.075);
    h->GetYaxis()->SetTitleOffset(0.6);
    h->GetYaxis()->SetLabelSize(0.06);


	TCanvas* c = new TCanvas("c","");

    TPad *pad1 = new TPad("pad1","pad1",0,0.33,1,1,0,0,0);
    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.33,0,0,0);
    pad2->SetBottomMargin(0.3);
    pad2->SetTopMargin(0);
    pad2->SetRightMargin(0.05);
    pad2->SetGridy(1);
    pad2->SetTicks();
    pad1->SetBottomMargin(0.);
    pad1->SetTopMargin(0.05);
    pad1->SetRightMargin(0.05);
    pad1->SetTicks();
    if(use_logscale){
		pad1->SetLogy();
		pad2->SetLogy();
	}
    pad1->Draw();
    pad2->Draw();
    pad1->cd();
    pad2->cd();

    pad1->cd();
    h->Draw();
	WriteGeneralInfo("",lumi,0.07,0.2,0.85);
	for(int i=0;i<n;i++) hist[i]->DrawClone("e1same");
	leg->Draw();

	TH2D* h2 = new TH2D("","",hist[0]->GetXaxis()->GetNbins(),hist[0]->GetXaxis()->GetXmin(),hist[0]->GetXaxis()->GetXmax(),2.,y2min,y2max);
	h2->SetStats(kFALSE);
	//h2->SetXTitle(hist[0]->GetXaxis()->GetTitle());
	h2->SetXTitle(h->GetXaxis()->GetTitle());
	h2->SetYTitle(y2name);
    h2->GetXaxis()->SetTitleSize(0.15);
    h2->GetXaxis()->SetTitleOffset(0.85);
    h2->GetYaxis()->SetTitleSize(0.11);
    h2->GetYaxis()->SetTitleOffset(0.38);
    h2->GetYaxis()->SetNdivisions(4);
    h2->GetXaxis()->SetLabelSize(0.123);
    h2->GetYaxis()->SetLabelSize(0.123);

	pad2->cd();
	h2->Draw();
	for(int i=0;i<n;i++) { 
		hist_ratio[i]->Divide(hdenumerator);
		hist_ratio[i]->Draw("same");

	}
	pad2->RedrawAxis("g");
	pad2->RedrawAxis();

	c->SaveAs(dirname + name + ".pdf");
	c->SaveAs(dirname + name + ".png");
	delete c;
	delete h;
	delete h2;
}
void plotGraphs(vector<TGraph*>& graph,TLegend* /*leg*/,TString dirname,TString name,TString lumi){
	
	TString xname = name;

	if (xname.Contains("fatJet1_pt")) xname = "Leading Top-jet candidate p_{T} [GeV]";
	else if (xname.Contains("fatJet1_mass")) xname = "Leading Top-jet candidate mass [GeV]";
	else if (xname.Contains("fatJet1_eta")) xname = "Leading Top-jet candidate |#eta|";
	else if (xname.Contains("fatJet1_energy")) xname = "Leading Top-jet candidate energy";
	else if (xname.Contains("fatJet1_phi")) xname = "Leading Top-jet candidate #phi";
	else if (xname.Contains("fatJet1_y")) xname = "Leading Top-jet candidate |y|";
	else if (xname.Contains("fatJet1_split12")) xname = "Leading Top-jet candidate #sqrt{d_{12}} [GeV]";
	else if (xname.Contains("fatJet1_tau32")) xname = "Leading Top-jet candidate #tau_{32}";
	else if (xname.Contains("fatJet1_tau21")) xname = "Leading Top-jet candidate #tau_{21}";
	else if (xname.Contains("fatJet2_pt")) xname = "2nd leading Top-jet candidate p_{T} [GeV]";
	else if (xname.Contains("fatJet2_mass")) xname = "2nd leading Top-jet candidate mass [GeV]";
	else if (xname.Contains("fatJet2_eta")) xname = "2nd leading Top-jet candidate |#eta|";
	else if (xname.Contains("fatJet2_energy")) xname = "2nd leading Top-jet candidate energy";
	else if (xname.Contains("fatJet2_phi")) xname = "2nd leading Top-jet candidate #phi";
	else if (xname.Contains("fatJet2_y")) xname = "2nd leading Top-jet candidate |y|";
	else if (xname.Contains("fatJet2_split12")) xname = "2nd leading Top-jet candidate #sqrt{d_{12}} [GeV]";
	else if (xname.Contains("fatJet2_tau32")) xname = "2nd leading Top-jet candidate #tau_{32}";
	else if (xname.Contains("fatJet2_tau21")) xname = "2nd leading Top-jet candidate #tau_{21}";

	else if (xname.Contains("ttbar_pt")) xname = "t#bar{t} p_{T} [GeV]";
	else if (xname.Contains("ttbar_mass")) xname = "t#bar{t} invariant mass [GeV]";
	else if (xname.Contains("ttbar_eta")) xname = "t#bar{t} |#eta|";
	else if (xname.Contains("ttbar_energy")) xname = "t#bar{t} energy";
	else if (xname.Contains("ttbar_phi")) xname = "t#bar{t} #phi";
	else if (xname.Contains("ttbar_y")) xname = "t#bar{t} |y|";
	else if (xname.Contains("ttbar_deltaphi")) xname = "t#bar{t} |#Delta#phi|";

	else if (xname.Contains("jets_phi")) xname = "Small-R jets #phi";
	else if (xname.Contains("jets_eta")) xname = "Small-R jets #eta";
	else if (xname.Contains("jets_pt")) xname = "Small-R jets p_{T} [GeV]";
	else if (xname.Contains("bjets_phi")) xname = "Small-R b-jets #phi";
	else if (xname.Contains("bjets_eta")) xname = "Small-R b-jets #eta";
	else if (xname.Contains("bjets_pt")) xname = "Small-R b-jets p_{T} [GeV]";
	cout << name << " " << xname << endl;
	
	const int size= graph.size();
	//cout << "size " << size << endl;  
	double minimum=FLT_MAX,maximum=FLT_MIN;
	double x,y;
	int i=0;
	for(i=0;i<size;i++){
		x=graph[i]->GetYaxis()->GetXmax();
		y=graph[i]->GetYaxis()->GetXmin();
		//cout << y << " " <<x <<endl;
		if(maximum < x) maximum =x;
		if(minimum > y) minimum = y;
		
	}
	//cout << minimum << " " << maximum << endl;
	const double xmax = graph[0]->GetXaxis()->GetXmax();
	const double xmin = graph[0]->GetXaxis()->GetXmin();
	//cout << xmin << " " << xmax << endl;

	
	TH2D* h = new TH2D("h","",1,xmin,xmax,1,minimum/1.05,maximum*1.3);
	//TH2D* h = new TH2D("h","",1,xmin,xmax,1,0,150.);
	h->SetStats(kFALSE);
	h->SetXTitle(graph[0]->GetXaxis()->GetTitle());
	//h->SetXTitle(xname);
	h->SetYTitle(graph[0]->GetYaxis()->GetTitle());
    //h->SetYTitle("N_{events}");
    //h->SetYTitle("d#sigma/dp_{T} [pb/GeV]");
    h->GetXaxis()->SetTitleSize(0.05);
    h->GetXaxis()->SetTitleOffset(0.95);
    h->GetXaxis()->SetLabelSize(0.042);
    h->GetYaxis()->SetTitleSize(0.07);
    h->GetYaxis()->SetTitleOffset(0.7);
    h->GetYaxis()->SetLabelSize(0.04);
	
	
	TCanvas* c = new TCanvas("c","");
	c->cd();
	/*TPad *pad1 = new TPad("pad1","pad1",0,1,1,1,0,0,0);
	
	pad1->SetBottomMargin(0.);
    pad1->SetTopMargin(0.05);
    pad1->SetRightMargin(0.05);
    pad1->SetTicks();
    pad1->Draw();
    pad1->cd();*/
	h->Draw();
	WriteGeneralInfo("",lumi,0.049,0.2,0.955);
	for(i=0;i<size;i++) {
		 graph[i]->Draw("PC");
		 graph[i]->SetLineColor(i+1);
	 }
	//graph[0]->Draw("PC");
	//leg->Draw();
	
	TLegend *legGraphs = new TLegend(0.5,0.65,0.92,0.92);
	TString legend_name;
	for(i=0;i<size;i++){
		legend_name=graph[i]->GetName();
		legend_name.ReplaceAll("tight","");
		legend_name.ReplaceAll("_"," ");
		legend_name.ReplaceAll("Combined","new");
		legGraphs->AddEntry(graph[i]->GetName(),legend_name,"l");
		
	}
	legGraphs->Draw();
	
	c->SaveAs(dirname + name + ".pdf");
	c->SaveAs(dirname + name + ".png");
	delete c;
}
template<typename T>
void plot_histogram(T* h,TString dirname,TString name,TString lumi,TString option){
	TCanvas* c = new TCanvas("c","",1);
	c->cd();
	
	h->Draw(option);
	h->GetXaxis()->SetTitleSize(0.05);
    h->GetXaxis()->SetTitleOffset(0.95);
    h->GetXaxis()->SetLabelSize(0.04);
    h->GetYaxis()->SetTitleSize(0.05);
    h->GetYaxis()->SetTitleOffset(1.);
    h->GetYaxis()->SetLabelSize(0.037);
	WriteGeneralInfo("",lumi,0.049,0.2,0.955);
	if(option.Contains("text") || option.Contains("TEXT"))c->SetGrid();
	
	c->SaveAs(dirname + "/" + name + ".pdf");
	c->SaveAs(dirname + "/" + name + ".png");
	delete c;
}
template void plot_histogram<TH1D>(TH1D*,TString,TString,TString,TString);
template void plot_histogram<TH2D>(TH2D*,TString,TString,TString,TString);

void plotCorrelationHistograms(TH2* hcorrel,TString dirname,TString name,TString lumi,TString option){
	
	TH2D* h_normalized=(TH2D*)hcorrel->Clone();
	NormalizeColumns(h_normalized,0);
	h_normalized->Scale(100);
	plotTH2histogram(h_normalized,dirname,name + "_normalized_columns.png",lumi,option);
	plotTH2histogram(hcorrel,dirname,name + ".png",lumi,option);
	NormalizeRows(hcorrel);
	hcorrel->Scale(100);
	plotTH2histogram(hcorrel,dirname,name + "_normalized_rows.png",lumi,option);
}


void plotTH2histogram(TH2* h,TString dirname,TString name,TString lumi,TString option){
	TCanvas* c = new TCanvas("c","",1);
	c->cd();
	
	const int Number = 4; 
	double Red[Number]   = {1.00, 1.00, 0.75, 0.50}; 
	double Green[Number] = {1.00, 0.00, 0.00, 0.00}; 
	double Blue[Number]  = {0.00, 0.00, 0.00, 0.00}; 
	double Stops[Number] = {0.00, 0.60, 0.80, 1.00};

	Int_t nb = 255; 
	//Int_t nb = 999; 
	TColor::CreateGradientColorTable(Number,Stops,Red,Green,Blue,nb);
	gStyle->SetNumberContours(nb);
	
	h->GetXaxis()->SetLabelSize(0.06);
	h->GetYaxis()->SetLabelSize(0.06);
	h->GetXaxis()->SetLabelOffset(0.0015);
	h->GetYaxis()->SetLabelOffset(0.0015);
	h->GetXaxis()->SetTitleSize(0.05);
	h->GetYaxis()->SetTitleSize(0.05);
	h->GetXaxis()->SetTitleOffset(0.9);
	h->GetYaxis()->SetTitleOffset(0.9);
	
	
	h->Draw(option);
	WriteGeneralInfo("",lumi,0.039,0.15,0.91);
	if(option.Contains("text") || option.Contains("TEXT")){
		c->SetGrid();
		h->SetMarkerSize(1.7);
	}
	
	
	TString figurename=dirname + "/" + name;
	if(figurename.EndsWith(".pdf") || figurename.EndsWith(".png") )c->SaveAs(figurename);
	else c->SaveAs(figurename + ".pdf");
	
	delete c;
}
